<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

$loader = require __DIR__.'/../vendor/autoload.php';

// intl
if (!function_exists('intl_get_error_code')) {
    require_once __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs/functions.php';

    $loader->add('', __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs');
}

//Cargar las rutas para los plugins de bundles.
$loader->add('CoreSphere', __DIR__.'/../src/Cenfotec/Components');
$loader->add('Components', __DIR__.'/../src/Cenfotec');
$loader->add('FOS', __DIR__.'/../src/Cenfotec/Components');

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
