CREATE TEMPORARY TABLE r_report_registrados
SELECT cc.fecha_contacto_year, cc.fecha_contacto_month, count(1) cant
FROM t_contacto_crm cc 
GROUP BY cc.fecha_contacto_year, cc.fecha_contacto_month
ORDER BY cc.fecha_contacto_year, cc.fecha_contacto_month ASC;

SELECT fecha_contacto_year as year, fecha_contacto_month as month, cant 
FROM r_report_registrados;

SET p_flag_eliminado = 0;
SET p_tipo_contacto = 'PA';

BEGIN
	CREATE TEMPORARY TABLE r_report_registrados
	SELECT cc.fecha_contacto_year, cc.fecha_contacto_month, count(1) cant
	FROM t_contacto_crm cc 
			LEFT JOIN t_programa_academico tp ON tp.contacto_crm_id = cc.id
			LEFT JOIN t_acti ta ON ta.contacto_crm_id = cc.id
			LEFT JOIN t_posgrado tpos ON tpos.contacto_crm_id = cc.id
	WHERE true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'ACTI' THEN ta.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'POS' THEN tpos.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'TODOS' THEN (tp.flag_eliminado = p_flag_eliminado or ta.flag_eliminado = p_flag_eliminado or tpos.flag_eliminado = p_flag_eliminado)
				 END AND
		  true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.id is not null
					WHEN p_tipo_contacto = 'ACTI' THEN ta.id is not null
					WHEN p_tipo_contacto = 'POS' THEN tpos.id is not null
					WHEN p_tipo_contacto = 'TODOS' THEN true
				 END 
	GROUP BY cc.fecha_contacto_year, cc.fecha_contacto_month
	ORDER BY cc.fecha_contacto_year, cc.fecha_contacto_month ASC;

	SELECT fecha_contacto_year as year, fecha_contacto_month as month, cant 
	FROM r_report_registrados
	WHERE fecha_contacto_year <> 0 and fecha_contacto_year is not null;
END