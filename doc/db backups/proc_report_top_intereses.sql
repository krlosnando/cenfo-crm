BEGIN
	CREATE TEMPORARY TABLE r_top_intereses
	SELECT cxi.interes_id, count(cxi.interes_id) cant
	FROM t_contacto_crm cc 
		JOIN t_contacto_crm_x_interes cxi ON cxi.contacto_crm_id = cc.id
		LEFT JOIN t_programa_academico tp ON tp.contacto_crm_id = cc.id
		LEFT JOIN t_acti ta ON ta.contacto_crm_id = cc.id
		LEFT JOIN t_posgrado tpos ON tpos.contacto_crm_id = cc.id
	WHERE true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'ACTI' THEN ta.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'POS' THEN tpos.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'TODOS' THEN (tp.flag_eliminado = p_flag_eliminado or ta.flag_eliminado = p_flag_eliminado or tpos.flag_eliminado = p_flag_eliminado)
				 END AND
		  true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.id is not null
					WHEN p_tipo_contacto = 'ACTI' THEN ta.id is not null
					WHEN p_tipo_contacto = 'POS' THEN tpos.id is not null
					WHEN p_tipo_contacto = 'TODOS' THEN true
				 END 
	GROUP BY cxi.interes_id;

	INSERT INTO r_top_intereses(interes_id, cant)
	SELECT exi.interes_id, count(exi.interes_id) cant
	FROM t_empresa e
		JOIN t_empresa_x_interes exi ON exi.empresa_id = e.id
	GROUP BY exi.interes_id;

	INSERT INTO r_top_intereses(interes_id, cant)
	SELECT cxi.interes_id, count(cxi.interes_id) cant
	FROM t_colegio c
		JOIN t_colegio_x_interes cxi ON cxi.colegio_id = c.id
	GROUP BY cxi.interes_id;

	SELECT
	  t.interes_id, 
	  t.cant, 
	  i.nombre as interes, 
	  ai.nombre as area
	FROM
	  r_top_intereses t 
		JOIN t_interes i on t.interes_id = i.id
		JOIN t_area_interes ai on i.area_interes_id = ai.id
	ORDER BY cant DESC
	LIMIT 25;
END