BEGIN
	CREATE TEMPORARY TABLE r_top_primer_contacto
	SELECT cc.primer_contacto_id, count(cc.primer_contacto_id) cant
	FROM t_contacto_crm cc 
		LEFT JOIN t_programa_academico tp ON tp.contacto_crm_id = cc.id
		LEFT JOIN t_acti ta ON ta.contacto_crm_id = cc.id
		LEFT JOIN t_posgrado tpos ON tpos.contacto_crm_id = cc.id
	WHERE true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'ACTI' THEN ta.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'POS' THEN tpos.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'TODOS' THEN (tp.flag_eliminado = p_flag_eliminado or ta.flag_eliminado = p_flag_eliminado or tpos.flag_eliminado = p_flag_eliminado)
				 END AND
		  true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.id is not null
					WHEN p_tipo_contacto = 'ACTI' THEN ta.id is not null
					WHEN p_tipo_contacto = 'POS' THEN tpos.id is not null
					WHEN p_tipo_contacto = 'TODOS' THEN true
				 END 
	GROUP BY cc.primer_contacto_id;

	INSERT INTO r_top_primer_contacto(primer_contacto_id, cant)
	SELECT e.primer_contacto_id, count(e.primer_contacto_id) cant
	FROM t_empresa e
	GROUP BY e.primer_contacto_id;

	INSERT INTO r_top_primer_contacto(primer_contacto_id, cant)
	SELECT c.primer_contacto_id, count(c.primer_contacto_id) cant
	FROM t_colegio c
	GROUP BY c.primer_contacto_id;

	SELECT
	  t.primer_contacto_id, 
	  t.cant, 
	  i.nombre as primer_contacto
	FROM
	  r_top_primer_contacto t 
		JOIN t_primer_contacto i on t.primer_contacto_id = i.id
	ORDER BY cant DESC
	LIMIT 25;
END