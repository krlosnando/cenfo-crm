BEGIN
	CREATE TEMPORARY TABLE r_top_referencia
	SELECT cc.referencia_id, count(cc.referencia_id) cant
	FROM t_contacto_crm cc 
		LEFT JOIN t_programa_academico tp ON tp.contacto_crm_id = cc.id
		LEFT JOIN t_acti ta ON ta.contacto_crm_id = cc.id
		LEFT JOIN t_posgrado tpos ON tpos.contacto_crm_id = cc.id
	WHERE true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'ACTI' THEN ta.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'POS' THEN tpos.flag_eliminado = p_flag_eliminado
					WHEN p_tipo_contacto = 'TODOS' THEN (tp.flag_eliminado = p_flag_eliminado or ta.flag_eliminado = p_flag_eliminado or tpos.flag_eliminado = p_flag_eliminado)
				 END AND
		  true = CASE
					WHEN p_tipo_contacto = 'PA' THEN tp.id is not null
					WHEN p_tipo_contacto = 'ACTI' THEN ta.id is not null
					WHEN p_tipo_contacto = 'POS' THEN tpos.id is not null
					WHEN p_tipo_contacto = 'TODOS' THEN true
				 END 
	GROUP BY cc.referencia_id;

	INSERT INTO r_top_referencia(referencia_id, cant)
	SELECT e.referencia_id, count(e.referencia_id) cant
	FROM t_empresa e
	GROUP BY e.referencia_id;

	INSERT INTO r_top_referencia(referencia_id, cant)
	SELECT c.referencia_id, count(c.referencia_id) cant
	FROM t_colegio c
	GROUP BY c.referencia_id;

	SELECT
	  t.referencia_id, 
	  t.cant, 
	  i.nombre as referencia
	FROM
	  r_top_referencia t 
		JOIN t_referencia i on t.referencia_id = i.id
	ORDER BY cant DESC
	LIMIT 25;
END