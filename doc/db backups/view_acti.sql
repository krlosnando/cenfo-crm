select 
	cc.genero									AS genero,
	t.id 										AS acti_id,
	cc.id 										AS contacto_crm_id,
	cc.flag_matriculado 						AS flag_matriculado,
	t.flag_eliminado 							AS flag_eliminado,
	cc.nombre 									AS nombre,
	cc.apellido1 								AS apellido1,
	cc.apellido2 								AS apellido2,
	cc.nombre_completo 							AS nombre_completo,
	cc.identificacion 							AS identificacion,
	tti.nombre									AS tipo_identificacion,
	tp.nombre									AS pais_residencia,
	tlp.nombre									AS provincia,
	tlc.nombre									AS canton,
	cc.direccion								AS direccion,
	e.nombre 									AS empresa,
	tc.nombre									AS convenio,
	tpf.nombre									AS profesion,
	tpt.nombre									AS puesto,
	r.nombre 									AS referencia,
	p.nombre 									AS primer_contacto,
	tni.nombre									AS nivel_interes,
	th.nombre									AS horario,
	cc.comentario								AS comentarios,
	date_format(cc.fecha_contacto,'%Y-%m-%d') 	AS fecha_contacto,
	cc.fecha_modifico_bd						AS fecha_modifico_bd,
	
	(select group_concat(ct.correo separator '|') 
	 from t_contacto_crm_x_correo cxct 
			join t_correo ct on ct.id = cxct.correo_id
			join t_contacto_crm cct on cct.id = cxct.contacto_crm_id
	 where cct.id = cc.id) 						AS correos,
	 
	(select group_concat(ti.nombre separator '|') 
	 from t_contacto_crm_x_interes tcxi 
			join t_interes ti on ti.id = tcxi.interes_id
			join t_contacto_crm tcc on tcc.id = tcxi.contacto_crm_id
	 where tcc.id = cc.id) 						AS intereses,
	 
	(select group_concat(concat(ttn.nombre,' ',tn.numero,' ',tn.ext) separator '|') 
	 from t_contacto_crm_x_numero tcxn 
			join t_numero tn on tn.id = tcxn.numero_id
			join t_tipo_numero ttn on tn.tipo_numero_id = ttn.id 
			join t_contacto_crm tcc on tcc.id = tcxn.contacto_crm_id
	 where tcc.id = cc.id) 						AS numeros,
	
	(select i.nombre 
	 from t_matricula tm 
		inner join t_interes i on tm.interes_id = i.id
	 where tm.contacto_crm_id = cc.id
	 order by tm.fecha_matricula desc
	 LIMIT 1) AS matricula,
	
	(select date_format(tm.fecha_matricula,'%Y-%m-%d') 
	 from t_matricula tm 
	 where tm.contacto_crm_id = cc.id
	 order by tm.fecha_matricula desc
	 LIMIT 1) AS fecha_matricula,
	
	(select count(tccxs.contacto_crm_id) 
	 from t_contacto_crm_x_seguimiento tccxs 
	 where tccxs.contacto_crm_id = cc.id) 		AS cant_seguimientos,
	 
	(select count(tccxs.contacto_crm_id) 
	 from t_matricula tccxs 
	 where tccxs.contacto_crm_id = cc.id) 		AS cant_matriculas,
	 
	 (select group_concat(tempts.nombre separator '|') 
	  from t_seguimiento temps
			inner join t_tipo_seguimiento tempts 
				on temps.tipo_seguimiento_id = tempts.id
			inner join t_contacto_crm_x_seguimiento tccxs 
				on tccxs.seguimiento_id = temps.id 
	  where tccxs.contacto_crm_id = cc.id) 		AS seguimientos,
	  
	  (select group_concat(i.nombre separator '|') 
	   from t_matricula tm 
			inner join t_interes i on tm.interes_id = i.id
	   where tm.contacto_crm_id = cc.id) 		AS matriculas
	 
 from t_acti t
		join t_contacto_crm cc on t.contacto_crm_id = cc.id 
		left join t_tipo_identificacion tti on cc.tipo_identificacion_id = tti.id
		left join t_pais tp on cc.pais_id = tp.id
		left join t_canton tlc on cc.canton_id = tlc.id
		left join t_provincia tlp on tlc.provincia_id = tlp.id
		left join t_empresa e on cc.empresa_id = e.id
		left join t_convenio tc on cc.convenio_id = tc.id
		left join t_profesion tpf on cc.profesion_id = tpf.id
		left join t_puesto tpt on cc.puesto_id = tpt.id
		left join t_referencia r on cc.referencia_id = r.id
		left join t_primer_contacto p on cc.primer_contacto_id = p.id
		left join t_nivel_interes tni on cc.nivel_interes_id = tni.id
		left join t_horario th on cc.horario_id = th.id
		