select 
	c.id 							AS colegio_id,
	c.nombre 						AS nombre,
	lp.nombre						AS provincia,
	lc.nombre						AS canton,
	tc.nombre 						AS tipo,
	cc.nombre 						AS clase,
	cr.nombre						AS referencia,
	cpc.nombre						AS primer_contacto,
	c.poblacion_total 				AS poblacion_total,
	c.poblacion_ultimo_nivel 		AS poblacion_ultimo_nivel,
	ec.nombre 						AS especialidad,
	c.flag_eliminado 				AS flag_eliminado,
	date_format(c.fecha_contacto,'%Y-%m-%d') 	AS fecha_contacto,
	c.fecha_modifico_bd				AS fecha_modifico_bd,
	c.comentario					AS comentario,
	
	(select Group_concat(ti.nombre separator '|') 
	 from t_colegio_x_interes tcxi 
			join t_interes ti on ti.id = tcxi.interes_id 
			join t_colegio tcc on tcc.id = tcxi.colegio_id 
	 where tcc.id = c.id) 			AS intereses, 

	(select group_concat(concat(ttn.nombre,' ',tn.numero,' ',tn.ext) separator '|') 
	 from t_colegio_x_numero tcxn 
			join t_numero tn on tn.id = tcxn.numero_id
			join t_tipo_numero ttn on tn.tipo_numero_id = ttn.id 
			join t_colegio tcc on tcc.id = tcxn.colegio_id
	 where tcc.id = c.id) 			AS numeros,
	 
	(select group_concat(cbt.nombre_completo separator '|') 
	 from t_colegio_x_contacto_basico excbt 
			join t_colegio et on et.id = excbt.colegio_id 
			join t_contacto_basico cbt on cbt.id = excbt.contacto_basico_id
	 where et.id = c.id) 			AS contactos,
	 
	(select group_concat(ct.correo separator '|') 
	 from t_colegio_x_contacto_basico excbt 
			join t_colegio et on et.id = excbt.colegio_id 
			join t_contacto_basico cbt on cbt.id = excbt.contacto_basico_id
			join t_contacto_basico_x_correo cbct on cbt.id = cbct.contacto_basico_id
			join t_correo ct on cbct.correo_id = ct.id
	 where et.id = c.id) 			AS correos_contactos,
	 
	(select group_concat(nt.numero separator '|') 
	 from t_colegio_x_contacto_basico excbt 
			join t_colegio et on et.id = excbt.colegio_id
			join t_contacto_basico cbt on cbt.id = excbt.contacto_basico_id
			join t_contacto_basico_x_numero cbnt on cbt.id = cbnt.contacto_basico_id
			join t_numero nt on cbnt.numero_id = nt.id
	where et.id = c.id) 			AS numeros_contactos,
	
	(select count(tcxs.colegio_id) 
	 from t_colegio_x_seguimiento tcxs 
	 where tcxs.colegio_id = c.id) 	AS cant_seguimientos,
	 
	(select group_concat(tempts.nombre separator '|') 
	 from t_seguimiento temps
		inner join t_tipo_seguimiento tempts 
			on temps.tipo_seguimiento_id = tempts.id
		inner join t_colegio_x_seguimiento tccxs 
			on tccxs.seguimiento_id = temps.id 
	 where tccxs.colegio_id = c.id) 		AS seguimientos
 
 from t_colegio c 
		left join t_clase_colegio cc on c.clase_colegio_id = cc.id
		left join t_especialidad_colegio ec on c.especialidad_colegio_id = ec.id
		left join t_tipo_colegio tc on c.tipo_colegio_id = tc.id
		left join t_canton lc on c.canton_id = lc.id
		left join t_provincia lp on lc.provincia_id = lp.id
		left join t_referencia cr on c.referencia_id = cr.id
		left join t_primer_contacto cpc on c.primer_contacto_id = cpc.id
		