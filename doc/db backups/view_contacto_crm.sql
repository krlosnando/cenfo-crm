select 
	cc.id 										AS contacto_crm_id,
	cc.programa_academico_id					AS programa_academico_id,
	cc.acti_id									AS acti_id,
	cc.posgrado_id								AS posgrado_id,
	cc.nombre 									AS nombre,
	cc.apellido1 								AS apellido1,
	cc.apellido2 								AS apellido2,
	cc.nombre_completo 							AS nombre_completo,
	cc.identificacion 							AS identificacion,
	e.nombre 									AS empresa,
	r.nombre 									AS referencia,
	p.nombre 									AS primer_contacto,
	tpa.flag_eliminado							AS eliminado_pa,
	ta.flag_eliminado							AS eliminado_acti,
	tp.flag_eliminado							AS eliminado_pos,
	date_format(cc.fecha_contacto,'%Y-%m-%d') 	AS fecha_contacto,
	
	(select group_concat(ct.correo separator '|') 
	 from t_contacto_crm_x_correo cxct 
			join t_correo ct on ct.id = cxct.correo_id
			join t_contacto_crm cct on cct.id = cxct.contacto_crm_id
	 where cct.id = cc.id) 						AS correos,
	 
	(select group_concat(ti.nombre separator '|') 
	 from t_contacto_crm_x_interes tcxi 
			join t_interes ti on ti.id = tcxi.interes_id
			join t_contacto_crm tcc on tcc.id = tcxi.contacto_crm_id
	 where tcc.id = cc.id) 						AS intereses,
	 
	(select group_concat(concat(ttn.nombre,' ',tn.numero,' ',tn.ext) separator '|') 
	 from t_contacto_crm_x_numero tcxn 
			join t_numero tn on tn.id = tcxn.numero_id
			join t_tipo_numero ttn on tn.tipo_numero_id = ttn.id 
			join t_contacto_crm tcc on tcc.id = tcxn.contacto_crm_id
	 where tcc.id = cc.id) 						AS numeros,
	 
	(select count(tccxs.contacto_crm_id) 
	 from t_contacto_crm_x_seguimiento tccxs 
	 where tccxs.contacto_crm_id = cc.id) 		AS cant_seguimientos 
	 
 from t_contacto_crm cc
		left join t_empresa e on cc.empresa_id = e.id
		left join t_referencia r on cc.referencia_id = r.id
		left join t_primer_contacto p on cc.primer_contacto_id = p.id
		left join t_programa_academico tpa on cc.id = tpa.contacto_crm_id
		left join t_acti ta on cc.id = ta.contacto_crm_id
		left join t_posgrado tp on cc.id = tp.contacto_crm_id
		