SELECT 
	e.id                                AS empresa_id, 
	e.nombre                            AS nombre, 
	lp.nombre							AS provincia,
	lc.nombre							AS canton,
	e.comentario                        AS comentario, 
	a.nombre							AS actividad,
	r.nombre                            AS referencia, 
	e.direccion							AS direccion,
	e.flag_convenio						AS flag_convenio,
	pc.nombre                           AS primer_contacto, 
	e.flag_eliminado                    AS flag_eliminado,
	date_format(e.fecha_contacto,'%Y-%m-%d') 	AS fecha_contacto,
	e.fecha_modifico_bd					AS fecha_modifico_bd,
	
	(SELECT Group_concat(ti.nombre SEPARATOR '|') 
	FROM t_empresa_x_interes tcxi 
			JOIN t_interes ti ON ti.id = tcxi.interes_id 
			JOIN t_empresa tcc ON tcc.id = tcxi.empresa_id 
	WHERE tcc.id = e.id) 				AS intereses, 

	(SELECT Group_concat(cbt.nombre_completo SEPARATOR '|') 
	FROM t_empresa_x_contacto_basico excbt 
			JOIN t_empresa et ON et.id = excbt.empresa_id 
			JOIN t_contacto_basico cbt ON cbt.id = excbt.contacto_basico_id
	WHERE et.id = e.id)           		AS contactos, 

	(SELECT Group_concat(ct.correo SEPARATOR '|') 
	FROM t_empresa_x_contacto_basico excbt 
			JOIN t_empresa et ON et.id = excbt.empresa_id
			JOIN t_contacto_basico cbt ON cbt.id = excbt.contacto_basico_id 
			JOIN t_contacto_basico_x_correo cbct ON  cbt.id = cbct.contacto_basico_id 
			JOIN t_correo ct ON  cbct.correo_id = ct.id
	WHERE  et.id = e.id)           		AS correos_contactos, 

	(SELECT Group_concat(nt.numero SEPARATOR '|') 
	FROM t_empresa_x_contacto_basico excbt 
			JOIN t_empresa et ON et.id = excbt.empresa_id 
			JOIN t_contacto_basico cbt ON cbt.id = excbt.contacto_basico_id 
			JOIN t_contacto_basico_x_numero cbnt ON cbt.id = cbnt.contacto_basico_id
			JOIN t_numero nt ON cbnt.numero_id = nt.id 
	WHERE et.id = e.id)           		AS numeros_contactos, 

	(SELECT Count(texs.empresa_id) 
	FROM t_empresa_x_seguimiento texs 
	WHERE texs.empresa_id = e.id ) 		AS cant_seguimientos,
	
	(select group_concat(tempts.nombre separator '|') 
	 from t_seguimiento temps
		inner join t_tipo_seguimiento tempts 
			on temps.tipo_seguimiento_id = tempts.id
		inner join t_empresa_x_seguimiento tccxs 
			on tccxs.seguimiento_id = temps.id 
	 where tccxs.empresa_id = e.id) 		AS seguimientos
		
FROM t_empresa e 
		LEFT JOIN t_actividad a ON e.actividad_id = a.id 
		LEFT JOIN t_referencia r ON e.referencia_id = r.id 
        LEFT JOIN t_primer_contacto pc ON e.primer_contacto_id = pc.id 
		left join t_canton lc on e.canton_id = lc.id
		left join t_provincia lp on lc.provincia_id = lp.id