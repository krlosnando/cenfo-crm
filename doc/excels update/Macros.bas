Sub RemoveFormula()
    Set from = Selection
    For Each Cell In from
        Cell.Value = Cell.Value
    Next
End Sub
Sub GenerateNumbers()
    Set from = Selection
    Dim Count As Integer
    Count = 2382
    For Each Cell In from
        If Not Cell.EntireRow.Hidden Then
            Cell.Value = Count
            Count = Count + 1
        End If
    Next
End Sub
Sub DoTrim()
    Set from = Selection
    For Each Cell In from
        Cell.Value = Trim(Replace(Cell.Value, Chr(13), ""))
    Next
End Sub
Sub UPPERTRIM()
    Set from = Selection
    For Each Cell In from
        Cell.Value = UCase(Trim(Cell.Value))
    Next
End Sub
Sub FormatPhoneNumber()
    Set from = Selection
    Dim temp As String
    Dim firt As String
    Dim newVal As String
    
    For Each Cell In from
        temp = Trim(Cell.Value)
        If Not temp = "" Then
            temp = Replace(temp, "ERROR ", "")
            temp = Replace(temp, " ", "")
            temp = Replace(temp, "-", "")
            
            If Len(temp) = 8 Then
                firt = Left(temp, 1)
            
                If firt = "2" Or firt = "4" Then
                    newVal = Left(temp, 4) & "-" & Right(temp, 4) & ",Tel"
                Else
                    newVal = Left(temp, 4) & "-" & Right(temp, 4) & ",Cel"
                End If
                
                Cell.Value = newVal
            Else
                If Len(temp) = 7 Then
                    firt = Left(temp, 1)
                
                    If firt = "2" Or firt = "4" Then
                        newVal = Left(temp, 3) & "-" & Right(temp, 4) & ",Tel"
                    Else
                        newVal = Left(temp, 3) & "-" & Right(temp, 4) & ",Cel"
                    End If
                    
                    Cell.Value = newVal
                Else
                    Cell.Value = "ERROR " & Cell.Value
                End If
            End If
        End If
    Next
End Sub
Sub FormatPhoneNumber2()
    Set from = Selection
    Dim temp As String
    Dim firt As String
    Dim newVal As String
    
    For Each Cell In from
        temp = Trim(Cell.Value)
        If Not temp = "" Then
            temp = Replace(temp, "ERROR ", "")
            temp = Replace(temp, " ", "")
            temp = Replace(temp, "-", "")
            temp = Replace(temp, """", "")
            
            If Len(temp) = 8 Then
                Cell.Value = """+506" & temp & """"
            End If
        End If
    Next
End Sub


Sub SubstringDate()
    Set from = Selection
    Dim tempLeft As String
    Dim dia As String
    Dim mes As String
    Dim tempRight As String
    For Each Cell In from
        tempLeft = Left(Cell, 5)
        dia = Left(tempLeft, 2)
        mes = Right(tempLeft, 2)
        tempRight = Right(Cell, 5)
        Cell.Value = mes & "/" & dia & tempRight
    Next
End Sub
Sub SentenceCase()
    Set from = Selection
    Dim temp As String
    For Each Cell In from
        temp = Trim(StrConv(Cell, vbProperCase))
        temp = Replace(temp, " Y ", " y ")
        temp = Replace(temp, " De ", " de ")
        temp = Replace(temp, " A ", " a ")
        temp = Replace(temp, " Con ", " con ")
        temp = Replace(temp, " En ", " en ")
        temp = Replace(temp, " La ", " la ")
        temp = Replace(temp, " Para ", " para ")
        temp = Replace(temp, " Por ", " por ")
        temp = Replace(temp, " Del ", " del ")
        temp = Replace(temp, " Desde ", " desde ")
        temp = Replace(temp, " Entre ", " entre ")
        temp = Replace(temp, " Sin ", " sin ")
        Cell.Value = temp
    Next
End Sub

Sub CodeNumber()
    Set from = Selection
    Dim tempRight As String
    For Each Cell In from
        Cell.Value = Replace(Cell.Value, "506", "00506")
        Cell.Value = Replace(Cell.Value, "0000506", "00506")
        
        Cell.Value = Replace(Cell.Value, "505", "00505")
        Cell.Value = Replace(Cell.Value, "0000505", "00505")
    Next
End Sub

Sub CopyToMobilePhone()
    Set from = Selection
    Dim tempRight As String
    For Each Cell In from
        d = Split(Cell.Value, """")
        t = IsArrayEmpty(d)
        If Not IsArrayEmpty(d) Then
            Dim tempNum As String
            tempNum = d(1)
            Range("V" & Cell.Row).Value = Replace(tempNum, "+", "00")
            Cell.Value = ""
        End If
    Next
End Sub

Private Function IsArrayEmpty(arr As Variant)
  ' This function returns true if array is empty
  Dim l As Long

  On Error Resume Next
  l = Len(Join(arr))
  If l = 0 Then
    IsArrayEmpty = True
  Else
    IsArrayEmpty = False
  End If

  If Err.Number > 0 Then
      IsArrayEmpty = True
  End If

  On Error GoTo 0
End Function
