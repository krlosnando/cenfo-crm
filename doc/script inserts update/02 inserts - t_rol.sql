SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_rol;
INSERT INTO `t_rol` (`id`, `nombre`, `descripcion`) VALUES
(1,	'Administrador',	'Rol administrador del sistema');