SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_usuario;
INSERT INTO `t_usuario` (`id`, `nombre`, `apellido1`, `apellido2`, `nombre_usuario`, `salt`, `password`, `correo`) VALUES
(1,	'Carlos',	'Dominguez',	'Lara',	'CDominguez',	'b42202414a4e7c45b7ec8ab8e3a8a9bf',	'rbXVImI4kh3wSKn2uJbIXh4hHDEf7d2sZ0m1MxR+vbO1C9jqqh9S2viCNCuZcW42SkLvHZBlGgMaGv+fIK/qHA==',	'krlosnando@hotmail.com');
