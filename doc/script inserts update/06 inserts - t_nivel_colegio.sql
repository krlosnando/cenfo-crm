SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

INSERT INTO `t_nivel_colegio` (`id`, `nombre`) VALUES
(1, 'Duodécimo'),
(2, 'Undécimo');