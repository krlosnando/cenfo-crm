SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_referencia;
INSERT INTO `t_referencia` (`id`, `nombre`) VALUES
(1,'Buses'),
(2,'Google y otros buscadores'),
(3,'Formulario'),
(4,'Facebook'),
(5,'Ferias colegiales y vocacionales'),
(6,'Vive cerca Cenfotec'),
(7,'Referido estudiante activo'),
(8,'Info'),
(9,'Prensa'),
(10,'Exposiciones'),
(11,'Otros'),
(12,'Referido de un familiar'),
(13,'Instituciones aliadas'),
(14,'Universidadescr.com'),
(15,'Referido egresado'),
(16,'Taco bell'),
(17,'Chat'),
(18,'Estudiante o egresado'),
(19,'AM PM'),
(20,'Charlas en cenfotec'),
(21,'Television'),
(22,'Freshmarket'),
(23,'Spoon'),
(24,'Boletines instituciones aliadas'),
(25,'Charlas fuera de cenfotec');