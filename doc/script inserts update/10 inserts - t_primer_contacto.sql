SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_primer_contacto;
INSERT INTO `t_primer_contacto` (`id`, `nombre`) VALUES
(1,'Llamada'),
(2,'Visita no programado'),
(3,'Otros'),
(4,'Pide horario nocturno'),
(5,'Email'),
(6,'Formulario web');