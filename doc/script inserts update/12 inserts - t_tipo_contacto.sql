SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_tipo_contacto;
INSERT INTO `t_tipo_contacto` (`id`, `nombre`) VALUES
(1, 'PENDIENTE'),
(2, 'PA'),
(3, 'ACTI'),
(4, 'POS');