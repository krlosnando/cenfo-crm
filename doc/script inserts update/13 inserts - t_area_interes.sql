SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM `t_area_interes`;
INSERT INTO `t_area_interes` (`id`,`nombre`,`tipo_contacto_id` ) VALUES
(1,'Pendiente', (select id from t_tipo_contacto where nombre = 'Pendiente')),
(2,'Desarrollo de Software', (select id from t_tipo_contacto where nombre = 'PA')),
(3,'Tecnologias Información y Comunicación', (select id from t_tipo_contacto where nombre = 'PA')),
(4,'Telecomunicaciones', (select id from t_tipo_contacto where nombre = 'PA')),
(5,'Tecnologias Web', (select id from t_tipo_contacto where nombre = 'PA')),
(6,'Bases de Datos', (select id from t_tipo_contacto where nombre = 'PA')),
(7,'Ciberseguridad', (select id from t_tipo_contacto where nombre = 'PA')),
(8,'Calidad Software', (select id from t_tipo_contacto where nombre = 'PA')),
(9,'Idiomas', (select id from t_tipo_contacto where nombre = 'PA')),
(10,'Administracion y Procesos', (select id from t_tipo_contacto where nombre = 'ACTI')),
(11,'Desarrollo de Software', (select id from t_tipo_contacto where nombre = 'ACTI')),
(12,'Factor Humano', (select id from t_tipo_contacto where nombre = 'ACTI')),
(13,'Infraestructura', (select id from t_tipo_contacto where nombre = 'ACTI')),
(14,'Bases de Datos', (select id from t_tipo_contacto where nombre = 'ACTI')),
(15,'Web', (select id from t_tipo_contacto where nombre = 'ACTI')),
(16,'Ofimatica', (select id from t_tipo_contacto where nombre = 'ACTI')),
(17,'Colaboracion y comunicación', (select id from t_tipo_contacto where nombre = 'ACTI')),
(18,'Administracion de proyectos', (select id from t_tipo_contacto where nombre = 'ACTI')),
(19,'Inteligencia de negocios', (select id from t_tipo_contacto where nombre = 'ACTI')),
(20,'Pendiente', (select id from t_tipo_contacto where nombre = 'ACTI')),
(21,'Pendiente', (select id from t_tipo_contacto where nombre = 'PA')),
(22,'Pendiente', (select id from t_tipo_contacto where nombre = 'POS'));