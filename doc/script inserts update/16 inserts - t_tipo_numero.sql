SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

DELETE FROM t_tipo_numero;
INSERT INTO `t_tipo_numero` (`id`, `nombre`) VALUES
(1, 'Tel'),
(2, 'Cel');