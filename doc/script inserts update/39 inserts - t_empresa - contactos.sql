SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DELIMITER ;;

INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1491, 'Futura Tec', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1492, 'Fusionet', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1493, 'Analisis Mbc S.a.', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1494, 'Rpost', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1495, 'Cbl Construcciones', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1496, 'Imago', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1497, 'Grupo Prides', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1498, 'Consultec', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1499, 'Costa Rica Software Fact.', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1500, 'Cpic', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1501, 'Kzamora', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1502, 'Daytonasoft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1503, 'Banco Nacional', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1504, 'Efibs-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1505, 'Codittel-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1506, 'Coopeguanacaste', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1507, 'Eprom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1508, 'Cisco Eprom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1509, 'Alfa-group', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1510, 'Electromatica', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1511, 'Telme', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1512, 'Gbsys-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1513, 'Habcoit', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1514, 'Rsl Telecom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1515, 'Innova Technology', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1516, 'Ceramica San Angel', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1517, 'Art-soft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1518, 'Bcmp', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1519, 'Cis', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1520, 'Cnet-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1521, 'Cr Conectividad', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1522, 'Construcciones Rápidas S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1523, 'Acconsulting', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1524, 'Codsi', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1525, 'Ditel S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1526, 'Dualux S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1527, 'Edificios Inteligentes - Edintel', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1528, 'Almacen El Electrico', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1529, 'Fractalseo', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1530, 'Fusion Works', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1531, 'Future-kids', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1532, 'Gci', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1533, 'Geotecnologias', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1534, 'Gpi', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1535, 'Logical-data', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1536, 'Innove-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1537, 'Cost', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1538, 'Inter Cr.net', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1539, 'Internexo', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1540, 'Electro Waldir', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1541, 'Novacomp', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1542, 'Ex2-outcoding', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1543, 'Procom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1544, 'Quadrant', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1545, 'Seosapeincom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1546, 'Smart Networks', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1547, 'Smartsoft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1548, 'Soportek', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1549, 'Tecnova', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1550, 'Tp4b', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1551, 'Quantica', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1552, 'Informatica-cys', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1553, 'Intel Telefonia', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1554, 'Alpha-it.sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1555, 'Web-host-costa-rica', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1556, 'Claica-sa', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1557, 'Aselcom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1558, 'Aktek S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1559, 'Ci Ladrillera', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1560, 'Bc Ing y Desarrollo', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1561, 'Via', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1562, 'Adn-solutions', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1563, 'Atelcom', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1564, 'Infotrec', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1565, 'Sql-consultores', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1566, 'Alcatel Lucent', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1567, 'Global Park', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1568, 'Tecapro', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1569, 'Grupo-cte', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1570, 'Multiconstructora Mc, García y Otoya S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1571, 'Northek', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1572, 'Rempro-software', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1573, 'Arkkosoft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1574, 'Gears-soft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1575, 'Saret de Costa Rica, S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1576, 'Hermes-soft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1577, 'Asp de Cr S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1578, 'Exceltec', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1579, 'Aec Electrónica S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1580, 'Intertel', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1581, 'Soluciones para la Alta Gerencia S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1582, 'Tecnosuministros', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1583, 'Dws-center', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1584, 'Constructora Valba S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1585, 'Cisoft', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1586, 'Aura Iteractiva', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1587, 'Bearcom S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1588, 'Bizsolutions', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1589, 'Bbcross', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1590, 'Bolwar Srl', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1591, 'Cabletica Empresarial', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1592, 'Cable Visión', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1593, 'Colac R.l', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1594, 'Colombia Cooperativa (colombia)', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1595, 'Soto Val S.a', now(), 1);
INSERT IGNORE INTO `t_empresa` (`id`, `nombre`, `fecha_registro`, `flagContacto`) VALUES  (1596, 'Compuredes', now(), 1);


UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Futura Tec';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Fusionet';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Analisis Mbc S.a.';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Rpost';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cbl Construcciones';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Imago';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Grupo Prides';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Consultec';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Costa Rica Software Fact.';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cpic';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Kzamora';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Daytonasoft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Banco Nacional';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Efibs-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Codittel-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Coopeguanacaste';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Eprom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cisco Eprom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Alfa-group';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Electromatica';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Telme';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Gbsys-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Habcoit';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Rsl Telecom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Innova Technology';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Ceramica San Angel';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Art-soft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bcmp';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cis';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cnet-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cr Conectividad';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Construcciones Rápidas S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Acconsulting';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Codsi';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Ditel S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Dualux S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Edificios Inteligentes - Edintel';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Almacen El Electrico';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Fractalseo';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Fusion Works';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Future-kids';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Gci';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Geotecnologias';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Gpi';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Logical-data';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Innove-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cost';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Inter Cr.net';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Internexo';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Electro Waldir';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Novacomp';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Ex2-outcoding';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Procom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Quadrant';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Seosapeincom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Smart Networks';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Smartsoft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Soportek';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Tecnova';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Tp4b';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Quantica';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Informatica-cys';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Intel Telefonia';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Alpha-it.sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Web-host-costa-rica';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Claica-sa';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Aselcom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Aktek S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Ci Ladrillera';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bc Ing y Desarrollo';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Via';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Adn-solutions';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Atelcom';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Infotrec';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Sql-consultores';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Alcatel Lucent';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Global Park';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Tecapro';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Grupo-cte';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Multiconstructora Mc, García y Otoya S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Northek';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Rempro-software';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Arkkosoft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Gears-soft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Saret de Costa Rica, S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Hermes-soft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Asp de Cr S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Exceltec';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Aec Electrónica S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Intertel';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Soluciones para la Alta Gerencia S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Tecnosuministros';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Dws-center';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Constructora Valba S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cisoft';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Aura Iteractiva';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bearcom S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bizsolutions';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bbcross';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Bolwar Srl';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cabletica Empresarial';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Cable Visión';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Colac R.l';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Colombia Cooperativa (colombia)';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Soto Val S.a';
UPDATE `t_empresa` SET flagContacto = 1 WHERE nombre = 'Compuredes';
