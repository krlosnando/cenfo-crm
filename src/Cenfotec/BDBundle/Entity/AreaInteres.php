<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\AreaInteres
 *
 * @ORM\Table(name="t_area_interes",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="nombre_tipo_unique",columns={"nombre","tipo_contacto_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\AreaInteresRepository")
 */
class AreaInteres
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="TipoContacto", inversedBy="areasInteres")
     * @ORM\JoinColumn(name="tipo_contacto_id", referencedColumnName="id")
     */
    private $tipoContacto;
    
    /**
     * @ORM\OneToMany(targetEntity="Interes", mappedBy="area")
     * @ORM\OrderBy({"nombre"="ASC"})
     */
    private $intereses;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->intereses = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return AreaInteres
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipoContacto
     *
     * @param \Cenfotec\BDBundle\Entity\TipoContacto $tipoContacto
     * @return AreaInteres
     */
    public function setTipoContacto(\Cenfotec\BDBundle\Entity\TipoContacto $tipoContacto = null)
    {
        $this->tipoContacto = $tipoContacto;
    
        return $this;
    }

    /**
     * Get tipoContacto
     *
     * @return \Cenfotec\BDBundle\Entity\TipoContacto 
     */
    public function getTipoContacto()
    {
        return $this->tipoContacto;
    }

    /**
     * Add intereses
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $intereses
     * @return AreaInteres
     */
    public function addInterese(\Cenfotec\BDBundle\Entity\Interes $intereses)
    {
        $this->intereses[] = $intereses;
    
        return $this;
    }

    /**
     * Remove intereses
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $intereses
     */
    public function removeInterese(\Cenfotec\BDBundle\Entity\Interes $intereses)
    {
        $this->intereses->removeElement($intereses);
    }

    /**
     * Get intereses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIntereses()
    {
        return $this->intereses;
    }
}