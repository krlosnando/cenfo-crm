<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Canton
 *
 * @ORM\Table(name="t_canton",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="nombre_provincia_id_unique",columns={"nombre","provincia_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\CantonRepository")
 */
class Canton
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    
    /**
     * @ORM\ManyToOne(targetEntity="Provincia", inversedBy="cantones")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    private $provincia;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Canton
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set provincia
     *
     * @param \Cenfotec\BDBundle\Entity\Provincia $provincia
     * @return Canton
     */
    public function setProvincia(\Cenfotec\BDBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \Cenfotec\BDBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}