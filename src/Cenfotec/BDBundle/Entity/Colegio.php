<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Colegio
 *
 * @ORM\Table(name="t_colegio",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="nombre_colegio_unique",columns={"nombre"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ColegioRepository")
 */
class Colegio
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @ORM\Column(name="director", type="string", length=50)
     */
    private $director;
    
    /**
     * @ORM\Column(name="poblacion_total", type="integer")
     */
    private $poblacionTotal;

    /**
     * @ORM\Column(name="poblacion_ultimo_nivel", type="integer")
     */
    private $poblacionUltimoNivel;
    
    /**
     * @ORM\Column(name="comentario", type="string", length=255)
     */
    private $comentario;
        
    /**
     * Esta fecha puede ser la fecha en que el usuario tomo apuntes del contacto
     * y despues lo ingreso al sistema. Ej: Se ingresa un contacto que se apunto
     * en una nota hace 2 semanas, se tiene que poner la fecha de hace 2 semanas
     * 
     * @ORM\Column(name="fecha_contacto", type="datetime", nullable=true)
     */
    private $fechaContacto;
    
    /**
     * @ORM\ManyToOne(targetEntity="Canton")
     * @ORM\JoinColumn(name="canton_id", referencedColumnName="id", nullable=true)
     */
    private $canton;

    /**
     * @ORM\ManyToOne(targetEntity="ClaseColegio")
     * @ORM\JoinColumn(name="clase_colegio_id", referencedColumnName="id")
     */
    private $clase;
    
    /**
     * @ORM\ManyToMany(targetEntity="ContactoBasico", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinTable(name="t_colegio_x_contacto_basico",
     *      joinColumns={@ORM\JoinColumn(name="colegio_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contacto_basico_id", referencedColumnName="id")}
     *      )
     */
    private $contactos;
    
    /**
     * @ORM\ManyToOne(targetEntity="EspecialidadColegio")
     * @ORM\JoinColumn(name="especialidad_colegio_id", referencedColumnName="id")
     */
    private $especialidad;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoColegio")
     * @ORM\JoinColumn(name="tipo_colegio_id", referencedColumnName="id")
     */
    private $tipo;
    
    /**
     * @ORM\ManyToMany(targetEntity="Interes")
     * @ORM\JoinTable(name="t_colegio_x_interes",
     *      joinColumns={@ORM\JoinColumn(name="colegio_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="interes_id", referencedColumnName="id")}
     *      )
     */
    private $intereses;
        
    /**
     * @ORM\ManyToMany(targetEntity="Numero", cascade={"persist"})
     * @ORM\JoinTable(name="t_colegio_x_numero",
     *      joinColumns={@ORM\JoinColumn(name="colegio_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="numero_id", referencedColumnName="id")}
     *      )
     */
    private $numeros;
    
    /**
     * @ORM\ManyToOne(targetEntity="PrimerContacto")
     * @ORM\JoinColumn(name="primer_contacto_id", referencedColumnName="id", nullable=true)
     */
    private $primerContacto;
    
    /**
     * @ORM\ManyToOne(targetEntity="Referencia")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id", nullable=true)
     */
    private $referencia;
    
    /**
     * @ORM\ManyToMany(targetEntity="Seguimiento", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinTable(name="t_colegio_x_seguimiento",
     *      joinColumns={@ORM\JoinColumn(name="colegio_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="seguimiento_id", referencedColumnName="id")}
     *      )
     */
    private $seguimientos;
    
    /**
     * @ORM\Column(name="flag_eliminado", type="boolean", nullable=false)
     */
    private $flagEliminado;
   
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_modifico_id", referencedColumnName="id", nullable=true)
     */
    private $usuarioModificoBD;
        
   /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_modifico_bd", type="datetime", nullable=true)
     */
    private $fechaModificoBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_registro_id", referencedColumnName="id")
     */
    private $usuarioRegistroBD;

    /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flagEliminado   = false;
        $this->fechaRegistroBD = new \DateTime();
        $this->contactos       = new \Doctrine\Common\Collections\ArrayCollection();
        $this->intereses       = new \Doctrine\Common\Collections\ArrayCollection();
        $this->numeros         = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Colegio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Colegio
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set director
     *
     * @param string $director
     * @return Colegio
     */
    public function setDirector($director)
    {
        $this->director = $director;
    
        return $this;
    }

    /**
     * Get director
     *
     * @return string 
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Set poblacionTotal
     *
     * @param integer $poblacionTotal
     * @return Colegio
     */
    public function setPoblacionTotal($poblacionTotal)
    {
        $this->poblacionTotal = $poblacionTotal;
    
        return $this;
    }

    /**
     * Get poblacionTotal
     *
     * @return integer 
     */
    public function getPoblacionTotal()
    {
        return $this->poblacionTotal;
    }

    /**
     * Set poblacionUltimoNivel
     *
     * @param integer $poblacionUltimoNivel
     * @return Colegio
     */
    public function setPoblacionUltimoNivel($poblacionUltimoNivel)
    {
        $this->poblacionUltimoNivel = $poblacionUltimoNivel;
    
        return $this;
    }

    /**
     * Get poblacionUltimoNivel
     *
     * @return integer 
     */
    public function getPoblacionUltimoNivel()
    {
        return $this->poblacionUltimoNivel;
    }

    /**
     * Set comentario
     *
     * @param $comentario
     * @return Colegio
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechaContacto
     *
     * @param \DateTime $fechaContacto
     * @return ContactoCRM
     */
    public function setFechaContacto($fechaContacto)
    {
        $this->fechaContacto = $fechaContacto;
        return $this;
    }

    /**
     * Get fechaContacto
     *
     * @return \DateTime 
     */
    public function getFechaContacto()
    {
        return $this->fechaContacto;
    }
    
    /**
     * Set canton
     *
     * @param \Cenfotec\BDBundle\Entity\Canton $canton
     * @return ContactoCRM
     */
    public function setCanton(\Cenfotec\BDBundle\Entity\Canton $canton = null)
    {
        $this->canton = $canton;
    
        return $this;
    }

    /**
     * Get canton
     *
     * @return \Cenfotec\BDBundle\Entity\Canton 
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * Set clase
     *
     * @param \Cenfotec\BDBundle\Entity\ClaseColegio $clase
     * @return Colegio
     */
    public function setClase(\Cenfotec\BDBundle\Entity\ClaseColegio $clase = null)
    {
        $this->clase = $clase;
    
        return $this;
    }

    /**
     * Get clase
     *
     * @return \Cenfotec\BDBundle\Entity\ClaseColegio 
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Add contactos
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoBasico $contactos
     * @return Colegio
     */
    public function addContacto(\Cenfotec\BDBundle\Entity\ContactoBasico $contactos)
    {
        $this->contactos[] = $contactos;
    
        return $this;
    }

    /**
     * Remove contactos
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoBasico $contactos
     */
    public function removeContacto(\Cenfotec\BDBundle\Entity\ContactoBasico $contactos)
    {
        $this->contactos->removeElement($contactos);
    }

    /**
     * Get contactos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set especialidad
     *
     * @param \Cenfotec\BDBundle\Entity\EspecialidadColegio $especialidad
     * @return Colegio
     */
    public function setEspecialidad(\Cenfotec\BDBundle\Entity\EspecialidadColegio $especialidad = null)
    {
        $this->especialidad = $especialidad;
    
        return $this;
    }

    /**
     * Get especialidad
     *
     * @return \Cenfotec\BDBundle\Entity\EspecialidadColegio 
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }

    /**
     * Set tipo
     *
     * @param \Cenfotec\BDBundle\Entity\TipoColegio $tipo
     * @return Colegio
     */
    public function setTipo(\Cenfotec\BDBundle\Entity\TipoColegio $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Cenfotec\BDBundle\Entity\TipoColegio 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    /**
     * Add seguimientos
     *
     * @param \Cenfotec\BDBundle\Entity\Seguimiento $seguimientos
     * @return Empresa
     */
    public function addSeguimiento(\Cenfotec\BDBundle\Entity\Seguimiento $seguimientos)
    {
        $this->seguimientos[] = $seguimientos;
    
        return $this;
    }

    /**
     * Remove seguimientos
     *
     * @param \Cenfotec\BDBundle\Entity\Seguimiento $seguimientos
     */
    public function removeSeguimiento(\Cenfotec\BDBundle\Entity\Seguimiento $seguimientos)
    {
        $this->seguimientos->removeElement($seguimientos);
    }

    /**
     * Get seguimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeguimientos()
    {
        return $this->seguimientos;
    }
    
    /**
     * Add intereses
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $intereses
     * @return Empresa
     */
    public function addInteres(\Cenfotec\BDBundle\Entity\Interes $intereses)
    {
        $this->intereses[] = $intereses;
    
        return $this;
    }

    /**
     * Remove intereses
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $intereses
     */
    public function removeInteres(\Cenfotec\BDBundle\Entity\Interes $intereses)
    {
        $this->intereses->removeElement($intereses);
    }

    /**
     * Get intereses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIntereses()
    {
        return $this->intereses;
    }

    /**
     * Add numeros
     *
     * @param \Cenfotec\BDBundle\Entity\Numero $numeros
     * @return ContactoBasico
     */
    public function addNumero(\Cenfotec\BDBundle\Entity\Numero $numeros)
    {
        $this->numeros[] = $numeros;
    
        return $this;
    }

    /**
     * Remove numeros
     *
     * @param \Cenfotec\BDBundle\Entity\Numero $numeros
     */
    public function removeNumero(\Cenfotec\BDBundle\Entity\Numero $numeros)
    {
        $this->numeros->removeElement($numeros);
    }

    /**
     * Get numeros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNumeros()
    {
        return $this->numeros;
    }
    
    /**
     * Set primerContacto
     *
     * @param \Cenfotec\BDBundle\Entity\PrimerContacto $primerContacto
     * @return ContactoCRM
     */
    public function setPrimerContacto(\Cenfotec\BDBundle\Entity\PrimerContacto $primerContacto = null)
    {
        $this->primerContacto = $primerContacto;
    
        return $this;
    }

    /**
     * Get primerContacto
     *
     * @return \Cenfotec\BDBundle\Entity\PrimerContacto 
     */
    public function getPrimerContacto()
    {
        return $this->primerContacto;
    }

    /**
     * Set referencia
     *
     * @param \Cenfotec\BDBundle\Entity\Referencia $referencia
     * @return ContactoCRM
     */
    public function setReferencia(\Cenfotec\BDBundle\Entity\Referencia $referencia = null)
    {
        $this->referencia = $referencia;
    
        return $this;
    }

    /**
     * Get referencia
     *
     * @return \Cenfotec\BDBundle\Entity\Referencia 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }
    
    /**
     * Set flagEliminado
     *
     * @param boolean $flagEliminado
     * @return ContactoCRM
     */
    public function setFlagEliminado($flagEliminado)
    {
        $this->flagEliminado = $flagEliminado;
    
        return $this;
    }

    /**
     * Get flagEliminado
     *
     * @return boolean 
     */
    public function getFlagEliminado()
    {
        return $this->flagEliminado;
    }
    
    /**
     * Get Estado
     *
     * @return string 
     */
    public function getEstado()
    {
        $estado = "";
        if ($this->flagEliminado) {
            $estado = "Eliminado en Colegio.";
        }else{
            $estado = "Contacto activo.";
        }
        return $estado;
    }
    
    /**
     * Set fechaModificoBD
     *
     * @param \DateTime $fechaModificoBD
     * @return ContactoCRM
     */
    public function setFechaModificoBD($fechaModificoBD)
    {
        $this->fechaModificoBD = $fechaModificoBD;
        return $this;
    }

    /**
     * Get fechaModificoBD
     *
     * @return \DateTime 
     */
    public function getFechaModificoBD()
    {
        return $this->fechaModificoBD;
    }
    
    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }
    
    /**
     * Set usuarioRegistroBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD
     * @return ContactoCRM
     */
    public function setUsuarioRegistroBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD = null)
    {
        $this->usuarioRegistroBD = $usuarioRegistroBD;
        return $this;
    }

    /**
     * Get usuarioRegistroBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioRegistroBD()
    {
        return $this->usuarioRegistroBD;
    }    

    /**
     * Set usuarioModificoBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD
     * @return ContactoCRM
     */
    public function setUsuarioModificoBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD = null)
    {
        $this->usuarioModificoBD = $usuarioModificoBD;
        return $this;
    }

    /**
     * Get usuarioModificoBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioModificoBD()
    {
        return $this->usuarioModificoBD;
    }  
}