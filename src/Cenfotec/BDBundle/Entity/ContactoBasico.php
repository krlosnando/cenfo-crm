<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\ContactoBasico
 *
 * @ORM\Table(name="t_contacto_basico")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ContactoBasicoRepository")
 */
class ContactoBasico
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=true)
     */
    private $apellido1;    

    /**
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;    

    /**
     * @ORM\Column(name="nombre_completo", type="string", length=250, nullable=true)
     */
    private $nombreCompleto; 
    
    /**
     * @ORM\Column(name="comentario", type="string", length=500, nullable=true)
     */
    private $comentario;
    
    /**
     * @ORM\ManyToOne(targetEntity="Puesto")
     * @ORM\JoinColumn(name="puesto_id", referencedColumnName="id", nullable=true)
     */
    private $puesto;
    
   /**
    * @ORM\ManyToMany(targetEntity="Correo")
    * @ORM\JoinTable(name="t_contacto_basico_x_correo",
    *      joinColumns={@ORM\JoinColumn(name="contacto_basico_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="correo_id", referencedColumnName="id")}
    *      )
    */
    private $correos;
    
    /**
     * @ORM\ManyToMany(targetEntity="Numero")
     * @ORM\JoinTable(name="t_contacto_basico_x_numero",
     *      joinColumns={@ORM\JoinColumn(name="contacto_basico_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="numero_id", referencedColumnName="id")}
     *      )
     */
    private $numeros;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->numeros = new \Doctrine\Common\Collections\ArrayCollection();
        $this->correos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ContactoBasico
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     * @return ContactoBasico
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;
    
        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string 
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     * @return ContactoBasico
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;
    
        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string 
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     * @return string
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    
        return $this;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ContactoBasico
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set puesto
     *
     * @param \Cenfotec\BDBundle\Entity\Puesto $puesto
     * @return ContactoBasico
     */
    public function setPuesto(\Cenfotec\BDBundle\Entity\Puesto $puesto = null)
    {
        $this->puesto = $puesto;
    
        return $this;
    }

    /**
     * Get puesto
     *
     * @return \Cenfotec\BDBundle\Entity\Puesto 
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Add numeros
     *
     * @param \Cenfotec\BDBundle\Entity\Numero $numeros
     * @return ContactoBasico
     */
    public function addNumero(\Cenfotec\BDBundle\Entity\Numero $numeros)
    {
        $this->numeros[] = $numeros;
    
        return $this;
    }

    /**
     * Remove numeros
     *
     * @param \Cenfotec\BDBundle\Entity\Numero $numeros
     */
    public function removeNumero(\Cenfotec\BDBundle\Entity\Numero $numeros)
    {
        $this->numeros->removeElement($numeros);
    }

    /**
     * Get numeros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNumeros()
    {
        return $this->numeros;
    }
    
    /**
     * Add correos
     *
     * @param \Cenfotec\BDBundle\Entity\Correo $correos
     * @return ContactoBasico
     */
    public function addCorreo(\Cenfotec\BDBundle\Entity\Correo $correo)
    {
        $this->correos[] = $correo;
    
        return $this;
    }

    /**
     * Remove correos
     *
     * @param \Cenfotec\BDBundle\Entity\Correo $correos
     */
    public function removeCorreo(\Cenfotec\BDBundle\Entity\Correo $correo)
    {
        $this->correos->removeElement($correo);
    }

    /**
     * Get correos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCorreos()
    {
        return $this->correos;
    }
}