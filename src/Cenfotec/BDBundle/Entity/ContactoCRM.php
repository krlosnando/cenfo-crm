<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\ContactoCRM
 *
 * @ORM\Table(name="t_contacto_crm")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ContactoCRMRepository")
 */
class ContactoCRM
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(name="apellido1", type="string", length=50, nullable=true)
     */
    private $apellido1;    

    /**
     * @ORM\Column(name="apellido2", type="string", length=50, nullable=true)
     */
    private $apellido2;    
    
    /**
     * @ORM\Column(name="nombre_completo", type="string", length=250, nullable=true)
     */
    private $nombreCompleto; 
    
    /**
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(name="identificacion", type="string", length=20, nullable=true)
     */
    private $identificacion;

    /**
    * @ORM\ManyToMany(targetEntity="Correo")
    * @ORM\JoinTable(name="t_contacto_crm_x_correo",
    *      joinColumns={@ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="correo_id", referencedColumnName="id")}
    *      )
    */
    private $correos;
    
    /**
     * Esta fecha puede ser la fecha en que el usuario tomo apuntes del contacto
     * y despues lo ingreso al sistema. Ej: Se ingresa un contacto que se apunto
     * en una nota hace 2 semanas, se tiene que poner la fecha de hace 2 semanas
     * 
     * @ORM\Column(name="fecha_contacto", type="datetime", nullable=true)
     */
    private $fechaContacto;
    
    /**
     * @ORM\Column(name="fecha_contacto_year", type="integer", nullable=true)
     */
    private $fechaContactoYear;
    
    /**
     * @ORM\Column(name="fecha_contacto_month", type="integer", nullable=true)
     */
    private $fechaContactoMonth;
    
    /**
     * @ORM\Column(name="comentario", type="string", length=500, nullable=true)
     */
    private $comentario;
    
    /**
     * @ORM\Column(name="genero", type="string", length=1, nullable=true)
     */
    private $genero;
    
    /**
     * @ORM\Column(name="flag_matriculado", type="boolean", nullable=false)
     */
    private $flagMatriculado;
    
    /**
     * @ORM\ManyToOne(targetEntity="Canton")
     * @ORM\JoinColumn(name="canton_id", referencedColumnName="id", nullable=true)
     */
    private $canton;

    /**
     * @ORM\ManyToOne(targetEntity="Convenio")
     * @ORM\JoinColumn(name="convenio_id", referencedColumnName="id", nullable=true)
     */
    private $convenio;

    /**
     * @ORM\ManyToOne(targetEntity="Horario")
     * @ORM\JoinColumn(name="horario_id", referencedColumnName="id", nullable=true)
     */
    private $horarioPreferencia;
    
    /**
     * @ORM\OneToMany(targetEntity="ContactoCRMXInteres", mappedBy="contactoCRM")
     */
    private $CRMXIntereses;
    
    /**
     * @ORM\OneToMany(targetEntity="Matricula", mappedBy="contactoCRM")
     */
    private $matriculas;
    
    /**
     * @ORM\ManyToOne(targetEntity="NivelInteres")
     * @ORM\JoinColumn(name="nivel_interes_id", referencedColumnName="id", nullable=true)
     */
    private $nivelInteres;
    
    /**
     * @ORM\OneToMany(targetEntity="ContactoCRMXNumero", mappedBy="contactoCRM")
     */
    private $CRMXNumeros;
    
    /**
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumn(name="pais_id", referencedColumnName="id", nullable=true)
     */
    private $paisResidencia;
    
    /**
     * @ORM\ManyToOne(targetEntity="PrimerContacto")
     * @ORM\JoinColumn(name="primer_contacto_id", referencedColumnName="id", nullable=true)
     */
    private $primerContacto;
    
    /**
     * @ORM\ManyToOne(targetEntity="Referencia")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id", nullable=true)
     */
    private $referencia;
    
    /**
    * @ORM\ManyToMany(targetEntity="Seguimiento", cascade={"persist"}, orphanRemoval=true)
    * @ORM\JoinTable(name="t_contacto_crm_x_seguimiento",
    *      joinColumns={@ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="seguimiento_id", referencedColumnName="id")}
    *      )
    */
    private $seguimientos;
    
    /**
     * @ORM\ManyToOne(targetEntity="ProgramaAcademico")
     * @ORM\JoinColumn(name="programa_academico_id", referencedColumnName="id", nullable=true)
     */
    private $programaAcademico;
    
    /**
     * @ORM\ManyToOne(targetEntity="Acti")
     * @ORM\JoinColumn(name="acti_id", referencedColumnName="id", nullable=true)
     */
    private $acti;
    
    /**
     * @ORM\ManyToOne(targetEntity="Posgrado")
     * @ORM\JoinColumn(name="posgrado_id", referencedColumnName="id", nullable=true)
     */
    private $posgrado;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoIdentificacion")
     * @ORM\JoinColumn(name="tipo_identificacion_id", referencedColumnName="id")
     */
    private $tipoIdentificacion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", nullable=true)
     */
    private $empresa;    
    
    /**
     * @ORM\ManyToOne(targetEntity="Profesion")
     * @ORM\JoinColumn(name="profesion_id", referencedColumnName="id", nullable=true)
     */
    private $profesion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Puesto")
     * @ORM\JoinColumn(name="puesto_id", referencedColumnName="id", nullable=true)
     */
    private $puesto;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_modifico_id", referencedColumnName="id", nullable=true)
     */
    private $usuarioModificoBD;
        
   /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_modifico_bd", type="datetime", nullable=true)
     */
    private $fechaModificoBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_registro_id", referencedColumnName="id")
     */
    private $usuarioRegistroBD;

    /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flagMatriculado = false;
        $this->correos         = new \Doctrine\Common\Collections\ArrayCollection();
        $this->CRMXIntereses   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->CRMXNumeros     = new \Doctrine\Common\Collections\ArrayCollection();
        $this->matriculas      = new \Doctrine\Common\Collections\ArrayCollection();
        $this->seguimientos    = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechaRegistroBD = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ContactoCRM
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     * @return ContactoCRM
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;
    
        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string 
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     * @return ContactoCRM
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;
    
        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string 
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     * @return string
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    
        return $this;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }
    
    /**
     * Set direccion
     *
     * @param string $direccion
     * @return ContactoCRM
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set identificacion
     *
     * @param string $identificacion
     * @return ContactoCRM
     */
    public function setIdentificacion($identificacion)
    {
        $this->identificacion = $identificacion;
    
        return $this;
    }

    /**
     * Get identificacion
     *
     * @return string 
     */
    public function getIdentificacion()
    {
        return $this->identificacion;
    }

    /**
     * Set fechaContacto
     *
     * @param \DateTime $fechaContacto
     * @return ContactoCRM
     */
    public function setFechaContacto($fechaContacto)
    {
        $this->fechaContacto = $fechaContacto;
        $this->setFechaContactoYear(intval($fechaContacto->format("Y")));
        $this->setFechaContactoMonth(intval($fechaContacto->format("m")));
        return $this;
    }

    /**
     * Get fechaContacto
     *
     * @return \DateTime 
     */
    public function getFechaContacto()
    {
        return $this->fechaContacto;
    }
    
    /**
     * Set fechaContactoYear
     *
     * @param int $fechaContactoYear
     * @return ContactoCRM
     */
    public function setFechaContactoYear($year)
    {
        $this->fechaContactoYear = $year;
        return $this;
    }

    /**
     * Get fechaContactoYear
     *
     * @return int 
     */
    public function getFechaContactoYear()
    {
        return $this->fechaContactoYear;
    }
    
    /**
     * Set fechaContactoMonth
     *
     * @param int $fechaContactoMonth
     * @return ContactoCRM
     */
    public function setFechaContactoMonth($month)
    {
        $this->fechaContactoMonth = $month;
        return $this;
    }

    /**
     * Get fechaContactoMonth
     *
     * @return int 
     */
    public function getFechaContactoMonth()
    {
        return $this->fechaContactoMonth;
    }
    
    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ContactoCRM
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return ContactoCRM
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;
    
        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set flagMatriculado
     *
     * @param boolean $flagMatriculado
     * @return ContactoCRM
     */
    public function setFlagMatriculado($flagMatriculado)
    {
        $this->flagMatriculado = $flagMatriculado;
    
        return $this;
    }

    /**
     * Get flagMatriculado
     *
     * @return boolean 
     */
    public function getFlagMatriculado()
    {
        return $this->flagMatriculado;
    }

    /**
     * Add correos
     *
     * @param \Cenfotec\BDBundle\Entity\Correo $correos
     * @return ContactoCRM
     */
    public function addCorreo(\Cenfotec\BDBundle\Entity\Correo $correos)
    {
        $this->correos[] = $correos;
    
        return $this;
    }

    /**
     * Remove correos
     *
     * @param \Cenfotec\BDBundle\Entity\Correo $correos
     */
    public function removeCorreo(\Cenfotec\BDBundle\Entity\Correo $correos)
    {
        $this->correos->removeElement($correos);
    }

    /**
     * Get correos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function existCorreo()
    {
        return $this->correos;
    }
    
    /**
     * Get correos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCorreos()
    {
        return $this->correos;
    }

    /**
     * Set canton
     *
     * @param \Cenfotec\BDBundle\Entity\Canton $canton
     * @return ContactoCRM
     */
    public function setCanton(\Cenfotec\BDBundle\Entity\Canton $canton = null)
    {
        $this->canton = $canton;
    
        return $this;
    }

    /**
     * Get canton
     *
     * @return \Cenfotec\BDBundle\Entity\Canton 
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * Set convenio
     *
     * @param \Cenfotec\BDBundle\Entity\Convenio $convenio
     * @return ContactoCRM
     */
    public function setConvenio(\Cenfotec\BDBundle\Entity\Convenio $convenio = null)
    {
        $this->convenio = $convenio;
    
        return $this;
    }

    /**
     * Get convenio
     *
     * @return \Cenfotec\BDBundle\Entity\Convenio 
     */
    public function getConvenio()
    {
        return $this->convenio;
    }

    /**
     * Set horarioPreferencia
     *
     * @param \Cenfotec\BDBundle\Entity\Horario $horarioPreferencia
     * @return ContactoCRM
     */
    public function setHorarioPreferencia(\Cenfotec\BDBundle\Entity\Horario $horarioPreferencia = null)
    {
        $this->horarioPreferencia = $horarioPreferencia;
    
        return $this;
    }

    /**
     * Get horarioPreferencia
     *
     * @return \Cenfotec\BDBundle\Entity\Horario 
     */
    public function getHorarioPreferencia()
    {
        return $this->horarioPreferencia;
    }

    /**
     * Add CRMXInteres
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRMXInteres $CRMXInteres
     * @return ContactoCRM
     */
    public function addCRMXInteres(\Cenfotec\BDBundle\Entity\ContactoCRMXInteres $CRMXInteres)
    {
        $this->CRMXIntereses[] = $CRMXInteres;
        return $this;
    }

    /**
     * Remove CRMXInteres
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRMXInteres $CRMXInteres
     */
    public function removeCRMXInteres(\Cenfotec\BDBundle\Entity\ContactoCRMXInteres $CRMXInteres)
    {
        $this->CRMXIntereses->removeElement($CRMXInteres);
    }

    /**
     * Get CRMXIntereses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCRMXIntereses()
    {
        return $this->CRMXIntereses;
    }

    /**
     * Get intereses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIntereses()
    {
        $intereses = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->CRMXIntereses as $objCRMXInteres) {
            $intereses[] = $objCRMXInteres->getInteres();
        }
        return $intereses;
    }
    
    /**
     * Add matriculas
     *
     * @param \Cenfotec\BDBundle\Entity\Matricula $matriculas
     * @return ContactoCRM
     */
    public function addMatricula(\Cenfotec\BDBundle\Entity\Matricula $matriculas)
    {
        $this->matriculas[] = $matriculas;
    
        return $this;
    }

    /**
     * Remove matriculas
     *
     * @param \Cenfotec\BDBundle\Entity\Matricula $matriculas
     */
    public function removeMatricula(\Cenfotec\BDBundle\Entity\Matricula $matriculas)
    {
        $this->matriculas->removeElement($matriculas);
    }

    /**
     * Get matriculas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMatriculas()
    {
        return $this->matriculas;
    }

    /**
     * Set nivelInteres
     *
     * @param \Cenfotec\BDBundle\Entity\NivelInteres $nivelInteres
     * @return ContactoCRM
     */
    public function setNivelInteres(\Cenfotec\BDBundle\Entity\NivelInteres $nivelInteres = null)
    {
        $this->nivelInteres = $nivelInteres;
    
        return $this;
    }

    /**
     * Get nivelInteres
     *
     * @return \Cenfotec\BDBundle\Entity\NivelInteres 
     */
    public function getNivelInteres()
    {
        return $this->nivelInteres;
    }

    /**
     * Set paisResidencia
     *
     * @param \Cenfotec\BDBundle\Entity\Pais $paisResidencia
     * @return ContactoCRM
     */
    public function setPaisResidencia(\Cenfotec\BDBundle\Entity\Pais $paisResidencia = null)
    {
        $this->paisResidencia = $paisResidencia;
    
        return $this;
    }

    /**
     * Get paisResidencia
     *
     * @return \Cenfotec\BDBundle\Entity\Pais 
     */
    public function getPaisResidencia()
    {
        return $this->paisResidencia;
    }

    /**
     * Set primerContacto
     *
     * @param \Cenfotec\BDBundle\Entity\PrimerContacto $primerContacto
     * @return ContactoCRM
     */
    public function setPrimerContacto(\Cenfotec\BDBundle\Entity\PrimerContacto $primerContacto = null)
    {
        $this->primerContacto = $primerContacto;
    
        return $this;
    }

    /**
     * Get primerContacto
     *
     * @return \Cenfotec\BDBundle\Entity\PrimerContacto 
     */
    public function getPrimerContacto()
    {
        return $this->primerContacto;
    }

    /**
     * Set referencia
     *
     * @param \Cenfotec\BDBundle\Entity\Referencia $referencia
     * @return ContactoCRM
     */
    public function setReferencia(\Cenfotec\BDBundle\Entity\Referencia $referencia = null)
    {
        $this->referencia = $referencia;
    
        return $this;
    }

    /**
     * Get referencia
     *
     * @return \Cenfotec\BDBundle\Entity\Referencia 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Add seguimientos
     *
     * @param \Cenfotec\BDBundle\Entity\Seguimiento $seguimientos
     * @return ContactoCRM
     */
    public function addSeguimiento(\Cenfotec\BDBundle\Entity\Seguimiento $seguimientos)
    {
        $this->seguimientos[] = $seguimientos;
    
        return $this;
    }

    /**
     * Remove seguimientos
     *
     * @param \Cenfotec\BDBundle\Entity\Seguimiento $seguimientos
     */
    public function removeSeguimiento(\Cenfotec\BDBundle\Entity\Seguimiento $seguimientos)
    {
        $this->seguimientos->removeElement($seguimientos);
    }

    /**
     * Get seguimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeguimientos()
    {
        return $this->seguimientos;
    }

    /**
     * Set programaAcademico
     *
     * @param \Cenfotec\BDBundle\Entity\ProgramaAcademico $programaAcademico
     * @return ContactoCRM
     */
    public function setProgramaAcademico(\Cenfotec\BDBundle\Entity\ProgramaAcademico $programaAcademico = null)
    {
        $this->programaAcademico = $programaAcademico;
        return $this;
    }

    /**
     * Get programaAcademico
     *
     * @return \Cenfotec\BDBundle\Entity\ProgramaAcademico 
     */
    public function getProgramaAcademico()
    {
        return $this->programaAcademico;
    }
    
    /**
     * Set acti
     *
     * @param \Cenfotec\BDBundle\Entity\Acti $acti
     * @return ContactoCRM
     */
    public function setActi(\Cenfotec\BDBundle\Entity\Acti $acti = null)
    {
        $this->acti = $acti;
        return $this;
    }

    /**
     * Get acti
     *
     * @return \Cenfotec\BDBundle\Entity\Acti 
     */
    public function getActi()
    {
        return $this->acti;
    }
    
    /**
     * Set posgrado
     *
     * @param \Cenfotec\BDBundle\Entity\Posgrado $posgrado
     * @return ContactoCRM
     */
    public function setPosgrado(\Cenfotec\BDBundle\Entity\Posgrado $posgrado = null)
    {
        $this->posgrado = $posgrado;
        return $this;
    }

    /**
     * Get posgrado
     *
     * @return \Cenfotec\BDBundle\Entity\Posgrado 
     */
    public function getPosgrado()
    {
        return $this->posgrado;
    }
    
    /**
     * Set tipoIdentificacion
     *
     * @param \Cenfotec\BDBundle\Entity\TipoIdentificacion $tipoIdentificacion
     * @return ContactoCRM
     */
    public function setTipoIdentificacion(\Cenfotec\BDBundle\Entity\TipoIdentificacion $tipoIdentificacion = null)
    {
        $this->tipoIdentificacion = $tipoIdentificacion;
    
        return $this;
    }

    /**
     * Get tipoIdentificacion
     *
     * @return \Cenfotec\BDBundle\Entity\TipoIdentificacion 
     */
    public function getTipoIdentificacion()
    {
        return $this->tipoIdentificacion;
    }
    
    /**
     * Set fechaModificoBD
     *
     * @param \DateTime $fechaModificoBD
     * @return ContactoCRM
     */
    public function setFechaModificoBD($fechaModificoBD)
    {
        $this->fechaModificoBD = $fechaModificoBD;
        return $this;
    }

    /**
     * Get fechaModificoBD
     *
     * @return \DateTime 
     */
    public function getFechaModificoBD()
    {
        return $this->fechaModificoBD;
    }
    
    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }
    
    /**
     * Set usuarioRegistroBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD
     * @return ContactoCRM
     */
    public function setUsuarioRegistroBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD = null)
    {
        $this->usuarioRegistroBD = $usuarioRegistroBD;
        return $this;
    }

    /**
     * Get usuarioRegistroBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioRegistroBD()
    {
        return $this->usuarioRegistroBD;
    }    

    /**
     * Set usuarioModificoBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD
     * @return ContactoCRM
     */
    public function setUsuarioModificoBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD = null)
    {
        $this->usuarioModificoBD = $usuarioModificoBD;
        return $this;
    }

    /**
     * Get usuarioModificoBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioModificoBD()
    {
        return $this->usuarioModificoBD;
    }    

    /**
     * Set empresa
     *
     * @param \Cenfotec\BDBundle\Entity\Empresa $empresa
     * @return ContactoCRM
     */
    public function setEmpresa(\Cenfotec\BDBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;
    
        return $this;
    }

    /**
     * Get empresa
     *
     * @return \Cenfotec\BDBundle\Entity\Empresa 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set profesion
     *
     * @param \Cenfotec\BDBundle\Entity\Profesion $profesion
     * @return ACTI
     */
    public function setProfesion(\Cenfotec\BDBundle\Entity\Profesion $profesion = null)
    {
        $this->profesion = $profesion;
    
        return $this;
    }

    /**
     * Get profesion
     *
     * @return \Cenfotec\BDBundle\Entity\Profesion 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }
    
    /**
     * Set puesto
     *
     * @param \Cenfotec\BDBundle\Entity\Puesto $puesto
     * @return ContactoBasico
     */
    public function setPuesto(\Cenfotec\BDBundle\Entity\Puesto $puesto = null)
    {
        $this->puesto = $puesto;
    
        return $this;
    }

    /**
     * Get puesto
     *
     * @return \Cenfotec\BDBundle\Entity\Puesto 
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Add CRMXNumero
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRMXNumero $CRMXNumero
     * @return ContactoCRM
     */
    public function addCRMXNumero(\Cenfotec\BDBundle\Entity\ContactoCRMXNumero $CRMXNumero)
    {
        $this->CRMXNumeros[] = $CRMXNumero;
        return $this;
    }

    /**
     * Remove CRMXNumero
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRMXNumero $CRMXNumero
     */
    public function removeCRMXNumero(\Cenfotec\BDBundle\Entity\ContactoCRMXNumero $CRMXNumero)
    {
        $this->CRMXNumeros->removeElement($CRMXNumero);
    }

    /**
     * Get CRMXNumeros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCRMXNumeros()
    {
        return $this->CRMXNumeros;
    }
    
    /**
     * Get numeros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNumeros()
    {
        $numeros = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->CRMXNumeros as $objCRMXNumero) {
            $numeros[] = $objCRMXNumero->getNumero();
        }
        return $numeros;
    }
}