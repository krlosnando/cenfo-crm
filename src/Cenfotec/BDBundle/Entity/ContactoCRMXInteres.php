<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\ContactoCRMXNumero
 *
 * @ORM\Table(name="t_contacto_crm_x_interes",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="crm_interes_modalida_unique",columns={"contacto_crm_id","interes_id", "modalidad_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ContactoCRMXInteresRepository")
 */
class ContactoCRMXInteres
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="ContactoCRM")
     * @ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id", nullable=false)
     */
    private $contactoCRM;

    /**
     * @ORM\ManyToOne(targetEntity="Interes", cascade={"persist"})
     * @ORM\JoinColumn(name="interes_id", referencedColumnName="id", nullable=false)
     */
    private $interes;
    
    /**
     * @ORM\ManyToOne(targetEntity="Modalidad")
     * @ORM\JoinColumn(name="modalidad_id", referencedColumnName="id")
     */
    private $modalidad;

    /**
     * @ORM\Column(name="fecha_interes", type="datetime", nullable=true)
     */
    private $fechaInteres;
    
    /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
        
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaRegistroBD = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set contactoCRM
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM
     * @return ContactoCRMXNumero
     */
    public function setContactoCRM(\Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM)
    {
        $this->contactoCRM = $contactoCRM;
        return $this;
    }

    /**
     * Get contactoCRM
     *
     * @return \Cenfotec\BDBundle\Entity\ContactoCRM 
     */
    public function getContactoCRM()
    {
        return $this->contactoCRM;
    }
    
    /**
     * Set interes
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $interes
     * @return Interes
     */
    public function setInteres(\Cenfotec\BDBundle\Entity\Interes $interes)
    {
        $this->interes = $interes;
        return $this;
    }

    /**
     * Get interes
     *
     * @return \Cenfotec\BDBundle\Entity\Interes
     */
    public function getInteres()
    {
        return $this->interes;
    }
    
    /**
     * Set fechaInteres
     *
     * @param \DateTime $fechaInteres
     * @return ContactoCRMXInteres
     */
    public function setFechaInteres($fechaInteres)
    {
        $this->fechaInteres = $fechaInteres;
        return $this;
    }

    /**
     * Get fechaInteres
     *
     * @return \DateTime 
     */
    public function getFechaInteres()
    {
        return $this->fechaInteres;
    }
    
    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }
    
    /**
     * Set modalidad
     *
     * @param \Cenfotec\BDBundle\Entity\Modalidad $modalidad
     * @return ContactoCRM
     */
    public function setModalidad(\Cenfotec\BDBundle\Entity\Modalidad $modalidad = null)
    {
        $this->modalidad = $modalidad;
    
        return $this;
    }

    /**
     * Get modalidad
     *
     * @return \Cenfotec\BDBundle\Entity\Modalidad 
     */
    public function getModalidad()
    {
        return $this->modalidad;
    }

}