<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\ContactoCRMXNumero
 *
 * @ORM\Table(name="t_contacto_crm_x_numero")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ContactoCRMXNumeroRepository")
 */
class ContactoCRMXNumero
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ContactoCRM")
     * @ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id", nullable=false)
     */
    private $contactoCRM;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Numero", cascade={"persist"})
     * @ORM\JoinColumn(name="numero_id", referencedColumnName="id", nullable=false)
     */
    private $numero;
    
    /**
     * @ORM\Column(name="comentario", type="string", length=255, nullable=true)
     */
    private $comentario;
    
    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ContactoCRMXNumero
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set contactoCRM
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM
     * @return ContactoCRMXNumero
     */
    public function setContactoCRM(\Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM)
    {
        $this->contactoCRM = $contactoCRM;
    
        return $this;
    }

    /**
     * Get contactoCRM
     *
     * @return \Cenfotec\BDBundle\Entity\ContactoCRM 
     */
    public function getContactoCRM()
    {
        return $this->contactoCRM;
    }

    /**
     * Set numero
     *
     * @param \Cenfotec\BDBundle\Entity\Numero $numero
     * @return ContactoCRMXNumero
     */
    public function setNumero(\Cenfotec\BDBundle\Entity\Numero $numero)
    {
        $this->numero = $numero;
    
        return $this;
    }

    /**
     * Get numero
     *
     * @return \Cenfotec\BDBundle\Entity\Numero 
     */
    public function getNumero()
    {
        return $this->numero;
    }
}