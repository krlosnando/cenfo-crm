<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\SeguridadBundle\Entity\Excepcion
 *
 * @ORM\Table(name="t_excepcion")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ExcepcionRepository")
 */
class Excepcion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;
    
    /**
     * @ORM\Column(name="metodo", type="string", length=1000)
     */
    private $metodo;
    
    /**
     * @ORM\Column(name="excepcion", type="string", length=5000)
     */
    private $excepcion;
    
    /**
     * @ORM\Column(name="usuario_id", type="integer")
     */
    private $usuarioId;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fecha = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Excepcion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set metodo
     *
     * @param string $metodo
     * @return Excepcion
     */
    public function setMetodo($metodo)
    {
        $this->metodo = $metodo;
    
        return $this;
    }

    /**
     * Get metodo
     *
     * @return string 
     */
    public function getMetodo()
    {
        return $this->metodo;
    }

    /**
     * Set excepcion
     *
     * @param string $excepcion
     * @return Excepcion
     */
    public function setExcepcion($excepcion)
    {
        $this->excepcion = $excepcion;
    
        return $this;
    }

    /**
     * Get excepcion
     *
     * @return string 
     */
    public function getExcepcion()
    {
        return $this->excepcion;
    }

    /**
     * Set usuarioId
     *
     * @param string $usuarioId
     * @return Excepcion
     */
    public function setUsuarioId($usuarioId)
    {
        $this->usuarioId = $usuarioId;
        return $this;
    }

    /**
     * Get usuarioId
     *
     * @return string 
     */
    public function getUsuarioId()
    {
        return $this->usuarioId;
    }
}