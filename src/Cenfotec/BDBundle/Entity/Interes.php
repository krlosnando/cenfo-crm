<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Interes
 *
 * @ORM\Table(name="t_interes",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="nombre_area_unique",columns={"nombre","area_interes_id"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\InteresRepository")
 */
class Interes
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;
    
    /**
     * @ORM\ManyToOne(targetEntity="AreaInteres", inversedBy="intereses")
     * @ORM\JoinColumn(name="area_interes_id", referencedColumnName="id")
     */
    private $area;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Interes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set area
     *
     * @param \Cenfotec\BDBundle\Entity\AreaInteres $area
     * @return Interes
     */
    public function setArea(\Cenfotec\BDBundle\Entity\AreaInteres $area = null)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return \Cenfotec\BDBundle\Entity\AreaInteres 
     */
    public function getArea()
    {
        return $this->area;
    }
}