<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Matricula
 *
 * @ORM\Table(name="t_matricula")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\MatriculaRepository")
 */
class Matricula
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=true)
     */
    private $fechaInicio;

    /**
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true)
     */
    private $fechaFin;
    
    /**
     * @ORM\Column(name="fecha_matricula", type="datetime", nullable=true)
     */
    private $fechaMatricula;
    
    /**
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="ContactoCRM", inversedBy="matriculas")
     * @ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id")
     */
    private $contactoCRM;
    
    /**
     * @ORM\ManyToOne(targetEntity="Interes")
     * @ORM\JoinColumn(name="interes_id", referencedColumnName="id")
     */
    private $interes;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaRegistroBD = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Matricula
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Matricula
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set fechaMatricula
     *
     * @param \DateTime $fechaMatricula
     * @return Matricula
     */
    public function setFechaMatricula($fechaMatricula)
    {
        $this->fechaMatricula = $fechaMatricula;
    
        return $this;
    }

    /**
     * Get fechaMatricula
     *
     * @return \DateTime 
     */
    public function getFechaMatricula()
    {
        return $this->fechaMatricula;
    }
    
    /**
     * Set fechaRegistroBD
     *
     * @param \DateTime $fechaRegistroBD
     * @return Matricula
     */
    public function setFechaRegistroBD($fechaRegistroBD)
    {
        $this->fechaRegistroBD = $fechaRegistroBD;
        return $this;
    }

    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }

    /**
     * Set contactoCRM
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM
     * @return Matricula
     */
    public function setContactoCRM(\Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM = null)
    {
        $this->contactoCRM = $contactoCRM;
    
        return $this;
    }

    /**
     * Get contactoCRM
     *
     * @return \Cenfotec\BDBundle\Entity\ContactoCRM 
     */
    public function getContactoCRM()
    {
        return $this->contactoCRM;
    }

    /**
     * Set interes
     *
     * @param \Cenfotec\BDBundle\Entity\Interes $interes
     * @return Matricula
     */
    public function setInteres(\Cenfotec\BDBundle\Entity\Interes $interes = null)
    {
        $this->interes = $interes;
    
        return $this;
    }

    /**
     * Get interes
     *
     * @return \Cenfotec\BDBundle\Entity\Interes 
     */
    public function getInteres()
    {
        return $this->interes;
    }

    /**
     * Set usuario
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuario
     * @return Matricula
     */
    public function setUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}