<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Numero
 *
 * @ORM\Table(name="t_numero",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="numero_ext_unique",columns={"numero","ext"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\NumeroRepository")
 */
class Numero
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="numero", type="string", length=20)
     */
    private $numero;

    /**
     * @ORM\Column(name="ext", type="string", length=10, nullable=true)
     */
    private $ext;

    /**
     * @ORM\Column(name="tipo_numero_id", type="integer")
     */
    private $tipoId;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoNumero")
     * @ORM\JoinColumn(name="tipo_numero_id", referencedColumnName="id", nullable=true)
     */
    private $tipo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    
        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Get tipoId
     *
     * @return Integer 
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }
    
    /**
     * Set ext
     *
     * @param string $ext
     * @return Numero
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
    
        return $this;
    }

    /**
     * Get ext
     *
     * @return string 
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set tipo
     *
     * @param \Cenfotec\BDBundle\Entity\TipoNumero $tipo
     * @return Numero
     */
    public function setTipo(\Cenfotec\BDBundle\Entity\TipoNumero $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Cenfotec\BDBundle\Entity\TipoNumero 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}