<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Pais
 *
 * @ORM\Table(name="t_pais")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\PaisRepository")
 */
class Pais
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Pais
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}