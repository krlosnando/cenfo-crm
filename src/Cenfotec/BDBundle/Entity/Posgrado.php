<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Posgrado
 *
 * @ORM\Table(name="t_posgrado")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\PosgradoRepository")
 */
class Posgrado
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="ContactoCRM")
     * @ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id", nullable=false)
     **/
    private $contactoCRM;
            
    /**
     * @ORM\Column(name="flag_eliminado", type="boolean", nullable=false)
     */
    private $flagEliminado;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flagEliminado = false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactoCRM
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM
     * @return Posgrado
     */
    public function setContactoCRM(\Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM)
    {
        $this->contactoCRM = $contactoCRM;
    
        return $this;
    }

    /**
     * Get contactoCRM
     *
     * @return \Cenfotec\BDBundle\Entity\ContactoCRM 
     */
    public function getContactoCRM()
    {
        return $this->contactoCRM;
    }
            
    /**
     * Set flagEliminado
     *
     * @param boolean $flagEliminado
     * @return ContactoCRM
     */
    public function setFlagEliminado($flagEliminado)
    {
        $this->flagEliminado = $flagEliminado;
    
        return $this;
    }

    /**
     * Get flagEliminado
     *
     * @return boolean 
     */
    public function getFlagEliminado()
    {
        return $this->flagElminado;
    }
}