<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\PrimerContacto
 *
 * @ORM\Table(name="t_primer_contacto")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\PrimerContactoRepository")
 */
class PrimerContacto
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_modifico_id", referencedColumnName="id", nullable=true)
     */
    private $usuarioModificoBD;
        
   /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_modifico_bd", type="datetime", nullable=true)
     */
    private $fechaModificoBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_registro_id", referencedColumnName="id")
     */
    private $usuarioRegistroBD;

    /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaRegistroBD = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return PrimerContacto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set fechaModificoBD
     *
     * @param \DateTime $fechaModificoBD
     * @return ContactoCRM
     */
    public function setFechaModificoBD($fechaModificoBD)
    {
        $this->fechaModificoBD = $fechaModificoBD;
        return $this;
    }

    /**
     * Get fechaModificoBD
     *
     * @return \DateTime 
     */
    public function getFechaModificoBD()
    {
        return $this->fechaModificoBD;
    }
    
    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }
    
    /**
     * Set usuarioRegistroBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD
     * @return ContactoCRM
     */
    public function setUsuarioRegistroBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD = null)
    {
        $this->usuarioRegistroBD = $usuarioRegistroBD;
        return $this;
    }

    /**
     * Get usuarioRegistroBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioRegistroBD()
    {
        return $this->usuarioRegistroBD;
    }    

    /**
     * Set usuarioModificoBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD
     * @return ContactoCRM
     */
    public function setUsuarioModificoBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD = null)
    {
        $this->usuarioModificoBD = $usuarioModificoBD;
        return $this;
    }

    /**
     * Get usuarioModificoBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioModificoBD()
    {
        return $this->usuarioModificoBD;
    }   
}