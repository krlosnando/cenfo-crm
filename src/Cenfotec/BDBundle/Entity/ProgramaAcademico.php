<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\ProgramaAcademico
 *
 * @ORM\Table(name="t_programa_academico")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ProgramaAcademicoRepository")
 */
class ProgramaAcademico
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Colegio")
     * @ORM\JoinColumn(name="colegio_id", referencedColumnName="id", nullable=true)
     */
    private $colegio;

    /**
     * @ORM\Column(name="contacto_crm_id", type="integer")
     */
    private $idContactoCRM;
    
    /**
     * @ORM\OneToOne(targetEntity="ContactoCRM")
     * @ORM\JoinColumn(name="contacto_crm_id", referencedColumnName="id")
     **/
    private $contactoCRM;
    
    /**
     * @ORM\ManyToOne(targetEntity="NivelColegio")
     * @ORM\JoinColumn(name="nivel_colegio_id", referencedColumnName="id", nullable=true)
     */
    private $nivelColegio;
            
    /**
     * @ORM\Column(name="flag_eliminado", type="boolean", nullable=false)
     */
    private $flagEliminado;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flagEliminado = false;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set colegio
     *
     * @param \Cenfotec\BDBundle\Entity\Colegio $colegio
     * @return ProgramaAcademico
     */
    public function setColegio(\Cenfotec\BDBundle\Entity\Colegio $colegio = null)
    {
        $this->colegio = $colegio;
    
        return $this;
    }

    /**
     * Get colegio
     *
     * @return \Cenfotec\BDBundle\Entity\Colegio 
     */
    public function getColegio()
    {
        return $this->colegio;
    }

    /**
     * Set contactoCRM
     *
     * @param \Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM
     * @return ProgramaAcademico
     */
    public function setContactoCRM(\Cenfotec\BDBundle\Entity\ContactoCRM $contactoCRM = null)
    {
        $this->contactoCRM = $contactoCRM;
    
        return $this;
    }

    /**
     * Get contactoCRM
     *
     * @return \Cenfotec\BDBundle\Entity\ContactoCRM 
     */
    public function getContactoCRM()
    {
        return $this->contactoCRM;
    }

    /**
     * Set nivelColegio
     *
     * @param \Cenfotec\BDBundle\Entity\NivelColegio $nivelColegio
     * @return ProgramaAcademico
     */
    public function setNivelColegio(\Cenfotec\BDBundle\Entity\NivelColegio $nivelColegio = null)
    {
        $this->nivelColegio = $nivelColegio;
    
        return $this;
    }

    /**
     * Get nivelColegio
     *
     * @return \Cenfotec\BDBundle\Entity\NivelColegio 
     */
    public function getNivelColegio()
    {
        return $this->nivelColegio;
    }
            
    /**
     * Set flagEliminado
     *
     * @param boolean $flagEliminado
     * @return ContactoCRM
     */
    public function setFlagEliminado($flagEliminado)
    {
        $this->flagEliminado = $flagEliminado;
    
        return $this;
    }

    /**
     * Get flagEliminado
     *
     * @return boolean 
     */
    public function getFlagEliminado()
    {
        return $this->flagElminado;
    }
}