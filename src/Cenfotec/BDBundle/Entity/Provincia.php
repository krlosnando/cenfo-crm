<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Provincia
 *
 * @ORM\Table(name="t_provincia",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="nombre_provincia_unique",columns={"nombre"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ProvinciaRepository")
 */
class Provincia
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Canton", mappedBy="provincia")
     */
    private $cantones;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cantones = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add cantones
     *
     * @param \Cenfotec\BDBundle\Entity\Canton $cantones
     * @return Provincia
     */
    public function addCantone(\Cenfotec\BDBundle\Entity\Canton $cantones)
    {
        $this->cantones[] = $cantones;
    
        return $this;
    }

    /**
     * Remove cantones
     *
     * @param \Cenfotec\BDBundle\Entity\Canton $cantones
     */
    public function removeCantone(\Cenfotec\BDBundle\Entity\Canton $cantones)
    {
        $this->cantones->removeElement($cantones);
    }

    /**
     * Get cantones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCantones()
    {
        return $this->cantones;
    }
}