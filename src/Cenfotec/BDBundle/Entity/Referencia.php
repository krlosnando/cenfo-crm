<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Referencia
 *
 * @ORM\Table(name="t_referencia")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\ReferenciaRepository")
 */
class Referencia
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoReferencia", inversedBy="referencias")
     * @ORM\JoinColumn(name="tipo_referencia_id", referencedColumnName="id")
     */
    private $tipo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Referencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo
     *
     * @param \Cenfotec\BDBundle\Entity\TipoReferencia $tipo
     * @return Referencia
     */
    public function setTipo(\Cenfotec\BDBundle\Entity\TipoReferencia $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Cenfotec\BDBundle\Entity\TipoReferencia 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}