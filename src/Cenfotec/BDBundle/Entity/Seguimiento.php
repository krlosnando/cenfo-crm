<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\Seguimiento
 *
 * @ORM\Table(name="t_seguimiento")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\SeguimientoRepository")
 */
class Seguimiento
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="comentario", type="string", length=255)
     */
    private $comentario;
    
    /**
     * @ORM\Column(name="fecha_seguimiento", type="datetime")
     */
    private $fechaSeguimiento;
    
    /**
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoSeguimiento")
     * @ORM\JoinColumn(name="tipo_seguimiento_id", referencedColumnName="id")
     */
    private $tipo;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaRegistroBD = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Seguimiento
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechaSeguimiento
     *
     * @param \DateTime $fechaSeguimiento
     * @return Seguimiento
     */
    public function setFechaSeguimiento($fechaSeguimiento)
    {
        $this->fechaSeguimiento = $fechaSeguimiento;
    
        return $this;
    }

    /**
     * Get fechaSeguimiento
     *
     * @return \DateTime 
     */
    public function getFechaSeguimiento()
    {
        return $this->fechaSeguimiento;
    }

    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }

    /**
     * Set tipo
     *
     * @param \Cenfotec\BDBundle\Entity\TipoSeguimiento $tipo
     * @return Seguimiento
     */
    public function setTipo(\Cenfotec\BDBundle\Entity\TipoSeguimiento $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Cenfotec\BDBundle\Entity\TipoSeguimiento 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set usuario
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuario
     * @return Seguimiento
     */
    public function setUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}