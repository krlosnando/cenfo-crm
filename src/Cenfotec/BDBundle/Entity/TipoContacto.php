<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\TipoContacto
 *
 * @ORM\Table(name="t_tipo_contacto")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\TipoContactoRepository")
 */
class TipoContacto
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="AreaInteres", mappedBy="tipoContacto")
     */
    private $areasInteres;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->areasInteres = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoContacto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add areasInteres
     *
     * @param \Cenfotec\BDBundle\Entity\AreaInteres $areasInteres
     * @return TipoContacto
     */
    public function addAreaInteres(\Cenfotec\BDBundle\Entity\AreaInteres $areasInteres)
    {
        $this->areasInteres[] = $areasInteres;
    
        return $this;
    }

    /**
     * Remove areasInteres
     *
     * @param \Cenfotec\BDBundle\Entity\AreaInteres $areasInteres
     */
    public function removeAreaInteres(\Cenfotec\BDBundle\Entity\AreaInteres $areasInteres)
    {
        $this->areasInteres->removeElement($areasInteres);
    }

    /**
     * Get areasInteres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAreasInteres()
    {
        return $this->areasInteres;
    }
}