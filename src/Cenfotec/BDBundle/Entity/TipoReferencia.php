<?php

namespace Cenfotec\BDBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\BDBundle\Entity\TipoReferencia
 *
 * @ORM\Table(name="t_tipo_referencia")
 * @ORM\Entity(repositoryClass="\Cenfotec\BDBundle\Repository\TipoReferenciaRepository")
 */
class TipoReferencia
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    
    /**
     * @ORM\OneToMany(targetEntity="Referencia", mappedBy="tipo")
     */
    private $referencias;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_modifico_id", referencedColumnName="id", nullable=true)
     */
    private $usuarioModificoBD;
        
   /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_modifico_bd", type="datetime", nullable=true)
     */
    private $fechaModificoBD;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Cenfotec\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuario_registro_id", referencedColumnName="id")
     */
    private $usuarioRegistroBD;

    /**
     * Fecha de registro en base de datos
     * @ORM\Column(name="fecha_registro_bd", type="datetime")
     */
    private $fechaRegistroBD;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaRegistroBD = new \DateTime();
        $this->referencias     = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoReferencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add referencias
     *
     * @param \Cenfotec\BDBundle\Entity\Referencia $referencias
     * @return TipoReferencia
     */
    public function addReferencia(\Cenfotec\BDBundle\Entity\Referencia $referencias)
    {
        $this->referencias[] = $referencias;
    
        return $this;
    }

    /**
     * Remove referencias
     *
     * @param \Cenfotec\BDBundle\Entity\Referencia $referencias
     */
    public function removeReferencia(\Cenfotec\BDBundle\Entity\Referencia $referencias)
    {
        $this->referencias->removeElement($referencias);
    }

    /**
     * Get referencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }
    
    /**
     * Set fechaModificoBD
     *
     * @param \DateTime $fechaModificoBD
     * @return ContactoCRM
     */
    public function setFechaModificoBD($fechaModificoBD)
    {
        $this->fechaModificoBD = $fechaModificoBD;
        return $this;
    }

    /**
     * Get fechaModificoBD
     *
     * @return \DateTime 
     */
    public function getFechaModificoBD()
    {
        return $this->fechaModificoBD;
    }
    
    /**
     * Get fechaRegistroBD
     *
     * @return \DateTime 
     */
    public function getFechaRegistroBD()
    {
        return $this->fechaRegistroBD;
    }
    
    /**
     * Set usuarioRegistroBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD
     * @return ContactoCRM
     */
    public function setUsuarioRegistroBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioRegistroBD = null)
    {
        $this->usuarioRegistroBD = $usuarioRegistroBD;
        return $this;
    }

    /**
     * Get usuarioRegistroBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioRegistroBD()
    {
        return $this->usuarioRegistroBD;
    }    

    /**
     * Set usuarioModificoBD
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD
     * @return ContactoCRM
     */
    public function setUsuarioModificoBD(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarioModificoBD = null)
    {
        $this->usuarioModificoBD = $usuarioModificoBD;
        return $this;
    }

    /**
     * Get usuarioModificoBD
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuarioModificoBD()
    {
        return $this->usuarioModificoBD;
    }  
}