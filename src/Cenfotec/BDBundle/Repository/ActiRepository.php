<?php

namespace Cenfotec\BDBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ActiRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ActiRepository extends EntityRepository
{
    public function searchAll($pflagComplexTable = "")
    {
        $conn  = $this->getEntityManager()->getConnection();
        $query = " SELECT va.*
                   FROM view_acti va
                   WHERE va.flag_eliminado = 0
                   ORDER BY va.fecha_modifico_bd desc ";
        if (empty($pflagComplexTable)) {
            $query .= " LIMIT 500 ";
        }
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function searchAllEliminados()
    {
        $conn  = $this->getEntityManager()->getConnection();
        $query = " SELECT v.*
                   FROM view_acti v
                   WHERE v.flag_eliminado = 1";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    /**
     * Funcion que se encarga de obtener los datos de acti segun los criterios de busquedas
     * @param string $pnombre
     * @param string $papellido1
     * @param string $papellido2
     * @param string $pcorreo
     * @param string $pnumero
     * @param string $pinteres
     * @param string $pflagCount
     * @return array
     */
    public function searchDataXFiltro(
            $pnombre = "", $papellido1 = "", $papellido2 = "", $pcorreo = "",
            $pnumero = "", $pfechaInicioContacto = "", $pfechaFinContacto = "", 
            $pfechaInicioInteres = "", $pfechaFinInteres = "", $pinteres = "", 
            $pflagCount = false)
    {
        $conn  = $this->getEntityManager()->getConnection();
        $where = "";
        $join  = "";
        $filterFechaInteres = "";
        
        if(!empty($pnombre)){
            $where .= "(v.nombre like '%$pnombre%') AND ";
        }
        if(!empty($papellido1)){
            $where .= "(v.apellido1 like '%$papellido1%') AND ";
        }
        if(!empty($papellido2)){
            $where .= "(v.apellido2 like '%$papellido2%') AND ";
        }
        if(!empty($pcorreo)){
            $where .= "(v.correos like '%$pcorreo%') AND ";
        }
        if(!empty($pnumero)){
            $where .= "(v.numeros like '%$pnumero%') AND ";
        }
        if(!empty($pfechaInicioContacto) && !empty($pfechaFinContacto)){
            $where .= "(v.fecha_contacto between '$pfechaInicioContacto' AND '$pfechaFinContacto') AND ";
        }
        if(!empty($pfechaInicioInteres) && !empty($pfechaFinInteres)){
            $filterFechaInteres .= " AND (cxi.fecha_interes between '$pfechaInicioInteres' AND '$pfechaFinInteres') ";
        }
        if(!empty($pinteres)){
            if(is_array($pinteres)){
                $pinteres = implode(",", $pinteres);
            }
            $join  .= "LEFT JOIN t_contacto_crm_x_interes cxi ON cxi.contacto_crm_id = v.contacto_crm_id $filterFechaInteres
                       LEFT JOIN t_interes i ON i.id = cxi.interes_id ";
            $where .= "i.id IN ($pinteres) AND ";
        }
        
        //Buscar solo contactos que no esten eliminado
        $where .= " (v.flag_eliminado = 0) ";
        
        //Eliminamos el ultimo " AND " que se concateno mal.
        //$where  = substr($where,0,-4);
        
        $select = ($pflagCount) ? "count(v.acti_id) as cant" : "v.*";
        $query  = " SELECT 
                       $select
                    FROM
                       view_acti v " .
                   ($join != "" ? $join : "").
                   ($where != "" ? "WHERE ". $where : "");
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function searchDataXCriterio($pcriterio = "", $pflagCount = false)
    {
        $conn  = $this->getEntityManager()->getConnection();
        $where = "";
        if(!empty($pcriterio)){
            if (strpos($pcriterio,"*") !== false) {
                $id = str_replace("*", "", $pcriterio);
                $where .= "(v.contacto_crm_id = $id ) ";
            }else{
                $where .= "(v.nombre_completo like '%$pcriterio%' OR ";
                $where .= " v.correos like '%$pcriterio%' OR ";
                $where .= " v.identificacion like '%$pcriterio%' OR ";
                $where .= " v.numeros like '%$pcriterio%') ";
            }
            //Buscar solo contactos que no esten eliminado
            $where .= "AND (v.flag_eliminado = 0) ";
        }
        $select = ($pflagCount) ? "count(v.acti_id) as cant" : "v.*";
        $query  = "  SELECT 
                        $select
                     FROM
                        view_acti v " .
                    ($where != "" ? "WHERE ". $where : "");
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}
