<?php

namespace Cenfotec\CRMBundle\Clases;

/**
 * Description of ChartHelper
 *
 * @author Carlos Dominguez Lara
 */
class ChartHelper {
    private $doctrine;
    
    public function __construct($doctrine){
        $this->doctrine = $doctrine;
    }
    
    /**
     * Obtiene los datos para el grafico.
     * @return array Json Con el formato correcto para crear el Grafico
     */
    public function getDataChart($pquery, $pqueryParameters, $options){ 
        $connection = $this->doctrine->getConnection();
        $query      = $pquery;
                      
        $statement = $connection->prepare($query);        
        $statement->execute($pqueryParameters);
        
        $colors     = array(
            '#FF6300', '#FF00D0', '#004D06', '#000FFF', 
            '#481D57', '#008CFE', '#FF82A2', '#FF0A00', '#220077'
        );
        //Make randoms positions for colors in array.
        shuffle($colors);
        $categories = array();
        
        if (isset($options["isMultiDataSerie"]) && $options["isMultiDataSerie"]) {
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $dataSeries = array();
            $indexColor = 0;
            
            $oldKey = "";
            foreach ($result as $row) {
                $currentKey = $row[$options['dataSerieGroup']];
                if ($currentKey != $oldKey) {
                    if ($oldKey != "") {
                        $dataSeries[] = $dataSeriesGroup;
                    }
                    $dataSeriesGroup = array(
                        'name' => $currentKey,
                        'data' => $options["dataGroupDefaultValues"],
                        'color' => $colors[$indexColor]
                    );
                    $indexColor++;
                }
                $dataSeriesGroup['data'][($row[$options["dataKey"]]-1)] = intval($row[$options["dataValue"]]);
                
                $oldKey = $currentKey;
            }
            if (count($result) == 0) {
                $dataSeriesGroup = array(
                    'name' => 'No se encontraron datos'
                );
            }
            $dataSeries[] = $dataSeriesGroup;
            $categories   = $options["categories"];
        }else{
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $dataSeries = array();
            
            $dataSeriesLine = array(
                'name' => $options["dataSerieName"],
                'data' => array(),
                'dataLabels' => array(
                    'enabled'   => true,
                    'rotation'  => -90,
                    'color'     => 'black',
                    'align'     => 'right',
                    'x'         => 4,
                    'y'         => -30,
                    'style'     => array(
                        'fontSize'   => '13px',
                        'fontFamily' => 'Verdana, sans-serif',
                        'textShadow' => '1px 1px 3px #cdd0d7'
                    )
                )
            );
            
            foreach ($result as $categoryData) {
                $categories[] = $categoryData[$options["dataKey"]];
                $dataSeriesLine["data"][] = intval($categoryData[$options["dataValue"]]);
            }
            
            $dataSeries[] = $dataSeriesLine;
        }
        
        
        return array(
            'dataSeries' => $dataSeries,
            'categories' => $categories
        ); 
    }
}
