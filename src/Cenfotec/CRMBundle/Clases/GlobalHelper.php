<?php

namespace Cenfotec\CRMBundle\Clases;

use Cenfotec\BDBundle\Entity\Correo;
use Cenfotec\BDBundle\Entity\Numero;
use Cenfotec\BDBundle\Entity\ContactoCRMXInteres;
use Cenfotec\BDBundle\Entity\ContactoCRMXNumero;

/**
 * Clase auxiliar para manejar variables globales.
 * @author Carlos Dominguez <krlosnando@gmail.com>
 */
class GlobalHelper {
    
    const TIPO_EVENTO_NUEVO_REGISTRO = 1;
    const TIPO_EVENTO_MODIFICAR = 2;
    const TIPO_EVENTO_ELIMINAR = 3;
    
    static public function getClienteCRM($pkey) {
        $cliente = array(
            'keyTipoContacto' => $pkey
        );
            
        switch ($pkey) {
            case 1: 
            case 'PA': 
                $cliente['titulo']      = 'Prog. Académico';
                $cliente['descripcion'] = 'Programa Académico';
                $cliente['adminPath']   = 'cenfo_prog_academico_listar';
                $cliente['gridLayout']  = 'cenfo_prog_academico_grid_layout';
                $cliente['gridData']    = 'cenfo_prog_academico_grid_data';
                break;   
            case 2: 
            case 'ACTI': 
                $cliente['titulo']      = 'ACTI';
                $cliente['descripcion'] = 'Actualización Profesional en Tecnología';
                $cliente['adminPath']   = 'cenfo_acti_listar';
                $cliente['gridLayout']  = 'cenfo_acti_grid_layout';
                $cliente['gridData']    = 'cenfo_acti_grid_data';
                break;
            case 3: 
            case 'POS': 
                $cliente['titulo']      = 'Posgrado';
                $cliente['descripcion'] = 'Especializaciones Profesionales';
                $cliente['adminPath']   = 'cenfo_posgrado_listar';
                $cliente['gridLayout']  = 'cenfo_posgrado_grid_layout';
                $cliente['gridData']    = 'cenfo_posgrado_grid_data';
                break; 
            case 4: 
            case 'EM': 
                $cliente['titulo']      = 'Empresa';
                $cliente['descripcion'] = 'Empresa';
                $cliente['adminPath']   = 'cenfo_empresa_listar';
                $cliente['gridLayout']  = 'cenfo_empresa_grid_layout';
                $cliente['gridData']    = 'cenfo_empresa_grid_data';
                break;
            case 5: 
            case 'CO': 
                $cliente['titulo']      = 'Colegio';
                $cliente['descripcion'] = 'Colegio';
                $cliente['adminPath']   = 'cenfo_colegio_listar';
                $cliente['gridLayout']  = 'cenfo_colegio_grid_layout';
                $cliente['gridData']    = 'cenfo_colegio_grid_data';
                break;
        }
        return $cliente;
    }

    static public function registrarBitacora(\Doctrine\ORM\EntityManager $pem, $pdatosBitacora)
    {
        $objBitacora = new \Cenfotec\SeguridadBundle\Entity\Bitacora();
        $objTipoEvento = $pem->getRepository('CenfotecSeguridadBundle:TipoEvento')->find($pdatosBitacora['idTipoEvento']);
        $objBitacora->setTipoEvento($objTipoEvento);
        $objBitacora->setFechaEvento(new \DateTime());
        $objBitacora->setDescripcion($pdatosBitacora['descripcion']);
        $objBitacora->setUsuario($pdatosBitacora['objUsuario']);
        $pem->persist($objBitacora);
        $pem->flush();
    }

    static public function registrarExcepcion($controller, $pdatosExcepcion)
    {
        // Re open Entity Manager Conection
        $container = $controller->getContainer();
        $container->set('doctrine.orm.entity_manager', null);
        $container->set('doctrine.orm.default_entity_manager', null);
        $em = $controller->getDoctrine()->getEntityManager();

        $objExcepcion = new \Cenfotec\BDBundle\Entity\Excepcion();
        $objExcepcion->setExcepcion($pdatosExcepcion['excepcion']);
        $objExcepcion->setMetodo($pdatosExcepcion['metodo']);
        $objExcepcion->setUsuarioId($controller->getUser()->getId());
        $em->persist($objExcepcion);
        $em->flush();
    }
    
    static public function bindDataContactoCRM(&$objContactoCRM, $data, &$em, $action, $flagCombinar = false)
    {
        $updateFields = "";
                   
        //Validar indices que pueden no existir en la variable $data
        $data['canton'] = (!empty($data['canton'])) ? $data['canton'] : "";
            
        //$action = Registrar / Modificar
        if ($action == 'Registrar') {
            $objContactoCRM->setUsuarioRegistroBD($data["usuario"]);
        }
        if ($action == 'Modificar') {
            $objContactoCRM->setFechaModificoBD(new \DateTime());
            $objContactoCRM->setUsuarioModificoBD($data["usuario"]);
        }
        
        $objContactoCRM->setNombreCompleto(trim($data['nombre'] . " " . $data['apellido1'] . " " . $data['apellido2']));
        
        if (!empty($data['genero']) && $data['genero'] != $objContactoCRM->getGenero()) {
            $updateFields .= "Género: '" . $objContactoCRM->getGenero() . "' => '" . $data['genero'] . "', ";
            $objContactoCRM->setGenero($data['genero']);
        }
        if (!empty($data['nombre']) && $data['nombre'] != $objContactoCRM->getNombre()) {
            $updateFields .= "Nombre: '" . $objContactoCRM->getNombre() . "' => '" . $data['nombre'] . "', ";
            $objContactoCRM->setNombre($data['nombre']);
        }
        if (!empty($data['apellido1']) && $data['apellido1'] != $objContactoCRM->getApellido1()) {
            $updateFields .= "Primer apellido: '" . $objContactoCRM->getApellido1() . "' => '" . $data['apellido1'] . "', ";
            $objContactoCRM->setApellido1($data['apellido1']);
        }
        if (!empty($data['apellido2']) && $data['apellido2'] != $objContactoCRM->getApellido2()) {
            $updateFields .= "Segundo apellido: '" . $objContactoCRM->getApellido2() . "' => '" . $data['apellido2'] . "', ";
            $objContactoCRM->setApellido2($data['apellido2']);
        }
        if (!empty($data['direccion']) && $data['direccion'] != $objContactoCRM->getDireccion()) {
            $updateFields .= "Dirección: '" . $objContactoCRM->getDireccion() . "' => '" . $data['direccion'] . "', ";
            $objContactoCRM->setDireccion($data['direccion']);
        }
        if (!empty($data['identificacion']) && $data['identificacion'] != $objContactoCRM->getIdentificacion()) {
            $updateFields .= "Identificación: '" . $objContactoCRM->getIdentificacion() . "' => '" . $data['identificacion'] . "', ";
            $objContactoCRM->setIdentificacion($data['identificacion']);
        }
        if (!empty($data['comentario']) && $data['comentario'] != $objContactoCRM->getComentario()) {
            $updateFields .= "Comentarios: '" . $objContactoCRM->getComentario() . "' => '" . $data['comentario'] . "', ";
            $objContactoCRM->setComentario($data['comentario']);
        }
        
        //[Fecha contactó]
        //----------------------------------------------------------------------
        if (!$flagCombinar) {
            if ($objContactoCRM->getFechaContacto() != null && $data['fechaContacto'] != $objContactoCRM->getFechaContacto()->format('Y-m-d')) {
                $updateFields .= "Fecha contactó: '" . $objContactoCRM->getFechaContacto()->format('Y-m-d') . "' => '" . $data['fechaContacto'] . "', ";
            }else if($objContactoCRM->getFechaContacto() == null && $action == 'Modificar'){
                $updateFields .= "Fecha contactó: '" . $data['fechaContacto'] . "', ";
            }
            $objContactoCRM->setFechaContacto(new \DateTime($data['fechaContacto']));
        }
        
        //[País de residencia]
        //----------------------------------------------------------------------
        if(!empty($data['paisResidencia'])){
            $objPaisResidencia = $em->getRepository('CenfotecBDBundle:Pais')->find($data['paisResidencia']);
            
            if ($objContactoCRM->getPaisResidencia() != null && $objPaisResidencia->getId() != $objContactoCRM->getPaisResidencia()->getId()) {
                $updateFields .= "País de residencia: '" . $objContactoCRM->getPaisResidencia()->getNombre() . "' => '" . $objPaisResidencia->getNombre() . "', ";
            }else if($objContactoCRM->getPaisResidencia() == null && $action == 'Modificar'){
                $updateFields .= "País de residencia: '" . $objPaisResidencia->getNombre() . "', ";
            }
            $objContactoCRM->setPaisResidencia($objPaisResidencia);
            
            //Si no es Costa Rica remover el canton.
            if($objPaisResidencia->getNombre() != 'Costa Rica'){
                $data['canton'] = "";
                
                if ($objContactoCRM->getCanton() != null) {
                    $updateFields .= "Cantón: '" . $objContactoCRM->getCanton()->getNombre() . "' => '', ";
                    $objContactoCRM->setCanton(null);
                }
            }
        }else{
            if ($objContactoCRM->getPaisResidencia() != null) {
                $updateFields .= "País de residencia: '" . $objContactoCRM->getPaisResidencia()->getNombre() . "' => '', ";
                $objContactoCRM->setPaisResidencia(null);
            }
        }
        
        //[Canton]
        //----------------------------------------------------------------------
        if(!empty($data['canton'])){
            $objCanton = $em->getRepository('CenfotecBDBundle:Canton')->find($data['canton']);
            if ($objContactoCRM->getCanton() != null && $objCanton->getId() != $objContactoCRM->getCanton()->getId()) {
                $updateFields .= "Cantón: '" . $objContactoCRM->getCanton()->getNombre() . "' => '" . $objCanton->getNombre() . "', ";
            }else if($objContactoCRM->getCanton() == null && $action == 'Modificar'){
                $updateFields .= "Cantón: '" . $objCanton->getNombre() . "', ";
            }
            $objContactoCRM->setCanton($objCanton);
        }else{
            if ($objContactoCRM->getCanton() != null) {
                $updateFields .= "Cantón: '" . $objContactoCRM->getCanton()->getNombre() . "' => '', ";
                $objContactoCRM->setCanton(null);
            }
        }
        
        //[Tipo de identificación]
        //----------------------------------------------------------------------
        if(!empty($data['tipoIdentificacion'])){
            $objTipoIdentificacion = $em->getRepository('CenfotecBDBundle:TipoIdentificacion')->find($data['tipoIdentificacion']);
            if ($objContactoCRM->getTipoIdentificacion() != null && $objTipoIdentificacion->getId() != $objContactoCRM->getTipoIdentificacion()->getId()) {
                $updateFields .= "Tipo de identificación: '" . $objContactoCRM->getTipoIdentificacion()->getNombre() . "' => '" . $objTipoIdentificacion->getNombre() . "', ";
            }else if($objContactoCRM->getTipoIdentificacion() == null && $action == 'Modificar'){
                $updateFields .= "Tipo de identificación: '" . $objTipoIdentificacion->getNombre() . "', ";
            }
            $objContactoCRM->setTipoIdentificacion($objTipoIdentificacion);    
        }else{
            if ($objContactoCRM->getTipoIdentificacion() != null) {
                $updateFields .= "Tipo de identificación: '" . $objContactoCRM->getTipoIdentificacion()->getNombre() . "' => '', ";
                $objContactoCRM->setTipoIdentificacion(null);    
            }
        }
        
        //[Convenio]
        //----------------------------------------------------------------------
        if(!empty($data['convenio'])){
            $objConvenio = $em->getRepository('CenfotecBDBundle:Convenio')->find($data['convenio']);
            if ($objContactoCRM->getConvenio() != null && $objConvenio->getId() != $objContactoCRM->getConvenio()->getId()) {
                $updateFields .= "Convenio: '" . $objContactoCRM->getConvenio()->getNombre() . "' => '" . $objConvenio->getNombre() . "', ";
            }else if($objContactoCRM->getConvenio() == null && $action == 'Modificar'){
                $updateFields .= "Convenio: '" . $objConvenio->getNombre() . "', ";
            }
            $objContactoCRM->setConvenio($objConvenio);
        }else{
            if ($objContactoCRM->getConvenio() != null) {
                $updateFields .= "Convenio: '" . $objContactoCRM->getConvenio()->getNombre() . "' => '', ";
                $objContactoCRM->setConvenio(null);
            }
        }
        
        //[Empresa]
        //----------------------------------------------------------------------
        if(!empty($data['empresa'])){
            $objEmpresa = $em->getRepository('CenfotecBDBundle:Empresa')->find($data['empresa']);
            if ($objContactoCRM->getEmpresa() != null && $objEmpresa->getId() != $objContactoCRM->getEmpresa()->getId()) {
                $updateFields .= "Empresa: '" . $objContactoCRM->getEmpresa()->getNombre() . "' => '" . $objEmpresa->getNombre() . "', ";
            }else if($objContactoCRM->getEmpresa() == null && $action == 'Modificar'){
                $updateFields .= "Empresa: '" . $objEmpresa->getNombre() . "', ";
            }
            $objContactoCRM->setEmpresa($objEmpresa);
        }else{
            if ($objContactoCRM->getEmpresa() != null) {
                $updateFields .= "Empresa: '" . $objContactoCRM->getEmpresa()->getNombre() . "' => '', ";
                $objContactoCRM->setEmpresa(null);
            }
        }
        
        //[Profesión]
        //----------------------------------------------------------------------
        if(!empty($data['profesion'])){
            $objProfesion = $em->getRepository('CenfotecBDBundle:Profesion')->find($data['profesion']);
            if ($objContactoCRM->getProfesion() != null && $objProfesion->getId() != $objContactoCRM->getProfesion()->getId()) {
                $updateFields .= "Profesión: '" . $objContactoCRM->getProfesion()->getNombre() . "' => '" . $objProfesion->getNombre() . "', ";
            }else if($objContactoCRM->getProfesion() == null && $action == 'Modificar'){
                $updateFields .= "Profesión: '" . $objProfesion->getNombre() . "', ";
            }
            $objContactoCRM->setProfesion($objProfesion);
        }else{
            if ($objContactoCRM->getProfesion() != null) {
                $updateFields .= "Profesión: '" . $objContactoCRM->getProfesion()->getNombre() . "' => '', ";
                $objContactoCRM->setProfesion(null);
            }
        }
        
        //[Puesto]
        //----------------------------------------------------------------------
        if(!empty($data['puesto'])){
            $objPuesto = $em->getRepository('CenfotecBDBundle:Puesto')->find($data['puesto']);
            if ($objContactoCRM->getPuesto() != null && $objPuesto->getId() != $objContactoCRM->getPuesto()->getId()) {
                $updateFields .= "Puesto: '" . $objContactoCRM->getPuesto()->getNombre() . "' => '" . $objPuesto->getNombre() . "', ";
            }else if($objContactoCRM->getPuesto() == null && $action == 'Modificar'){
                $updateFields .= "Puesto: '" . $objPuesto->getNombre() . "', ";
            }
            $objContactoCRM->setPuesto($objPuesto);
        }else{
            if ($objContactoCRM->getPuesto() != null) {
                $updateFields .= "Puesto: '" . $objContactoCRM->getPuesto()->getNombre() . "' => '', ";
                $objContactoCRM->setPuesto(null);
            }
        }
        
        //[Nivel de interés]
        //----------------------------------------------------------------------
        if(!empty($data['nivelInteres'])){
            $objNivelInteres = $em->getRepository('CenfotecBDBundle:NivelInteres')->find($data['nivelInteres']);
            if ($objContactoCRM->getNivelInteres() != null && $objNivelInteres->getId() != $objContactoCRM->getNivelInteres()->getId()) {
                $updateFields .= "Nivel de interés: '" . $objContactoCRM->getNivelInteres()->getNombre() . "' => '" . $objNivelInteres->getNombre() . "', ";
            }else if($objContactoCRM->getNivelInteres() == null && $action == 'Modificar'){
                $updateFields .= "Nivel de interés: '" . $objNivelInteres->getNombre() . "', ";
            }
            $objContactoCRM->setNivelInteres($objNivelInteres);
        }else{
            if ($objContactoCRM->getNivelInteres() != null) {
                $updateFields .= "Nivel de interés: '" . $objContactoCRM->getNivelInteres()->getNombre() . "' => '', ";
                $objContactoCRM->setNivelInteres(null);
            }
        }
        
        //[Horario]
        //----------------------------------------------------------------------
        if(!empty($data['horario'])){
            $objHorario = $em->getRepository('CenfotecBDBundle:Horario')->find($data['horario']);
            if ($objContactoCRM->getHorarioPreferencia() != null && $objHorario->getId() != $objContactoCRM->getHorarioPreferencia()->getId()) {
                $updateFields .= "Horario: '" . $objContactoCRM->getHorarioPreferencia()->getNombre() . "' => '" . $objHorario->getNombre() . "', ";
            }else if($objContactoCRM->getHorarioPreferencia() == null && $action == 'Modificar'){
                $updateFields .= "Horario: '" . $objHorario->getNombre() . "', ";
            }
            $objContactoCRM->setHorarioPreferencia($objHorario);
        }else{
            if ($objContactoCRM->getHorarioPreferencia() != null) {
                $updateFields .= "Horario: '" . $objContactoCRM->getHorarioPreferencia()->getNombre() . "' => '', ";
                $objContactoCRM->setHorarioPreferencia(null);
            }
        }
        
        //[Referencia]
        //----------------------------------------------------------------------
        if (!$flagCombinar) {
            if(!empty($data['referencia'])){
                $objReferencia = $em->getRepository('CenfotecBDBundle:Referencia')->find($data['referencia']);
                if ($objContactoCRM->getReferencia() != null && $objReferencia->getId() != $objContactoCRM->getReferencia()->getId()) {
                    $updateFields .= "Referencia: '" . $objContactoCRM->getReferencia()->getNombre() . "' => '" . $objReferencia->getNombre() . "', ";
                }else if($objContactoCRM->getReferencia() == null && $action == 'Modificar'){
                    $updateFields .= "Referencia: '" . $objReferencia->getNombre() . "', ";
                }
                $objContactoCRM->setReferencia($objReferencia);
            }else{
                if ($objContactoCRM->getReferencia() != null) {
                    $updateFields .= "Referencia: '" . $objContactoCRM->getReferencia()->getNombre() . "' => '', ";
                    $objContactoCRM->setReferencia(null);
                }
            }
        }
        
        //[Primer contacto]
        //----------------------------------------------------------------------
        if (!$flagCombinar) {
            if(!empty($data['primerContacto'])){
                $objPrimerContacto = $em->getRepository('CenfotecBDBundle:PrimerContacto')->find($data['primerContacto']);
                if ($objContactoCRM->getPrimerContacto() != null && $objPrimerContacto->getId() != $objContactoCRM->getPrimerContacto()->getId()) {
                    $updateFields .= "Primer contacto: '" . $objContactoCRM->getPrimerContacto()->getNombre() . "' => '" . $objPrimerContacto->getNombre() . "', ";
                }else if($objContactoCRM->getPrimerContacto() == null && $action == 'Modificar'){
                    $updateFields .= "Primer contacto: '" . $objPrimerContacto->getNombre() . "', ";
                }
                $objContactoCRM->setPrimerContacto($objPrimerContacto);
            }else{
                if ($objContactoCRM->getPrimerContacto() != null) {
                    $updateFields .= "Primer contacto: '" . $objContactoCRM->getPrimerContacto()->getNombre() . "' => '', ";
                    $objContactoCRM->setPrimerContacto(null);
                }
            }
        }
        
        //[Correos]
        //----------------------------------------------------------------------
        if (isset($data['correos'])) {
            GlobalHelper::actualizarCorreos($objContactoCRM, $data['correos'], $em);
        }
        
        //[Numeros]
        //----------------------------------------------------------------------
        if (isset($data['numeros'])) {
            GlobalHelper::actualizarCRMXNumeros($objContactoCRM, $data['numeros'], $em);
        }
        
        //[Intereses]
        //----------------------------------------------------------------------
        if (isset($data['intereses'])) {
            $updateIntereses = GlobalHelper::actualizarCRMXIntereses($objContactoCRM, $data['intereses'], $em, $data["tipoContacto"]);
            if ($action == 'Modificar') {
                $updateFields .= $updateIntereses;
            }
        }
    
        if (!empty($updateFields)) {
            //Remover el ultimo caracter
            $updateFields  = substr($updateFields,0,-2);
        }
        return $updateFields;
    }
    
    static public function actualizarContactosBasicos(&$objEntity, $dataContactos, &$em)
    {
        $repoContactoBasico = $em->getRepository('CenfotecBDBundle:ContactoBasico');
        $repoPuesto          = $em->getRepository('CenfotecBDBundle:Puesto');
        
        if(!empty($dataContactos['registrar']) && count($dataContactos['registrar'])>0){
            foreach ($dataContactos['registrar'] as $dataContacto) {
                $objContactoBasico = new \Cenfotec\BDBundle\Entity\ContactoBasico();
                $objContactoBasico->setNombre($dataContacto["nombre"]);
                $objContactoBasico->setApellido1($dataContacto["apellido1"]);
                $objContactoBasico->setApellido2($dataContacto["apellido2"]);
                $objContactoBasico->setNombreCompleto(trim($dataContacto['nombre'] . " " . $dataContacto['apellido1'] . " " . $dataContacto['apellido2']));
                
                if(!empty($dataContacto["idPuesto"])){
                    $objPuesto = $repoPuesto->find($dataContacto["idPuesto"]);
                    $objContactoBasico->setPuesto($objPuesto);
                }
                
                if(isset($dataContacto["correos"])) {
                    GlobalHelper::actualizarCorreos($objContactoBasico, $dataContacto["correos"], $em);
                }
                if(isset($dataContacto["numeros"])) {
                    GlobalHelper::actualizarNumeros($objContactoBasico, $dataContacto["numeros"], $em);
                }
                
                $objEntity->addContacto($objContactoBasico);
            }
        }
        if(!empty($dataContactos['actualizar']) && count($dataContactos['actualizar'])>0){
            foreach ($dataContactos['actualizar'] as $idContacto => $dataContacto) {
                $objContactoBasico = $repoContactoBasico->find($idContacto);
                $objContactoBasico->setNombre($dataContacto["nombre"]);
                $objContactoBasico->setApellido1($dataContacto["apellido1"]);
                $objContactoBasico->setApellido2($dataContacto["apellido2"]);
                $objContactoBasico->setNombreCompleto(trim($dataContacto['nombre'] . " " . $dataContacto['apellido1'] . " " . $dataContacto['apellido2']));
                
                if(!empty($dataContacto["idPuesto"])){
                    $objPuesto = $repoPuesto->find($dataContacto["idPuesto"]);
                    $objContactoBasico->setPuesto($objPuesto);
                }else{
                    $objContactoBasico->setPuesto(null);
                }

                if(isset($dataContacto["correos"])) {
                    GlobalHelper::actualizarCorreos($objContactoBasico, $dataContacto["correos"], $em);
                }
                if(isset($dataContacto["numeros"])) {
                    GlobalHelper::actualizarNumeros($objContactoBasico, $dataContacto["numeros"], $em);
                }
            }
        }
        if(!empty($dataContactos['eliminar'])){
            $contactoBasicosEliminar = explode(",", $dataContactos['eliminar']);
            foreach ($contactoBasicosEliminar as $idContactoBasicoEliminar) {
                if (!empty($idContactoBasicoEliminar)) {
                    $objContactoBasicoEliminar = $repoContactoBasico->find($idContactoBasicoEliminar);
                    $objEntity->removeContacto($objContactoBasicoEliminar);
                }
            }
        }   
    }
    
    static public function actualizarCorreos(&$objEntity, $dataCorreo, &$em)
    {
        $repoCorreo = $em->getRepository('CenfotecBDBundle:Correo');
        
        if(!empty($dataCorreo['registrar']) && count($dataCorreo['registrar'])>0){
            foreach ($dataCorreo['registrar'] as $correo) {
                $objCorreo = $repoCorreo->findOneBy(array('correo' => $correo));

                //Si el correo no existe en la BD agregarlo
                if($objCorreo == null){
                    $objCorreo = new Correo();
                    $objCorreo->setCorreo($correo);
                    $em->persist($objCorreo);
                }

                //Relacionamos al contacto CRM el correo que obtuvimos anteriormente solo si no tiene la relacion.
                //Ya que la puede tener cuando se va por aqui por culpa de la parte de combinar informacion.
                if(!$objEntity->getCorreos()->contains($objCorreo)){
                    $objEntity->addCorreo($objCorreo);
                }
            }
        }
        if(!empty($dataCorreo['actualizar']) && count($dataCorreo['actualizar'])>0){
            foreach ($dataCorreo['actualizar'] as $idCorreo => $correo) {
                $objCorreo      = $repoCorreo->find($idCorreo);
                $cantRelaciones = $repoCorreo->cantRelaciones($objCorreo);
                $objExistCorreo = $repoCorreo->findOneBy(array('correo' => $correo));
                
                //Si el correo no existe en la BD modificarlos
                if($objExistCorreo == null){
                    $objCorreo->setCorreo($correo);
                }else{
                    //Si el correo que se va actualizar es el mismo no hacer nada.
                    if($idCorreo != $objExistCorreo->getId()){

                        //Solo que el correo no este relacionado con otro contacto se actualiza, sino se registra uno nuevo.
                        if($cantRelaciones == 1){
                            //Eliminamos la relacion con el correo que no vamos actualizar, ya que otro contacto lo tiene.
                            $objEntity->removeCorreo($objCorreo);

                            //Relacionamos al contacto CRM con el correo que existe actualmente.
                            $objEntity->addCorreo($objExistCorreo);
                        }else{
                            //Eliminamos la relacion con el correo que no vamos actualizar, ya que otro contacto lo tiene.
                            $objEntity->removeCorreo($objCorreo);

                            $objNewCorreo = new Correo();
                            $objNewCorreo->setCorreo($correo);
                            $em->persist($objNewCorreo);

                            $objEntity->addCorreo($objNewCorreo);
                        }                    
                    }
                }
            }
        }
        if(!empty($dataCorreo['eliminar'])){
            $correosEliminar = explode(",", $dataCorreo['eliminar']);
            foreach ($correosEliminar as $idCorreoEliminar) {
                if ($idCorreoEliminar) {
                    $objCorreoEliminar = $repoCorreo->find($idCorreoEliminar);
                    $objEntity->removeCorreo($objCorreoEliminar);
                }
            }
        }
    }
    
    static public function actualizarNumeros(&$objEntity, $dataNumeros, &$em)
    {
        $repoNumero     = $em->getRepository('CenfotecBDBundle:Numero');
        $repoTipoNumero = $em->getRepository('CenfotecBDBundle:TipoNumero');
        
        if(!empty($dataNumeros['registrar']) && count($dataNumeros['registrar'])>0){
            foreach ($dataNumeros['registrar'] as $dataNumero) {
                $ext = (!empty($dataNumero['ext'])) ? $dataNumero['ext'] : "";

                if(!empty($ext)){ 
                    $objNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero'], 'ext' => $ext));
                }else{ 
                    $objNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero']));
                }

                //Si no existe creamos el Objeto Numero
                if($objNumero == null){
                    $objTipo = $repoTipoNumero->find($dataNumero['tipoNumero']);

                    $objNumero = new Numero();
                    $objNumero->setNumero($dataNumero['numero']);
                    $objNumero->setExt($ext);
                    $objNumero->setTipo($objTipo);
                    $em->persist($objNumero);
                }
                $objEntity->addNumero($objNumero);
            }
        }
        if(!empty($dataNumeros['actualizar']) && count($dataNumeros['actualizar'])>0){
            foreach ($dataNumeros['actualizar'] as $idNumero => $dataNumero) {
                $objNumero      = $repoNumero->find($idNumero);
                $cantRelaciones = $repoNumero->cantRelaciones($objNumero); //TODO: Analizar bien lo de cantRelacioines
                $ext            = (!empty($dataNumero['ext'])) ? $dataNumero['ext'] : "";
                $objTipo        = $repoTipoNumero->find($dataNumero['tipoNumero']);

                //Verificar si el nuevo numero no existe actualmente en base de datos
                $objExistNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero'], 'ext' => $ext));
                if ($objExistNumero == null) {
                    //Actualizamos el numero
                    $objNumero->setNumero($dataNumero['numero']);
                    $objNumero->setExt($ext);
                    $objNumero->setTipo($objTipo);
                }else{
                    //Solo que el numero viejo no este relacionado con otro contacto se actualiza, sino se registra uno nuevo.
                    if($cantRelaciones == 1){
                        //Si la relacion que se va eliminar es la misma que se va agregar no hacer nada.
                        if($idNumero != $objExistNumero->getId()){
                            //Eliminamos la relacion con el numero que no vamos actualizar, ya que otro contacto lo tiene.
                            $objEntity->removeNumero($objNumero);
                            //Relacionamos al contacto con el numero que existe actualmente.
                            $objEntity->addNumero($objExistNumero);
                        }
                    }else{
                        //Eliminamos la relacion con el numero que no vamos actualizar, ya que otro contacto lo tiene.
                        $objEntity->removeNumero($objNumero);

                        //Relacionamos al contacto CRM con el numero que existe actualmente.
                        $objEntity->addNumero($objExistNumero);
                    }
                }
            }
        }
        if(!empty($dataNumeros['eliminar'])){
            $numerosEliminar = explode(",", $dataNumeros['eliminar']);
            foreach ($numerosEliminar as $idNumeroEliminar) {
                if (!empty($idNumeroEliminar)) {
                    $objNumeroEliminar = $repoNumero->find($idNumeroEliminar);
                    $objEntity->removeNumero($objNumeroEliminar);
                }
            }
        }
    }
    
    static public function actualizarCRMXNumeros(&$objEntity, $dataNumeros, &$em)
    {
        $repoNumero     = $em->getRepository('CenfotecBDBundle:Numero');
        $repoCRMXNumero = $em->getRepository('CenfotecBDBundle:ContactoCRMXNumero');
        $repoTipoNumero = $em->getRepository('CenfotecBDBundle:TipoNumero');
        
        //Si el objeto no existe en base de datos registrarlo, ya que no se puede relacionar ContactoCRMXNumero
        //si no esta registrado en la base de datos
        if ($objEntity->getId() == 0) {
            $em->persist($objEntity);
            $em->flush();
        }
        
        if(!empty($dataNumeros['registrar']) && count($dataNumeros['registrar'])>0){
            foreach ($dataNumeros['registrar'] as $dataNumero) {
                $ext = (!empty($dataNumero['ext'])) ? $dataNumero['ext'] : "";

                if(!empty($ext)){ 
                    $objNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero'], 'ext' => $ext));
                }else{ 
                    $objNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero']));
                }

                //Si no existe creamos el Objeto Numero
                if($objNumero == null){
                    $objTipo = $repoTipoNumero->find($dataNumero['tipoNumero']);

                    $objNumero = new Numero();
                    $objNumero->setNumero($dataNumero['numero']);
                    $objNumero->setExt($ext);
                    $objNumero->setTipo($objTipo);
                    $em->persist($objNumero);
                    $em->flush();
                }

                $objContactoXNum  = $repoCRMXNumero->findOneBy(array(
                    'contactoCRM' => $objEntity->getId(),
                    'numero'      => $objNumero->getId()
                ));
                
                //Relacionamos al contacto CRM con el numero que obtuvimos anteriormente solo si no tiene la relacion.
                //Ya que la puede tener cuando se va por aqui por culpa de la parte de combinar informacion.
                if ($objContactoXNum == null) {
                    $objContactoXNum = new ContactoCRMXNumero();
                    $objContactoXNum->setNumero($objNumero);
                    $objContactoXNum->setContactoCRM($objEntity);
                    $em->persist($objContactoXNum);
                }
            }
        }
        if(!empty($dataNumeros['actualizar']) && count($dataNumeros['actualizar'])>0){
            foreach ($dataNumeros['actualizar'] as $idNumero => $dataNumero) {
                $objNumero      = $repoNumero->find($idNumero);
                $cantRelaciones = $repoNumero->cantRelaciones($objNumero);
                $ext            = (!empty($dataNumero['ext'])) ? $dataNumero['ext'] : "";
                $objTipo        = $repoTipoNumero->find($dataNumero['tipoNumero']);

                //Verificar si el nuevo numero no existe actualmente en base de datos
                $objExistNumero = $repoNumero->findOneBy(array('numero' => $dataNumero['numero'], 'ext' => $ext));
                
                //Si no existe en base de datos un numero con la nueva informacion que ingreso el usuario
                //actualizarlo.
                if ($objExistNumero == null) {
                    //Si tiene solo una relacion quiere decir que el numero que se va actualizar solo le pertenece
                    //al contacto que se edito.
                    if ($cantRelaciones == 1) {
                        $objNumero->setNumero($dataNumero['numero']);
                        $objNumero->setExt($ext);
                        $objNumero->setTipo($objTipo);
                        $em->merge($objNumero);
                    }else{
                        $objNewNumero = new Numero();
                        $objNewNumero->setNumero($dataNumero['numero']);
                        $objNewNumero->setExt($ext);
                        $objNewNumero->setTipo($objTipo);
                        $em->persist($objNewNumero);
                        $em->flush();
                        
                        //Relacionamos al contacto CRM con el numero que existe actualmente.
                        $objContactoXNum = new ContactoCRMXNumero();
                        $objContactoXNum->setNumero($objNewNumero);
                        $objContactoXNum->setContactoCRM($objEntity);
                        $em->persist($objContactoXNum);
                    }
                }else{
                    //Si existe en base de datos un numero con la nueva informacion que ingreso el usuario,
                    //eliminar la relacion vieja y crear una nueva relacion.
                    $objCRMXNumero  = $em->getRepository('CenfotecBDBundle:ContactoCRMXNumero')->findOneBy(array(
                        'contactoCRM' => $objEntity->getId(),
                        'numero'      => $idNumero
                    ));
                    
                    if($objExistNumero->getId() != $idNumero){
                        $objEntity->removeCRMXNumero($objCRMXNumero);

                        //Relacionamos al contacto CRM con el numero que existe actualmente.
                        $objContactoXNum = new ContactoCRMXNumero();
                        $objContactoXNum->setNumero($objExistNumero);
                        $objContactoXNum->setContactoCRM($objEntity);
                        $em->persist($objContactoXNum);
                    }
                }
            }
        }
        if(!empty($dataNumeros['eliminar'])){
            $numerosEliminar = explode(",", $dataNumeros['eliminar']);
            foreach ($numerosEliminar as $idNumeroEliminar) {
                if (!empty($idNumeroEliminar)) {
                    $objNumeroEliminar = $repoNumero->find($idNumeroEliminar);
                    $objCRMXNumeroElim = $em->getRepository('CenfotecBDBundle:ContactoCRMXNumero')->findOneBy(array(
                        'contactoCRM' => $objEntity->getId(),
                        'numero'      => $objNumeroEliminar->getId()
                    ));
                    $em->remove($objCRMXNumeroElim);
                }
            }
        }
    }
    
    static public function actualizarIntereses(&$objEntity, $dataIntereses, &$em)
    {
        $repoInteres = $em->getRepository('CenfotecBDBundle:Interes');
        if(count($dataIntereses)>0){
            $intereses = $objEntity->getIntereses();
            foreach ($dataIntereses as $idInteres) {
                $flag = 0;
                //recorro los intereses de la empresa para validar si ya existe ese interes a ese contacto no lo vuelva a registrar
                foreach ($intereses as $objInteres) {
                    //valido si el interes actual del arreglo del contacto es igual al interes del array de interes
                    if($objInteres->getId() == $idInteres){
                        $flag = 1;
                        break 1;
                    }
                }
                //si el flag es 0 le asigna ese interes a la entidad.
                if($flag == 0){
                    $objInteres = $repoInteres->find($idInteres);
                    $objEntity->addInteres($objInteres);
                }
            }
            //recorro los intereses de la empresa para ver si se elimino un interes que ya tenia asignado el usuario
            foreach ($intereses as $objInteres) {
                $flag = 0;
                foreach ($dataIntereses as $idInteres) {
                    //valido si el interes actual del arreglo de la empresa es igual al interes del array de interes
                    if($objInteres->getId() == $idInteres){
                        $flag = 1;
                        break 1;
                    }
                }
                //si el flag es 0 se le remueve ese interes a la empresa
                if($flag == 0){
                    $objEntity->removeInteres($objInteres);
                }
            }
        }
    }
    
    static public function actualizarCRMXIntereses(&$objEntity, $dataIntereses, &$em, $tipoContacto = "")
    {
        $updateFields    = "";
        $repoInteres     = $em->getRepository('CenfotecBDBundle:Interes');
        $repoModalidad   = $em->getRepository('CenfotecBDBundle:Modalidad');
        $repoCRMXInteres = $em->getRepository('CenfotecBDBundle:ContactoCRMXInteres');
        
        //Si el objeto no existe en base de datos registrarlo, ya que no se puede relacionar ContactoCRMXInteres
        //si no esta registrado en la base de datos
        if ($objEntity->getId() == 0) {
            $em->persist($objEntity);
            $em->flush();
        }
        
        if(!empty($dataIntereses['registrar']) && count($dataIntereses['registrar'])>0){
            foreach ($dataIntereses['registrar'] as $dataInteres) {
                $objInteres = $repoInteres->find($dataInteres['idInteres']);
                if ($objInteres != null) {
                    $objModalidad = $repoModalidad->find($dataInteres['idModalidad']);
                        
                    //Relacionamos al contacto CRM con el interes que obtuvimos anteriormente.
                    $objCRMXInteres = $repoCRMXInteres->findOneBy(array(
                        'contactoCRM' => $objEntity->getId(),
                        'interes'     => $objInteres->getId(),
                        'modalidad'   => $objModalidad->getId()
                    ));

                    if($objCRMXInteres == null){
                        $objCRMXInteres = new ContactoCRMXInteres();
                        $objCRMXInteres->setContactoCRM($objEntity);
                        $objCRMXInteres->setInteres($objInteres);
                        $objCRMXInteres->setModalidad($objModalidad);
                        if (!empty($dataInteres['fecha'])) {
                            $objCRMXInteres->setFechaInteres(new \DateTime($dataInteres['fecha']));
                        }
                        $em->persist($objCRMXInteres);
                    }else{
                        if (!empty($dataInteres['fecha'])) {
                            $objCRMXInteres->setFechaInteres(new \DateTime($dataInteres['fecha']));
                        }
                    }
                    $updateFields .= "Nuevo interes: '" . $objInteres->getNombre() . " " . $dataInteres['fecha'] . "', ";
                }
            }
        }
        if(!empty($dataIntereses['actualizar']) && count($dataIntereses['actualizar'])>0){
            foreach ($dataIntereses['actualizar'] as $idInteres => $dataInteres) {
                //Obtenemos el ObjCRMXInteres para actualizar los datos
                $objCRMXInteres  = $repoCRMXInteres->find($idInteres);

                //Viejo objeto interes que tenia el contacto
                $objInteres = $objCRMXInteres->getInteres();
                
                //Viejo objeto modalidad que tenia el contacto
                $objModalidad = $objCRMXInteres->getModalidad();
                
                //Cargamos el nuevo objeto Interes que selecciono el usuario
                $objInteresSel   = $repoInteres->find($dataInteres['idInteres']);
                
                //Cargamos la nueva modalidad que selecciono el usuario
                $objModalidadSel   = $repoModalidad->find($dataInteres['idModalidad']);
                
                //Fechas
                $fechaInteres    = ($objCRMXInteres->getFechaInteres() != null) ? (" " . $objCRMXInteres->getFechaInteres()->format('Y-m-d')) : "";
                $fechaInteresSel = (!empty($dataInteres['fecha'])) ? (" " . $dataInteres['fecha']) : "";
                
                //Mensaje para bitacora de fecha
                if ($fechaInteres != $fechaInteresSel) {
                    if (!empty($dataInteres['fecha'])) {
                        $objCRMXInteres->setFechaInteres(new \DateTime($dataInteres['fecha']));
                    }else{
                        $objCRMXInteres->setFechaInteres(null);
                    }
                    $updateFields   .= "Fecha: '" . $fechaInteres . "' => '" . $fechaInteresSel . "', ";
                }
                
                //Mensaje para bitacora de interes
                if ($objInteres->getId() != $objInteresSel->getId()) {
                    $objCRMXInteres->setInteres($objInteresSel);
                    $updateFields .= "Interes: '" . $objInteres->getNombre() . "' => '" . $objInteresSel->getNombre() . "', ";
                }
                
                //Mensaje para bitacora de modalidad
                if ($objModalidad->getId() != $objModalidadSel->getId()) {
                    $objCRMXInteres->setModalidad($objModalidadSel);
                    $updateFields .= "Modalidad: '" . $objModalidad->getNombre() . "' => '" . $objModalidadSel->getNombre() . "', ";
                }
            }
        }
        if(!empty($dataIntereses['eliminar'])){
            $interesesEliminar = explode(",", $dataIntereses['eliminar']);
            foreach ($interesesEliminar as $idCRMXInteresEliminar) {
                if (!empty($idCRMXInteresEliminar)) {
                    $objCRMXInteresElim = $repoCRMXInteres->find($idCRMXInteresEliminar);
                    if($objCRMXInteresElim != null){
                        $em->remove($objCRMXInteresElim);
                        $updateFields .= "Se elimino interes: '" . $objCRMXInteresElim->getInteres()->getNombre() . "', ";
                    }
                }
            }
        }
        
        return $updateFields;
    }
}
