<?php

namespace Cenfotec\CRMBundle\Clases;

use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator;
use Components\EJSTreeGridBundle\Framework\GridLayoutGenerator;
use Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

/**
 * Clase auxiliar para manejar variables globales.
 * @author Carlos Dominguez <krlosnando@gmail.com>
 */
class SimpleAdminEntityHelper {
    
    
    public static function listar($controller, $datos) 
    {
        $router               = $controller->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGrid' . $controller->entityClassName);
        
        $gridOptionsGenerator
            ->setGridId('Grid' . $controller->entityClassName)
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_admin_' . $controller->entityActionName . '_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_admin_' . $controller->entityActionName . '_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return $controller->render("CenfotecCRMBundle:Administracion:listar.html.twig", array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwPageTitle'            => $datos['pageTitle'],
            'ptwEntityName'           => $controller->entityName,
            'ptwEntityActionName'     => $controller->entityActionName,
            'ptwEntityClassName'      => $controller->entityClassName
        ));
    }
    
    public static function gridLayout($controller)
    {
        $layoutGenerator = new GridLayoutGenerator();
        
        
        $layoutGenerator->setConfigurationOption('SelectingSingle', 0);
        $layoutGenerator->addTopRowFilter(array(
            'id' => ""
        ));
        
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Int", 'Width' => 0, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "Name",    'Type' => "Text", 'Width'    => 0, 'CanExport' => 0 ),
            array( 'Name' => "CNombre", 'Type' => "Text", 'RelWidth' => 1, 'CanEdit'   => 0 ),
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id'      => 'ID',
            'CNombre' => 'Nombre'
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return $controller->render("ComponentsEJSTreeGridBundle::gridLayout.json.twig", array( 
            'gridLayoutGenerator' => $layoutGenerator
        ));
    }
    
    public static function gridData($controller) 
    { 
        $dataFormatter = new GridDataTreePagingFormatter();
        $datos         = $controller->getDoctrine()->getRepository('CenfotecBDBundle:' . $controller->entityClassName )->findBy(array(), array('nombre'=>'asc'));
        
        foreach ($datos as $objEntity) {
            $dataFormatter->addRow(array(
                'id'      => $objEntity->getId(),
                'Name'    => $objEntity->getNombre(),
                'CNombre' => $objEntity->getNombre()
            ));
        }
        
        return $controller->render("ComponentsEJSTreeGridBundle::gridData.json.twig", array( 
            'gridDataFormatter' => $dataFormatter
        ));
    }
    
    public static function formRegistrar($controller)
    {
        $router = $controller->get('router');
        
        return $controller->render("CenfotecCRMBundle:Administracion:formAdministrar.html.twig", array( 
            'ptwEntityName'       => $controller->entityName,
            'ptwEntityActionName' => $controller->entityActionName,
            'ptwFormAction'       => $router->generate('cenfo_' . $controller->entityActionName . '_registrar')
        ));
    }
    
    public static function formModificar($controller)
    {
        $router         = $controller->get('router');
        $d              = $controller->getDoctrine();
        $r              = $controller->getRequest()->query;
        $pidEntity      = $r->get('pidEntity');
        $objEntity      = $d->getRepository("CenfotecBDBundle:" . $controller->entityClassName)->find($pidEntity);
        
        return $controller->render("CenfotecCRMBundle:Administracion:formAdministrar.html.twig", array( 
            'ptwObjEntity'        => $objEntity,
            'ptwEntityName'       => $controller->entityName,
            'ptwEntityActionName' => $controller->entityActionName,
            'ptwFormAction'       => $router->generate('cenfo_' . $controller->entityActionName . '_modificar', array(
                'pidEntity'       => $pidEntity
            ))
        ));
    }
    
    public static function registrar($controller)
    {
        try {
            $r              = $controller->getRequest();
            $em             = $controller->getDoctrine()->getEntityManager();
            $data           = $r->request->get('data');
            $objEntity      = new $controller->entityFullClassName;
            
            $objEntity->setNombre($data['nombre']);
            $objEntity->setUsuarioRegistroBD($controller->getUser());
            
            //Registrar la entidad
            $em->persist($objEntity);
            $em->flush();
                    
            //Insertar en bitacora
            $descripcion = $controller->entityName . " ID:" . $objEntity->getId() . " " . $objEntity->getNombre() . ".";
            
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_NUEVO_REGISTRO,
                'descripcion'  => $descripcion,
                'objUsuario'   => $controller->getUser()
            ));
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status' => 'ok',
                'msg'    => $controller->entityName . " 'ID:" . $objEntity->getId() . " " . $objEntity->getNombre() . "' se registro correctamente."
            ))); 
            
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($controller, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    public static function modificar($controller)
    {
        try {
            $cambios      = "";
            $r            = $controller->getRequest();
            $em           = $controller->getDoctrine()->getEntityManager();
            $data         = $r->request->get('data');
            $pidEntity    = $r->query->get('pidEntity');
            $objEntity    = $em->getRepository('CenfotecBDBundle:' . $controller->entityClassName)->find($pidEntity);
            
            if ($data['nombre'] != $objEntity->getNombre()) {
                $cambios .= "Nombre: '" . $objEntity->getNombre() . "' => '" . $data['nombre'] . "' ";
                $objEntity->setNombre($data['nombre']);
            }
            
            $objEntity->setUsuarioModificoBD($controller->getUser());
            $objEntity->setFechaModificoBD(new \DateTime());
            
            //Guardar los cambios en la base de datos
            $em->flush();
            
            //Registrar a Bitacora
            $descripcion =  $controller->entityName . " ID:" . $objEntity->getId() . " " . $objEntity->getNombre() . " $cambios.";
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_MODIFICAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $controller->getUser()
            ));
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status' => 'ok',
                'msg'    => $controller->entityName . " 'ID:" . $objEntity->getId() . " " . $objEntity->getNombre() . "' se modifico correctamente."
            ))); 
            
        } catch (\Exception $exc) {
            GlobalHelper::registrarExcepcion($controller, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    public static function eliminar($controller)
    {
        try {
            $r              = $controller->getRequest();
            $em             = $controller->getDoctrine()->getEntityManager();
            $pidEntity      = $r->get('pidEntity');
            $msg            = "";
            $objEntity      = $em->getRepository('CenfotecBDBundle:' . $controller->entityClassName)->find($pidEntity);
            $msg            = $controller->entityName . " 'ID:" . $objEntity->getId() . " " . $objEntity->getNombre() . "' se ha eliminado correctamente.";
            
            //Eliminar la entidad de base de datos
            $em->remove($objEntity);
            $em->flush();
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($controller, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
}
