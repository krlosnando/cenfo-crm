<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter,
    Cenfotec\CRMBundle\Clases\GlobalHelper;
use Cenfotec\BDBundle\Entity\ContactoCRM;
use Cenfotec\BDBundle\Entity\ProgramaAcademico;
use Cenfotec\BDBundle\Entity\Acti;
use Cenfotec\BDBundle\Entity\ContactoCRMXNumero;
use Cenfotec\BDBundle\Entity\ContactoCRMXInteres;
use Cenfotec\BDBundle\Entity\Correo;
use Cenfotec\BDBundle\Entity\Numero;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/acti")
*/
class ActiController extends Controller
{
    /**
     * @Route("/", name="cenfo_acti_listar")
     * @Template()
     */
    public function listarAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridACTI');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridACTI')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_acti_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_acti_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('ACTI'),
            'ptwModulo'               => 'Administrar',
            'ptwIntereses'            => $intereses
        );
    }
    
    /**
     * @Route("/eliminados", name="cenfo_acti_listar_eliminados")
     * @Template("CenfotecCRMBundle:Acti:listar.html.twig")
     */
    public function listarEliminadosAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridACTI');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridACTI')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_acti_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_acti_grid_data', array(
                    'pflagEliminados' => '1'
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('ACTI'),
            'ptwModulo'               => 'Eliminados',
            'ptwIntereses'            => $intereses
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_acti_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $r                        = $this->getRequest();
        $router                   = $this->get('router');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $layoutGenerator          = new GridLayoutGenerator();
        $ColumWidthID             = (!empty($pflagComplexTable)) ? 0 : 40;
        $ColumWidthNombre         = (!empty($pflagComplexTable)) ? 150 : 80;
        
        $layoutGenerator->setConfigurationOption('ChildPaging', 2);
        $layoutGenerator->setConfigurationOption('ChildParts', 2);
        $layoutGenerator->addTopRowFilter(array(
            "idType" => "Int",
            "id"     => ""
        ));
        
        //IDContactoCRM
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Text", 'Width' => $ColumWidthID, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "IDACTI",         'Type' => "Int",    'Width'   => 0,  'CanEdit'   => 0 ),
            array( 'Name' => "Name",           'Type' => "Text",   'Width'   => 0,  'CanExport' => 0 ),
            array( 'Name' => "Nombre",         'Type' => "Text",   'Width'   => $ColumWidthNombre, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Apellido1",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Apellido2",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Correo",         'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantCorreo",     'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Interes",        'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantInteres",    'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Numero",         'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantNumero",     'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Empresa",        'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Profesion",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Identificacion", 'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Referencia",     'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "PrimerContacto",     'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 )
        ));
        
        $layoutGenerator->addRightColumn(array( 
            'Name' => 'Seg', 'CanEdit' => 0, 'Width' => 30, 'CanFilter' => 1, 'CanSort' => 1, 'Type' => "Int", 'Align' => 'Center'
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id' => 'ID'
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,BtnDownload,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
            
            'BtnDownload'             => "1",
            'BtnDownloadButtonText'   => "Descargar todos los registrados en Excel",
            'BtnDownloadType'         => "Button",
            'BtnDownloadOnClick'      => "$.downloadExcel('" . $router->generate('cenfo_acti_download') . "', null)"
        ));
        
        //Solo si es Telemercadeo.
        if($pflagTelemercadeoAsignar == 1){
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 80,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'Encargado',
                'Type'      => "Text",
                'Align'     => 'Left',
            ));
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 100,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'UltimaLlamada',
                'Type'      => "Date",
                'Align'     => 'Left',
            ));
        }
        
        //Solo si es ComplexTable
        if($pflagComplexTable == 1){
            //Hacer mas grande el Grid
            $layoutGenerator->setConfigurationOption('MaxVScroll', '550');
            //Agregar el total
            $layoutGenerator->addRightColumn(array(
                'Name'       => 'Total',
                'CanFilter'  => 0,
                'Type'       => 'Int',
                'Width'      => 80,
            ));
            // Filas normales.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'R',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas con subfilas.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'Node',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas creadas cuando se agrupan 
            $layoutGenerator->addDefaultRow(array(
                'Name'            => 'Group',
                'Def'             => 'Node',
                'CanSelect'       => '0',
                'AggChildren'     => '1',
                'CanFilter'       => '2',
                'TotalFormula'    => 'count("id", 4)',
                'TotalIntFormat'  => '<i style="font-style:italic">0</i>'
            ));
            $layoutGenerator->setSolidRows(array(
                array(
                    'id'    => 'Group',
                    'Kind'  => 'Group',
                    'Space' => '0',
                )
            ));
        }
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/download", name="cenfo_acti_download")
     */
    public function downloadAction() 
    {  
        ini_set('memory_limit', '-1');
        $dateTime         = new \DateTime();
        $repoActi         = $this->getDoctrine()->getRepository('CenfotecBDBundle:Acti');
        $contactos        = $repoActi->searchAll(true);
        $cantCorreos      = 2;
        $cantNumeros      = 2;
        $cantIntereses    = 2;
        $cantSeguimientos = 3;
        $cantMatriculas   = 1;
        
        $fileName = 'CRM ACTI ' . date_format($dateTime, 'Y-m-d H_i_s');
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setStatusCode(200);

        $response->headers->set('Content-Type', "application/vnd.ms-excel");
        $response->headers->set('Cache-Control', "must-revalidate");
        $response->headers->set('Set-Cookie', "fileDownload=true; path=/");
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s.xls"', $fileName));
        
        $response->setContent($this->renderView(
            "CenfotecCRMBundle:CRM:download.xls.twig", array(
                "ptwTipoContacto"   => "ACTI",
                "contactos"         => $contactos,
                "cantCorreos"       => $cantCorreos,
                "cantNumeros"       => $cantNumeros,
                "cantIntereses"     => $cantIntereses,
                "cantSeguimientos"  => $cantSeguimientos,
                "cantMatriculas"    => $cantMatriculas
             )
        ));
        
        return $response;
    }
    
    /**
     * @Route("/grid-data", name="cenfo_acti_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    {
        $r                        = $this->getRequest();
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagEliminados          = $r->query->get('pflagEliminados');
        $ptipoBusqueda            = $r->query->get('ptipoBusqueda');
        $pcriterio                = $r->query->get('pcriterio');
        $pnombre                  = $r->query->get('pnombre');
        $papellido1               = $r->query->get('papellido1');
        $papellido2               = $r->query->get('papellido2');
        $pcorreo                  = $r->query->get('pcorreo');
        $pnumero                  = $r->query->get('pnumero');
        $pinteres                 = $r->query->get('pinteres');
        $pfechaInicioContacto     = $r->query->get('pfechaInicioContacto');
        $pfechaFinContacto        = $r->query->get('pfechaFinContacto');
        $pfechaInicioInteres      = $r->query->get('pfechaInicioInteres');
        $pfechaFinInteres         = $r->query->get('pfechaFinInteres');
        $dataFormatter            = new GridDataTreePagingFormatter();
        $repoActi                 = $this->getDoctrine()->getRepository('CenfotecBDBundle:Acti');
        
        if(!empty($pflagEliminados) && $pflagEliminados == "1"){
            $contactos = $repoActi->searchAllEliminados();
        }else if($ptipoBusqueda == 'Filtro'){
            $contactos = $repoActi->searchDataXFiltro(
                    $pnombre, $papellido1, $papellido2, $pcorreo, 
                    $pnumero, $pfechaInicioContacto, $pfechaFinContacto,
                    $pfechaInicioInteres, $pfechaFinInteres, $pinteres);
        }else if($ptipoBusqueda == 'Criterio'){
            $contactos = $repoActi->searchDataXCriterio($pcriterio);
        }else{
            $contactos = $repoActi->searchAll($pflagComplexTable);
        }
        
        $dateTime = new \DateTime();
        
        foreach ($contactos as $dataContacto) {
            //Obtener los Correos
            $correos      = (!empty($dataContacto['correos'])) ? explode('|', $dataContacto['correos']) : array();
            $enumCorreos  = "";
            $defautCorreo = "";
            
            foreach ($correos as $strCorreo) {
                $defautCorreo     = $strCorreo;
                $enumCorreos .= "|" . $strCorreo;
            }
            
            //Obtener los intereses
            $intereses       = (!empty($dataContacto['intereses'])) ? explode('|', $dataContacto['intereses']) : array();
            $enumIntereses   = "";
            $defautInteres   = "";
            
            foreach ($intereses as $strInteres) {
                $defautInteres  = $strInteres;
                $enumIntereses .= "|" . $strInteres;
            }
            
            //Obtener los numeros
            $numeros       = (!empty($dataContacto['numeros'])) ? explode('|', $dataContacto['numeros']) : array();
            $enumNumeros   = "";
            $defautNumeros = "";
            
            foreach ($numeros as $strNumero) {
                $defautNumeros  = $strNumero;
                $enumNumeros   .= "|" . $strNumero;
            }
             
            $row = array(
                'id'              => $dataContacto['contacto_crm_id'],
                'Name'            => $dataContacto['nombre'],
                'IDACTI'          => $dataContacto['acti_id'],
                'Nombre'          => $dataContacto['nombre'],
                'Apellido1'       => $dataContacto['apellido1'],
                'Apellido2'       => $dataContacto['apellido2'],
                'Identificacion'  => $dataContacto['identificacion'],
                'Empresa'         => $dataContacto['empresa'],
                'Profesion'       => $dataContacto['profesion'],
                'Referencia'      => $dataContacto['referencia'],
                'PrimerContacto'      => $dataContacto['primer_contacto'],
                'CantCorreo'      => count($correos),
                'Correo'          => $defautCorreo,
                'CorreoDefaults'  => $enumCorreos,
                'CantInteres'     => count($intereses),
                'Interes'         => $defautInteres,
                'InteresDefaults' => $enumIntereses,
                'CantNumero'      => count($numeros),
                'Numero'          => $defautNumeros,
                'NumeroDefaults'  => $enumNumeros,
                'FechaContacto'   => $dataContacto['fecha_contacto'],
                'Seg'             => $dataContacto['cant_seguimientos']
            );

            //Solo si es Telemercadeo.
            if($pflagTelemercadeoAsignar == 1){
                $row['Encargado']     = $dataContacto['acti_id'] ? 'CDominguez' : 'No';
                $row['UltimaLlamada'] = $dataContacto['acti_id'] ? $dateTime->format('Y/m/d H:i') : '';
                if($row['Encargado'] != 'No'){
                    $row['Color'] = '#e8ffcd';
                }
            }
            $dataFormatter->addRow($row);
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_acti_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Acti:formAdministrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $tipoIdentificaciones = $d->getRepository('CenfotecBDBundle:TipoIdentificacion')->findBy(array(), array('nombre'=>'asc'));
        $convenios            = $d->getRepository('CenfotecBDBundle:Convenio')->findBy(array(), array('nombre'=>'asc'));
        $empresas             = $d->getRepository('CenfotecBDBundle:Empresa')->findBy(array(), array('nombre'=>'asc'));
        $profesiones          = $d->getRepository('CenfotecBDBundle:Profesion')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $horarios             = $d->getRepository('CenfotecBDBundle:Horario')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        $modalidades          = $d->getRepository('CenfotecBDBundle:Modalidad')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => 'Registrar nuevo contacto de ACTI',
            'ptwAction'               => 'Registrar',
            'ptwFormAction'           => $router->generate('cenfo_acti_registrar'),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwTipoIdentificaciones' => $tipoIdentificaciones,
            'ptwConvenios'            => $convenios,
            'ptwEmpresas'             => $empresas,
            'ptwProfesiones'          => $profesiones,
            'ptwPuestos'              => $puestos,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwHorarios'             => $horarios,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwModalidades'          => $modalidades
        );
    }

    /**
     * @Route("/form-modificar", name="cenfo_acti_form_modificar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Acti:formAdministrar.html.twig")
     */
    public function formModificarAction()
    {
        $router               = $this->get('router');
        $r                    = $this->getRequest();
        $d                    = $this->getDoctrine();
        $pidActi              = $r->query->get('pidActi');
        $objActi              = $d->getRepository('CenfotecBDBundle:Acti')->find($pidActi);
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $tipoIdentificaciones = $d->getRepository('CenfotecBDBundle:TipoIdentificacion')->findBy(array(), array('nombre'=>'asc'));
        $convenios            = $d->getRepository('CenfotecBDBundle:Convenio')->findBy(array(), array('nombre'=>'asc'));
        $empresas             = $d->getRepository('CenfotecBDBundle:Empresa')->findBy(array(), array('nombre'=>'asc'));
        $profesiones          = $d->getRepository('CenfotecBDBundle:Profesion')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $horarios             = $d->getRepository('CenfotecBDBundle:Horario')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        $modalidades          = $d->getRepository('CenfotecBDBundle:Modalidad')->findBy(array(), array('nombre'=>'asc'));
        $objContactoCRM       = $objActi->getContactoCRM();
        $canMatriculas        = count($objContactoCRM->getMatriculas());
        
        return array(
            'ptwTituloPagina'         => "Modificar ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto(),
            'ptwAction'               => 'Modificar',        
            'ptwFormAction'           => $router->generate('cenfo_acti_modificar', array('pidActi'=>$pidActi)),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwTipoIdentificaciones' => $tipoIdentificaciones,
            'ptwConvenios'            => $convenios,
            'ptwEmpresas'             => $empresas,
            'ptwProfesiones'          => $profesiones,
            'ptwPuestos'              => $puestos,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwHorarios'             => $horarios,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwCantMatriculas'       => $canMatriculas,
            'ptwObjActi'              => $objActi,
            'ptwModalidades'          => $modalidades
        );
    }
    
    /**
     * @Route("/registrar", name="cenfo_acti_registrar")
     */
    public function registrarAction()
    {
        try{
            $r                    = $this->getRequest();
            $doctrine             = $this->getDoctrine();
            $em                   = $doctrine->getEntityManager();
            $data                 = $r->request->get('data');
            $usuario              = $this->getUser();
            $objNewContactoCRM    = new \Cenfotec\BDBundle\Entity\ContactoCRM();
            $objNewActi           = new \Cenfotec\BDBundle\Entity\Acti();
            $data["usuario"]      = $usuario;
            $data["tipoContacto"] = "ACTI";
            
            GlobalHelper::bindDataContactoCRM($objNewContactoCRM, $data, $em, 'Registrar');
            
            //Registrar a Bitacora
            $descripcion =  "ACTI ID:" . $objNewContactoCRM->getId() . " " . $objNewContactoCRM->getNombreCompleto() . ".";
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_NUEVO_REGISTRO,
                'descripcion'  => $descripcion,
                'objUsuario'   => $usuario
            ));
            
            //Registrar nuevo contacto de programa academico
            $objNewActi->setContactoCRM($objNewContactoCRM);
            $em->persist($objNewActi);
            $em->flush();
            
            //Relacionar el ID de ACTI que se genero con el Contacto CRM
            $objNewContactoCRM->setActi($objNewActi);
            $em->flush();
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "El contacto de ACTI 'ID:" . $objNewContactoCRM->getId() . " " . $objNewContactoCRM->getNombreCompleto() . "' se registro correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_acti_form_modificar', array(
                    'pidActi' => $objNewActi->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/modificar", name="cenfo_acti_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        try{
            $request              = $this->getRequest();
            $doctrine             = $this->getDoctrine();
            $em                   = $doctrine->getEntityManager();
            $idActi               = $request->query->get('pidActi');
            $idContactoCRM        = $request->query->get('pidContactoCRM');
            $data                 = $request->request->get('data');
            $usuario              = $this->getUser();
            $data["usuario"]      = $usuario;
            $data["tipoContacto"] = "ACTI";
            $flagCombinar         = false;
            
            //Se llamo desde la parte de combianar si existe un contacto con informacion parecida
            if (!empty($idContactoCRM)) {
                $flagCombinar   = true;
                $objContactoCRM = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($idContactoCRM);
                $objActi        = $objContactoCRM->getActi();
                
                //Si el contacto crm no tiene acti registrarlo
                if ($objActi == null) {
                    $objActi = new \Cenfotec\BDBundle\Entity\Acti();
                    $objActi->setContactoCRM($objContactoCRM);
                    $em->persist($objActi);
                    $em->flush();
                    
                    //Relacionar el ID de acti que se genero con el Contacto CRM
                    $objContactoCRM->setActi($objActi);
                    $em->flush();
                }
            }else{
                //busco el objeto acti para obtener el objeto contacto.
                $objActi = $em->getRepository('CenfotecBDBundle:Acti')->find($idActi);
                $objContactoCRM   = $objActi->getContactoCRM();
            }
            
            $cambios = GlobalHelper::bindDataContactoCRM($objContactoCRM, $data, $em, 'Modificar', $flagCombinar);
            
            //Establecer el Contacto como activo si estaba eliminado.
            $objActi->setFlagEliminado(false);
            
            //Guardar los cambios en base de datos.
            $em->flush();
            
            //Registrar a Bitacora
            $descripcion =  "ACTI ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . " $cambios.";
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_MODIFICAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $usuario
            ));
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "El contacto de ACTI 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se modifico correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_acti_form_modificar', array(
                    'pidActi' => $objActi->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/eliminar", name="cenfo_acti_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        try {
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $pidActi        = $r->get('pidActi');
            $msg            = "";
            $objActi        = $em->getRepository('CenfotecBDBundle:Acti')->find($pidActi);
            $objContactoCRM = $objActi->getContactoCRM();
            
            $objActi->setFlagEliminado(true);
            $em->merge($objActi);
            $em->flush();
            
            $msg = "El contacto de ACTI 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se ha eliminado correctamente.";
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objContactoCRM->getId(),
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/copy-to-prog-academico", name="cenfo_acti_copy_to_prog_academico", options={"expose"=true})
     */
    public function copyToProgAcademicoAction()
    {
        try{
            $r        = $this->getRequest()->request;
            $idActi   = $r->get('pidActi');
            $msg      = "";

            if(!empty($idActi)){
                $em               = $this->getDoctrine()->getEntityManager();
                $objActi          = $em->getRepository('CenfotecBDBundle:Acti')->find($idActi);
                $objContactoCRM   = $objActi->getContactoCRM();

                //Validar si ya existe el ContactoCRM en Programa Academico.
                $objProgAcademico = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->findBy(array('contactoCRM'=>$objContactoCRM->getId()));
                if($objProgAcademico == null){
                    $objProgAcademico = new ProgramaAcademico();
                    $objProgAcademico->setContactoCRM($objContactoCRM);
                    $em->persist($objProgAcademico);
                    $em->flush();

                    //Relacionar el ID de programa Academico que se genero con el Contacto CRM
                    $objContactoCRM->setProgramaAcademico($objProgAcademico);
                    $em->flush();

                    $msg = "El contacto de ACTI con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se copió correctamente a la BD de Programa Académico.";
                }else{
                    $msg = "<p>- El contacto de ACTI con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' ya se encuentra actualmente en la BD de Programa Académico.</p>";
                    return new \Symfony\Component\HttpFoundation\Response($msg); 
                }
            }else{
                $msg = "<p>- Identificador del contacto ACTI es nulo.</p>";
                return new \Symfony\Component\HttpFoundation\Response($msg); 
            }

            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'    => $msg,
                'status' => "ok"
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    /**
     * @Route("/copy-to-posgrado", name="cenfo_acti_copy_to_posgrado", options={"expose"=true})
     */
    public function copyToPosgradoAction()
    {
        try{
            $r      = $this->getRequest()->request;
            $idActi = $r->get('pidActi');
            $msg    = "";

            if(!empty($idActi)){
                $em             = $this->getDoctrine()->getEntityManager();
                $objActi        = $em->getRepository('CenfotecBDBundle:Acti')->find($idActi);
                $objContactoCRM = $objActi->getContactoCRM();

                //Validar si ya existe el ContactoCRM en Posgrado.
                $objPosgrado = $em->getRepository('CenfotecBDBundle:Posgrado')->findBy(array('contactoCRM'=>$objContactoCRM->getId()));
                if($objPosgrado == null){
                    $objPosgrado = new \Cenfotec\BDBundle\Entity\Posgrado();
                    $objPosgrado->setContactoCRM($objContactoCRM);
                    $em->persist($objPosgrado);
                    $em->flush();

                    //Relacionar el ID de Posgrado que se genero con el Contacto CRM
                    $objContactoCRM->setPosgrado($objPosgrado);
                    $em->flush();

                    $msg = "El contacto de ACTI con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se copió correctamente a la BD de Posgrado.";
                }else{
                    $msg = "<p>- El contacto de ACTI con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' ya se encuentra actualmente en la BD de Posgrado.</p>";
                    return new \Symfony\Component\HttpFoundation\Response($msg); 
                }
            }else{ 
                $msg = "<p>- Identificador del contacto ACTI es nulo.</p>";
                return new \Symfony\Component\HttpFoundation\Response($msg); 
            }

            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'    => $msg,
                'status' => "ok"
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
}
