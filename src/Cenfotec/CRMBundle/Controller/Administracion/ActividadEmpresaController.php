<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/actividad-empresa")
*/
class ActividadEmpresaController extends Controller
{
    public $entityName           = "Actividad de empresas";
    public $entityClassName      = "Actividad";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\Actividad";
    public $entityActionName     = "actividad_empresa";
    
    /**
     * @Route("/", name="cenfo_admin_actividad_empresa")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => 'Actividades de empresa'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_actividad_empresa_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutActividadAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_actividad_empresa_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataActividadAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_actividad_empresa_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_actividad_empresa_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_actividad_empresa_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_actividad_empresa_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_actividad_empresa_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
