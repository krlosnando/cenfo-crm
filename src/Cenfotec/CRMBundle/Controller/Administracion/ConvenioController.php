<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/convenio")
*/
class ConvenioController extends Controller
{
    public $entityName           = "Convenio";
    public $entityClassName      = "Convenio";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\Convenio";
    public $entityActionName     = "convenio";
    
    /**
     * @Route("/", name="cenfo_admin_convenio")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => 'Convenios'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_convenio_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutConvenioAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_convenio_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataConvenioAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_convenio_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_convenio_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_convenio_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_convenio_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_convenio_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
