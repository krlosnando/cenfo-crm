<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\CRMBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/curso")
*/
class CursoController extends Controller
{
    /**
     * @Route("/", name="cenfo_admin_curso")
     * @Template("CenfotecCRMBundle:Administracion/Curso:index.html.twig")
     */
    public function indexAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridCurso');
        
        $gridOptionsGenerator
            ->setGridId('GridCurso')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_admin_curso_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_admin_curso_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_curso_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutCursoAction()
    {
        $layoutGenerator = new GridLayoutGenerator();
        
        $columns = array(
            array(
                'CanEdit'   => 1,
                'Width'     => 0,
                'Name'      => "id",
                'Type'      => "Text",
            ),array(
                'Width'     => 0,
                'Name'      => "Name",
                'Type'      => "Text", 
            ),array(
                'CanEdit'   => 1,
                'RelWidth'  => 2,
                'MinWidth'  => 70,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Type'      => "Text",
                'Align'     => 'Left',
                'Name'      => "Nombre"
            ),array(
                'CanEdit'   => 1,
                'RelWidth'  => 2,
                'MinWidth'  => 70,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Type'      => "Text",
                'Align'     => 'Left',
                'Name'      => "Area"
            )
        );
        
        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        $layoutGenerator->setVariableColumns($columns);
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_curso_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataCursoAction() 
    { 
        $dataFormatter = new GridDataTreePagingFormatter();
        for ($index = 0; $index < 30; $index++) {
            
            $dataFormatter->addRow(array(
                'id'         => $index,
                'Name'       => "Test" . $index,
                'Nombre'     => "BISOFT - BACHILLERATO " . $index,
                'Area'       => "PA - DESARROLLO DE SOFTWARE " . $index
            ));
        }
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_admin_curso_form_registrar")
     * @Template("CenfotecCRMBundle:Administracion/Curso:formRegistrar.html.twig")
     */
    public function registrarAction()
    {
        return array();
    }

    /**
     * @Route("/form-modificar", name="cenfo_admin_curso_form_modificar")
     * @Template("CenfotecCRMBundle:Administracion/Curso:formModificar.html.twig")
     */
    public function modificarAction()
    {
        return array();
    }

    /**
     * @Route("/borrar", name="cenfo_admin_curso_borrar")
     * @Template("CenfotecCRMBundle:Administracion/Curso:borrar.html.twig")
     */
    public function borrarAction()
    {
        return array();
    }
}
