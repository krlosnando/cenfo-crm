<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\CRMBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;
// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/interes")
 */
class InteresController extends Controller {

    /**
     * @Route("/", name="cenfo_admin_interes")
     * @Template("CenfotecCRMBundle:Administracion/Interes:index.html.twig")
     */
    public function indexAction() {
        $router = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridInteres');

        $gridOptionsGenerator
                ->setGridId('GridInteres')
                ->setOptions(array(
                    'Layout_Url' => $router->generate('cenfo_admin_interes_grid_layout'),
                    'Data_Url' => $router->generate('cenfo_admin_interes_grid_data'),
                    'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                    'Export_Url' => $router->generate('cd_ejstreegrid_export'),
        ));

        return array(
            'ptwGridOptionsGenerator' => $gridOptionsGenerator
        );
    }

    /**
     * @Route("/grid-layout", name="cenfo_admin_interes_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutInteresAction() {
        $layoutGenerator = new GridLayoutGenerator();

        $layoutGenerator->setConfiguration(array(
            'SearchExpand' => 1,
            'NoFormatEscape' => 1,
            'StandardFilter' => 2,
            'NoVScroll' => 1,
            'SelectingSingle' => 0,
            'Deleting' => 1,
            'Adding' => 1,
            'Undo' => 1,
            'Dragging' => 1,
            'MainCol' => "NodeCol"
        ));

        $layoutGenerator->setVariableColumns(array(
            array('Name' => "id", 'Type' => "Text", 'Width' => 0, 'CanEdit' => 1),
            array('Name' => "Name", 'Type' => "Text", 'Width' => 0, 'CanExport' => 0),
            array('Name' => "NodeCol", 'Type' => "Text", 'RelWidth' => 2, 'CanEdit' => 1, 'Align' => 'Left', 'CaseSensitive' => 0),
            array('Name' => "Nombre", 'Type' => "Text", 'RelWidth' => 4, 'CanEdit' => 1, 'Align' => 'Left', 'CaseSensitive' => 0)
        ));

        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter",
            'AreaType' => "Text",
            'TipoType' => "Text",
        ));

        // Todos los hijos de Root tendran como Def="Level1", y en Root solo se aceptan Filas con Def="Level1" 
        $layoutGenerator->setRootRow(array(
            'CDef' => "Level1",
            'AcceptDef' => "Level1"
        ));

        //Level 1:Tipo: Todas las filas con Def="Level1", solo aceptaran Def="Level2", y al cargar el grid si los hijos de Def="Level1" no tienen Def, se pondra por default Def="Level2"
        $layoutGenerator->addDefaultRow(array(
            'Name' => "Level1",
            'AcceptDef' => "Level2",
            'CDef' => "Level2",
            'Expanded' => 1,
            'CanEdit' => 0,
            'CanSelect' => 0,
            'CanDelete' => 0,
        ));

        //Level 2:Area: Todas las filas con Def="Level2", solo aceptaran Def="Level3"
        $layoutGenerator->addDefaultRow(array(
            'Name' => "Level2",
            'AcceptDef' => "Level3",
            'CDef' => "Level3",
            'Expanded' => 0,
            'NodeColCanEditFormula' => "(Row.Level == 2) ? 0 : 1",
            'CanSelectFormula' => "(Row.Level == 2) ? 0 : 1",
            'CanEdit' => 0,
            'Recalc' => 1,
            'Calculated' => 1,
            'CalcOrder' => "NodeColCanEdit,CanSelect",
        ));

        //Level 3:Interes: Todas las filas con Def="Level3", solo aceptaran Def="R"
        $layoutGenerator->addDefaultRow(array(
            'Name' => "Level3",
            'AcceptDef' => "",
            'CDef' => "",
            'NodeCol' => "",
            'NodeColCanEdit' => 0
        ));

        $layoutGenerator->setCellsToolbar('Reload,Undo,Redo,AddChild,Cnt,Sel');

        return array('gridLayoutGenerator' => $layoutGenerator);
    }

    /**
     * @Route("/grid-data", name="cenfo_admin_interes_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataInteresAction() {
        $doctrine = $this->getDoctrine();
        $dataFormatter = new GridDataTreePagingFormatter();
        $tiposContacto = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));

        //Recorrer todos los tipos
        foreach ($tiposContacto as $objTipo) {
            $dataFormatter->addRow(array(
                'id' => 'Tipo_' . $objTipo->getId(),
                'Name' => $objTipo->getNombre(),
                'NodeCol' => $objTipo->getNombre()
            ));

            $areas = $objTipo->getAreasInteres();
            //Recorrer todas las ares
            foreach ($areas as $objArea) {
                $dataFormatter->addSubRow(array(
                    'id' => 'Area_' . $objArea->getId(),
                    'Name' => $objArea->getNombre(),
                    'NodeCol' => $objArea->getNombre(),
                ));

                $intereses = $objArea->getIntereses();
                //Recorrer todos los intereses
                foreach ($intereses as $objInteres) {
                    $dataFormatter->addSubRow(array(
                        'id' => 'Interes_' . $objInteres->getId(),
                        'Name' => $objInteres->getNombre(),
                        'Nombre' => $objInteres->getNombre(),
                            ), 2);
                }
            }
        }
        return array('gridDataFormatter' => $dataFormatter);
    }

    /**
     * @Route("/guardar", name="cenfo_admin_interes_guardar", options={"expose"=true})
     */
    public function guardarAction() {
        try {
            $r = $this->getRequest()->request;
            $cambios = $r->get("pcambios");
            $cambiosArea = isset($cambios['Area']) ? $cambios['Area'] : array();
            $cambiosInteres = isset($cambios['Interes']) ? $cambios['Interes'] : array();
            $em = $this->getDoctrine()->getEntityManager();
            $mapNuevasAreas = array();
            $errorBorrar = array(
                'errores' => "",
                'interes_sql_errors'
            );

            //---------------------------------------------------------------
            //Area - registrar
            if (isset($cambiosArea['registrar']) && count($cambiosArea['registrar']) > 0) {
                foreach ($cambiosArea['registrar'] as $datos) {
                    $objTipo = $em->getRepository('CenfotecBDBundle:TipoContacto')->find($datos['idParent']);
                    $objArea = new \Cenfotec\BDBundle\Entity\AreaInteres();
                    $objArea->setNombre($datos['nombre']);
                    $objArea->setTipoContacto($objTipo);
                    $em->persist($objArea);

                    //Lo hacemos para generar el id en base de datos
                    $em->flush();
                    $mapNuevasAreas[$datos['idTemp']] = $objArea->getId();
                }
            }

            //------------------------------------------------------------------
            //Interes - registrar
            if (isset($cambiosInteres['registrar']) && count($cambiosInteres['registrar']) > 0) {
                foreach ($cambiosInteres['registrar'] as $datos) {
                    //Validar si el id de la Area es "A_" y buscar el verdadero id que se registro
                    if (isset($mapNuevasAreas[$datos['idParent']])) {
                        $datos['idParent'] = $mapNuevasAreas[$datos['idParent']];
                    }
                    $objArea = $em->getRepository('CenfotecBDBundle:AreaInteres')->find($datos['idParent']);
                    $objInteres = new \Cenfotec\BDBundle\Entity\Interes();
                    $objInteres->setNombre($datos['nombre']);
                    $objInteres->setArea($objArea);
                    $em->persist($objInteres);
                    $em->flush();
                }
            }

            //------------------------------------------------------------------
            //Interes - actualizar
            if (isset($cambiosInteres['actualizar']) && count($cambiosInteres['actualizar']) > 0) {
                foreach ($cambiosInteres['actualizar'] as $datos) {
                    //Validar si el id de la Area es "A_" y buscar el verdadero id que se registro
                    if (isset($mapNuevasAreas[$datos['idParent']])) {
                        $datos['idParent'] = $mapNuevasAreas[$datos['idParent']];
                    }
                    $objArea = $em->getRepository('CenfotecBDBundle:AreaInteres')->find($datos['idParent']);
                    $objInteres = $em->getRepository('CenfotecBDBundle:Interes')->find($datos['id']);
                    $objInteres->setNombre($datos['nombre']);
                    $objInteres->setArea($objArea);
                    $em->merge($objInteres);
                    $em->flush();
                }
            }

            //------------------------------------------------------------------
            //Interes - borrar
            if (isset($cambiosInteres['borrar']) && count($cambiosInteres['borrar']) > 0) {
                foreach ($cambiosInteres['borrar'] as $datos) {
                    //Validar si se cae al borrar por que tal vez tenga alguna relacion
                    try {
                        $objInteres = $em->getRepository('CenfotecBDBundle:Interes')->find($datos['id']);
                        $em->remove($objInteres);
                        $em->flush();
                    } catch (\Exception $exc) {
                        //Registrar Excepcion
                        GlobalHelper::registrarExcepcion($this, array(
                            'excepcion' => $exc->getMessage(),
                            'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
                        ));

                        if (strpos($exc->getMessage(), "Cannot delete or update a parent row: a foreign key constraint fails") !== false) {
                            $errorBorrar['errores'] .= "<p>* Interes '" . $objInteres->getArea()->getNombre() . " - " . $objInteres->getNombre() . "'.</p>";
                            
                            // Re open Entity Manager Conection
                            $container = $this->container;
                            $container->set('doctrine.orm.entity_manager', null);
                            $container->set('doctrine.orm.default_entity_manager', null);
                            $em = $this->getDoctrine()->getEntityManager();
                        } else {
                            throw $exc;
                        }
                    }
                }
            }

            //------------------------------------------------------------------
            //Area - actualizar
            if (isset($cambiosArea['actualizar']) && count($cambiosArea['actualizar']) > 0) {
                foreach ($cambiosArea['actualizar'] as $datos) {
                    $objTipo = $em->getRepository('CenfotecBDBundle:TipoContacto')->find($datos['idParent']);
                    $objArea = $em->getRepository('CenfotecBDBundle:AreaInteres')->find($datos['id']);
                    $objArea->setNombre($datos['nombre']);
                    $objArea->setTipoContacto($objTipo);
                    $em->merge($objArea);
                    $em->flush();
                }
            }

            //------------------------------------------------------------------
            //Area - borrar
            if (isset($cambiosArea['borrar']) && count($cambiosArea['borrar']) > 0) {
                foreach ($cambiosArea['borrar'] as $datos) {
                    //Validar si se cae al borrar por que tal vez tenga alguna relacion
                    try {
                        $objArea = $em->getRepository('CenfotecBDBundle:AreaInteres')->find($datos['id']);
                        $em->remove($objArea);
                        $em->flush();
                    } catch (\Exception $exc) {
                        //Registrar Excepcion
                        GlobalHelper::registrarExcepcion($this, array(
                            'excepcion' => $exc->getMessage(),
                            'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
                        ));
                        
                        if (strpos($exc->getMessage(), "Cannot delete or update a parent row: a foreign key constraint fails") !== false) {
                            $errorBorrar['errores'] .= "<p>* Area '" . $objArea->getNombre() . "'.</p>";

                            // Re open Entity Manager Conection
                            $container = $this->container;
                            $container->set('doctrine.orm.entity_manager', null);
                            $container->set('doctrine.orm.default_entity_manager', null);
                            $em = $this->getDoctrine()->getEntityManager();
                        } else {
                            throw $exc;
                        }
                    }
                }
            }
            //Validar si hubieron errores al eliminar una Area o Interes
            if (!empty($errorBorrar['errores'])) {
                return new \Symfony\Component\HttpFoundation\Response(json_encode($errorBorrar));
            } else {
                return new \Symfony\Component\HttpFoundation\Response('ok');
            }
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage());
        }
    }

    /**
     * @Route("/borrar", name="cenfo_admin_interes_borrar")
     * @Template("CenfotecCRMBundle:Administracion/Interes:borrar.html.twig")
     */
    public function borrarAction() {
        return array();
    }

}
