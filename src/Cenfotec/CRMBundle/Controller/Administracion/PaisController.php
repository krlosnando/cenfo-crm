<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\CRMBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/pais")
*/
class PaisController extends Controller
{
    /**
     * @Route("/form-registrar", name="cenfo_pais_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Administracion/Pais:formRegistrar.html.twig")
     */
    public function registrarAction()
    {
        return array();
    }

    /**
     * @Route("/form-modificar", name="cenfo_pais_form_modificar")
     * @Template("CenfotecCRMBundle:Administracion/Pais:formModificar.html.twig")
     */
    public function modificarAction()
    {
        return array();
    }

    /**
     * @Route("/borrar", name="cenfo_pais_borrar")
     * @Template("CenfotecCRMBundle:Administracion/Pais:borrar.html.twig")
     */
    public function borrarAction()
    {
        return array();
    }
}
