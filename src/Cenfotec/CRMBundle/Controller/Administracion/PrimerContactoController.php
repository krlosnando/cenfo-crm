<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/primer-contacto")
*/
class PrimerContactoController extends Controller
{
    public $entityName           = "Primer Contacto";
    public $entityClassName      = "PrimerContacto";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\PrimerContacto";
    public $entityActionName     = "primer_contacto";
    
    /**
     * @Route("/", name="cenfo_admin_primer_contacto")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => '¿Cómo se contactó? (Primer Contacto)'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_primer_contacto_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutPrimerContactoAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_primer_contacto_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataPrimerContactoAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_primer_contacto_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_primer_contacto_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_primer_contacto_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_primer_contacto_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_primer_contacto_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
