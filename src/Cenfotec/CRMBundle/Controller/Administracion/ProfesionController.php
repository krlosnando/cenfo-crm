<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/profesion")
*/
class ProfesionController extends Controller
{
    public $entityName           = "Profesión";
    public $entityClassName      = "Profesion";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\Profesion";
    public $entityActionName     = "profesion";
    
    /**
     * @Route("/", name="cenfo_admin_profesion")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => 'Prefesiones'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_profesion_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutProfesionAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_profesion_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataProfesionAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_profesion_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_profesion_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_profesion_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_profesion_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_profesion_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
