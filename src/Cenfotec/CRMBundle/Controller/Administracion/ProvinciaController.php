<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\CRMBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/provincia")
*/
class ProvinciaController extends Controller
{
    /**
     * @Route("/form-registrar", name="cenfo_provincia_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Administracion/Provincia:formRegistrar.html.twig")
     */
    public function registrarAction()
    {
        return array();
    }

    /**
     * @Route("/form-modificar", name="cenfo_provincia_form_modificar")
     * @Template("CenfotecCRMBundle:Administracion/Provincia:formModificar.html.twig")
     */
    public function modificarAction()
    {
        return array();
    }

    /**
     * @Route("/borrar", name="cenfo_provincia_borrar")
     * @Template("CenfotecCRMBundle:Administracion/Provincia:borrar.html.twig")
     */
    public function borrarAction()
    {
        return array();
    }
}
