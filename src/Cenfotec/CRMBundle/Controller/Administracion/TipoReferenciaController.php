<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/tipo-referencia")
*/
class TipoReferenciaController extends Controller
{
    public $entityName           = "Tipo Referencia";
    public $entityClassName      = "TipoReferencia";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\TipoReferencia";
    public $entityActionName     = "tipo_referencia";
    
    /**
     * @Route("/", name="cenfo_admin_tipo_referencia")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => 'Tipos de referencia'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_tipo_referencia_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutTipoReferenciaAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_tipo_referencia_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataTipoReferenciaAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_tipo_referencia_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_tipo_referencia_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_tipo_referencia_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_tipo_referencia_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_tipo_referencia_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
