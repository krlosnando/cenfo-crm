<?php

namespace Cenfotec\CRMBundle\Controller\Administracion;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\SimpleAdminEntityHelper;

/**
* @Route("/tipo-seguimiento")
*/
class TipoSeguimientoController extends Controller
{
    public $entityName           = "Tipo Seguimiento";
    public $entityClassName      = "TipoSeguimiento";
    public $entityFullClassName  = "Cenfotec\BDBundle\Entity\TipoSeguimiento";
    public $entityActionName     = "tipo_seguimiento";
    
    /**
     * @Route("/", name="cenfo_admin_tipo_seguimiento")
     */
    public function listarAction()
    {
        return SimpleAdminEntityHelper::listar($this, array(
            'pageTitle' => 'Tipos de seguimiento'
        ));
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_admin_tipo_seguimiento_grid_layout", defaults={"_format" = "json"})
     */
    public function gridLayoutTipoSeguimientoAction()
    {
        return SimpleAdminEntityHelper::gridLayout($this);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_admin_tipo_seguimiento_grid_data", defaults={"_format" = "json"})
     */
    public function gridDataTipoSeguimientoAction() 
    { 
        return SimpleAdminEntityHelper::gridData($this);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_tipo_seguimiento_form_registrar", options={"expose"=true})
     */
    public function formRegistrarAction()
    {
        return SimpleAdminEntityHelper::formRegistrar($this);
    }

    /**
     * @Route("/form-modificar", name="cenfo_tipo_seguimiento_form_modificar", options={"expose"=true})
     */
    public function formModificarAction()
    {
        return SimpleAdminEntityHelper::formModificar($this);
    }

    /**
     * @Route("/registrar", name="cenfo_tipo_seguimiento_registrar", options={"expose"=true})
     */
    public function registrarAction()
    {
        return SimpleAdminEntityHelper::registrar($this);
    }

    /**
     * @Route("/modificar", name="cenfo_tipo_seguimiento_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        return SimpleAdminEntityHelper::modificar($this);
    }
    
    /**
     * @Route("/eliminar", name="cenfo_tipo_seguimiento_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        return SimpleAdminEntityHelper::eliminar($this);
    }
}
