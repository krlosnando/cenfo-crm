<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/bitacora")
*/
class BitacoraController extends Controller
{
    /**
     * @Route("/", name="cenfo_bitacora_index")
     * @Template()
     */
    public function indexAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridBitacora');
        
        $gridOptionsGenerator
            ->setGridId('GridBitacora')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_bitacora_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_bitacora_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_bitacora_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutBitacoraAction()
    {
        $layoutGenerator = new GridLayoutGenerator();
        
        $textColumnDefault = array(
            'CanEdit'   => 0,
            'RelWidth'  => 2,
            'MinWidth'  => 70,
            'CanFilter' => 1,
            'CanSort'   => 1,
            'Type'      => "Text",
            'Align'     => 'Left',
        );
        
        $columns = array(
            array(
                'CanEdit'   => 1,
                'Width'     => 0,
                'Name'      => "id",
                'Type'      => "Text",
            ),array(
                'Width'     => 0,
                'Name'      => "Name",
                'Type'      => "Text", 
            ),array_merge(array(
                'Name' => "Evento"
            ), $textColumnDefault),
           array(
                'Name'      => "Descripcion",
                'RelWidth'  => 10,
                'CanEdit'   => 0,
                'MinWidth'  => 70,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Type'      => "Lines",
                'Height'    => 20,
                'Align'     => 'Left'
            ), array(
                'CanEdit'   => 0,
                'RelWidth'  => 2,
                'MinWidth'  => 70,
                'Name'      => "Fecha",
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Type'      => "Date", 
                'Align'     => 'Left',
            ),array_merge(array(
                'Name' => "Usuario"
            ), $textColumnDefault)
        );
        
        // Filas creadas cuando se agrupan 
        //Hacer mas grande el Grid
        $layoutGenerator->setConfigurationOption('MaxVScroll', '550');
        //Agregar el total
        $layoutGenerator->addRightColumn(array(
            'Name'       => 'Total',
            'CanFilter'  => 0,
            'Type'       => 'Int',
            'Width'      => 80,
        ));
        // Filas normales.
        $layoutGenerator->addDefaultRow(array(
            'Name'           => 'R',
            'Expanded'       => '0',
            'Calculated'     => '1',
            'CalcOrder'      => 'Total',
            'TotalType'      => 'Enum',
            'TotalFormula'   => 'count()',
            'TotalCanFilter' => '0',
            'TotalAlign'     => 'Right',
            'TotalEnum'      => '| |1', 
            'TotalIntFormat' => '0'
        ));
        // Filas con subfilas.
        $layoutGenerator->addDefaultRow(array(
            'Name'           => 'Node',
            'Expanded'       => '0',
            'Calculated'     => '1',
            'CalcOrder'      => 'Total',
            'TotalType'      => 'Enum',
            'TotalFormula'   => 'count()',
            'TotalCanFilter' => '0',
            'TotalAlign'     => 'Right',
            'TotalEnum'      => '| |1', 
            'TotalIntFormat' => '0'
        ));
        // Filas creadas cuando se agrupan 
        $layoutGenerator->addDefaultRow(array(
            'Name'            => 'Group',
            'Def'             => 'Node',
            'CanSelect'       => '0',
            'AggChildren'     => '1',
            'CanFilter'       => '2',
            'TotalFormula'    => 'count("id", 4)',
            'TotalIntFormat'  => '<i style="font-style:italic">0</i>'
        ));
        $layoutGenerator->setSolidRows(array(
            array(
                'id'    => 'Group',
                'Kind'  => 'Group',
                'Space' => '0',
            )
        ));

        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        $layoutGenerator->setVariableColumns($columns);
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_bitacora_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataBitacoraAction() 
    { 
        $em            = $this->getDoctrine()->getEntityManager();
        $bitacoras     = $em->getRepository('CenfotecSeguridadBundle:Bitacora')->findBy(array(),array('fechaEvento'=>'desc'));
        $dataFormatter = new GridDataTreePagingFormatter();
        foreach ($bitacoras as $objBitacora) {
            $dataFormatter->addRow(array(
                'id'             => $objBitacora->getId(),
                'Name'           => $objBitacora->getDescripcion(),
                'Evento'         => $objBitacora->getTipoEvento()->getNombre(),
                'Descripcion'    => $objBitacora->getDescripcion(),
                'Fecha'          => $objBitacora->getFechaEvento()->format('Y-m-d H:i'),
                'Usuario'        => $objBitacora->getUsuario()->getNombreCompleto()
            ));
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
}
