<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/busqueda")
*/
class BusquedaController extends Controller
{
    /**
     * @Route("/", name="cenfo_busqueda_index")
     * @Template("CenfotecCRMBundle:Busqueda:index.html.twig")
     */
    public function indexAction()
    {
        $d                    = $this->getDoctrine();
        $ptipoContacto        = $this->getRequest()->query->get('ptipoContacto');
        $pcriterio            = $this->getRequest()->query->get('pcriterio');
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridBusqueda');
        $intereses            = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        switch ($ptipoContacto) {
            case 'Programa Académico':
                $urlLayout = $router->generate('cenfo_prog_academico_grid_layout');
                $urlData   = $router->generate('cenfo_prog_academico_grid_data', array(
                    'ptipoBusqueda' => 'Criterio',
                    'pcriterio'     => $pcriterio
                ));
                break;

            case 'ACTI':
                $urlLayout = $router->generate('cenfo_acti_grid_layout');
                $urlData   = $router->generate('cenfo_acti_grid_data', array(
                    'ptipoBusqueda' => 'Criterio',
                    'pcriterio'     => $pcriterio
                ));
                break;
            
            case 'Posgrado':
                $urlLayout = $router->generate('cenfo_posgrado_grid_layout');
                $urlData   = $router->generate('cenfo_posgrado_grid_data', array(
                    'ptipoBusqueda' => 'Criterio',
                    'pcriterio'     => $pcriterio
                ));
                break;
            
            case 'Empresa':
                $urlLayout = $router->generate('cenfo_empresa_grid_layout');
                $urlData   = $router->generate('cenfo_empresa_grid_data', array(
                    'ptipoBusqueda' => 'Criterio',
                    'pcriterio'     => $pcriterio
                ));
                break;
            
            case 'Colegio':
                $urlLayout = $router->generate('cenfo_colegio_grid_layout');
                $urlData   = $router->generate('cenfo_colegio_grid_data', array(
                    'ptipoBusqueda' => 'Criterio',
                    'pcriterio'     => $pcriterio
                ));
                break;
            
            default:
                $urlLayout = $router->generate('cenfo_prog_academico_grid_layout');
                $urlData = "";
                break;
        }
        
        $gridOptionsGenerator
            ->setGridId('GridBusqueda')
            ->setOptions(array(
                'Layout_Url' => $urlLayout,
                'Data_Url'   => $urlData,
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwTipoContacto'         => $ptipoContacto,
            'ptwCriterio'             => $pcriterio,
            'ptwIntereses'            => $intereses,
            'ptwCountResult'          => $this->indexCountResultados($pcriterio),
            'ptwModulo'               => 'Busqueda'
        );
    }

    /**
     * @Route("/count-resultado", name="cenfo_busqueda_count_resultados", options={"expose"=true})
     */
    public function countResultadosAction()
    {
        $r                     = $this->getRequest();
        $d                     = $this->getDoctrine();
        $pcriterio             = $r->request->get('pcriterio');
        $ptipoBusqueda         = $r->request->get('ptipoBusqueda');
        $pnombre               = $r->request->get('pnombre');
        $papellido1            = $r->request->get('papellido1');
        $papellido2            = $r->request->get('papellido2');
        $pcorreo               = $r->request->get('pcorreo');
        $pnumero               = $r->request->get('pnumero');
        $pinteres              = $r->request->get('pinteres');
        $repoProgramaAcademico = $d->getRepository('CenfotecBDBundle:ProgramaAcademico');
        $repoActi              = $d->getRepository('CenfotecBDBundle:Acti');
        $repoPos               = $d->getRepository('CenfotecBDBundle:Posgrado');
        $repoEmpresa           = $d->getRepository('CenfotecBDBundle:Empresa');
        $repoColegio           = $d->getRepository('CenfotecBDBundle:Colegio');
        
        if($ptipoBusqueda == 'Filtro'){
           $cantProgAcademico = $repoProgramaAcademico->searchDataXFiltro($pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, "", "", $pinteres, true);
           $cantActi          = $repoActi->searchDataXFiltro($pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, "", "", "", "", $pinteres, true);
           $cantPos           = $repoPos->searchDataXFiltro($pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, "", "", "", "", $pinteres, true);
           $cantEmpresa       = $repoEmpresa->searchDataXFiltro($pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, "", "", $pinteres, true);
           $cantColegio       = $repoColegio->searchDataXFiltro($pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, "", "", $pinteres, true);
        }else if($ptipoBusqueda == 'Criterio'){
           $cantProgAcademico = $repoProgramaAcademico->searchDataXCriterio($pcriterio, true);
           $cantActi          = $repoActi->searchDataXCriterio($pcriterio, true);
           $cantPos           = $repoPos->searchDataXCriterio($pcriterio, true);
           $cantEmpresa       = $repoEmpresa->searchDataXCriterio($pcriterio, true);
           $cantColegio       = $repoColegio->searchDataXCriterio($pcriterio, true);
        }else{
           $cantProgAcademico = array( 0 => array ('cant' => 0)  );
           $cantActi          = array( 0 => array ('cant' => 0)  );
           $cantPos           = array( 0 => array ('cant' => 0)  );
           $cantEmpresa       = array( 0 => array ('cant' => 0)  );
           $cantColegio       = array( 0 => array ('cant' => 0)  );
        }
        
        return new Response(json_encode(array( 
            'pcantProgAcademico' => (isset($cantProgAcademico[0]['cant'])) ? $cantProgAcademico[0]['cant'] : 0,
            'pcantActi'          => (isset($cantActi[0]['cant'])) ? $cantActi[0]['cant'] : 0,
            'pcantPos'           => (isset($cantPos[0]['cant'])) ? $cantPos[0]['cant'] : 0,
            'pcantEmpresa'       => (isset($cantEmpresa[0]['cant'])) ? $cantEmpresa[0]['cant'] : 0,
            'pcantColegio'       => (isset($cantColegio[0]['cant'])) ? $cantColegio[0]['cant'] : 0,
        )));
    }
    
    /**
     * @Route("/", name="cenfo_busqueda_form_registrar")
     * @Template("CenfotecCRMBundle:Busqueda:formRegistrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $ptipoContacto = $this->getRequest()->query->get('ptipoContacto');
        return array( 
            'ptwTipoContacto' => $ptipoContacto
        );
    }

    public function indexCountResultados($pcriterio)
    {
        $r                     = $this->getRequest();
        $d                     = $this->getDoctrine();
        $repoProgramaAcademico = $d->getRepository('CenfotecBDBundle:ProgramaAcademico');
        $repoActi              = $d->getRepository('CenfotecBDBundle:Acti');
        $repoPos               = $d->getRepository('CenfotecBDBundle:Posgrado');
        $repoEmpresa           = $d->getRepository('CenfotecBDBundle:Empresa');
        $repoColegio           = $d->getRepository('CenfotecBDBundle:Colegio');
        
        if(!empty ($pcriterio)){
           $cantProgAcademico = $repoProgramaAcademico->searchDataXCriterio($pcriterio, true);
           $cantActi          = $repoActi->searchDataXCriterio($pcriterio, true);
           $cantPos           = $repoPos->searchDataXCriterio($pcriterio, true);
           $cantEmpresa       = $repoEmpresa->searchDataXCriterio($pcriterio, true);
           $cantColegio       = $repoColegio->searchDataXCriterio($pcriterio, true);
        }else{
           $cantProgAcademico = array( 0 => array('cant' => 0 ));
           $cantActi          = array( 0 => array('cant' => 0 ));
           $cantPos           = array( 0 => array('cant' => 0 ));
           $cantEmpresa       = array( 0 => array('cant' => 0 ));
           $cantColegio       = array( 0 => array('cant' => 0 ));
        }
        
        return array( 
            'pcantProgAcademico' => $cantProgAcademico[0]['cant'],
            'pcantActi'          => $cantActi[0]['cant'],
            'pcantPos'           => $cantPos[0]['cant'],
            'pcantEmpresa'       => $cantEmpresa[0]['cant'],
            'pcantColegio'       => $cantColegio[0]['cant']
        );
    }    
}
