<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\ChartHelper;
use Components\EJSTreeGridBundle\Framework\GridLayoutGenerator;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator;
use Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CRMController extends Controller
{
    /**
     * @Route("/", name="cenfo_index")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $tipoContacto = $request->get("ptipoContacto");
        $flagEliminado = $request->get("pflagEliminado");
       
        if (empty($flagEliminado)) {
            $tipoContacto = 'TODOS';
            $flagEliminado = false;
        }else{
            if ($flagEliminado == 'true') {
                $flagEliminado = true;
            }else{
                $flagEliminado = false;
            }
        }
        
        $queryParams = array(
            ':p_tipo_contacto' => $tipoContacto,
            ':p_flag_eliminado' => $flagEliminado
        );

        //Registrados por mes
        $dataChartRegistrados = $this->getDataChart("proc_report_registrados", $queryParams, array(
            'isMultiDataSerie'       => true,
            'dataKey'                => 'month',
            'dataValue'              => 'cant',
            'dataSerieGroup'         => 'year',
            'dataGroupDefaultValues' => array( 0,0,0,0,0,0,0,0,0,0,0,0 ),
            'categories'             => array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre')
        ));
        $dataChartRegistrados['idContainer']    = 'containerChartRegistrados';
        $dataChartRegistrados['tipoChart']      = 'line';
        $dataChartRegistrados['titulo']         = 'Registrados por mes';
        $dataChartRegistrados['subtitulo']      = 'Cenfo CRM';
        $dataChartRegistrados['tituloLado']     = 'Contactos registrados';
        $dataChartRegistrados['enableLengend']  = true;
                
        //Top 25 Referencias
        $dataChartTopReferencia = $this->getDataChart("proc_report_top_referencias", $queryParams, array(
            'dataSerieName' => 'Referencias',
            'dataKey'       => 'referencia',
            'dataValue'     => 'cant'
        ));
        $dataChartTopReferencia['idContainer'] = 'containerChartTopReferencia';
        $dataChartTopReferencia['tipoChart']   = 'column';
        $dataChartTopReferencia['titulo']      = 'Top 25 Referencias';
        $dataChartTopReferencia['subtitulo']   = 'Cenfo CRM';
        $dataChartTopReferencia['tituloLado']  = 'Referencias ¿Cómo se enteró?';
        
        //Top 10 Primer contacto
        $dataChartTopPrimerContacto = $this->getDataChart("proc_report_top_primer_contacto", $queryParams, array(
            'dataSerieName' => 'Primer Contacto',
            'dataKey'       => 'primer_contacto',
            'dataValue'     => 'cant'
        ));
        $dataChartTopPrimerContacto['idContainer'] = 'containerChartTopPrimerContacto';
        $dataChartTopPrimerContacto['tipoChart']   = 'column';
        $dataChartTopPrimerContacto['titulo']      = 'Top 10 Primer Contacto';
        $dataChartTopPrimerContacto['subtitulo']   = 'Cenfo CRM';
        $dataChartTopPrimerContacto['tituloLado']  = 'Primer Contacto ¿Cómo se contactó?';
        
        //Top 25 intereses
        $dataChartTopIntereses = $this->getDataChart("proc_report_top_intereses", $queryParams, array(
            'dataSerieName' => 'Intereses',
            'dataKey'       => 'interes',
            'dataValue'     => 'cant'
        ));
        $dataChartTopIntereses['idContainer'] = 'containerChartTopIntereses';
        $dataChartTopIntereses['tipoChart']   = 'column';
        $dataChartTopIntereses['titulo']      = 'Top 25 Intereses';
        $dataChartTopIntereses['subtitulo']   = 'Cenfo CRM';
        $dataChartTopIntereses['tituloLado']  = 'Intereses';
        
        return array(
            'ptwTipoContacto'               => $tipoContacto,
            'ptwFlagEliminado'              => $flagEliminado,
            'ptwDataChartRegistrados'       => $dataChartRegistrados,
            'ptwDataChartTopReferencias'    => $dataChartTopReferencia,
            'ptwDataChartTopPrimerContacto' => $dataChartTopPrimerContacto,
            'ptwDataChartTopIntereses'      => $dataChartTopIntereses
        );
    }
    
    /**
     * @Route("/listar-duplicados", name="cenfo_contacto_crm_listar_duplicados", options={"expose"=true})
     */
    public function listarDuplicadosAction()
    {
        $r               = $this->getRequest()->query;
        $ptipoContacto   = $r->get('ptipoContacto');
        $pnombre         = $r->get('pnombre');
        $pidentificacion = $r->get('pidentificacion');
        $pcorreos        = $r->get('pcorreos');
        $pnumeros        = $r->get('pnumeros'); 
        $repoContactoCRM = $this->getDoctrine()->getRepository("CenfotecBDBundle:ContactoCRM");
        $params          = array(
            'tipoContacto'   => $ptipoContacto,
            'nombre'         => $pnombre,
            'identificacion' => $pidentificacion,
            'correos'        => $pcorreos,
            'numeros'        => $pnumeros
        );
        $contactos       = $repoContactoCRM->searchDuplicados($params, true);
        
        if (count($contactos) > 0 && isset($contactos[0]["cant"]) && $contactos[0]["cant"] > 0) {
            $router               = $this->get('router');
            $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridDuplicados');

            $gridOptionsGenerator
                ->setGridId('GridDuplicados')
                ->setOptions(array(
                    'Layout_Url' => $router->generate('cenfo_contacto_crm_grid_layout', array(
                        'ptipoContacto'   => $ptipoContacto
                    )),
                    'Data_Url'   => $router->generate('cenfo_contacto_crm_grid_data', array(
                        'ptipoContacto'   => $ptipoContacto,
                        'pnombre'         => $pnombre,
                        'pidentificacion' => $pidentificacion,
                        'pcorreos'        => $pcorreos,
                        'pnumeros'        => $pnumeros
                    )),
                    'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                    'Export_Url' => $router->generate('cd_ejstreegrid_export'),
                ));

            return $this->render("CenfotecCRMBundle:CRM:listarDuplicados.html.twig", array( 
                'ptwGridOptionsGenerator' => $gridOptionsGenerator
            ));
            
        }else{
            return new \Symfony\Component\HttpFoundation\Response("No existen contactos duplicados");
        }
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_contacto_crm_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $r               = $this->getRequest();
        $ptipoContacto   = $r->get('ptipoContacto');
        $layoutGenerator = new GridLayoutGenerator();
        $layoutGenerator->setConfigurationOption('ChildPaging', 2);
        $layoutGenerator->setConfigurationOption('ChildParts', 2);
        
        $layoutGenerator->addTopRowFilter(array(
            "idType" => "Int",
            "id"     => "0" 
        ));
        
        //IDContactoCRM - IDColegio - IDEmpresa
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Text", 'Width' => 40, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "Name",           'Type' => "Text",   'Width'   => 0,  'CanExport' => 0 ),
            array( 'Name' => "Nombre",         'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Interes",        'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantInteres",    'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Numero",         'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantNumero",     'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Referencia",     'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "PrimerContacto", 'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "FechaContacto",  'Type' => "Date",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0, 'Format' => 'yyyy-MM-dd' ),
            array( 'Name' => "Estado",         'Type' => "Text",   'Width'   => 300, 'CanEdit'   => 0 )
        ));
        
        switch ($ptipoContacto) {
            case "CO":
                
            case "EM":
                break;

            //PA - ACTI - POS
            default:
                $layoutGenerator->addVariableColumn(array('Name' => "IDPA", 'Type' => "Int", 'Width' => 0, 'CanEdit' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "IDACTI", 'Type' => "Int", 'Width' => 0, 'CanEdit' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "IDPOS", 'Type' => "Int", 'Width' => 0, 'CanEdit' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "Correo", 'Type' => "Select", 'CanEdit' => 1, 'CanExport' => 1, 'Align' => 'Left', 'CaseSensitive' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "CantCorreo", 'Type' => "Int", 'Width' => 25, 'CanExport' => 1, 'Align' => "Center"));
                $layoutGenerator->addVariableColumn(array('Name' => "Empresa", 'Type' => "Text", 'Width' => 80, 'CanEdit' => 0, 'Align' => 'Left', 'CaseSensitive' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "Identificacion", 'Type' => "Text", 'Width' => 80, 'CanEdit' => 0, 'Align' => 'Left', 'CaseSensitive' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "PA", 'Type' => "Bool", 'Width' => 80, 'CanEdit' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "ACTI", 'Type' => "Bool", 'Width' => 80, 'CanEdit' => 0));
                $layoutGenerator->addVariableColumn(array('Name' => "POS", 'Type' => "Bool", 'Width' => 80, 'CanEdit' => 0));
                
                break;
        }
        
        
        $layoutGenerator->setHeaderRow(array(
            'id' => 'ID'
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_contacto_crm_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    { 
        $r               = $this->getRequest();
        $ptipoContacto   = $r->query->get('ptipoContacto');
        $pnombre         = $r->query->get('pnombre');
        $pidentificacion = $r->query->get('pidentificacion');
        $pcorreos        = $r->query->get('pcorreos');
        $pnumeros        = $r->query->get('pnumeros');
        $dataFormatter   = new GridDataTreePagingFormatter();
        $repoContactoCRM = $this->getDoctrine()->getRepository('CenfotecBDBundle:ContactoCRM');
        $params          = array(
            'tipoContacto'   => $ptipoContacto,
            'nombre'         => $pnombre,
            'identificacion' => $pidentificacion,
            'correos'        => $pcorreos,
            'numeros'        => $pnumeros
        );
        $contactos       = $repoContactoCRM->searchDuplicados($params, false);
        
        foreach ($contactos as $dataContacto) {
            
            //Obtener los intereses
            $intereses       = (!empty($dataContacto['intereses'])) ? explode('|', $dataContacto['intereses']) : array();
            $enumIntereses   = "";
            $defautInteres   = "";
            
            foreach ($intereses as $strInteres) {
                $defautInteres  = $strInteres;
                $enumIntereses .= "|" . $strInteres;
            }
            
            //Obtener los numeros
            $numeros       = (!empty($dataContacto['numeros'])) ? explode('|', $dataContacto['numeros']) : array();
            $enumNumeros   = "";
            $defautNumeros = "";
            
            foreach ($numeros as $strNumero) {
                $defautNumeros  = $strNumero;
                $enumNumeros   .= "|" . $strNumero;
            }
            
            switch ($ptipoContacto) {
                case "CO":
                    $extraColumns = array('id' => $dataContacto['colegio_id']);
                    break;
                
                case "EM":
                    $extraColumns = array('id' => $dataContacto['empresa_id']);
                    break;

                //PA - ACTI - POS
                default:
                    //Obtener los Correos
                    $correos      = (!empty($dataContacto['correos'])) ? explode('|', $dataContacto['correos']) : array();
                    $enumCorreos  = "";
                    $defautCorreo = "";

                    foreach ($correos as $strCorreo) {
                        $defautCorreo     = $strCorreo;
                        $enumCorreos .= "|" . $strCorreo;
                    }
                    
                    $extraColumns = array(
                        'id'              => $dataContacto['contacto_crm_id'],
                        'IDPA'            => $dataContacto['programa_academico_id'],
                        'IDACTI'          => $dataContacto['acti_id'],
                        'IDPOS'           => $dataContacto['posgrado_id'],
                        'Apellido1'       => $dataContacto['apellido1'],
                        'Apellido2'       => $dataContacto['apellido2'],
                        'Identificacion'  => $dataContacto['identificacion'],
                        'Empresa'         => $dataContacto['empresa'],
                        'CantCorreo'      => count($correos),
                        'Correo'          => $defautCorreo,
                        'CorreoDefaults'  => $enumCorreos,
                        'PA'              => empty($dataContacto['programa_academico_id']) ? false : true,
                        'ACTI'            => empty($dataContacto['acti_id']) ? false : true,
                        'POS'             => empty($dataContacto['posgrado_id']) ? false : true,
                    );
                    break;
            }
            
            $row = array_merge($extraColumns, array(
                'Name'            => $dataContacto['nombre'],
                'Nombre'          => $dataContacto['nombre'],
                'Referencia'      => $dataContacto['referencia'],
                'PrimerContacto'  => $dataContacto['primer_contacto'],
                'CantInteres'     => count($intereses),
                'Interes'         => $defautInteres,
                'InteresDefaults' => $enumIntereses,
                'CantNumero'      => count($numeros),
                'Numero'          => $defautNumeros,
                'NumeroDefaults'  => $enumNumeros,
                'FechaContacto'   => $dataContacto['fecha_contacto'],
                'Estado'          => $this->obtenerEstadoContacto($dataContacto, $ptipoContacto)
            ));
            
            $dataFormatter->addRow($row);
        }
        
        return array(
            'gridDataFormatter' => $dataFormatter
        );
    }
    
    /**
     * Funcion que se encarga de obtener la información para el Grafico.
     */
    public function getDataChart($procName, $queryParameters, $options)
    { 
        $objChart   = new ChartHelper($this->getDoctrine());
        $query      = "call $procName("; 
        
        foreach ($queryParameters as $key => $value) {
            $query .= $key . ",";
        }
        
        if (count($queryParameters) > 0) {
            //Eliminamos la ultima coma.
            $query  = substr($query,0,-1);
        }
        $query .= ")";
        
        $dataResult = $objChart->getDataChart($query, $queryParameters, $options);
        return $dataResult;
    }

    /**
     * @Route("/ver-perfil", name="cenfo_contacto_crm_ver_perfil", options={"expose"=true})
     * @Template("CenfotecCRMBundle:CRM:verPerfil.html.twig")
     */
    public function verPerfilAction()
    {
        $r            = $this->getRequest();
        $d            = $this->getDoctrine();
        $idContacto   = $r->query->get('pidContactoCRM');
        $objContacto  = $d->getRepository('CenfotecBDBundle:ContactoCRM')->find($idContacto);
        
        return array(
            'ptwObjContacto' => $objContacto
        );
    }

    public function obtenerEstadoContacto($dataContacto, $tipoContacto)
    {
        $estado = "";
        $eliminado = "";
        
        switch ($tipoContacto) {
            case "CO":
                if ($dataContacto['flag_eliminado'] == "1") {
                    $eliminado .= "Colegio, ";
                }
                break;

            case "EM":
                if ($dataContacto['flag_eliminado'] == "1") {
                    $eliminado .= "Empresa, ";
                }
                break;

            //PA - ACTI - POS
            default:
                if ($dataContacto['eliminado_pa'] == "1") {
                    $eliminado .= "programa academico, ";
                }
                if ($dataContacto['eliminado_acti'] == "1") {
                    $eliminado .= "acti, ";
                }
                if ($dataContacto['eliminado_pos'] == "1") {
                    $eliminado = "posgrado, ";
                }
                break;
        }
        
        //Eliminamos el ultimo ", " que se concateno mal.
        $eliminado  = substr($eliminado,0,-2);
        if (!empty($eliminado)) {
            $estado = "Eliminado en " . $eliminado . ".";
        }else{
            $estado = "Contacto activo.";
        }
        return $estado;
    }
}
