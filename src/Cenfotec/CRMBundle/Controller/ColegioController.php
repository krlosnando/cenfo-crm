<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter,
    Cenfotec\CRMBundle\Clases\GlobalHelper;
use Cenfotec\BDBundle\Entity\ProgramaAcademico;
use Cenfotec\BDBundle\Entity\ContactoCRMXNumero;
use Cenfotec\BDBundle\Entity\ContactoCRMXInteres;
use Cenfotec\BDBundle\Entity\Correo;
use Cenfotec\BDBundle\Entity\Numero;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/colegio")
*/
class ColegioController extends Controller
{
    /**
     * @Route("/", name="cenfo_colegio_listar")
     * @Template()
     */
    public function listarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridColegio');
        
        $gridOptionsGenerator
            ->setGridId('GridColegio')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_colegio_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_colegio_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('CO')
        );
    }
    
    /**
     * @Route("/eliminados", name="cenfo_colegio_listar_eliminados")
     * @Template("CenfotecCRMBundle:Colegio:listar.html.twig")
     */
    public function listarEliminadosAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridColegio');
        
        $gridOptionsGenerator
            ->setGridId('GridColegio')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_colegio_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_colegio_grid_data', array(
                    'pflagEliminados' => '1'
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('CO'),
            'ptwModulo'               => 'Eliminados'
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_colegio_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $r                        = $this->getRequest();
        $router                   = $this->get('router');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $layoutGenerator          = new GridLayoutGenerator();
        $ColumWidthID             = (!empty($pflagComplexTable)) ? 0 : 40;
        $ColumWidthNombre         = (!empty($pflagComplexTable)) ? 150 : 80;
        
        $layoutGenerator->setConfigurationOption('ChildPaging', 2);
        $layoutGenerator->setConfigurationOption('ChildParts', 2);
        $layoutGenerator->addTopRowFilter(array(
            "idType" => "Int",
            "id"     => ""
        ));
        
        //IDContactoCRM
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Text", 'Width' => $ColumWidthID, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "Name",               'Type' => "Text",   'Width'      => 0,  'CanExport' => 0 ),
            array( 'Name' => "CNombre",            'Type' => "Text",   'Width'      => $ColumWidthNombre, 'CanEdit'   => 0 ),
            array( 'Name' => "CProvincia",         'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CCanton",            'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CTipo",              'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CClase",             'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CInteres",           'Type' => "Select", 'CanEdit'    => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CCantInteres",       'Type' => "Int",    'Width'      => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "CReferencia",        'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CPrimerContacto",    'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CPTotal",            'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CPTotalUltimoNivel", 'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0 ),
            array( 'Name' => "CNumero",            'Type' => "Select", 'CanEdit'    => 1,  'CanExport' => 1 ),
            array( 'Name' => "CCantNumero",        'Type' => "Int",    'Width'      => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "CContacto",          'Type' => "Select", 'RelWidth'   => 10, 'CanEdit'   => 1, 'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CCantContacto",      'Type' => "Int",    'RelWidth'   => 2,  'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "CComentario",        'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 )
        ));
        
        $layoutGenerator->addRightColumn(array( 
            'Name' => 'Seg', 'CanEdit' => 0, 'Width' => 30, 'CanFilter' => 1, 'CanSort' => 1, 'Type' => "Int", 'Align' => 'Center'
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id'                 => 'ID',
            'CNombre'            => "Nombre",
            'CProvincia'         => "Provincia",
            'CCanton'            => "Canton",
            'CTipo'              => "Tipo",
            'CClase'             => "Clase",
            'CInteres'           => "Interes",
            'CCantInteres'       => "CantInteres",
            'CReferencia'        => "Referencia",
            'CPrimerContacto'    => "PrimerContacto",
            'CPTotal'            => "Población Total",
            'CPTotalUltimoNivel' => "Población total ultimo nivel",
            'CNumero'            => "Numero",
            'CCantNumero'        => "CantNumero",
            'CContacto'          => "Contacto",
            'CCantContacto'      => "CCantContacto",
            'CComentario'        => "Comentario"
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,BtnDownload,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
            
            'BtnDownload'             => "1",
            'BtnDownloadButtonText'   => "Descargar todos los registrados en Excel",
            'BtnDownloadType'         => "Button",
            'BtnDownloadOnClick'      => "$.downloadExcel('" . $router->generate('cenfo_colegio_download') . "', null)"
        ));
        
        //Solo si es Telemercadeo.
        if($pflagTelemercadeoAsignar == 1){
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 80,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'Encargado',
                'Type'      => "Text",
                'Align'     => 'Left',
            ));
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 100,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'UltimaLlamada',
                'Type'      => "Date",
                'Align'     => 'Left',
            ));
        }
        
        //Solo si es ComplexTable
        if($pflagComplexTable == 1){
            //Hacer mas grande el Grid
            $layoutGenerator->setConfigurationOption('MaxVScroll', '550');
            //Agregar el total
            $layoutGenerator->addRightColumn(array(
                'Name'       => 'Total',
                'CanFilter'  => 0,
                'Type'       => 'Int',
                'Width'      => 80,
            ));
            // Filas normales.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'R',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas con subfilas.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'Node',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas creadas cuando se agrupan 
            $layoutGenerator->addDefaultRow(array(
                'Name'            => 'Group',
                'Def'             => 'Node',
                'CanSelect'       => '0',
                'AggChildren'     => '1',
                'CanFilter'       => '2',
                'TotalFormula'    => 'count("id", 4)',
                'TotalIntFormat'  => '<i style="font-style:italic">0</i>'
            ));
            $layoutGenerator->setSolidRows(array(
                array(
                    'id'    => 'Group',
                    'Kind'  => 'Group',
                    'Space' => '0',
                )
            ));
        }
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/download", name="cenfo_colegio_download")
     */
    public function downloadAction() 
    {  
        ini_set('memory_limit', '-1');
        $dateTime = new \DateTime();
        $repoColegio = $this->getDoctrine()->getRepository('CenfotecBDBundle:Colegio');
        $contactos = $repoColegio->searchAll(true);
        $cantCorreos = 2;
        $cantNumeros = 2;
        $cantIntereses = 2;
        $cantSeguimientos = 3;
        $cantContactos = 1;
        
        $fileName = 'CRM Colegios ' . date_format($dateTime, 'Y-m-d H_i_s');
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setStatusCode(200);

        $response->headers->set('Content-Type', "application/vnd.ms-excel");
        $response->headers->set('Cache-Control', "must-revalidate");
        $response->headers->set('Set-Cookie', "fileDownload=true; path=/");
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s.xls"', $fileName));
        
        $response->setContent($this->renderView(
            "CenfotecCRMBundle:Colegio:download.xls.twig", array(
                "contactos"        => $contactos,
                "cantCorreos"      => $cantCorreos,
                "cantNumeros"      => $cantNumeros,
                "cantIntereses"    => $cantIntereses,
                "cantSeguimientos" => $cantSeguimientos,
                "cantContactos"    => $cantContactos
             )
        ));
        
        return $response;
    }
    
    /**
     * @Route("/grid-data", name="cenfo_colegio_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    {
        $r                        = $this->getRequest();
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $pflagEliminados          = $r->query->get('pflagEliminados');
        $ptipoBusqueda            = $r->query->get('ptipoBusqueda');
        $pcriterio                = $r->query->get('pcriterio');
        $pnombre                  = $r->query->get('pnombre');
        $papellido1               = $r->query->get('papellido1');
        $papellido2               = $r->query->get('papellido2');
        $pcorreo                  = $r->query->get('pcorreo');
        $pnumero                  = $r->query->get('pnumero');
        $pfechaInicioContacto     = $r->query->get('pfechaInicioContacto');
        $pfechaFinContacto        = $r->query->get('pfechaFinContacto');
        $pinteres                 = $r->query->get('pinteres');
        $dataFormatter            = new GridDataTreePagingFormatter();
        $repoColegio              = $this->getDoctrine()->getRepository('CenfotecBDBundle:Colegio');
        
        if(!empty($pflagEliminados) && $pflagEliminados == "1"){
            $colegios = $repoColegio->searchAllEliminados();
        }else if($ptipoBusqueda == 'Filtro'){
            $colegios = $repoColegio->searchDataXFiltro(
                    $pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, 
                    $pfechaInicioContacto, $pfechaFinContacto, $pinteres);
        }else if($ptipoBusqueda == 'Criterio'){
            $colegios = $repoColegio->searchDataXCriterio($pcriterio);
        }else{
            $colegios = $repoColegio->searchAll();
        }
        
        $dateTime = new \DateTime();
        
        foreach ($colegios as $dataColegio) {
            //Obtener los numeros del colegio
            $numeros       = (!empty($dataColegio['numeros'])) ? explode('|', $dataColegio['numeros']) : array();
            $enumNumeros   = "";
            $defautNumeros = "";
            
            foreach ($numeros as $strNumero) {
                $defautNumeros  = $strNumero;
                $enumNumeros   .= "|" . $strNumero;
            }
            
            //Obtener los intereses
            $intereses       = (!empty($dataColegio['intereses'])) ? explode('|', $dataColegio['intereses']) : array();
            $enumIntereses   = "";
            $defautInteres   = "";
            
            foreach ($intereses as $strInteres) {
                $defautInteres  = $strInteres;
                $enumIntereses .= "|" . $strInteres;
            }
             
            //Obtener los contactos de la colegio
            $contactos          = (!empty($dataColegio['contactos'])) ? explode('|', $dataColegio['contactos']) : array();
            $enumContactos      = "";
            $defautContactactos = "";
            
            foreach ($contactos as $strContacto) {
                $defautContactactos  = $strContacto;
                $enumContactos .= "|" . $strContacto;
            }
            
            $row = array(
                'id'                 => $dataColegio['colegio_id'],
                'Name'               => $dataColegio['nombre'],
                'CNombre'            => $dataColegio['nombre'],
                'CProvincia'         => $dataColegio['provincia'],
                'CCanton'            => $dataColegio['canton'],
                'CTipo'              => $dataColegio['tipo'],
                'CClase'             => $dataColegio['clase'],
                'CInteres'           => $defautInteres,
                'CInteresDefaults'   => $enumIntereses,
                'CCantInteres'       => count($intereses),
                'CReferencia'        => $dataColegio['referencia'],
                'CPrimerContacto'    => $dataColegio['primer_contacto'],
                'CPTotal'            => $dataColegio['poblacion_total'],
                'CPTotalUltimoNivel' => $dataColegio['poblacion_ultimo_nivel'],
                'CNumero'            => $defautNumeros,
                'CNumeroDefaults'    => $enumNumeros,
                'CCantNumero'        => count($numeros),
                'CContacto'          => $defautContactactos,
                'CContactoDefaults'  => $enumContactos,
                'CCantContacto'       => count($contactos),
                'CComentario'        => $dataColegio['comentario'],
                'Seg'                => $dataColegio['cant_seguimientos']
            );

            //Solo si es Telemercadeo.
            if($pflagTelemercadeoAsignar == 1){
                $row['Encargado']     = $dataColegio['colegio_id'] ? 'CDominguez' : 'No';
                $row['UltimaLlamada'] = $dataColegio['colegio_id'] ? $dateTime->format('Y/m/d H:i') : '';
                if($row['Encargado'] != 'No'){
                    $row['Color'] = '#e8ffcd';
                }
            }
            $dataFormatter->addRow($row);
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_colegio_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Colegio:formAdministrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $actividades          = $d->getRepository('CenfotecBDBundle:Actividad')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $tiposColegio         = $d->getRepository('CenfotecBDBundle:TipoColegio')->findBy(array(), array('nombre'=>'asc'));
        $clases               = $d->getRepository('CenfotecBDBundle:ClaseColegio')->findBy(array(), array('nombre'=>'asc'));
        $especialidades       = $d->getRepository('CenfotecBDBundle:EspecialidadColegio')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => 'Registrar nuevo contacto de Colegio',
            'ptwAction'               => 'Registrar',
            'ptwFormAction'           => $router->generate('cenfo_colegio_registrar'),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwPuestos'               => $puestos,
            'ptwActividades'          => $actividades,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwTiposColegio'         => $tiposColegio,
            'ptwEspecialidades'       => $especialidades,
            'ptwClases'               => $clases,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto
        );
    }

    /**
     * @Route("/form-modificar", name="cenfo_colegio_form_modificar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Colegio:formAdministrar.html.twig")
     */
    public function formModificarAction()
    {   
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $pidColegio           = $this->getRequest()->query->get('pidColegio');
        $objColegio           = $d->getRepository('CenfotecBDBundle:Colegio')->find($pidColegio);
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $actividades          = $d->getRepository('CenfotecBDBundle:Actividad')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $tiposColegio         = $d->getRepository('CenfotecBDBundle:TipoColegio')->findBy(array(), array('nombre'=>'asc'));
        $clases               = $d->getRepository('CenfotecBDBundle:ClaseColegio')->findBy(array(), array('nombre'=>'asc'));
        $especialidades       = $d->getRepository('CenfotecBDBundle:EspecialidadColegio')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => "Modificar ID:" . $objColegio->getId() . " " . $objColegio->getNombre(),
            'ptwAction'               => 'Modificar',        
            'ptwFormAction'           => $router->generate('cenfo_colegio_modificar', array('pidColegio'=>$pidColegio)),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwPuestos'              => $puestos,
            'ptwActividades'          => $actividades,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwTiposColegio'         => $tiposColegio,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwEspecialidades'       => $especialidades,
            'ptwClases'               => $clases,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwObjColegio'           => $objColegio
        );
    }
    
    /**
     * @Route("/registrar", name="cenfo_colegio_registrar")
     */
    public function registrarAction()
    {
        try{
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $data           = $r->request->get('data');
            $objColegio     = new \Cenfotec\BDBundle\Entity\Colegio();
            $currentUser    = $this->getUser();
            
            $objColegio->setNombre($data["nombre"]);
            $objColegio->setDireccion($data["direccion"]);
            $objColegio->setDirector($data["director"]);
            $objColegio->setPoblacionTotal($data["poblacionTotal"]);
            $objColegio->setPoblacionUltimoNivel($data["poblacionUltimoNivel"]);
            $objColegio->setFechaContacto(new \DateTime($data["fechaContacto"]));
            $objColegio->setComentario($data["comentario"]);
            $objColegio->setUsuarioRegistroBD($currentUser);
            
            if(!empty($data["canton"])){
                $objCanton = $em->getRepository('CenfotecBDBundle:Canton')->find($data["canton"]);
                $objColegio->setCanton($objCanton);
            }
            if(!empty($data["tipo"])){
                $objTipo = $em->getRepository('CenfotecBDBundle:TipoColegio')->find($data["tipo"]);
                $objColegio->setTipo($objTipo);
            }
            if(!empty($data["clase"])){
                $objClase = $em->getRepository('CenfotecBDBundle:ClaseColegio')->find($data["clase"]);
                $objColegio->setClase($objClase);
                
                //Si no es Técnico remover la especialidad.
                if($objClase->getNombre() != 'Técnico'){
                    $data['especialidad'] = "";
                    $objColegio->setEspecialidad(null);
                }
            }
            if(!empty($data['especialidad'])){
                $objEspecialidad = $em->getRepository('CenfotecBDBundle:EspecialidadColegio')->find($data['especialidad']);
                $objColegio->setEspecialidad($objEspecialidad);
            }
            if(!empty($data["referencia"])){
                $objReferencia = $em->getRepository('CenfotecBDBundle:Referencia')->find($data["referencia"]);
                $objColegio->setReferencia($objReferencia);
            }
            if(!empty($data["primerContacto"])){
                $objPrimerContacto = $em->getRepository('CenfotecBDBundle:PrimerContacto')->find($data["primerContacto"]);
                $objColegio->setPrimerContacto($objPrimerContacto);
            }
            
            GlobalHelper::actualizarNumeros($objColegio, $data["numeros"], $em);
            GlobalHelper::actualizarIntereses($objColegio, $data["intereses"], $em);
            GlobalHelper::actualizarContactosBasicos($objColegio, $data["contactos"], $em);
            
            $em->persist($objColegio);
            $em->flush();
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "La Colegio 'ID:" . $objColegio->getId() . " " . $objColegio->getNombre() . "' se registro correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_colegio_form_modificar', array(
                    'pidColegio' => $objColegio->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/modificar", name="cenfo_colegio_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        try{
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $data           = $r->request->get('data');
            $idColegio      = $r->query->get('pidColegio');
            $idContactoCRM  = $r->query->get('pidContactoCRM');
            
            if (!empty($idContactoCRM)) {
                $idColegio = $idContactoCRM;
            }
            
            $objColegio     = $em->getRepository('CenfotecBDBundle:Colegio')->find($idColegio);
            $currentUser    = $this->getUser();
            
            $objColegio->setNombre($data["nombre"]);
            $objColegio->setDireccion($data["direccion"]);
            $objColegio->setDirector($data["director"]);
            $objColegio->setPoblacionTotal($data["poblacionTotal"]);
            $objColegio->setPoblacionUltimoNivel($data["poblacionUltimoNivel"]);
            $objColegio->setFechaContacto(new \DateTime($data["fechaContacto"]));
            $objColegio->setComentario($data["comentario"]);
            $objColegio->setUsuarioModificoBD($currentUser);
            $objColegio->setFechaModificoBD(new \DateTime());
                    
            if(!empty($data["canton"])){
                $objCanton = $em->getRepository('CenfotecBDBundle:Canton')->find($data["canton"]);
                $objColegio->setCanton($objCanton);
            }else{
                $objColegio->setCanton(null);
            }
            if(!empty($data["tipo"])){
                $objTipo = $em->getRepository('CenfotecBDBundle:TipoColegio')->find($data["tipo"]);
                $objColegio->setTipo($objTipo);
            }else{
                $objColegio->setTipo(null);
            }
            if(!empty($data["clase"])){
                $objClase = $em->getRepository('CenfotecBDBundle:ClaseColegio')->find($data["clase"]);
                $objColegio->setClase($objClase);
                
                //Si no es Técnico remover la especialidad.
                if($objClase->getNombre() != 'Técnico'){
                    $data['especialidad'] = "";
                    $objColegio->setEspecialidad(null);
                }
            }else{
                $objColegio->setClase(null);
                $objColegio->setEspecialidad(null);
            }
            if(!empty($data['especialidad'])){
                $objEspecialidad = $em->getRepository('CenfotecBDBundle:EspecialidadColegio')->find($data['especialidad']);
                $objColegio->setEspecialidad($objEspecialidad);
            }else{
                $objColegio->setEspecialidad(null);
            }
            if(!empty($data["referencia"])){
                $objReferencia = $em->getRepository('CenfotecBDBundle:Referencia')->find($data["referencia"]);
                $objColegio->setReferencia($objReferencia);
            }else{
                $objColegio->setReferencia(null);
            }
            if(!empty($data["primerContacto"])){
                $objPrimerContacto = $em->getRepository('CenfotecBDBundle:PrimerContacto')->find($data["primerContacto"]);
                $objColegio->setPrimerContacto($objPrimerContacto);
            }else{
                $objColegio->setPrimerContacto(null);
            }
            
            if(isset($data["numeros"])){
                GlobalHelper::actualizarNumeros($objColegio, $data["numeros"], $em);
            }
            if(isset($data["intereses"])){
                GlobalHelper::actualizarIntereses($objColegio, $data["intereses"], $em);
            }
            if(isset($data["contactos"])){
                GlobalHelper::actualizarContactosBasicos($objColegio, $data["contactos"], $em);
            }
            
            //Establecer el Contacto como activo si estaba eliminado.
            $objColegio->setFlagEliminado(false);
            
            //Guardar los cambios en base de datos
            $em->flush();
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "La Colegio 'ID:" . $objColegio->getId() . " " . $objColegio->getNombre() . "' se modifico correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_colegio_form_modificar', array(
                    'pidColegio' => $objColegio->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    /**
     * @Route("/eliminar", name="cenfo_colegio_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        try {
            $r          = $this->getRequest();
            $em         = $this->getDoctrine()->getEntityManager();
            $pidColegio = $r->get('pidColegio');
            $msg        = "";
            $objColegio = $em->getRepository('CenfotecBDBundle:Colegio')->find($pidColegio);
            
            $objColegio->setFlagEliminado(true);
            $em->merge($objColegio);
            $em->flush();
            
            $msg = "El contacto de Colegio 'ID:" . $objColegio->getId() . " " . $objColegio->getNombre() . "' se ha eliminado correctamente.";
            $descripcion = "Colegio ID:" . $objColegio->getId() . " " . $objColegio->getNombre() . ".";
            
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_ELIMINAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $this->getUser()
            ));
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objColegio->getId(),
                'status'   => 'ok'
            ))); 
            
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/ver-perfil", name="cenfo_colegio_ver_perfil", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Colegio:verPerfil.html.twig")
     */
    public function verPerfilAction()
    {
        $r        = $this->getRequest();
        $d        = $this->getDoctrine();
        $pidColegio  = $r->query->get('pidColegio');
        $objColegio  = $d->getRepository('CenfotecBDBundle:Colegio')->find($pidColegio);
        
        return array(
            'ptwObjColegio' => $objColegio
        );
    }

    /**
     * @Route("/copy-to-prog-academico", name="cenfo_colegio_copy_to_prog_academico", options={"expose"=true})
     */
    public function copyToProgAcademicoAction()
    {
        try{
            $r        = $this->getRequest()->request;
            $idColegio   = $r->get('pidColegio');
            $msg      = "";

            if(!empty($idColegio)){
                $em               = $this->getDoctrine()->getEntityManager();
                $objColegio          = $em->getRepository('CenfotecBDBundle:Colegio')->find($idColegio);
                $objContactoCRM   = $objColegio->getContactoCRM();

                //Validar si ya existe el ContactoCRM en Programa Academico.
                $objProgAcademico = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->findBy(array('contactoCRM'=>$objContactoCRM->getId()));
                if($objProgAcademico == null){
                    $objProgAcademico = new ProgramaAcademico();
                    $objProgAcademico->setContactoCRM($objContactoCRM);
                    $em->persist($objProgAcademico);
                    $em->flush();

                    $msg = "El contacto de Colegio con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se copió correctamente a la BD de Programa Académico.";
                }else{
                    $msg = "<p>- El contacto de Colegio con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' ya se encuentra actualmente en la BD de Programa Académico.</p>";
                    return new \Symfony\Component\HttpFoundation\Response($msg); 
                }
            }else{
                $msg = "<p>- Identificador del contacto Colegio es nulo.</p>";
                return new \Symfony\Component\HttpFoundation\Response($msg); 
            }

            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'    => $msg,
                'status' => "ok"
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
}
