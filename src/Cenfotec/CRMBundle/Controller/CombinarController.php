<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/combinar")
*/
class CombinarController extends Controller
{
    
    /**
     * @Route("/form", name="cenfo_combinar_form", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Combinar:formCombinar.html.twig")
     */
    public function formCombinarAction() {
        $request             = $this->getRequest();
        $entityName          = $request->query->get('pentityName');          //"Primer Contacto"
        $entityClassName     = $request->query->get('pentityClassName');     //"PrimerContacto"
        $entityActionName    = $request->query->get('pentityActionName');    //"primer_contacto"
        $datosCombinar       = $request->query->get("pdatosCombinar");
        $elementosExistentes = array();
        
        switch ($entityClassName) {
            case "Area":
                $tipos = $this->getDoctrine()->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
                foreach ($tipos as $objTipo) {
                    foreach ($objTipo->getAreasInteres() as $objArea) {
                        $elementosExistentes[] = array(
                            "id"     => $objArea->getId(),
                            "nombre" => $objTipo->getNombre() . ' / ' . $objArea->getNombre() 
                        );
                    }
                }

                break;

            case "Interes":
                $tipos = $this->getDoctrine()->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
                foreach ($tipos as $objTipo) {
                    foreach ($objTipo->getAreasInteres() as $objArea) {
                        foreach ($objArea->getIntereses() as $objInteres) {
                            $elementosExistentes[] = array(
                                "id"     => $objInteres->getId(),
                                "nombre" => $objTipo->getNombre() . ' / ' . $objArea->getNombre() . ' / ' . $objInteres->getNombre()
                            );
                        }
                    }
                }
                break;
            
            default:
                $datos = $this->getDoctrine()->getRepository('CenfotecBDBundle:' . $entityClassName )->findBy(array(), array('nombre'=>'asc'));

                foreach ($datos as $objEntity) {
                    $elementosExistentes[] = array(
                        "id"     => $objEntity->getId(),
                        "nombre" => $objEntity->getNombre()
                    );
                }
                break;
        }
        
        return array(
            'ptwElementosExistentes' => $elementosExistentes,
            'ptwEntityActionName'    => $entityActionName,
            'ptwEntityName'          => $entityName,
            'ptwEntityClassName'     => $entityClassName,
            'ptwDatosCombinar'       => $datosCombinar
        );
    }

    /**
     * @Route("/guardar", name="cenfo_combinar_guardar", options={"expose"=true})
     */
    public function guardar(){
        try {
            $request               = $this->getRequest()->request;
            $datosCombinar         = $request->get("pdatosCombinar");
            $idElementoNuevaInfo   = $request->get("pidElementoNuevaInfo");
            $entityClassName       = $request->get("pentityClassName");
            $entityActionName      = $request->get("pentityActionName");
            $repoEntity            = $this->getDoctrine()->getRepository('CenfotecBDBundle:' . $entityClassName );
            $em                    = $this->getDoctrine()->getEntityManager();
            $conn                  = $em->getConnection();

            switch ($entityClassName) {
                case "Area":
                    $this->combinarAreas($datosCombinar, $idElementoNuevaInfo, $repoEntity, $em, $conn);
                    break;

                case "Interes":
                    $this->combinarInteres($datosCombinar, $idElementoNuevaInfo, $repoEntity, $em, $conn);
                    break;

                default:
                    $this->combinarSimpleEntity($datosCombinar, $idElementoNuevaInfo, $entityActionName, $repoEntity, $em, $conn);
                    break;
            }
        
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status' => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            \Cenfotec\CRMBundle\Clases\GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    public function combinarAreas($datosCombinar, $idElementoNuevaInfo, $repoEntity, $em, $conn){
        foreach ($datosCombinar as $idElementoABorrar) {
            $query = " CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_TABLE AS (SELECT tccxi.id FROM t_contacto_crm_x_interes tccxi WHERE interes_id = $idElementoNuevaInfo);

                       UPDATE t_contacto_crm_x_interes ccxi
                       SET ccxi.interes_id = $idElementoNuevaInfo
                       WHERE ccxi.interes_id = $idElementoABorrar and ccxi.id not in (
                            SELECT tccxi.id
                            FROM TEMP_TABLE tccxi
                        );";

            $statement = $conn->prepare($query);
            $statement->execute();
            $statement->closeCursor();

            if ($idElementoABorrar != $idElementoNuevaInfo) {
                $em->remove($repoEntity->find($idElementoABorrar));
                $em->flush();
            }
        }
    }
    
    public function combinarInteres($datosCombinar, $idElementoNuevaInfo, $repoEntity, $em, $conn){
        foreach ($datosCombinar as $idElementoABorrar) {
            $query = " CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_TABLE AS (SELECT tccxi.id FROM t_contacto_crm_x_interes tccxi WHERE interes_id = $idElementoNuevaInfo);

                       UPDATE t_contacto_crm_x_interes ccxi
                       SET ccxi.interes_id = $idElementoNuevaInfo
                       WHERE ccxi.interes_id = $idElementoABorrar and ccxi.id not in (
                            SELECT tccxi.id
                            FROM TEMP_TABLE tccxi
                        );";

            $statement = $conn->prepare($query);
            $statement->execute();
            $statement->closeCursor();

            if ($idElementoABorrar != $idElementoNuevaInfo) {
                $em->remove($repoEntity->find($idElementoABorrar));
                $em->flush();
            }
        }
    }

    public function combinarSimpleEntity($datosCombinar, $idElementoNuevaInfo, $actionName, $repoEntity, $em, $conn){
        foreach ($datosCombinar as $idElementoABorrar) {
            switch ($entityClassName) {
                case "primer_contacto":
                    $query = " 
                       CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_TABLE AS (SELECT t.id FROM t_contacto_crm t WHERE t.primer_contacto_id = $idElementoNuevaInfo);

                       UPDATE t_contacto_crm ccxi
                       SET ccxi.primer_contacto_id = $idElementoNuevaInfo
                       WHERE ccxi.primer_contacto_id = $idElementoABorrar and ccxi.id not in (
                            SELECT t.id
                            FROM TEMP_TABLE t
                        );";
                    break;
                
                case "actividad_empresa":
                    $query = " 
                       CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_TABLE AS (SELECT t.id FROM t_empresa t WHERE t.actividad_id = $idElementoNuevaInfo);

                       UPDATE t_empresa ccxi
                       SET ccxi.actividad_id = $idElementoNuevaInfo
                       WHERE ccxi.actividad_id = $idElementoABorrar and ccxi.id not in (
                            SELECT t.id
                            FROM TEMP_TABLE t
                        );";
                    break;
                
                default:
                    $query = "
                       CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_TABLE AS (SELECT t.id FROM t_contacto_crm t WHERE t." . $actionName . "_id = $idElementoNuevaInfo);

                       UPDATE t_contacto_crm ccxi
                       SET ccxi." . $actionName . "_id = $idElementoNuevaInfo
                       WHERE ccxi." . $actionName . "_id = $idElementoABorrar and ccxi.id not in (
                            SELECT t.id
                            FROM TEMP_TABLE t
                        );";
                    break;
            }

            $statement = $conn->prepare($query);
            $statement->execute();
            $statement->closeCursor();

            if ($idElementoABorrar != $idElementoNuevaInfo) {
                $em->remove($repoEntity->find($idElementoABorrar));
                $em->flush();
            }
        }
    }
}
