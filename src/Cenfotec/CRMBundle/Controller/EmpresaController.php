<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter,
    Cenfotec\CRMBundle\Clases\GlobalHelper;
use Cenfotec\BDBundle\Entity\ProgramaAcademico;
use Cenfotec\BDBundle\Entity\ContactoCRMXNumero;
use Cenfotec\BDBundle\Entity\ContactoCRMXInteres;
use Cenfotec\BDBundle\Entity\Correo;
use Cenfotec\BDBundle\Entity\Numero;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/empresa")
*/
class EmpresaController extends Controller
{
    /**
     * @Route("/", name="cenfo_empresa_listar")
     * @Template()
     */
    public function listarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridEmpresa');
        
        $gridOptionsGenerator
            ->setGridId('GridEmpresa')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_empresa_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_empresa_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('EM')
        );
    }
    
    /**
     * @Route("/eliminados", name="cenfo_empresa_listar_eliminados")
     * @Template("CenfotecCRMBundle:Empresa:listar.html.twig")
     */
    public function listarEliminadosAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridEmpresa');
        
        $gridOptionsGenerator
            ->setGridId('GridEmpresa')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_empresa_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_empresa_grid_data', array(
                    'pflagEliminados' => '1'
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('EM'),
            'ptwModulo'               => 'Eliminados'
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_empresa_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $r                        = $this->getRequest();
        $router                   = $this->get('router');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $layoutGenerator          = new GridLayoutGenerator();
        $ColumWidthID             = (!empty($pflagComplexTable)) ? 0 : 40;
        $ColumWidthNombre         = (!empty($pflagComplexTable)) ? 150 : 80;
        
        $layoutGenerator->setConfigurationOption('ChildPaging', 2);
        $layoutGenerator->setConfigurationOption('ChildParts', 2);
        $layoutGenerator->addTopRowFilter(array(
            "idType" => "Int",
            "id"     => ""
        ));
        
        //IDContactoCRM
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Text", 'Width' => $ColumWidthID, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "Name",            'Type' => "Text",   'Width'      => 0,  'CanExport' => 0 ),
            array( 'Name' => "CNombre",         'Type' => "Text",   'Width'      => $ColumWidthNombre, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CActividad",      'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CInteres",        'Type' => "Select", 'RelWidth'   => 10, 'CanEdit'   => 1, 'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CCantInteres",    'Type' => "Int",    'RelWidth'   => 2,  'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "CReferencia",     'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CPrimerContacto", 'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CComentario",     'Type' => "Text",   'RelWidth'   => 10, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CContacto",       'Type' => "Select", 'RelWidth'   => 10, 'CanEdit'   => 1, 'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CCantContacto",   'Type' => "Int",    'RelWidth'   => 2,  'CanExport' => 1, 'Align' => "Center" ),
        ));
        
        $layoutGenerator->addRightColumn(array( 
            'Name' => 'Seg', 'CanEdit' => 0, 'Width' => 30, 'CanFilter' => 1, 'CanSort' => 1, 'Type' => "Int", 'Align' => 'Center'
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id'              => 'ID',
            "CNombre"         => "Nombre",
            "CActividad"      => "Actividad",
            "CInteres"        => "Interes",
            "CCantInteres"    => "CantInteres",
            "CReferencia"     => "Referencia",
            "CPrimerContacto" => "Primer Contacto",
            "CComentario"     => "Comentario",
            "CContacto"       => "Contacto",
            "CCantContacto"   => "CantContacto"
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,BtnDownload,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
            
            'BtnDownload'             => "1",
            'BtnDownloadButtonText'   => "Descargar todos los registrados en Excel",
            'BtnDownloadType'         => "Button",
            'BtnDownloadOnClick'      => "$.downloadExcel('" . $router->generate('cenfo_empresa_download') . "', null)"
        ));
        
        //Solo si es Telemercadeo.
        if($pflagTelemercadeoAsignar == 1){
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 80,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'Encargado',
                'Type'      => "Text",
                'Align'     => 'Left',
            ));
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 100,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'UltimaLlamada',
                'Type'      => "Date",
                'Align'     => 'Left',
            ));
        }
        
        //Solo si es ComplexTable
        if($pflagComplexTable == 1){
            //Hacer mas grande el Grid
            $layoutGenerator->setConfigurationOption('MaxVScroll', '550');
            //Agregar el total
            $layoutGenerator->addRightColumn(array(
                'Name'       => 'Total',
                'CanFilter'  => 0,
                'Type'       => 'Int',
                'Width'      => 80,
            ));
            // Filas normales.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'R',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas con subfilas.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'Node',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas creadas cuando se agrupan 
            $layoutGenerator->addDefaultRow(array(
                'Name'            => 'Group',
                'Def'             => 'Node',
                'CanSelect'       => '0',
                'AggChildren'     => '1',
                'CanFilter'       => '2',
                'TotalFormula'    => 'count("id", 4)',
                'TotalIntFormat'  => '<i style="font-style:italic">0</i>'
            ));
            $layoutGenerator->setSolidRows(array(
                array(
                    'id'    => 'Group',
                    'Kind'  => 'Group',
                    'Space' => '0',
                )
            ));
        }
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/download", name="cenfo_empresa_download")
     */
    public function downloadAction() 
    {  
        ini_set('memory_limit', '-1');
        $dateTime = new \DateTime();
        $repoEmpresa = $this->getDoctrine()->getRepository('CenfotecBDBundle:Empresa');
        $contactos = $repoEmpresa->searchAll(true);
        $cantCorreos = 2;
        $cantNumeros = 2;
        $cantIntereses = 2;
        $cantSeguimientos = 3;
        $cantContactos = 2;
        $cantCorreosContactos = 2;
        $cantNumerosContactos = 2;
        
        $fileName = 'CRM Empresas ' . date_format($dateTime, 'Y-m-d H_i_s');
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setStatusCode(200);

        $response->headers->set('Content-Type', "application/vnd.ms-excel");
        $response->headers->set('Cache-Control', "must-revalidate");
        $response->headers->set('Set-Cookie', "fileDownload=true; path=/");
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s.xls"', $fileName));
        
        $response->setContent($this->renderView(
            "CenfotecCRMBundle:Empresa:download.xls.twig", array(
                "contactos"            => $contactos,
                "cantCorreos"          => $cantCorreos,
                "cantNumeros"          => $cantNumeros,
                "cantIntereses"        => $cantIntereses,
                "cantSeguimientos"     => $cantSeguimientos,
                "cantContactos"        => $cantContactos,
                "cantCorreosContactos" => $cantCorreosContactos,
                "cantNumerosContactos" => $cantNumerosContactos
             )
        ));
        
        return $response;
    }
    
    /**
     * @Route("/grid-data", name="cenfo_empresa_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    {
        $r                        = $this->getRequest();
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $pflagEliminados          = $r->query->get('pflagEliminados');
        $ptipoBusqueda            = $r->query->get('ptipoBusqueda');
        $pcriterio                = $r->query->get('pcriterio');
        $pnombre                  = $r->query->get('pnombre');
        $papellido1               = $r->query->get('papellido1');
        $papellido2               = $r->query->get('papellido2');
        $pcorreo                  = $r->query->get('pcorreo');
        $pnumero                  = $r->query->get('pnumero');
        $pfechaInicioContacto     = $r->query->get('pfechaInicioContacto');
        $pfechaFinContacto        = $r->query->get('pfechaFinContacto');
        $pinteres                 = $r->query->get('pinteres');
        $dataFormatter            = new GridDataTreePagingFormatter();
        $repoEmpresa              = $this->getDoctrine()->getRepository('CenfotecBDBundle:Empresa');
        
        if(!empty($pflagEliminados) && $pflagEliminados == "1"){
            $empresas = $repoEmpresa->searchAllEliminados();
        }else if($ptipoBusqueda == 'Filtro'){
            $empresas = $repoEmpresa->searchDataXFiltro(
                    $pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, 
                    $pfechaInicioContacto, $pfechaFinContacto, $pinteres);
        }else if($ptipoBusqueda == 'Criterio'){
            $empresas = $repoEmpresa->searchDataXCriterio($pcriterio);
        }else{
            $empresas = $repoEmpresa->searchAll();
        }
        
        $dateTime = new \DateTime();
        
        foreach ($empresas as $dataEmpresa) {
            //Obtener los intereses
            $intereses       = (!empty($dataEmpresa['intereses'])) ? explode('|', $dataEmpresa['intereses']) : array();
            $enumIntereses   = "";
            $defautInteres   = "";
            
            foreach ($intereses as $strInteres) {
                $defautInteres  = $strInteres;
                $enumIntereses .= "|" . $strInteres;
            }
             
            //Obtener los contactos de la empresa
            $contactos          = (!empty($dataEmpresa['contactos'])) ? explode('|', $dataEmpresa['contactos']) : array();
            $enumContactos      = "";
            $defautContactactos = "";
            
            foreach ($contactos as $strContacto) {
                $defautContactactos  = $strContacto;
                $enumContactos .= "|" . $strContacto;
            }
            
            $row = array(
                'id'                 => $dataEmpresa['empresa_id'],
                'Name'               => $dataEmpresa['nombre'],
                'CNombre'            => $dataEmpresa['nombre'],
                'CActividad'         => $dataEmpresa['actividad'],
                'CInteres'           => $defautInteres,
                'CInteresDefaults'   => $enumIntereses,
                'CCantInteres'       => count($intereses),
                'CReferencia'        => $dataEmpresa['referencia'],
                'CPrimerContacto'    => $dataEmpresa['primer_contacto'],
                'CComentario'        => $dataEmpresa['comentario'],
                'CContacto'          => $defautContactactos,
                'CContactoDefaults'  => $enumContactos,
                'CCantContacto'       => count($contactos),
                'Seg'                => $dataEmpresa['cant_seguimientos']
            );

            //Solo si es Telemercadeo.
            if($pflagTelemercadeoAsignar == 1){
                $row['Encargado']     = $dataEmpresa['empresa_id'] ? 'CDominguez' : 'No';
                $row['UltimaLlamada'] = $dataEmpresa['empresa_id'] ? $dateTime->format('Y/m/d H:i') : '';
                if($row['Encargado'] != 'No'){
                    $row['Color'] = '#e8ffcd';
                }
            }
            $dataFormatter->addRow($row);
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_empresa_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Empresa:formAdministrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $puestos               = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $actividades          = $d->getRepository('CenfotecBDBundle:Actividad')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => 'Registrar nuevo contacto de Empresa',
            'ptwAction'               => 'Registrar',
            'ptwFormAction'           => $router->generate('cenfo_empresa_registrar'),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwPuestos'               => $puestos,
            'ptwActividades'          => $actividades,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto
        );
    }

    /**
     * @Route("/form-modificar", name="cenfo_empresa_form_modificar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Empresa:formAdministrar.html.twig")
     */
    public function formModificarAction()
    {   
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $pidEmpresa           = $this->getRequest()->query->get('pidEmpresa');
        $objEmpresa           = $d->getRepository('CenfotecBDBundle:Empresa')->find($pidEmpresa);
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $puestos               = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $actividades          = $d->getRepository('CenfotecBDBundle:Actividad')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => "Modificar ID:" . $objEmpresa->getId() . " " . $objEmpresa->getNombre(),
            'ptwAction'               => 'Modificar',        
            'ptwFormAction'           => $router->generate('cenfo_empresa_modificar', array('pidEmpresa'=>$pidEmpresa)),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwPuestos'               => $puestos,
            'ptwActividades'          => $actividades,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwObjEmpresa'           => $objEmpresa
        );
    }
    
    /**
     * @Route("/registrar", name="cenfo_empresa_registrar")
     */
    public function registrarAction()
    {
        try{
            $r              = $this->getRequest();
            $doctrine       = $this->getDoctrine();
            $em             = $doctrine->getEntityManager();
            $data           = $r->request->get('data');
            $objEmpresa     = new \Cenfotec\BDBundle\Entity\Empresa();
            $currentUser    = $this->getUser();
            
            $objEmpresa->setNombre($data["nombre"]);
            $objEmpresa->setDireccion($data["direccion"]);
            $objEmpresa->setFlagConvenio(!empty($data["convenio"]) ? $data["convenio"] : null);
            $objEmpresa->setFechaContacto(new \DateTime($data["fechaContacto"]));
            $objEmpresa->setComentario($data["comentario"]);
            $objEmpresa->setUsuarioRegistroBD($currentUser);
            $objEmpresa->setFlagContacto(true);
            
            if(!empty($data["canton"])){
                $objCanton = $doctrine->getRepository('CenfotecBDBundle:Canton')->find($data["canton"]);
                $objEmpresa->setCanton($objCanton);
            }
            if(!empty($data["actividad"])){
                $objActividad = $doctrine->getRepository('CenfotecBDBundle:Actividad')->find($data["actividad"]);
                $objEmpresa->setActividad($objActividad);
            }
            if(!empty($data["referencia"])){
                $objReferencia = $doctrine->getRepository('CenfotecBDBundle:Referencia')->find($data["referencia"]);
                $objEmpresa->setReferencia($objReferencia);
            }
            if(!empty($data["primerContacto"])){
                $objPrimerContacto = $doctrine->getRepository('CenfotecBDBundle:PrimerContacto')->find($data["primerContacto"]);
                $objEmpresa->setPrimerContacto($objPrimerContacto);
            }
            
            if (!empty($data["intereses"])) {
                GlobalHelper::actualizarIntereses($objEmpresa, $data["intereses"], $em);
            }
            
            if (!empty($data["contactos"])) {
                GlobalHelper::actualizarContactosBasicos($objEmpresa, $data["contactos"], $em);
            }
            
            $em->persist($objEmpresa);
            $em->flush();
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "La Empresa 'ID:" . $objEmpresa->getId() . " " . $objEmpresa->getNombre() . "' se registro correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_empresa_form_modificar', array(
                    'pidEmpresa' => $objEmpresa->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/modificar", name="cenfo_empresa_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        try{
            $r              = $this->getRequest();
            $doctrine       = $this->getDoctrine();
            $em             = $doctrine->getEntityManager();
            $idEmpresa      = $r->query->get('pidEmpresa');
            $idContactoCRM  = $r->query->get('pidContactoCRM');
            
            if (!empty($idContactoCRM)) {
                $idEmpresa = $idContactoCRM;
            }
            
            $data           = $r->request->get('data');
            $objEmpresa     = $doctrine->getRepository('CenfotecBDBundle:Empresa')->find($idEmpresa);
            $currentUser    = $this->getUser();
            
            $objEmpresa->setNombre($data["nombre"]);
            $objEmpresa->setDireccion($data["direccion"]);
            $objEmpresa->setFlagConvenio($data["convenio"]);
            $objEmpresa->setFechaContacto(new \DateTime($data["fechaContacto"]));
            $objEmpresa->setComentario($data["comentario"]);
            $objEmpresa->setUsuarioModificoBD($currentUser);
            $objEmpresa->setFechaModificoBD(new \DateTime());
            $objEmpresa->setFlagContacto(true);
            
            if(!empty($data["canton"])){
                $objCanton = $doctrine->getRepository('CenfotecBDBundle:Canton')->find($data["canton"]);
                $objEmpresa->setCanton($objCanton);
            }else{
                $objEmpresa->setCanton(null);
            }
            if(!empty($data["actividad"])){
                $objActividad = $doctrine->getRepository('CenfotecBDBundle:Actividad')->find($data["actividad"]);
                $objEmpresa->setActividad($objActividad);
            }else{
                $objEmpresa->setActividad(null);
            }
            if(!empty($data["referencia"])){
                $objReferencia = $doctrine->getRepository('CenfotecBDBundle:Referencia')->find($data["referencia"]);
                $objEmpresa->setReferencia($objReferencia);
            }else{
                $objEmpresa->setReferencia(null);
            }
            if(!empty($data["primerContacto"])){
                $objPrimerContacto = $doctrine->getRepository('CenfotecBDBundle:PrimerContacto')->find($data["primerContacto"]);
                $objEmpresa->setPrimerContacto($objPrimerContacto);
            }else{
                $objEmpresa->setPrimerContacto(null);
            }
            
            if(isset($data["intereses"])){
                GlobalHelper::actualizarIntereses($objEmpresa, $data["intereses"], $em);
            }
            if(isset($data["contactos"])){
                GlobalHelper::actualizarContactosBasicos($objEmpresa, $data["contactos"], $em);
            }
            
            //Establecer el Contacto como activo si estaba eliminado.
            $objEmpresa->setFlagEliminado(false);
            
            $em->merge($objEmpresa);
            $em->flush();
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "La Empresa 'ID:" . $objEmpresa->getId() . " " . $objEmpresa->getNombre() . "' se modifico correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_empresa_form_modificar', array(
                    'pidEmpresa' => $objEmpresa->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    /**
     * @Route("/eliminar", name="cenfo_empresa_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        try {
            $r          = $this->getRequest();
            $em         = $this->getDoctrine()->getEntityManager();
            $pidEmpresa = $r->get('pidEmpresa');
            $msg        = "";
            $objEmpresa = $em->getRepository('CenfotecBDBundle:Empresa')->find($pidEmpresa);
            
            $objEmpresa->setFlagEliminado(true);
            $em->merge($objEmpresa);
            $em->flush();
            
            $msg = "La Empresa 'ID:" . $objEmpresa->getId() . " " . $objEmpresa->getNombre() . "' se ha eliminado correctamente.";
            $descripcion = "Empresa ID:" . $objEmpresa->getId() . " " . $objEmpresa->getNombre() . ".";
            
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_ELIMINAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $this->getUser()
            ));
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objEmpresa->getId(),
                'status'   => 'ok'
            ))); 
            
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/ver-perfil", name="cenfo_empresa_ver_perfil", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Empresa:verPerfil.html.twig")
     */
    public function verPerfilAction()
    {
        $r        = $this->getRequest();
        $d        = $this->getDoctrine();
        $pidEmpresa  = $r->query->get('pidEmpresa');
        $objEmpresa  = $d->getRepository('CenfotecBDBundle:Empresa')->find($pidEmpresa);
        
        return array(
            'ptwObjEmpresa' => $objEmpresa
        );
    }

    /**
     * @Route("/copy-to-prog-academico", name="cenfo_empresa_copy_to_prog_academico", options={"expose"=true})
     */
    public function copyToProgAcademicoAction()
    {
        try{
            $r        = $this->getRequest()->request;
            $idEmpresa   = $r->get('pidEmpresa');
            $msg      = "";

            if(!empty($idEmpresa)){
                $em               = $this->getDoctrine()->getEntityManager();
                $objEmpresa          = $em->getRepository('CenfotecBDBundle:Empresa')->find($idEmpresa);
                $objContactoCRM   = $objEmpresa->getContactoCRM();

                //Validar si ya existe el ContactoCRM en Programa Academico.
                $objProgAcademico = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->findBy(array('contactoCRM'=>$objContactoCRM->getId()));
                if($objProgAcademico == null){
                    $objProgAcademico = new ProgramaAcademico();
                    $objProgAcademico->setContactoCRM($objContactoCRM);
                    $em->persist($objProgAcademico);
                    $em->flush();

                    $msg = "El contacto de Empresa con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se copió correctamente a la BD de Programa Académico.";
                }else{
                    $msg = "<p>- El contacto de Empresa con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' ya se encuentra actualmente en la BD de Programa Académico.</p>";
                    return new \Symfony\Component\HttpFoundation\Response($msg); 
                }
            }else{
                $msg = "<p>- Identificador del contacto Empresa es nulo.</p>";
                return new \Symfony\Component\HttpFoundation\Response($msg); 
            }

            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'    => $msg,
                'status' => "ok"
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
}
