<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cenfotec\CRMBundle\Clases\ChartHelper;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator;
use Components\EJSTreeGridBundle\Framework\GridLayoutGenerator;
use Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/excepcion")
*/
class ExcepcionController extends Controller
{
    /**
     * @Route("/", name="cenfo_excepcion_manager")
     * @Template()
     */
    public function indexAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridBitacora');
        
        $gridOptionsGenerator
            ->setGridId('GridBitacora')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_excepcion_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_excepcion_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_excepcion_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $layoutGenerator = new GridLayoutGenerator();
        
        $layoutGenerator->addTopRowFilter(array());
        
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Int", 'Width' => 40, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "CMetodo",    'Type' => "Text", 'RelWidth' => 1, 'CanEdit'   => 0 ),
            array( 'Name' => "CExcepcion", 'Type' => "Lines", 'RelWidth' => 4, 'CanEdit'   => 0, 'Height' => 20),
            array( 'Name' => "CFecha",     'Type' => "Text", 'RelWidth' => 1, 'CanEdit'   => 0 ),
            array( 'Name' => "CUsuario",   'Type' => "Text", 'RelWidth' => 1, 'CanEdit'   => 0 )
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id'         => 'ID',
            'CMetodo'    => 'Metodo',
            'CExcepcion' => 'Excepcion',
            'CFecha'     => 'Fecha',
            'CUsuario'   => 'Usuario'
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_excepcion_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    { 
        $em            = $this->getDoctrine()->getEntityManager();
        $excepciones   = $em->getRepository('CenfotecBDBundle:Excepcion')->findBy(array(),array('id'=>'desc'));
        $repoUsuario   = $em->getRepository('CenfotecSeguridadBundle:Usuario');
        $dataFormatter = new GridDataTreePagingFormatter();
        
        foreach ($excepciones as $objExcepcion) {
            $objUsuario = $repoUsuario->find($objExcepcion->getUsuarioId());
            $dataFormatter->addRow(array(
                'id'         => $objExcepcion->getId(),
                'CMetodo'    => $objExcepcion->getMetodo(),
                'CExcepcion' => $objExcepcion->getExcepcion(),
                'CFecha'     => $objExcepcion->getFecha()->format('Y-m-d H:i'),
                'CUsuario'   => $objUsuario->getNombreCompleto()
            ));
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
}
