<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/matricula")
*/
class MatriculaController extends Controller
{
    /**
     * @Route("/form-registrar", name="cenfo_matricula_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Matricula:formAdministrar.html.twig")
     */
    public function formAdministrarAction()
    {
        $r              = $this->getRequest()->query;
        $d              = $this->getDoctrine();
        $pidEntity      = $r->get('pidEntity');
        $ptipoContacto  = $r->get('ptipoContacto');
        $tiposContacto  = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $objContactoCRM = $d->getRepository('CenfotecBDBundle:ContactoCRM')->find($pidEntity);
        $matriculas     = $objContactoCRM->getMatriculas();
        
        return array(
            'ptwTipoContacto'  => $ptipoContacto,
            'ptwIdContactoCRM' => $pidEntity,
            'ptwTiposContacto' => $tiposContacto,
            'ptwMatriculas'    => $matriculas
        );
    }

    /**
     * @Route("/guardar", name="cenfo_matricula_guardar")
     */
    public function guardarAction()
    {
        try {
            $r               = $this->getRequest();
            $pidContactoCRM  = $r->query->get("pidContactoCRM");
            $data            = $r->request->get("data");
            $em              = $this->getDoctrine()->getEntityManager();
            $repoMatricula   = $em->getRepository('CenfotecBDBundle:Matricula');
            $repoInteres     = $em->getRepository('CenfotecBDBundle:Interes');
            $repoContactoCRM = $em->getRepository('CenfotecBDBundle:ContactoCRM');
            $msg             = "";
            
            $errorBorrar   = array(
                'errores' => "",
                'SQLSTATE[23000]'
            );

            $objContactoCRM = $repoContactoCRM->find($pidContactoCRM);
            //---------------------------------------------------------------
            //registrar
            if(isset($data['registrar']) && count($data['registrar']) > 0){
                foreach ($data['registrar'] as $datos) {
                    $objInteres     = $repoInteres->find($datos['idInteres']);
                    $objMatricula   = new \Cenfotec\BDBundle\Entity\Matricula();
                    
                    if (isset($datos['fechaMatricula'])) {
                        $objMatricula->setFechaMatricula(new \DateTime($datos['fechaMatricula']));
                    }else{
                        $objMatricula->setFechaInicio(new \DateTime($datos['fechaInicio']));
                        $objMatricula->setFechaFin(new \DateTime($datos['fechaFin']));
                    }
                    $objMatricula->setInteres($objInteres);
                    $objMatricula->setUsuario($this->getUser());
                    $objMatricula->setContactoCRM($objContactoCRM);
                    
                    $em->persist($objMatricula);
                    $em->flush();
                }
            }

            //actualizar
            if(isset($data['actualizar']) && count($data['actualizar']) > 0){
                foreach ($data['actualizar'] as $idMatricula => $datos) {
                    $objMatricula = $repoMatricula->find($idMatricula);
                    $objInteres   = $repoInteres->find($datos['idInteres']);
                    
                    if (isset($datos['fechaMatricula'])) {
                        $objMatricula->setFechaMatricula(new \DateTime($datos['fechaMatricula']));
                    }else{
                        $objMatricula->setFechaInicio(new \DateTime($datos['fechaInicio']));
                        $objMatricula->setFechaFin(new \DateTime($datos['fechaFin']));
                    }
                    $objMatricula->setInteres($objInteres);
                    $em->merge($objMatricula);
                    $em->flush();
                }
            }

            //borrar
            if(isset($data['borrar']) && !empty($data['borrar'])){
                $ids = explode(",", $data['borrar']);
                foreach ($ids as $idMatricula) {
                    //Validar si se cae al borrar por que tal vez tenga alguna relacion
                    try {
                        if(!empty($idMatricula)){
                            $objMatricula = $repoMatricula->find($idMatricula);
                            $em->remove($objMatricula);
                        }
                        $em->flush();
                    } catch (\Exception $exc) {
                        //Registrar Excepcion
                        GlobalHelper::registrarExcepcion($this, array(
                            'excepcion' => $exc->getMessage(),
                            'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
                        ));

                        $errorBorrar['errores'] .= "<p>- Matricula '" . $objMatricula->getId() . "'.</p>";
                    }
                }
            }
            
            $msg = "Los datos se guardaron correctamente al contacto 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "'.";
            $em->refresh($objContactoCRM);
            
            if(count($objContactoCRM->getMatriculas()) > 0){
                $objContactoCRM->setFlagMatriculado(true);
            }else{
                $objContactoCRM->setFlagMatriculado(false);
            }
            $em->merge($objContactoCRM);
            $em->flush();
            
            //Validar si hubieron errores al eliminar MoneySource
            if($errorBorrar['errores'] != ""){
                return new \Symfony\Component\HttpFoundation\Response(json_encode($errorBorrar));  
            }else{
                return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                    'msg'            => $msg,
                    'status'         => 'ok',
                    'cantMatriculas' => count($objContactoCRM->getMatriculas())
                )));  
            }
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
}
