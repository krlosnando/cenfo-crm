<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter,
    Cenfotec\CRMBundle\Clases\GlobalHelper;
use Cenfotec\BDBundle\Entity\ContactoCRM;
use Cenfotec\BDBundle\Entity\ProgramaAcademico;
use Cenfotec\BDBundle\Entity\ContactoCRMXNumero;
use Cenfotec\BDBundle\Entity\Correo;
use Cenfotec\BDBundle\Entity\Numero;
// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/prog-academico")
*/
class ProgramaAcademicoController extends Controller
{
    /**
     * @Route("/", name="cenfo_prog_academico_listar")
     * @Template()
     */
    public function listarAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridPA');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridPA')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_prog_academico_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('PA'),
            'ptwModulo'               => 'Administrar',
            'ptwIntereses'            => $intereses
        );
    }

    /**
     * @Route("/matriculados", name="cenfo_prog_academico_listar_matriculados")
     * @Template("CenfotecCRMBundle:ProgramaAcademico:listar.html.twig")
     */
    public function listarMatriculadosAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridPA');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridPA')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_prog_academico_grid_data', array(
                    'pflagMatriculados' => '1'
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('PA'),
            'ptwModulo'               => 'Matriculados',
            'ptwIntereses'            => $intereses
        );
    }
    
    /**
     * @Route("/eliminados", name="cenfo_prog_academico_listar_eliminados")
     * @Template("CenfotecCRMBundle:ProgramaAcademico:listar.html.twig")
     */
    public function listarEliminadosAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridPA');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridPA')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_prog_academico_grid_data', array(
                    'pflagEliminados' => '1'
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwClienteCRM'           => GlobalHelper::getClienteCRM('PA'),
            'ptwModulo'               => 'Eliminados',
            'ptwIntereses'            => $intereses
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_prog_academico_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutAction()
    {
        $r                        = $this->getRequest();
        $router                   = $this->get('router');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $layoutGenerator          = new GridLayoutGenerator();
        $ColumWidthID             = (!empty($pflagComplexTable)) ? 0 : 40;
        $ColumWidthNombre         = (!empty($pflagComplexTable)) ? 150 : 80;
        
        $layoutGenerator->setConfigurationOption('ChildPaging', 2);
        $layoutGenerator->setConfigurationOption('ChildParts', 2);
        
        $layoutGenerator->addTopRowFilter(array(
            "idType" => "Int",
            "id"     => "0" 
        ));
        
        //IDContactoCRM
        $layoutGenerator->addLeftColumn(array( 
            'Name' => "id", 'Type' => "Text", 'Width' => $ColumWidthID, 'CanEdit' => 0
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "IDPA",           'Type' => "Int",    'Width'   => 0,    'CanEdit' => 0 ),
            array( 'Name' => "Name",           'Type' => "Text",   'Width'   => 0,  'CanExport' => 0 ),
            array( 'Name' => "Nombre",         'Type' => "Text",   'Width'   => $ColumWidthNombre, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Apellido1",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Apellido2",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Correo",         'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantCorreo",     'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Interes",        'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantInteres",    'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Numero",         'Type' => "Select", 'CanEdit' => 1,  'CanExport' => 1, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "CantNumero",     'Type' => "Int",    'Width'   => 25, 'CanExport' => 1, 'Align' => "Center" ),
            array( 'Name' => "Empresa",        'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Colegio",        'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Identificacion", 'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Referencia",     'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "PrimerContacto", 'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "FechaContacto",  'Type' => "Date",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0, 'Format' => 'yyyy-MM-dd' ),
            array( 'Name' => "Matricula",      'Type' => "Text",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "FechaMatricula", 'Type' => "Date",   'Width'   => 80, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0, 'Format' => 'yyyy-MM-dd' )
        ));
        
        $layoutGenerator->addRightColumn(array( 
            'Name' => 'Mat', 'CanEdit' => 0, 'Width' => 33, 'CanFilter' => 1, 'CanSort' => 1, 'Type' => "Int", 'Align' => 'Center',
        ));
        $layoutGenerator->addRightColumn(array( 
            'Name' => 'Seg', 'CanEdit' => 0, 'Width' => 30, 'CanFilter' => 1, 'CanSort' => 1, 'Type' => "Int", 'Align' => 'Center'
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'id' => 'ID'
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,BtnDownload,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
            
            'BtnDownload'             => "1",
            'BtnDownloadButtonText'   => "Descargar todos los registrados en Excel",
            'BtnDownloadType'         => "Button",
            'BtnDownloadOnClick'      => "$.downloadExcel('" . $router->generate('cenfo_prog_academico_download') . "', null)"
        ));
        
        //Solo si es Telemercadeo.
        if($pflagTelemercadeoAsignar == 1){
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 80,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'Encargado',
                'Type'      => "Text",
                'Align'     => 'Left',
            ));
            $layoutGenerator->addRightColumn(array(
                'CanEdit'   => 0,
                'Width'     => 100,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Name'      => 'UltimaLlamada',
                'Type'      => "Date",
                'Align'     => 'Left',
            ));
        }
        
        //Solo si es ComplexTable
        if($pflagComplexTable == 1){
            //Hacer mas grande el Grid
            $layoutGenerator->setConfigurationOption('MaxVScroll', '550');
            //Agregar el total
            $layoutGenerator->addRightColumn(array(
                'Name'       => 'Total',
                'CanFilter'  => 0,
                'Type'       => 'Int',
                'Width'      => 80,
            ));
            // Filas normales.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'R',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas con subfilas.
            $layoutGenerator->addDefaultRow(array(
                'Name'           => 'Node',
                'Expanded'       => '0',
                'Calculated'     => '1',
                'CalcOrder'      => 'Total',
                'TotalType'      => 'Enum',
                'TotalFormula'   => 'count()',
                'TotalCanFilter' => '0',
                'TotalAlign'     => 'Right',
                'TotalEnum'      => '| |1', 
                'TotalIntFormat' => '0'
            ));
            // Filas creadas cuando se agrupan 
            $layoutGenerator->addDefaultRow(array(
                'Name'            => 'Group',
                'Def'             => 'Node',
                'CanSelect'       => '0',
                'AggChildren'     => '1',
                'CanFilter'       => '2',
                'TotalFormula'    => 'count("id", 4)',
                'TotalIntFormat'  => '<i style="font-style:italic">0</i>'
            ));
            $layoutGenerator->setSolidRows(array(
                array(
                    'id'    => 'Group',
                    'Kind'  => 'Group',
                    'Space' => '0',
                )
            ));
        }
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/download", name="cenfo_prog_academico_download")
     */
    public function downloadAction() 
    {  
        ini_set('memory_limit', '-1');
        $dateTime                 = new \DateTime();
        $repoProgramaAcademico    = $this->getDoctrine()->getRepository('CenfotecBDBundle:ProgramaAcademico');
        $contactos = $repoProgramaAcademico->searchAll(true);
        $cantCorreos = 2;
        $cantNumeros = 2;
        $cantIntereses = 2;
        $cantSeguimientos = 3;
        $cantMatriculas = 1;
        
        $fileName = 'CRM Programa Academico ' . date_format($dateTime, 'Y-m-d H_i_s');
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setStatusCode(200);

        $response->headers->set('Content-Type', "application/vnd.ms-excel");
        $response->headers->set('Cache-Control', "must-revalidate");
        $response->headers->set('Set-Cookie', "fileDownload=true; path=/");
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s.xls"', $fileName));
        
        $response->setContent($this->renderView(
            "CenfotecCRMBundle:CRM:download.xls.twig", array(
                "ptwTipoContacto"   => "PA",
                "contactos"         => $contactos,
                "cantCorreos"       => $cantCorreos,
                "cantNumeros"       => $cantNumeros,
                "cantIntereses"     => $cantIntereses,
                "cantSeguimientos"  => $cantSeguimientos,
                "cantMatriculas"    => $cantMatriculas
             )
        ));
        
        return $response;
    }
    
    /**
     * @Route("/grid-data", name="cenfo_prog_academico_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataAction() 
    {  
        $r                        = $this->getRequest();
        $pflagTelemercadeoAsignar = $r->query->get('pflagTelemercadeoAsignar');
        $pflagComplexTable        = $r->query->get('pflagComplexTable');
        $pflagEliminados          = $r->query->get('pflagEliminados');
        $pflagMatriculados        = $r->query->get('pflagMatriculados');
        $pcriterio                = $r->query->get('pcriterio');
        $ptipoBusqueda            = $r->query->get('ptipoBusqueda');
        $pnombre                  = $r->query->get('pnombre');
        $papellido1               = $r->query->get('papellido1');
        $papellido2               = $r->query->get('papellido2');
        $pcorreo                  = $r->query->get('pcorreo');
        $pnumero                  = $r->query->get('pnumero');
        $pfechaInicioContacto     = $r->query->get('pfechaInicioContacto');
        $pfechaFinContacto        = $r->query->get('pfechaFinContacto');
        $pinteres                 = $r->query->get('pinteres');
        $dataFormatter            = new GridDataTreePagingFormatter();
        $repoProgramaAcademico    = $this->getDoctrine()->getRepository('CenfotecBDBundle:ProgramaAcademico');
        
        if (!empty($pflagEliminados) && $pflagEliminados == "1") {
            $contactos = $repoProgramaAcademico->searchAllEliminados();
        }else if(!empty($pflagMatriculados) && $pflagMatriculados == "1") {
            $contactos = $repoProgramaAcademico->searchAllMatriculados();
        }else if($ptipoBusqueda == 'Filtro'){
            $contactos = $repoProgramaAcademico->searchDataXFiltro(
                    $pnombre, $papellido1, $papellido2, $pcorreo, $pnumero, 
                    $pfechaInicioContacto, $pfechaFinContacto, $pinteres);
        }else if($ptipoBusqueda == 'Criterio'){
            $contactos = $repoProgramaAcademico->searchDataXCriterio($pcriterio);
        }else{
            $contactos = $repoProgramaAcademico->searchAll($pflagComplexTable);
        }
        
        $dateTime = new \DateTime();
        
        foreach ($contactos as $dataContacto) {
            //Obtener los Correos
            $correos      = (!empty($dataContacto['correos'])) ? explode('|', $dataContacto['correos']) : array();
            $enumCorreos  = "";
            $defautCorreo = "";
            
            foreach ($correos as $strCorreo) {
                $defautCorreo     = $strCorreo;
                $enumCorreos .= "|" . $strCorreo;
            }
            
            //Obtener los intereses
            $intereses       = (!empty($dataContacto['intereses'])) ? explode('|', $dataContacto['intereses']) : array();
            $enumIntereses   = "";
            $defautInteres   = "";
            
            foreach ($intereses as $strInteres) {
                $defautInteres  = $strInteres;
                $enumIntereses .= "|" . $strInteres;
            }
            
            //Obtener los numeros
            $numeros       = (!empty($dataContacto['numeros'])) ? explode('|', $dataContacto['numeros']) : array();
            $enumNumeros   = "";
            $defautNumeros = "";
            
            foreach ($numeros as $strNumero) {
                $defautNumeros  = $strNumero;
                $enumNumeros   .= "|" . $strNumero;
            }
            
            $row = array(
                'id'              => $dataContacto['contacto_crm_id'],
                'Name'            => $dataContacto['nombre'],
                'IDPA'            => $dataContacto['programa_academico_id'],
                'Nombre'          => $dataContacto['nombre'],
                'Apellido1'       => $dataContacto['apellido1'],
                'Apellido2'       => $dataContacto['apellido2'],
                'Identificacion'  => $dataContacto['identificacion'],
                'Empresa'         => $dataContacto['empresa'],
                'Colegio'         => $dataContacto['colegio'],
                'Referencia'      => $dataContacto['referencia'],
                'PrimerContacto'  => $dataContacto['primer_contacto'],
                'CantCorreo'      => count($correos),
                'Correo'          => $defautCorreo,
                'CorreoDefaults'  => $enumCorreos,
                'CantInteres'     => count($intereses),
                'Interes'         => $defautInteres,
                'InteresDefaults' => $enumIntereses,
                'CantNumero'      => count($numeros),
                'Numero'          => $defautNumeros,
                'NumeroDefaults'  => $enumNumeros,
                'FechaContacto'   => $dataContacto['fecha_contacto'],
                'Matricula'       => $dataContacto['matricula'],
                'FechaMatricula'  => $dataContacto['fecha_matricula'],
                'Mat'             => $dataContacto['cant_matriculas'],
                'Seg'             => $dataContacto['cant_seguimientos']
            );

            //Solo si es Telemercadeo.
            if($pflagTelemercadeoAsignar == 1){
                $row['Encargado']     = $dataContacto['programa_academico_id'] ? 'CDominguez' : 'No';
                $row['UltimaLlamada'] = $dataContacto['programa_academico_id'] ? $dateTime->format('Y/m/d H:i') : '';
                if($row['Encargado'] != 'No'){
                    $row['Color'] = '#e8ffcd';
                }
            }
            $dataFormatter->addRow($row);
        }
        $count = count($contactos);
        return array(
            'gridDataFormatter' => $dataFormatter,
            'count'             => $count
        );
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_prog_academico_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:ProgramaAcademico:formAdministrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $router               = $this->get('router');
        $d                    = $this->getDoctrine();
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $tipoIdentificaciones = $d->getRepository('CenfotecBDBundle:TipoIdentificacion')->findBy(array(), array('nombre'=>'asc'));
        $colegios             = $d->getRepository('CenfotecBDBundle:Colegio')->findBy(array(), array('nombre'=>'asc'));
        $nivelesColegio       = $d->getRepository('CenfotecBDBundle:NivelColegio')->findBy(array(), array('nombre'=>'asc'));
        $convenios            = $d->getRepository('CenfotecBDBundle:Convenio')->findBy(array(), array('nombre'=>'asc'));
        $profesiones          = $d->getRepository('CenfotecBDBundle:Profesion')->findBy(array(), array('nombre'=>'asc'));
        $empresas             = $d->getRepository('CenfotecBDBundle:Empresa')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $horarios             = $d->getRepository('CenfotecBDBundle:Horario')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        $modalidades          = $d->getRepository('CenfotecBDBundle:Modalidad')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwTituloPagina'         => 'Registrar nuevo contacto de Prog. Académico',
            'ptwAction'               => 'Registrar',
            'ptwFormAction'           => $router->generate('cenfo_prog_academico_registrar'),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwTipoIdentificaciones' => $tipoIdentificaciones,
            'ptwColegios'             => $colegios,
            'ptwNivelesColegio'       => $nivelesColegio,
            'ptwConvenios'            => $convenios,
            'ptwProfesiones'          => $profesiones,
            'ptwEmpresas'             => $empresas,
            'ptwPuestos'              => $puestos,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwHorarios'             => $horarios,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwModalidades'          => $modalidades
        );
    }

    /**
     * @Route("/form-modificar", name="cenfo_prog_academico_form_modificar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:ProgramaAcademico:formAdministrar.html.twig")
     */
    public function formModificarAction()
    {
        $router               = $this->get('router');
        $r                    = $this->getRequest();
        $d                    = $this->getDoctrine();
        $pidPA                = $r->query->get('pidPA');
        $objPA                = $d->getRepository('CenfotecBDBundle:ProgramaAcademico')->find($pidPA);
        $paises               = $d->getRepository('CenfotecBDBundle:Pais')->findBy(array(), array('nombre'=>'asc'));
        $provincias           = $d->getRepository('CenfotecBDBundle:Provincia')->findBy(array(), array('nombre'=>'asc'));
        $tipoIdentificaciones = $d->getRepository('CenfotecBDBundle:TipoIdentificacion')->findBy(array(), array('nombre'=>'asc'));
        $colegios             = $d->getRepository('CenfotecBDBundle:Colegio')->findBy(array(), array('nombre'=>'asc'));
        $nivelesColegio       = $d->getRepository('CenfotecBDBundle:NivelColegio')->findBy(array(), array('nombre'=>'asc'));
        $convenios            = $d->getRepository('CenfotecBDBundle:Convenio')->findBy(array(), array('nombre'=>'asc'));
        $profesiones          = $d->getRepository('CenfotecBDBundle:Profesion')->findBy(array(), array('nombre'=>'asc'));
        $empresas             = $d->getRepository('CenfotecBDBundle:Empresa')->findBy(array(), array('nombre'=>'asc'));
        $puestos              = $d->getRepository('CenfotecBDBundle:Puesto')->findBy(array(), array('nombre'=>'asc'));
        $nivelesInteres       = $d->getRepository('CenfotecBDBundle:NivelInteres')->findBy(array(), array('nombre'=>'asc'));
        $tiposContacto        = $d->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        $horarios             = $d->getRepository('CenfotecBDBundle:Horario')->findBy(array(), array('nombre'=>'asc'));
        $tiposReferencia      = $d->getRepository('CenfotecBDBundle:TipoReferencia')->findBy(array(), array('nombre'=>'asc'));
        $primerosContacto     = $d->getRepository('CenfotecBDBundle:PrimerContacto')->findBy(array(), array('nombre'=>'asc'));
        $modalidades          = $d->getRepository('CenfotecBDBundle:Modalidad')->findBy(array(), array('nombre'=>'asc'));
        $objContactoCRM       = $objPA->getContactoCRM();
        $canMatriculas        = count($objContactoCRM->getMatriculas());
        
        return array(
            'ptwTituloPagina'         => "Modificar ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto(),
            'ptwAction'               => 'Modificar',
            'ptwFormAction'           => $router->generate('cenfo_prog_academico_modificar', array('pidPA'=>$pidPA)),
            'ptwPaises'               => $paises,
            'ptwProvincias'           => $provincias,
            'ptwTipoIdentificaciones' => $tipoIdentificaciones,
            'ptwColegios'             => $colegios,
            'ptwNivelesColegio'       => $nivelesColegio,
            'ptwConvenios'            => $convenios,
            'ptwProfesiones'          => $profesiones,
            'ptwEmpresas'             => $empresas,
            'ptwPuestos'              => $puestos,
            'ptwNivelesInteres'       => $nivelesInteres,
            'ptwTiposContacto'        => $tiposContacto,
            'ptwHorarios'             => $horarios,
            'ptwTiposReferencia'      => $tiposReferencia,
            'ptwPrimerosContacto'     => $primerosContacto,
            'ptwCantMatriculas'       => $canMatriculas,
            'ptwObjPA'                => $objPA,
            'ptwModalidades'          => $modalidades
        );
    }

    /**
     * @Route("/registrar", name="cenfo_prog_academico_registrar")
     */
    public function registrarAction()
    {
        try{
            $r                    = $this->getRequest();
            $doctrine             = $this->getDoctrine();
            $em                   = $doctrine->getEntityManager();
            $data                 = $r->request->get('data');
            $usuario              = $this->getUser();
            $objNewContactoCRM    = new ContactoCRM();
            $objNewPA             = new ProgramaAcademico();
            $data["usuario"]      = $usuario;
            $data["tipoContacto"] = 'PA';
            
            GlobalHelper::bindDataContactoCRM($objNewContactoCRM, $data, $em, 'Registrar');
            
            if(!empty($data['colegio'])){
                $objColegio = $doctrine->getRepository('CenfotecBDBundle:Colegio')->find($data['colegio']);
                $objNewPA->setColegio($objColegio);
            }
            if(!empty($data['nivelColegio'])){
                $objNivelColegio = $doctrine->getRepository('CenfotecBDBundle:NivelColegio')->find($data['nivelColegio']);
                $objNewPA->setNivelColegio($objNivelColegio);
            }
            
            //Registrar nuevo contacto de programa academico
            $objNewPA->setContactoCRM($objNewContactoCRM);
            $em->persist($objNewPA);
            $em->flush();
            
            //Relacionar el ID de programa Academico que se genero con el Contacto CRM
            $objNewContactoCRM->setProgramaAcademico($objNewPA);
            $em->flush();
            
            //Registrar a Bitacora
            $descripcion =  "Programa Académico ID:" . $objNewContactoCRM->getId() . " " . $objNewContactoCRM->getNombreCompleto() . ".";
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_NUEVO_REGISTRO,
                'descripcion'  => $descripcion,
                'objUsuario'   => $usuario
            ));
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "El contacto de Programa Académico ID:" . $objNewContactoCRM->getId() . " " . $objNewContactoCRM->getNombreCompleto() . "' se registro correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_prog_academico_form_modificar', array(
                    'pidPA' => $objNewPA->getId()
                ))
            ))); 
            
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/modificar", name="cenfo_prog_academico_modificar", options={"expose"=true})
     */
    public function modificarAction()
    {
        try{
            $r                    = $this->getRequest();
            $em                   = $this->getDoctrine()->getEntityManager();
            $idPA                 = $r->query->get('pidPA');
            $idContactoCRM        = $r->query->get('pidContactoCRM');
            $data                 = $r->request->get('data');
            $usuario              = $this->getUser();
            $data["usuario"]      = $usuario;
            $data["tipoContacto"] = 'PA';
            $flagCombinar         = false;
            
            //Se llamo desde la parte de combianar si existe un contacto con informacion parecida
            if (!empty($idContactoCRM)) {
                $flagCombinar     = true;
                $objContactoCRM   = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($idContactoCRM);
                $objProgAcademico = $objContactoCRM->getProgramaAcademico();
                
                //Si el contacto crm no tiene programa academico registrarlo
                if ($objProgAcademico == null) {
                    $objProgAcademico = new ProgramaAcademico();
                    $objProgAcademico->setContactoCRM($objContactoCRM);
                    $em->persist($objProgAcademico);
                    $em->flush();
                    
                    //Relacionar el ID de programa Academico que se genero con el Contacto CRM
                    $objContactoCRM->setProgramaAcademico($objProgAcademico);
                    $em->flush();
                }
            }else{
                //busco el objeto programa academico para obtener el objeto contacto.
                $objProgAcademico = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->find($idPA);
                $objContactoCRM   = $objProgAcademico->getContactoCRM();
            }
            
            $cambios = GlobalHelper::bindDataContactoCRM($objContactoCRM, $data, $em, "Modificar", $flagCombinar);

            if(!empty($data['colegio'])){
                $objColegio = $em->getRepository('CenfotecBDBundle:Colegio')->find($data['colegio']);
                $objProgAcademico->setColegio($objColegio);
            }
            
            if(!empty($data['nivelColegio'])){
                $objNivelColegio = $em->getRepository('CenfotecBDBundle:NivelColegio')->find($data['nivelColegio']);
                $objProgAcademico->setNivelColegio($objNivelColegio);
            }
            
            //Establecer el Contacto como activo si estaba eliminado.
            $objProgAcademico->setFlagEliminado(false);

            //Guardar todos los cambios en base de datos.
            $em->flush();
            
            //Registrar a Bitacora
            $descripcion =  "Programa Académico ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . " $cambios.";
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_MODIFICAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $usuario
            ));
            
            //Establecer mensaje en la session que se mostrara cuando la pagina de registrar
            //redireccione a la pagina de listar contactos.
            $this->get('session')->setFlash(
                "notice",
                "El contacto de Programa Académico 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se modifico correctamente."
            );
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'status'         => 'ok',
                'linkToRedirect' => $this->generateUrl('cenfo_prog_academico_form_modificar', array(
                    'pidPA' => $objProgAcademico->getId()
                ))
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/eliminar", name="cenfo_prog_academico_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        try {
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $pidPA          = $r->get('pidPA');
            $msg            = "";
            $objPA          = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->find($pidPA);
            $objContactoCRM = $objPA->getContactoCRM();
            $usuario         = $this->getUser();
            $objPA->setFlagEliminado(true);
            $em->merge($objPA);
            $em->flush();
            
            $msg = "El contacto de Programa Académico 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se ha eliminado correctamente.";
            $descripcion =  "Programa Académico ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . ".";
            
            GlobalHelper::registrarBitacora($em, array(
                'idTipoEvento' => GlobalHelper::TIPO_EVENTO_ELIMINAR,
                'descripcion'  => $descripcion,
                'objUsuario'   => $usuario
            ));
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objContactoCRM->getId(),
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/copy-to-acti", name="cenfo_prog_academico_copy_to_acti", options={"expose"=true})
     */
    public function copyToActiAction()
    {
        try{
            $r     = $this->getRequest()->request;
            $idPA  = $r->get('pidPA');
            $msg   = "";

            if(!empty($idPA)){
                $em             = $this->getDoctrine()->getEntityManager();
                $objPA          = $em->getRepository('CenfotecBDBundle:ProgramaAcademico')->find($idPA);
                $objContactoCRM = $objPA->getContactoCRM();

                //Validar si ya existe el ContactoCRM en Acti.
                $objActi = $em->getRepository('CenfotecBDBundle:Acti')->findBy(array('contactoCRM'=>$objContactoCRM->getId()));
                if($objActi == null){
                    $objActi = new \Cenfotec\BDBundle\Entity\Acti();
                    $objActi->setContactoCRM($objContactoCRM);
                    $em->persist($objActi);
                    $em->flush();

                    //Relacionar el ID de ACTI que se genero con el Contacto CRM
                    $objContactoCRM->setActi($objActi);
                    $em->flush();

                    $msg = "El contacto de Programa Académico con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' se copió correctamente a la BD de ACTI.";
                }else{
                    $msg = "<p>- El contacto de Programa Académico con 'ID:" . $objContactoCRM->getId() . " " . $objContactoCRM->getNombreCompleto() . "' ya se encuentra actualmente en la BD de ACTI.</p>";
                    return new \Symfony\Component\HttpFoundation\Response($msg); 
                }
            }else{
                $msg = "<p>- Identificador del contacto Programa Académico es nulo.</p>";
                return new \Symfony\Component\HttpFoundation\Response($msg); 
            }

            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'    => $msg,
                'status' => "ok"
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
}
