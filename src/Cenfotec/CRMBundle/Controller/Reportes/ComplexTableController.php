<?php

namespace Cenfotec\CRMBundle\Controller\Reportes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/complex-table")
*/
class ComplexTableController extends Controller
{
    /**
     * @Route("/", name="cenfo_reporte_complex_table")
     * @Template("CenfotecCRMBundle:Reporte:complexTable.html.twig")
     */
    public function complexTableAction()
    {
        $doctrine             = $this->getDoctrine();
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridComplexTable');
        $intereses            = $doctrine->getRepository('CenfotecBDBundle:TipoContacto')->findBy(array(), array('nombre'=>'asc'));
        
        $gridOptionsGenerator
            ->setGridId('GridComplexTable')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout', array('pflagComplexTable'=>1)),
                'Data_Url'   => "",
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        //Mostrar mensaje al cargar la pagina que tiene que seleccionar el tipo de contacto.
        $this->get('session')->setFlash(
            'notice',
            'Hacer click al tipo de contacto para mostrar en Complex Table.'
        );
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator,
            'ptwIntereses'         => $intereses
        );
    }
}
