<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter,
    Cenfotec\CRMBundle\Clases\GlobalHelper;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/seguimiento")
*/
class SeguimientoController extends Controller
{
    /**
     * @Route("/listar", name="cenfo_seguimiento_listar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Seguimiento:listar.html.twig")
     */
    public function listarAction()
    {
        $r                    = $this->getRequest()->query;
        $pidEntity            = $r->get('pidEntity');
        $ptipoContacto        = $r->get('ptipoContacto');    
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridSeg');
        
        $gridOptionsGenerator
            ->setGridId('GridSeg')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_seguimiento_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_seguimiento_grid_data', array(
                    'ptipoContacto' => $ptipoContacto,
                    'pidEntity'     => $pidEntity
                )),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array(
            'ptwGridOptionsGenerator' => $gridOptionsGenerator,
            'ptwIdEntity'             => $pidEntity, 
            'ptwTipoContacto'         => $ptipoContacto
        );
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_seguimiento_form_registrar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Seguimiento:formAdministrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $router         = $this->get('router');
        $r              = $this->getRequest()->query;
        $d              = $this->getDoctrine();
        $pidEntity      = $r->get('pidEntity');
        $ptipoContacto  = $r->get('ptipoContacto');
        $tipoSeguimientos = $d->getRepository("CenfotecBDBundle:TipoSeguimiento")->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwFormAction'       => $router->generate('cenfo_seguimiento_registrar', array(
                'pidEntity'     => $pidEntity,
                'ptipoContacto' => $ptipoContacto
            )),
            'ptwTipoSeguimientos' => $tipoSeguimientos
        );
    }
    
    /**
     * @Route("/form-modificar", name="cenfo_seguimiento_form_modificar", options={"expose"=true})
     * @Template("CenfotecCRMBundle:Seguimiento:formAdministrar.html.twig")
     */
    public function formModificarAction()
    {
        $router           = $this->get('router');
        $r                = $this->getRequest()->query;
        $d                = $this->getDoctrine();
        $pidSeg           = $r->get('pidSeg');
        $pidEntity        = $r->get('pidEntity');
        $ptipoContacto    = $r->get('ptipoContacto');
        $tipoSeguimientos = $d->getRepository("CenfotecBDBundle:TipoSeguimiento")->findBy(array(), array('nombre'=>'asc'));
        $objSeguimiento   = $d->getRepository("CenfotecBDBundle:Seguimiento")->find($pidSeg);
        
        return array(
            'ptwObjSeguimiento'   => $objSeguimiento,
            'ptwTipoSeguimientos' => $tipoSeguimientos,
            'ptwFormAction'       => $router->generate('cenfo_seguimiento_modificar', array(
                'pidSeg'        => $pidSeg,
                'pidEntity'     => $pidEntity,
                'ptipoContacto' => $ptipoContacto
            ))
        );
    }
    
    /**
     * @Route("/registrar", name="cenfo_seguimiento_registrar")
     */
    public function registrarAction()
    {
        try {
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $data           = $r->request->get('data');
            $pidEntity      = $r->get('pidEntity');
            $ptipoContacto  = $r->get('ptipoContacto');
            $msg            = "";
            $objTipoSeg     = $em->getRepository('CenfotecBDBundle:TipoSeguimiento')->find($data['idTipoSeguimiento']);
            $objSeguimiento = new \Cenfotec\BDBundle\Entity\Seguimiento();
            $objSeguimiento->setComentario($data['comentario']);
            $objSeguimiento->setFechaSeguimiento(new \DateTime($data['fechaSeguimiento'] . ' ' . $data['horaSeguimiento']));
            $objSeguimiento->setUsuario($this->getUser());
            $objSeguimiento->setTipo($objTipoSeg);
            
            switch ($ptipoContacto) {
                case "PA":
                case "ACTI":
                case "POS":
                    $objEntity = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($pidEntity);
                    $msg       = "El seguimiento se registro correctamente al contacto 'ID:" . $objEntity->getId() . " " . $objEntity->getNombreCompleto() . "'.";
                    break;

                case "EM":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Empresa')->find($pidEntity);
                    $msg       = "El seguimiento se registro correctamente a la empresa '" . $objEntity->getNombre() . "'.";
                    break;

                case "CO":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Colegio')->find($pidEntity);
                    $msg       = "El seguimiento se registro correctamente al colegio '" . $objEntity->getNombre() . "'.";
                    break;
            }
            
            //Agregar el seguimiento
            $objEntity->addSeguimiento($objSeguimiento);
            $em->merge($objEntity);
            $em->flush();
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objEntity->getId(),
                'countSeg' => count($objEntity->getSeguimientos()),
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }

    /**
     * @Route("/modificar", name="cenfo_seguimiento_modificar")
     */
    public function modificarAction()
    {
        try {
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $data           = $r->request->get('data');
            $pidSeg         = $r->get('pidSeg');
            $pidEntity      = $r->get('pidEntity');
            $ptipoContacto  = $r->get('ptipoContacto');
            $msg            = "";
            $objTipoSeg     = $em->getRepository('CenfotecBDBundle:TipoSeguimiento')->find($data['idTipoSeguimiento']);
            $objSeguimiento = $em->getRepository('CenfotecBDBundle:Seguimiento')->find($pidSeg);
            $objSeguimiento->setComentario($data['comentario']);
            $objSeguimiento->setFechaSeguimiento(new \DateTime($data['fechaSeguimiento'] . ' ' . $data['horaSeguimiento']));
            $objSeguimiento->setUsuario($this->getUser());
            $objSeguimiento->setTipo($objTipoSeg);
            
            $em->merge($objSeguimiento);
            $em->flush();
            switch ($ptipoContacto) {
                case "PA":
                case "ACTI":
                case "POS":
                    $objEntity = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($pidEntity);
                    $msg = "El seguimiento se modifico correctamente al contacto 'ID:" . $objEntity->getId() . " " . $objEntity->getNombreCompleto() . "'.";
                    break;

                case "EM":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Empresa')->find($pidEntity);
                    $msg = "El seguimiento se modifico correctamente a la empresa '" . $objEntity->getNombre() . "'.";
                    break;

                case "CO":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Colegio')->find($pidEntity);
                    $msg = "El seguimiento se modifico correctamente al colegio '" . $objEntity->getNombre() . "'.";
                    break;
            }
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objEntity->getId(),
                'countSeg' => count($objEntity->getSeguimientos()),
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    /**
     * @Route("/eliminar", name="cenfo_seguimiento_eliminar", options={"expose"=true})
     */
    public function eliminarAction()
    {
        try {
            $r              = $this->getRequest();
            $em             = $this->getDoctrine()->getEntityManager();
            $pidSeg         = $r->get('pidSeg');
            $pidEntity      = $r->get('pidEntity');
            $ptipoContacto  = $r->get('ptipoContacto');
            $msg            = "";
            $objSeguimiento = $em->getRepository('CenfotecBDBundle:Seguimiento')->find($pidSeg);
            
            switch ($ptipoContacto) {
                case "PA":
                case "ACTI":
                case "POS":
                    $objEntity = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($pidEntity);
                    $msg = "El seguimiento se elimino correctamente al contacto 'ID:" . $objEntity->getId() . " " . $objEntity->getNombreCompleto() . "'.";
                    break;

                case "EM":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Empresa')->find($pidEntity);
                    $msg = "El seguimiento se elimino correctamente a la empresa '" . $objEntity->getNombre() . "'.";
                    break;

                case "CO":
                    $objEntity = $em->getRepository('CenfotecBDBundle:Colegio')->find($pidEntity);
                    $msg = "El seguimiento se elimino correctamente al colegio '" . $objEntity->getNombre() . "'.";
                    break;
            }
            
            $objEntity->removeSeguimiento($objSeguimiento);
            $em->remove($objSeguimiento);
            $em->flush();
            
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array(
                'msg'      => $msg,
                'idEntity' => $objEntity->getId(),
                'countSeg' => count($objEntity->getSeguimientos()),
                'status'   => 'ok'
            ))); 
        } catch (\Exception $exc) {
            //Registrar Excepcion
            GlobalHelper::registrarExcepcion($this, array(
                'excepcion' => $exc->getMessage(),
                'metodo'    => __FUNCTION__." in ".__FILE__." at ".__LINE__
            ));
            
            return new \Symfony\Component\HttpFoundation\Response($exc->getMessage()); 
        }
    }
    
    /**
     * @Route("/ver-seguimientos/grid-layout", name="cenfo_seguimiento_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutSeguimientoAction()
    {
        $layoutGenerator = new GridLayoutGenerator();
        $layoutGenerator->setConfigurationOption('Sort','FechaSeguimiento');
        
        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        
        $layoutGenerator->setVariableColumns(array(
            array( 'Name' => "id",               'Type' => "Text", 'Width'    => 0,  'CanEdit'   => 1 ),
            array( 'Name' => "Name",             'Type' => "Text", 'Width'    => 0,  'CanExport' => 0 ),
            array( 'Name' => "FechaSeguimiento", 'Type' => "Date", 'RelWidth' => 3, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0, 'Format' => "yyyy-MM-dd hh:mm tt" ),
            array( 'Name' => "TipoSeguimiento",  'Type' => "Text", 'RelWidth' => 4, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Comentario",       'Type' => "Text", 'RelWidth' => 5, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 ),
            array( 'Name' => "Usuario",          'Type' => "Text", 'RelWidth' => 3, 'CanEdit'   => 0, 'Align' => 'Left',  'CaseSensitive' => 0 )
        ));
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/ver-seguimientos/grid-data", name="cenfo_seguimiento_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataSeguimientoAction() 
    { 
        $r              = $this->getRequest()->query;
        $em             = $this->getDoctrine()->getEntityManager();
        $pidEntity      = $r->get('pidEntity');
        $ptipoContacto  = $r->get('ptipoContacto');
        
        switch ($ptipoContacto) {
            case "PA":
            case "ACTI":
            case "POS":
                $objContactoCRM = $em->getRepository('CenfotecBDBundle:ContactoCRM')->find($pidEntity);
                $seguimientos   = $objContactoCRM->getSeguimientos();
                break;

            case "EM":
                $objEmpresa = $em->getRepository('CenfotecBDBundle:Empresa')->find($pidEntity);
                $seguimientos = $objEmpresa->getSeguimientos();
                break;

            case "CO":
                $objColegio = $em->getRepository('CenfotecBDBundle:Colegio')->find($pidEntity);
                $seguimientos = $objColegio->getSeguimientos();
                break;
        }

        $dataFormatter = new GridDataTreePagingFormatter();
        foreach ($seguimientos as $objSeguimiento) {
            $dataFormatter->addRow(array(
                'id'               => $objSeguimiento->getId(),
                'Name'             => $objSeguimiento->getComentario(),
                'TipoSeguimiento'  => $objSeguimiento->getTipo()->getNombre(),
                'Comentario'       => $objSeguimiento->getComentario(),
                'FechaSeguimiento' => $objSeguimiento->getFechaSeguimiento()->format('Y-m-d H:i'),
                'Usuario'          => $objSeguimiento->getUsuario()->getNombreCompleto()
            ));
        }
        return array('gridDataFormatter' => $dataFormatter);
    }
   
}
