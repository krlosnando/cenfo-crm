<?php

namespace Cenfotec\CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/telemercadeo")
*/
class TelemercadeoController extends Controller
{
    /**
     * @Route("/asignar", name="cenfo_telemercadeo_asignar")
     * @Template()
     */
    public function asignarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridTelemercadeo');
        
        $gridOptionsGenerator
            ->setGridId('GridTelemercadeo')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout', array('pflagTelemercadeoAsignar'=>1)),
                'Data_Url'   => "",
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        //Mostrar mensaje al cargar la pagina que tiene que seleccionar el tipo de contacto.
        $this->get('session')->setFlash(
            'notice',
            'Hacer click al tipo de contacto que se le dara Telemercadeo.'
        );
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/pendientes", name="cenfo_telemercadeo_pendientes")
     * @Template()
     */
    public function pendientesAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridTelemercadeo');
        
        $gridOptionsGenerator
            ->setGridId('GridTelemercadeo')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_prog_academico_grid_layout', array('pflagTelemercadeoPendiente'=>1)),
                'Data_Url'   => "",
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        //Mostrar mensaje al cargar la pagina que tiene que seleccionar el tipo de contacto.
        $this->get('session')->setFlash(
            'notice',
            'Hacer click al tipo de contacto que desea llamar.'
        );
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator
        );
    }
}
