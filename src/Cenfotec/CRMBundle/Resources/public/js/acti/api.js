var _apiActi = {
    //Copiar contacto de ACTI a BD Programa Academico
    copyToProgAcademico : {
        fn      : function(options){
            //Pasar el contacto de Acti a la base de datos de Programa Academico.
            $.ajax({
                url     : Routing.generate('cenfo_acti_copy_to_prog_academico'),
                data    : {
                    pidActi : options['idActi']
                },
                beforeSend : function(){
                    $('#content-area').showLoading();
                },
                success : function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Cuando todo se haya copiado correctamente.
                        if (_apiActi.copyToProgAcademico.success) {
                            _apiActi.copyToProgAcademico.success();
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    
                    $('#content-area').hideLoading();
                },
                error:function (xhr, textStatus, thrownError){  
                    $('#content-area').hideLoading();
                    if(xhr['responseText']){
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + xhr['responseText'] 
                        });
                    }else{
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + thrownError 
                        });
                    }
                },
                type: "POST"
            });
        },
        event   : function(e, options){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);

            if(selRow){
                //Pasar el contacto de Acti a la base de datos de Posgrado.
                _apiActi.copyToProgAcademico.fn({
                    idActi        : selRow.IDACTI,
                    idContactoCRM : selRow.id
                });
            }

            e.preventDefault();
        },
        success : null
    },
    //Copiar contacto de ACTI a Posgrado
    copyToPosgrado : {
        fn      : function(options){
            //Pasar el contacto de Acti a la base de datos de Posgrado.
            $.ajax({
                url     : Routing.generate('cenfo_acti_copy_to_posgrado'),
                data    : {
                    pidActi : options['idActi']
                },
                beforeSend : function(){
                    $('#content-area').showLoading();
                },
                success : function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            content : '<b>' + datos.msg + '</b> '
                        });
                    } else {
                        $.showAlert({
                            type    : 'error',
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
            
                    $('#content-area').hideLoading();
                },
                error:function (xhr, textStatus, thrownError){  
                    $('#content-area').hideLoading();
                    if(xhr['responseText']){
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + xhr['responseText'] 
                        });
                    }else{
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + thrownError 
                        });
                    }
                },
                type: "POST"
            });
        },
        event   : function(e, options){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);

            if(selRow){
                //Pasar el contacto de Acti a la base de datos de Posgrado.
                _apiActi.copyToPosgrado.fn({
                    idActi        : selRow.IDACTI,
                    idContactoCRM : selRow.id
                });
            }

            e.preventDefault();
        },
        success : null
    },
    //Registrar nuevo contacto
    loadFormRegistrar : {
        fn : function(options){
            var url = Routing.generate('cenfo_acti_form_registrar');
            window.location.href = url;
            $('#content-area').showLoading();
        },
        event : function(e){
            _apiActi.loadFormRegistrar.fn();
            e.preventDefault();
        }
    },
    //Modificar contacto
    loadFormModificar : {
        fn : function(options){
            //Agregamos el id del contacto de programa acadmico que se selecciono.
            var url = Routing.generate('cenfo_acti_form_modificar');
            window.open($.addToQueryString(url, 'pidActi', options['idActi']), '_blank');
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiActi.loadFormModificar.fn({
                    idActi : selRow.IDACTI
                });
            }
        }
    },
    //Elimina un contacto
    eliminar : {
        fn : function(options){
            var url = Routing.generate('cenfo_acti_eliminar');
            url = $.addToQueryString( url, 'pidActi', options['idActi']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : '#content-area', 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridACTI']){
                            var TGrid = Grids['GridACTI'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Deleted = 1;
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#content-area').hideLoading();
                },
                beforeSend: function(){
                    $('#content-area').showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#content-area').hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "POST"
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
                
            if (selRow) {
                _apiActi.eliminar.fn({
                    idActi : selRow.IDACTI
                });
            }
            e.preventDefault();
        }
    }
};

//ACTI
//------------------------------------------------------------------------------
//Funcionalidad para Copiar un Contacto de Acti a PA
$("#uiBtnCopyActiToPA").on("click", _apiActi.copyToProgAcademico.event);

//Funcionalidad para Copiar un Contacto de Acti a Posgrado
$("#uiBtnCopyActiToPOS").on("click", _apiActi.copyToPosgrado.event);

//Ver informacion del contacto
$("#uiVerInformacionActi").on("click", _apiCRM.verPerfil.event);

//Modificar contacto de ACTI
$("#uiBtnModificarActi").on("click", _apiActi.loadFormModificar.event);

//Registrar nuevo Contacto de ACTI
$("#uiBtnRegistrarActi").on("click", _apiActi.loadFormRegistrar.event);

//Eliminar Contacto de ACTI
$("#uiBtnEliminarActi").on("click", _apiActi.eliminar.event);

//Seguimientos
//------------------------------------------------------------------------------
if (typeof _apiSeg != "undefined") { 
    _apiSeg.settings.tipoContacto = 'ACTI';

    //Ver seguimientos
    $("#uiBtnVerSeguimientosActi").on("click", _apiSeg.modalVerSeguimientos.event);

    //Agregar seguimientos
    $("#uiBtnAgregarSeguimientoActi").on("click", _apiSeg.modalFormRegistrar.event);
}    