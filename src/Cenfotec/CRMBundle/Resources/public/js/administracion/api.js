var _apiAdminEntity = {
    //Configuracion Global para el api de administrar entitdades
    settings : {
        idEntity        : 0,
        refreshLoadings : function(element){
            var $e = $(element);
            //Actualizar las variables dondes se muestran los loadings y alerts
            if ($e.data('alert-success-to')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementAlertSuccess = $e.data('alert-success-to');
            }
            if ($e.data('data-alert-error-to')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementAlertError = $e.data('data-alert-error-to');
            }
            if ($e.data('loading-saving-to')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementLoadingSaving = $e.data('loading-saving-to');
            }
            if ($e.data('loading-opening-to')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementLoadingOpening = $e.data('loading-opening-to');
            }
            if ($e.data('alert-error-to')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementAlertError = $e.data('alert-error-to');
            }
            if ($e.data('alert-sel-row')) {
                _apiAdminEntity.modalFormAdministrar.settings.elementAlertSelRow = $e.data('alert-sel-row');
            }
        }
    },
    //Formulario para administrar los datos de la entidad    
    modalFormAdministrar : {
        //Configuracion Global para el api de seguimientos
        settings : {
            elementAlertSelRow    : '#content-area',        //btn-attr: data-alert-sel-row
            elementAlertSuccess   : '#content-area',        //btn-attr: data-alert-success-to
            elementAlertError     : '#modal-entidad',       //btn-attr: data-alert-error-to
            elementLoadingSaving  : '#modal-entidad',       //btn-attr: data-loading-saving-to
            elementLoadingOpening : '#content-area'         //btn-attr: data-loading-opening-to
        },
        //Function que se ejecuta cuando los datos se registran correctamente en el formulario de registrar.
        success : function(response) {
            var datos;

            if (response.indexOf("ok") >= 0) {
                datos = JSON.parse(response);
                $.showAlert({
                    type    : 'success',
                    showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertSuccess, 
                    content : '<b>' + datos.msg + '</b> '
                });

                //Verificar si existe el Grid de seguimientos para refrescarlo
                if(Grids[_apiAdminEntityCfg.IDGrid]){
                    Grids[_apiAdminEntityCfg.IDGrid].Reload();
                }
                
                //Ocultar el Loading antes de cerrar el Popup, por que sino queda el loading en pantalla.
                $(_apiAdminEntity.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
            
                //Cerrar el popup
                $('#btnCancelar').click();
            } else {
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError, 
                    content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                });
            }
            //Ocultar el Loading
            $(_apiAdminEntity.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
        },
        //Function que se ejecuta antes de guardar la informacion del seguimiento
        beforeSubmit : function(arr, $form, options) {
            $(_apiAdminEntity.modalFormAdministrar.settings.elementLoadingSaving).showLoading();
        },
        //Function que se ejecuta cuando ocurre un error al tratar de guardar los datos del seguimiento.
        error : function(xhr, textStatus, thrownError) {
            //Ocultar el Loading
            $(_apiAdminEntity.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
            
            if(xhr['responseText']){
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError,
                    content : '<b>Error.</b> ' + xhr['responseText']
                });
            }else{
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError,
                    content : '<b>Error.</b> ' + thrownError
                });
            }
        }        
    },
    //Muestra el formulario para registrar una nueva entidad      
    modalFormRegistrar : {
        //Muestra el formulario para registrar una entidad
        show : function(options){
            var url = Routing.generate('cenfo_' + _apiAdminEntityCfg.entityActionName + '_form_registrar');
            
            $.cdAjaxModal({
                url           : url,
                title         : 'Registrar ' + _apiAdminEntityCfg.entityName,
                showLoadingTo : _apiAdminEntity.modalFormAdministrar.settings.elementLoadingOpening,
                heigth        : 100,
                success       : function($modal){
                    //Codigo a ejecutar cuando el modal se muestra en pantalla.
                }
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiAdminEntity.settings.refreshLoadings(this);
            _apiAdminEntity.modalFormRegistrar.show();
            
            e.preventDefault();
        }
    },
    //Muestra el formulario para modificar un seguimiento
    modalFormModificar : {
        //Muestra el formulario
        show : function(options){
            var url = Routing.generate('cenfo_' + _apiAdminEntityCfg.entityActionName + '_form_modificar');
            url = $.addToQueryString( url, 'pidEntity', options['idEntity']);
            
            $.cdAjaxModal({
                url           : url,
                title         : 'Modificar ' + _apiAdminEntityCfg.entityName,
                showLoadingTo : _apiAdminEntity.modalFormAdministrar.settings.elementLoadingOpening,
                heigth        : 100,
                success       : function($modal){
                    //Codigo que se ejecutara cuando se abra el formulario de  modificar
                }
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiAdminEntity.settings.refreshLoadings(this);
            
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid, _apiAdminEntity.modalFormAdministrar.settings.elementAlertSelRow);
                
            if (selRow) {
                _apiAdminEntity.modalFormModificar.show({
                    idEntity : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Elimina un seguimiento
    eliminar : {
        //Elimina un seguimiento
        fn : function(options){
            var url = Routing.generate('cenfo_' + _apiAdminEntityCfg.entityActionName + '_eliminar');
            url = $.addToQueryString( url, 'pidEntity', options['idEntity']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertSuccess, 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar los datos 
                        if(Grids[_apiAdminEntityCfg.IDGrid]){
                            Grids[_apiAdminEntityCfg.IDGrid].Reload();
                        }
                       
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError, 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#' + _apiAdminEntityCfg.IDGridContainer).hideLoading();
                },
                beforeSend: function(){
                    $('#' + _apiAdminEntityCfg.IDGridContainer).showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#' + _apiAdminEntityCfg.IDGridContainer).hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError,
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiAdminEntity.modalFormAdministrar.settings.elementAlertError,
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "POST"
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiAdminEntity.settings.refreshLoadings(this);
            
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid, _apiAdminEntity.modalFormAdministrar.settings.elementAlertSelRow);
                
            if (selRow) {                
                _apiAdminEntity.eliminar.fn({
                    idEntity : selRow.id
                });
            }
            e.preventDefault();
        }
    }
};