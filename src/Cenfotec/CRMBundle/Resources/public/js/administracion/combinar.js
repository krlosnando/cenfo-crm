
//Evento para mostrar el formulario de combinar filas
$("#uiBtnCombinar").on("click", function(e){
    //Obtener las filas que se seleccionaron
    var $btn          = $(this);
    var grid          = Grids[$btn.attr('grid-id')];
    var rows          = grid.GetSelRows();
    var entity        = "";
    var datosCombinar = [];
    
    //Validar si seleccionaron filas
    if($(rows).size() != 0){
        $.each(rows, function(){
            datosCombinar.push({
                text  : this.id,
                value : this.Name
            });
        });

        $.cdAjaxModal({
            url           : Routing.generate('cenfo_combinar_form',{
                pentityName      : entity,
                pentityClassName : entity,
                pdatosCombinar   : datosCombinar
            }),
            showLoadingTo : '#uiBtnCombinar',
            heigth        : 100,
            success       : function($modal){
                //Radio Buttons
                $modal.find(".uniform-me").length > 0 && $(".uniform-me").uniform({radioClass: "uni-radio", buttonClass: "uni-button"});
                
                //Plugin chosen
                $modal.find("select").chosen({allow_single_deselect: true});
                
                //Calcular el nuevo tamaño para el Chosen.
                resize_chosen();
            },
            btnSave       : {
                click : function($modal){
                }
            }
        });
    }else{
        $.showAlert({
            type    : 'error',
            content : 'Por favor seleccione al menos 1 fila para hacer la combinación.'
        });
    }
    e.preventDefault();
});