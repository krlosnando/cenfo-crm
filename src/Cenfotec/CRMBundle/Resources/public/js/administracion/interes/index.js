//Valida que solo se selccionen filas de un mismo tipo para hacer la funcionalidad
//de combinar datos.
Grids.OnSelect = function(TGrid, TRow, Deselect){
    var rows     = TGrid.GetSelRows();
    var entity   = "";
    var datos    = null;
    var returval = false;
    
    if($(rows).size() != 0){
        $.each(rows, function(){
            entity = this.Level;
            return false;
        });

        if(entity != TRow.Level){
            returval = true;
        }
    }
    return returval;
};

/********************************************************************************************************************************************
 * Funcion que se ejecutara al momento de guardar los datos del grid.
 * Params (parametros):
 *      G: Grind en el que se ejecuto el evento.
 *      row: Fila en el grid.
 *      autoupdate: Indica si los cambios se guardan automaticamente.
 * Return (retorna): --
 ********************************************************************************************************************************************/
Grids.OnSave = function (G,R,autoupdate){
    //XMLString almacena el xml de cambios que retorna el Grid en el siguiente formato:
    //<Grid>
    //      <IO/>
    //      <Changes>
    //          <I id="1" Changed="1" W2="345" W4="345" W5="345" W6="345"/>
    //          <I id="2" Changed="1" W2="345" W3="345" W4="345" W5="345" W6="345"/>
    //      </Changes>
    //</Grid>
    var XMLString = G.GetChanges();
    
    if(XMLString != "<Grid><IO/><Changes></Changes></Grid>"){
        var xmlDoc = $.parseXML( XMLString );
        var $XMLChanges = $( xmlDoc );
        var results = $XMLChanges.find( "I" );
        var cambios  = {
            Area : {
                registrar  : [],
                actualizar : [],
                borrar     : []
            },
            Interes : {
                registrar  : [],
                actualizar : [],
                borrar     : []
            }
        };

        $.each(results, function() {
            var idRow    = this.getAttribute("id");
            var objRow   = G.GetRowById(idRow);
            
            //Validar que solo se actualize Interes y Area
            if(objRow.Level != 0){
                var entidad  = (objRow.Level == 2) ? 'Interes' : 'Area';
                var entidadP = (objRow.Level == 2) ? 'Area' : 'Tipo';
                var id       = objRow.id.replace(entidad+"_", "");
                var idParent = objRow.parentNode.id.replace(entidadP+"_", "");

                //Se agrego una nueva fila
                if(objRow.Added){
                    cambios[entidad]['registrar'].push({
                        idTemp   : objRow.id, 
                        nombre   : (objRow.Level == 2) ? objRow.Nombre : objRow.NodeCol,
                        idParent : idParent
                    });
                }

                //Se cambio una fila
                if((objRow.Changed || objRow.Moved) && !objRow.Added){
                    cambios[entidad]['actualizar'].push({
                        id     : id,
                        nombre : (objRow.Level == 2) ? objRow.Nombre : objRow.NodeCol,
                        idParent : idParent
                    });
                }

                //Se elimino una fila
                if(objRow.Deleted){
                    cambios[entidad]['borrar'].push({
                        id : id
                    });
                }
            }
        });
        //Guardamos o actualizamos los datos por ajax, por medio de un Action.
        $.ajax({
            url: Routing.generate('cenfo_admin_interes_guardar'),
            data: {
                pcambios: cambios
            },
            success: function(response){
                $('.msg').unblock();
                if(response == 'ok'){
                    $.showAlert({
                        type    : 'success',
                        content : 'Los datos se guardaron correctamente.'
                    });
                    G.Reload();
                }else{
                    if (response.indexOf("interes_sql_errors") >= 0){
                        var datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'error',
                            content : 'No se puede eliminar los siguientes datos ya que tienen información relacionada:' + datos.errores
                        });
                        G.Reload();
                    }else{
                        $.showAlert({
                            type    : 'error',
                            content : 'Un error ocurrio, por favor trata de nuevo.' + response
                        });
                    }
                }
            },
            beforeSend: function(){
                $('.msg').block({
                    message: 'Guardando los cambios...',
                    css: {
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .8,
                        color: '#fff',
                        border: '2px solid #81BEF7'
                    }
                });
            },
            error:function (xhr, ajaxOptions, thrownError){
                $('.msg').unblock();
                $.showAlert({
                    type    : 'error',
                    content : 'Un error ocurrio, por favor trata de nuevo.' + thrownError
                });
            },
            type: "POST"
        });
    }else{
        $.showAlert({
            type    : 'warning',
            content : 'No hay cambios que guardar.'
        });
    }
};

//Evento para mostrar el formulario de combinar filas
$("#uiBtnCombinar").on("click", function(e){
    //Obtener las filas que se seleccionaron
    var $btn          = $(this);
    var grid          = Grids[$btn.attr('grid-id')];
    var rows          = grid.GetSelRows();
    var entity        = "";
    var datosCombinar = [];
    var splitResult   = null;
    var parentRow     = null;
    var tipo         = null;
    var area         = null;
    
    //Validar si seleccionaron filas
    if($(rows).size() != 0){
        $.each(rows, function(){
            splitResult = this.id.split("_");
            entity      = splitResult[0];
            
            if(entity == 'Area'){
                parentRow = this.parentNode;
                datosCombinar.push({
                    text  : parentRow.NodeCol + ' - ' + this.NodeCol,
                    value : this.NodeCol.id.replace("Area_","")
                });
            }
            if(entity == 'Interes'){
                tipo = this.parentNode.parentNode.Name;
                area = this.parentNode.Name;

                datosCombinar.push( {
                    text  : tipo + ' - ' + area + ' : ' + this.Nombre,
                    value : this.id.replace("Interes_","")
                });
            }
        });

        $.cdAjaxModal({
            url           : Routing.generate('cenfo_combinar_form',{
                pentityName      : entity,
                pentityClassName : entity,
                pdatosCombinar   : datosCombinar
            }),
            showLoadingTo : '#uiBtnCombinar',
            heigth        : 100,
            success       : function($modal){
                //Radio Buttons
                $modal.find(".uniform-me").length > 0 && $(".uniform-me").uniform({radioClass: "uni-radio", buttonClass: "uni-button"});
                
                //Plugin chosen
                $modal.find("select").chosen({allow_single_deselect: true});
                
                //Calcular el nuevo tamaño para el Chosen.
                resize_chosen();
            },
            btnSave       : {
                click : function($modal){
                }
            }
        });
    }else{
        $.showAlert({
            type    : 'error',
            content : 'Por favor seleccione al menos 1 fila para hacer la combinación.'
        });
    }
    e.preventDefault();
});