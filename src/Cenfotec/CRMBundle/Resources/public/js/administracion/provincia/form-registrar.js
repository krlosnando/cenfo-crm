//Evento al agregar provincias desde cualquier parte
$(".uiBtnProvincia").on("click", function(e){
    $.cdAjaxModal({
        url           : Routing.generate('cenfo_provincia_form_registrar'),
        title         : 'Información del provincia',
        showLoadingTo : '.uiBtnProvincia',
        heigth        : 100,
        success       : function($modal){
        },
        btnSave       : {
            click : function($modal){
            }
        }
    });
    e.preventDefault();
});