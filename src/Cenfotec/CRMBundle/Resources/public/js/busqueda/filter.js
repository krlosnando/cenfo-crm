$(document).ready(function() {
    $('#uiBtnBuscar').click(function(e){
        var $this        = $(this);
        var dataUrl      = $this.attr('grid-data-url');
        var G            = Grids[0];
        var pnombre      = "", 
            papellido1   = "",
            papellido2   = "", 
            pcorreo      = "",
            pnumero      = "", 
            pinteres     = "",
            pcriterio    = "";
        
        if($('#ckbCriterio').is(':checked')){
            //Busqueda por medio del Criterio
            dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Criterio');
            if($('#uiTxtCriterio').val() !== ""){
                pcriterio = $('#uiTxtCriterio').val();
                dataUrl  = $.addToQueryString( dataUrl, 'pcriterio', pcriterio);
            }
        }else{
            //Busqueda por medio del Filtro
            dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Filtro');
            if($('#uiTxtNombre').val() !== ""){
                pnombre = $('#uiTxtNombre').val();
                dataUrl  = $.addToQueryString( dataUrl, 'pnombre',pnombre);
            }
            if($('#uiTxtApellido1').val() !== ""){
                papellido1 = $('#uiTxtApellido1').val();
                dataUrl    = $.addToQueryString(dataUrl, 'papellido1',papellido1);
            }
            if($('#uiTxtApellido2').val() !== ""){
                papellido2 = $('#uiTxtApellido2').val();
                dataUrl    = $.addToQueryString(dataUrl, 'papellido2',papellido2);
            }
            if($('#uiTxtCorreo').val() !== ""){
                pcorreo = $('#uiTxtCorreo').val();
                dataUrl = $.addToQueryString(dataUrl, 'pcorreo',pcorreo);
            }
            if($('#uiTxtNumero').val() !== ""){
                pnumero = $('#uiTxtNumero').val();
                dataUrl = $.addToQueryString(dataUrl, 'pnumero',pnumero);
            }
            if($('#uiSelInteres').val() !== null){
                pinteres = $('#uiSelInteres').val();
                dataUrl  = $.addToQueryString(dataUrl, 'pinteres',pinteres);
            }
        }
        
        //Actulizamos las URL para los datos.
        G.Source.Data.Url = dataUrl;
        G.Reload();
                
        //Prevenir que se refresque la pagina al dar click en <a></a>
        e.preventDefault();
    });
    $('#ckbCriterio').change(function(){
        if($(this).is(':checked')){
            $('#uiTxtCriterio').attr('disabled', false);
            $('#uiTxtNombre').attr('disabled', true);
            $('#uiTxtNombre').val("");
            $('#uiTxtApellido1').attr('disabled', true);
            $('#uiTxtApellido1').val("");
            $('#uiTxtApellido2').attr('disabled', true);
            $('#uiTxtApellido2').val("");
            $('#uiTxtCorreo').attr('disabled', true);
            $('#uiTxtCorreo').val("");
            $('#uiTxtNumero').attr('disabled', true);
            $('#uiTxtNumero').val("");
            $('#uiSelInteres').attr('disabled', true);
            $('#uiSelInteres').trigger("liszt:updated");
        }else{
            $('#uiTxtCriterio').attr('disabled', true);
            $('#uiTxtCriterio').val("");
            $('#uiTxtNombre').attr('disabled', false);
            $('#uiTxtApellido1').attr('disabled', false);
            $('#uiTxtApellido2').attr('disabled', false);
            $('#uiTxtCorreo').attr('disabled', false);
            $('#uiTxtNumero').attr('disabled', false);
            $('#uiSelInteres').attr('disabled', false);
            $('#uiSelInteres').trigger("liszt:updated");
        }
    });
    
    $('#ckbCriterio').trigger('change'); 
    $('#uiTxtNumero').mask("999999?9999");
    
    $('#uiBtnLimpiar').click(function(){
        $('#frmFilters')[0].reset();
    });
});

