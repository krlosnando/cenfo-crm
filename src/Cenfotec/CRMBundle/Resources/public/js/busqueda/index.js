$(document).ready(function() {
   
    $('.btn-toolbar').find('.button').click(function(e){
        var $this        = $(this);
        var tipoContacto = $this.attr('tipo-contacto');
        var G            = Grids[0];
        var pnombre      = "", 
            papellido1   = "",
            papellido2   = "", 
            pcorreo      = "",
            pnumero      = "", 
            pinteres     = "",
            pcriterio    = "",
            dataUrl      = "",
            layoutUrl    = "";
        
        //Esconder todos los toolbars, ya que todos tienen la clase "crm-box-toolbar"
        $('.crm-box-toolbar').hide();
        switch(tipoContacto){
            case 'ProgAcademico' :
                dataUrl   = _global.urls.dataUrlProgAcademico;
                layoutUrl = _global.urls.layoutUrlProgAcademico;
                $('.crm-box-pa').show();
                break;

            case 'ACTI' :
                dataUrl   = _global.urls.dataUrlACTI;
                layoutUrl = _global.urls.layoutUrlACTI;
                $('.crm-box-acti').show();
                break;
                
            case 'POS' :
                dataUrl   = _global.urls.dataUrlPosgrado;
                layoutUrl = _global.urls.layoutUrlPosgrado;
                $('.crm-box-posgrado').show();
                break;
                
            case 'Empresa' :
                dataUrl   = _global.urls.dataUrlEmpresa;
                layoutUrl = _global.urls.layoutUrlEmpresa;
                $('.crm-box-empresa').show();
                break;

            case 'Colegio' :
                dataUrl   = _global.urls.dataUrlColegio;
                layoutUrl = _global.urls.layoutUrlColegio;
                $('.crm-box-colegio').show();
                break;
        }
        
        
        if($('#ckbCriterio').is(':checked')){
            //Busqueda por medio del Criterio
            dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Criterio');
            if($('#uiTxtCriterio').val() !== ""){
                pcriterio = $('#uiTxtCriterio').val();
                dataUrl  = $.addToQueryString( dataUrl, 'pcriterio', pcriterio);
            }
        }else{
            //Busqueda por medio del Filtro
            dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Filtro');
            if($('#uiTxtNombre').val() !== ""){
                pnombre = $('#uiTxtNombre').val();
                dataUrl  = $.addToQueryString( dataUrl, 'pnombre',pnombre);
            }
            if($('#uiTxtApellido1').val() !== ""){
                papellido1 = $('#uiTxtApellido1').val();
                dataUrl    = $.addToQueryString(dataUrl, 'papellido1',papellido1);
            }
            if($('#uiTxtApellido2').val() !== ""){
                papellido2 = $('#uiTxtApellido2').val();
                dataUrl    = $.addToQueryString(dataUrl, 'papellido2',papellido2);
            }
            if($('#uiTxtCorreo').val() !== ""){
                pcorreo = $('#uiTxtCorreo').val();
                dataUrl = $.addToQueryString(dataUrl, 'pcorreo',pcorreo);
            }
            if($('#uiTxtNumero').val() !== ""){
                pnumero = $('#uiTxtNumero').val();
                dataUrl = $.addToQueryString(dataUrl, 'pnumero',pnumero);
            }
            if($('#uiSelInteres').val() !== null){
                pinteres = $('#uiSelInteres').val();
                dataUrl  = $.addToQueryString(dataUrl, 'pinteres',pinteres);
            }
        }
        
        //Actulizamos las URL para los datos.
        G.Source.Data.Url   = dataUrl;
        G.Source.Layout.Url = layoutUrl;
        G.Reload();
        
        //Cerrrar el mensaje si existe.
        $('.close').click();
        
        //Remover la clase disabled del boton que se habia presionado anteriormente
        $('.btn-toolbar').find('.button').removeClass('disabled');
        
        //Agregar la clase disabled al boton que se dio click
        $this.addClass('disabled');
        
        //Poner el texto al titulo de la pagina
        $('#page-header-title').text('Busqueda en ' + $this.text() + ' resultados');
        
        //Prevenir que se refresque la pagina al dar click en <a></a>
        e.preventDefault();
    });
    
    $('#ckbCriterio').change(function(){
        if($(this).is(':checked')){
            $('#uiTxtCriterio').attr('disabled', false);
            $('#uiTxtNombre').attr('disabled', true);
            $('#uiTxtNombre').val("");
            $('#uiTxtApellido1').attr('disabled', true);
            $('#uiTxtApellido1').val("");
            $('#uiTxtApellido2').attr('disabled', true);
            $('#uiTxtApellido2').val("");
            $('#uiTxtCorreo').attr('disabled', true);
            $('#uiTxtCorreo').val("");
            $('#uiTxtNumero').attr('disabled', true);
            $('#uiTxtNumero').val("");
            $('#uiSelInteres').attr('disabled', true);
            $('#uiSelInteres').trigger("liszt:updated");
        }else{
            $('#uiTxtCriterio').attr('disabled', true);
            $('#uiTxtCriterio').val("");
            $('#uiTxtNombre').attr('disabled', false);
            $('#uiTxtApellido1').attr('disabled', false);
            $('#uiTxtApellido2').attr('disabled', false);
            $('#uiTxtCorreo').attr('disabled', false);
            $('#uiTxtNumero').attr('disabled', false);
            $('#uiSelInteres').attr('disabled', false);
            $('#uiSelInteres').trigger("liszt:updated");
        }
    });
    
    $('#ckbCriterio').trigger('change'); 
    $('#uiTxtNumero').mask("999999?9999");
    
    $('#uiBtnLimpiar').click(function(){
        $('#frmBuscarContacto').reset();
    });
    
    $('#uiBtnBuscar').click(function(){
        var tipoBusqueda;
        if($('#ckbCriterio').is(':checked')){
            tipoBusqueda = 'Criterio';
        }else{
            tipoBusqueda = 'Filtro';
        }
        //Buscar cuantos datos hay para cada entidad
        $.ajax({
            url     : Routing.generate('cenfo_busqueda_count_resultados'),
            data    : {
                ptipoBusqueda : tipoBusqueda,
                pcriterio     : $('#uiTxtCriterio').val(),
                pnombre       : $('#uiTxtNombre').val(),
                papellido1    : $('#uiTxtApellido1').val(),
                papellido2    : $('#uiTxtApellido2').val(),
                pcorreo       : $('#uiTxtCorreo').val(),
                pnumero       : $('#uiTxtNumero').val(),
                pinteres      : $('#uiSelInteres').val()
            },
            beforeSend : function(){
                $('.grid-container').showLoading();
            },
            success : function(response){
                $('.grid-container').hideLoading();
                var datos = JSON.parse(response);
                $('.btn-toolbar').find('[tipo-contacto="ProgAcademico"]').find('span').text(datos.pcantProgAcademico);
                $('.btn-toolbar').find('[tipo-contacto="ACTI"]').find('span').text(datos.pcantActi);
                $('.btn-toolbar').find('[tipo-contacto="POS"]').find('span').text(datos.pcantPos);
                $('.btn-toolbar').find('[tipo-contacto="Empresa"]').find('span').text(datos.pcantEmpresa);
                $('.btn-toolbar').find('[tipo-contacto="Colegio"]').find('span').text(datos.pcantColegio);
            },
            error:function (xhr, textStatus, thrownError){  
                $('.grid-container').hideLoading();
                if(xhr['responseText']){
                    $.showAlert({
                        type    : 'error',
                        content : '<b>Error.</b> ' + xhr['responseText']
                    });
                }else{
                    $.showAlert({
                        type    : 'error',
                        content : '<b>Error.</b> ' + thrownError
                    });
                }
            },
            type: "POST"
        });       

        //Validar si el usuario ya apreto un tipo de contacto para ver en el Grid
        if($('.btn-toolbar').find('.disabled').length == 1){
            $('.btn-toolbar').find('.disabled').click();
        }
    });
    $('.crm-box-toolbar').hide();

    //Cuando se copie el contacto de ACTI a Programa Academico refrescar los contadores.
    _apiActi.copyToProgAcademico.success = function(){
        //Refrescar los datos
        $('#uiBtnBuscar').click();
    };
});

