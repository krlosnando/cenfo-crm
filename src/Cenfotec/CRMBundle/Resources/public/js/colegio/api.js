var _apiColegio = {
    //Mostrar popup para ver la informacion del contacto
    verPerfil : {
        fn : function(options){
            var url = Routing.generate('cenfo_colegio_ver_perfil');
            url = $.addToQueryString( url, 'pidColegio', options['idColegio']);

            $.cdAjaxModal({
                url           : url,
                title         : 'Información del contacto',
                showLoadingTo : '#content-area',
                heigth        : 100
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiColegio.verPerfil.fn({
                    idColegio : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Registrar nuevo contacto
    loadFormRegistrar : {
        fn : function(options){
            var url = Routing.generate('cenfo_colegio_form_registrar');
            window.location.href = url;
            $('#content-area').showLoading();
        },
        event : function(e){
            _apiColegio.loadFormRegistrar.fn();
            e.preventDefault();
        }
    },
    //Modificar contacto
    loadFormModificar : {
        fn : function(options){
            //Agregamos el id del contacto de programa acadmico que se selecciono.
            var url = Routing.generate('cenfo_colegio_form_modificar');
            window.open($.addToQueryString(url, 'pidColegio', options['idColegio']), '_blank');
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiColegio.loadFormModificar.fn({
                    idColegio : selRow.id
                });
            }
        }
    },
    //Elimina un contacto
    eliminar : {
        fn : function(options){
            var url = Routing.generate('cenfo_colegio_eliminar');
            url = $.addToQueryString( url, 'pidColegio', options['idColegio']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : '#content-area', 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridCO']){
                            var TGrid = Grids['GridCO'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Deleted = 1;
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#content-area').hideLoading();
                },
                beforeSend: function(){
                    $('#content-area').showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#content-area').hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "COT"
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
                
            if (selRow) {
                _apiColegio.eliminar.fn({
                    idColegio : selRow.id
                });
            }
            e.preventDefault();
        }
    }
};

//CO
//------------------------------------------------------------------------------
//Ver informacion del contacto
$("#uiVerInformacionColegio").on("click", _apiColegio.verPerfil.event);

//Modificar contacto de CO
$("#uiBtnModificarColegio").on("click", _apiColegio.loadFormModificar.event);

//Registrar nuevo Contacto de CO
$("#uiBtnRegistrarColegio").on("click", _apiColegio.loadFormRegistrar.event);

//Eliminar Contacto de CO
$("#uiBtnEliminarColegio").on("click", _apiColegio.eliminar.event);

//Seguimientos
//------------------------------------------------------------------------------
if (typeof _apiSeg != "undefined") { 
    _apiSeg.settings.tipoContacto = 'CO';

    //Ver seguimientos
    $("#uiBtnVerSeguimientosColegio").on("click", _apiSeg.modalVerSeguimientos.event);

    //Agregar seguimientos
    $("#uiBtnAgregarSeguimientoColegio").on("click", _apiSeg.modalFormRegistrar.event);
}