//Mostrar Especialidades solo cuando se selecciona la clase Técnico
$('.cgClasesColegio input').on("change", function(e){
    if($('.cgClasesColegio input:checked').val() == "1"){
        $(".cgEspecialidadColegio").show();
        $(".cgEspecialidadColegio").removeAttr("disabled");
    }else{
        $(".cgEspecialidadColegio").hide();
        $(".cgEspecialidadColegio").attr("disabled", true);
    }
    $(".cgEspecialidadColegio").trigger('liszt:updated');
    resize_chosen();
});
$('.cgClasesColegio input').trigger('change');

$.initSimpleTable({
    idTable     : '#table-numeros',
    btnAgregar  : '.btn-agregar-numero',
    btnBorrar   : '.btn-borrar-numero',
    rowTemplate : rowTablaNumeros,
    prefixIdRow : 'fila-tabla-numeros',
    addIdRowTo  : '.uiSelTipoNumero',
    infoBorrado : '#info-numeros-eliminados',
    onRowAdded  : function($newRow, key, idRowBD){
        //Agregar el Mask para el Numero y la extension.
        $newRow.find(".uiTxtNumero").mask("999999?9999");
        $newRow.find(".uiTxtExt").mask("99?99");

        //Verificar si hay que actualizar o eliminar segun la variable idRowBD
        var nameSelTipoNumero = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][tipoNumero]" : "data[numeros][registrar]["+key+"][tipoNumero]";
        var nameTxtNumer = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][numero]" : "data[numeros][registrar]["+key+"][numero]";
        var nameTxtExt = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][ext]" : "data[numeros][registrar]["+key+"][ext]";

        $newRow.find(".uiSelTipoNumero").attr("name", nameSelTipoNumero).attr("id", nameSelTipoNumero);
        $newRow.find(".uiTxtNumero").attr("name", nameTxtNumer).attr("id", nameTxtNumer);
        $newRow.find(".uiTxtExt").attr("name", nameTxtExt).attr("id", nameTxtExt);
    },
    onSuccess   : function($table){
        //Evento al cambiar de tipo de numero.
        $table.on("change", ".uiSelTipoNumero", function(e){
            var $e      = $(this);
            var $row    = $('#'+$e.attr('row-id'));
            var selVal  = $e.val();
            var selText = $e.find('option[value="'+selVal+'"]').text();
            if(selText=='Teléfono'){
                $row.find('.uiTxtExt').removeAttr('disabled');
            }else{
                $row.find('.uiTxtExt').attr('disabled', true);
            }
        });
        $(".uiSelTipoNumero").change();
    }
}); 
        
//Crear el accordion para manejar los contactos de la colegio
$.initSimpleAccordion({
    idAccordion : '#sap-accordion-contactos',
    btnAgregar  : '.sap-cfg-btn-agregar-contacto',
    btnBorrar   : '.sap-cfg-btn-borrar-contacto',
    rowTemplate : rowAccordionContactos,
    prefixIdRow : 'sap-fila-accordion-contacto',
    infoBorrado : '#sap-cfg-info-contactos-eliminados',
    onRowAdded  : function($newRow, key, idRowBD){
        var idToggleRow    = (idRowBD != 0) ? "sap-row-accordion-a-"+idRowBD : "sap-row-accordion-r-"+key;
        
        //Relacionar el togle para que se abra y cierre para la fila
        $newRow.find(".accordion-toggle").attr("href", "#"+idToggleRow);
        $newRow.find(".accordion-body").attr("id", idToggleRow);
                
        //Verificar si hay que actualizar o eliminar segun la variable idRowBD
        var uiTxtNombre    = (idRowBD != 0) ? "data[contactos][actualizar]["+idRowBD+"][nombre]" : "data[contactos][registrar]["+key+"][nombre]";
        var uiTxtApellido1 = (idRowBD != 0) ? "data[contactos][actualizar]["+idRowBD+"][apellido1]" : "data[contactos][registrar]["+key+"][apellido1]";
        var uiTxtApellido2 = (idRowBD != 0) ? "data[contactos][actualizar]["+idRowBD+"][apellido2]" : "data[contactos][registrar]["+key+"][apellido2]";
        var uiSelPuesto     = (idRowBD != 0) ? "data[contactos][actualizar]["+idRowBD+"][idPuesto]" : "data[contactos][registrar]["+key+"][idPuesto]";
        var base           = (idRowBD != 0) ? "data[contactos][actualizar]["+idRowBD+"]" : "data[contactos][registrar]["+key+"]";
        
        $newRow.find(".sap-field-nombre").attr("name", uiTxtNombre).attr("id", uiTxtNombre);
        $newRow.find(".sap-field-apellido1").attr("name", uiTxtApellido1).attr("id", uiTxtApellido1);
        $newRow.find(".sap-field-apellido2").attr("name", uiTxtApellido2).attr("id", uiTxtApellido2);
        $newRow.find(".sap-field-puesto").attr("name", uiSelPuesto).attr("id", uiSelPuesto);
        
        //Agregar el nombre del contacto en el accordion
        $newRow.find('.sap-field-nombre,.sap-field-apellido1,.sap-field-apellido2').change(function(){
            $newRow.find(".sap-field-nombre-completo").text(
                $newRow.find(".sap-field-nombre").val() + " " +
                $newRow.find(".sap-field-apellido1").val() + " " +
                $newRow.find(".sap-field-apellido2").val()
            );
        });
        
        $newRow.find(".sap-field-puesto").chosen({
            allow_single_deselect: true
        }); 

        //Calcular el nuevo tamaño para el Chosen.
        resize_chosen();
        
        //Simple Table Correos
        //----------------------------------------------------------------------
        var idTableCorreos  = (idRowBD != 0) ? "sap-table-correos-contactos-a-"+idRowBD : "sap-table-correos-contactos-r-"+key;
        var idAgregarCorreo = (idRowBD != 0) ? "sap-btn-agregar-correo-a-"+idRowBD : "sap-btn-agregar-correo-r-"+key;
        var prefixIdRow     = (idRowBD != 0) ? "sap-fila-tabla-correos-a-"+idRowBD : "sap-fila-tabla-correos-r-"+key;
        var idInfoBorrado   = (idRowBD != 0) ? "sap-info-borrado-correo-a-"+idRowBD : "sap-info-borrado-correo-r-"+key;
        
        $newRow.find(".sap-stp-table-correos").attr("id", idTableCorreos);
        $newRow.find(".sap-stp-btn-agregar-correo").attr("id", idAgregarCorreo);
        $newRow.find(".sap-stp-id-borrados-correo").attr("id", idInfoBorrado);
        
        //Crear la tabla para manejar los correos del contacto
        $.initSimpleTable({
            idTable     : '#' + idTableCorreos,
            btnAgregar  : '#' + idAgregarCorreo,
            btnBorrar   : '.sap-stp-btn-borrar-correo',
            rowTemplate : rowTablaCorreos,
            prefixIdRow : prefixIdRow,
            infoBorrado : '#' + idInfoBorrado,
            onRowAdded  : function($newRow, key, idRowBD){
                //Verificar si hay que actualizar o eliminar segun la variable idRowBD
                var nameTxtCorreo = (idRowBD != 0) ? (base + "[correos][actualizar]["+idRowBD+"]") : (base + "[correos][registrar]["+key+"]");
                $newRow.find(".uiTxtCorreo").attr("name", nameTxtCorreo).attr("id", nameTxtCorreo);
            }
        }); 

        //Simple Table Numeros
        //----------------------------------------------------------------------
        var idTableNumeros   = (idRowBD != 0) ? "sap-table-numeros-contactos-a-"+idRowBD : "sap-table-numeros-contactos-r-"+key;
        var idAgregarNumero  = (idRowBD != 0) ? "sap-btn-agregar-numero-a-"+idRowBD : "sap-btn-agregar-numero-r-"+key;
        var prefixIdRowNum   = (idRowBD != 0) ? "sap-fila-tabla-numeros-a-"+idRowBD : "sap-fila-tabla-numeros-r-"+key;
        var idInfoBorradoNum = (idRowBD != 0) ? "sap-info-borrado-numero-a-"+idRowBD : "sap-info-borrado-numbero-r-"+key;
        
        $newRow.find(".sap-stp-table-numeros").attr("id", idTableNumeros);
        $newRow.find(".sap-stp-btn-agregar-numero").attr("id", idAgregarNumero);
        $newRow.find(".sap-stp-id-borrados-numero").attr("id", idInfoBorradoNum);
        
        //Crear la tabla para manejar los numeros del contacto
        $.initSimpleTable({
            idTable     : '#' + idTableNumeros,
            btnAgregar  : '#' + idAgregarNumero,
            btnBorrar   : '.sap-stp-id-borrados-numero',
            addIdRowTo  : '.sap-stp-sel-tipo-numero',
            rowTemplate : rowTablaNumeros,
            prefixIdRow : prefixIdRowNum,
            infoBorrado : '#' + idInfoBorradoNum,
            onRowAdded  : function($newRow, key, idRowBD){
                //Agregar el Mask para el Numero y la extension.
                $newRow.find(".uiTxtNumero").mask("999999?9999");
                $newRow.find(".uiTxtExt").mask("99?99");

                //Verificar si hay que actualizar o eliminar segun la variable idRowBD
                var nameSelTipoNumero = (idRowBD != 0) ? (base + "[numeros][actualizar]["+idRowBD+"][tipoNumero]") : (base + "[numeros][registrar]["+key+"][tipoNumero]");
                var nameTxtNumer      = (idRowBD != 0) ? (base + "[numeros][actualizar]["+idRowBD+"][numero]") : (base + "[numeros][registrar]["+key+"][numero]");
                var nameTxtExt        = (idRowBD != 0) ? (base + "[numeros][actualizar]["+idRowBD+"][ext]") : (base + "[numeros][registrar]["+key+"][ext]");

                $newRow.find(".uiSelTipoNumero").attr("name", nameSelTipoNumero).attr("id", nameSelTipoNumero);
                $newRow.find(".uiTxtNumero").attr("name", nameTxtNumer).attr("id", nameTxtNumer);
                $newRow.find(".uiTxtExt").attr("name", nameTxtExt).attr("id", nameTxtExt);
            },
            onSuccess   : function($table){
                //Evento al cambiar de tipo de numero.
                $table.on("change", '.sap-stp-sel-tipo-numero', function(e){
                    var $e      = $(this);
                    var $row    = $('#'+$e.attr('row-id'));
                    var selVal  = $e.val();
                    var selText = $e.find('option[value="'+selVal+'"]').text();
                    if(selText=='Teléfono'){
                        $row.find('.uiTxtExt').removeAttr('disabled');
                    }else{
                        $row.find('.uiTxtExt').attr('disabled', true);
                    }
                });
                $table.find('.sap-stp-sel-tipo-numero').change();
            }
        }); 
    }
}); 

//Configuracion
_formAdministrarBase.vars.tipoContacto = "CO";

//Crear la tabla para manejar los intereses del contacto
_formAdministrarBase.initTableIntereses();

//Ajax Form para guardar los cambios de programa academico.
_formAdministrarBase.frmAdministrarAjaxForm();
