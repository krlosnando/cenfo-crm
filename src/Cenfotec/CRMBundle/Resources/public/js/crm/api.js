var _apiCRM = {
    vars : {
        actionModificarUrl : ""
    },
    //Mostrar popup para ver la informacion del contacto
    verPerfil : {
        fn : function(options){
            var url = Routing.generate('cenfo_contacto_crm_ver_perfil');
            url = $.addToQueryString( url, 'pidContactoCRM', options['idContactoCRM']);

            $.cdAjaxModal({
                url           : url,
                title         : 'Información del contacto',
                showLoadingTo : options['loadingTo'],
                heigth        : 100
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e        = $(this);
            var grid      = Grids[$e.data('grid-id')];
            var loadingTo = $e.data('loading-to') ? $e.data('loading-to') : '#content-area';
            var selRow    = getSelectedRow(grid);
            
            if(selRow){
                _apiCRM.verPerfil.fn({
                    idContactoCRM : selRow.id,
                    loadingTo     : loadingTo
                });
            }
            e.preventDefault();
        }
    },   
    //Combinar información
    combinarInformacion : {
        fn      : function(options){
            //Pasar el contacto de Prog Academico a la base de datos de Acti.
            var nuevaUrl = Routing.generate(_apiCRM.vars.actionModificarUrl,{
                pidContactoCRM : options["idContactoCRM"]
            });
            
            $('#frmAdministrar').attr("action", nuevaUrl);
            
            //Quitar Popup
            $('[data-dismiss]').click();
            
            //Enviar el formulario
            $('#frmAdministrar').submit();
        },
        event   : function(e, options){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid, $e.data('alert-select-row'));

            if(selRow){
                //Pasar el contacto de Acti a la base de datos de Posgrado.
                _apiCRM.combinarInformacion.fn({
                    idContactoCRM : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Continuar con la operación
    continuarOperacion : {
        event   : function(e, options){
            //Quitar Popup
            $('[data-dismiss]').click();
            
            //Enviar el formulario
            $('#frmAdministrar').submit();
            e.preventDefault();
        }
    }
};