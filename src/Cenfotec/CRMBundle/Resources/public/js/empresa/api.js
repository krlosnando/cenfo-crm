var _apiEmpresa = {
    //Mostrar popup para ver la informacion del contacto
    verPerfil : {
        fn : function(options){
            var url = Routing.generate('cenfo_empresa_ver_perfil');
            url = $.addToQueryString( url, 'pidEmpresa', options['idEmpresa']);

            $.cdAjaxModal({
                url           : url,
                title         : 'Información del contacto',
                showLoadingTo : '#content-area',
                heigth        : 100
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiEmpresa.verPerfil.fn({
                    idEmpresa : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Registrar nuevo contacto
    loadFormRegistrar : {
        fn : function(options){
            var url = Routing.generate('cenfo_empresa_form_registrar');
            window.location.href = url;
            $('#content-area').showLoading();
        },
        event : function(e){
            _apiEmpresa.loadFormRegistrar.fn();
            e.preventDefault();
        }
    },
    //Modificar contacto
    loadFormModificar : {
        fn : function(options){
            //Agregamos el id del contacto de programa acadmico que se selecciono.
            var url = Routing.generate('cenfo_empresa_form_modificar');
            window.open($.addToQueryString(url, 'pidEmpresa', options['idEmpresa']), '_blank');
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiEmpresa.loadFormModificar.fn({
                    idEmpresa : selRow.id
                });
            }
        }
    },
    //Elimina un contacto
    eliminar : {
        fn : function(options){
            var url = Routing.generate('cenfo_empresa_eliminar');
            url = $.addToQueryString( url, 'pidEmpresa', options['idEmpresa']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : '#content-area', 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridEM']){
                            var TGrid = Grids['GridEM'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Deleted = 1;
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#content-area').hideLoading();
                },
                beforeSend: function(){
                    $('#content-area').showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#content-area').hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "EMT"
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
                
            if (selRow) {
                _apiEmpresa.eliminar.fn({
                    idEmpresa : selRow.id
                });
            }
            e.preventDefault();
        }
    }
};

//EM
//------------------------------------------------------------------------------
//Ver informacion del contacto
$("#uiVerInformacionEmpresa").on("click", _apiEmpresa.verPerfil.event);

//Modificar contacto de EM
$("#uiBtnModificarEmpresa").on("click", _apiEmpresa.loadFormModificar.event);

//Registrar nuevo Contacto de EM
$("#uiBtnRegistrarEmpresa").on("click", _apiEmpresa.loadFormRegistrar.event);

//Eliminar Contacto de EM
$("#uiBtnEliminarEmpresa").on("click", _apiEmpresa.eliminar.event);

//Seguimientos
//------------------------------------------------------------------------------
if (typeof _apiSeg != "undefined") { 
    _apiSeg.settings.tipoContacto = 'EM';

    //Ver seguimientos
    $("#uiBtnVerSeguimientosEmpresa").on("click", _apiSeg.modalVerSeguimientos.event);

    //Agregar seguimientos
    $("#uiBtnAgregarSeguimientoEmpresa").on("click", _apiSeg.modalFormRegistrar.event);
}
