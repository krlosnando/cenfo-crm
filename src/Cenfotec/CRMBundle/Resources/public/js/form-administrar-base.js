
var _formAdministrarBase = {
    //Variables globales
    vars : {
        //PA - ACTI - POS - EM - CO
        tipoContacto : ""
    },
    
    //Validar formulario y buscar si no existe registros duplicados para guardar los cambios
    validarFormularioMostrarDuplicados : function(){
        var $validate = $('#frmAdministrar').validate();
        if($validate.form()){
            if (ptwAction == "Registrar") {
                
                //Verificar si existe algun contacto con algun correo o numero de los que se ingresaron.
                var url = Routing.generate('cenfo_contacto_crm_listar_duplicados');
                var contador = 0;
                
                //Agregar el tipo de contacto a la URL
                url = $.addToQueryString( url, 'ptipoContacto', _formAdministrarBase.vars.tipoContacto);
                
                switch(_formAdministrarBase.vars.tipoContacto){
                    case 'EM' :
                    case 'CO' :
                        //Agregar el nombre del colegio
                        url = $.addToQueryString( url, 'pnombre', $('#uiTxtNombre').val());

                        //Agregar los numeros
                        $('#table-numeros').find('.fila-tabla-numeros').each(function(){
                            var $e = $(this);
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][tipoNumero]', $e.find('.uiSelTipoNumero').val());
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][numero]', $e.find('.uiTxtNumero').val());
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][ext]', $e.find('.uiTxtExt').val());
                            url = $.addToQueryString( url, 'pnumeros['+contador+']', $e.find('.uiTxtNumero').val());
                            contador ++;
                        });
                        break;
                    
                    //PA - ACTI - POS
                    default:
                        //Agregar el nombre del colegio
                        url = $.addToQueryString( url, 'pnombre', $('#uiTxtNombre').val());
                        
                        //Agregar la identificacion
                        url = $.addToQueryString( url, 'pidentificacion', $('#uiTxtIdentificacion').val());

                        //Agregar los correos
                        $('#table-correos').find('.uiTxtCorreo').each(function(){ 
                            url = $.addToQueryString( url, 'pcorreos[]', this.value);
                        });

                        //Agregar los numeros
                        $('#table-numeros').find('.fila-tabla-numeros').each(function(){
                            var $e = $(this);
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][tipoNumero]', $e.find('.uiSelTipoNumero').val());
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][numero]', $e.find('.uiTxtNumero').val());
                            //url = $.addToQueryString( url, 'pnumeros['+contador+'][ext]', $e.find('.uiTxtExt').val());
                            url = $.addToQueryString( url, 'pnumeros['+contador+']', $e.find('.uiTxtNumero').val());
                            contador ++;
                        });
                        break;
                }
                
                //Cargamos los datos que tendra el modal.
                $.ajax({
                    url     : url,
                    success : function(htmlResponse){
                        $("#content-area").hideLoading();

                        if (htmlResponse == "No existen contactos duplicados") {
                            //Registrar o modificar el contacto
                            $('#frmAdministrar').submit();
                        }else{
                            $.cdAjaxModal({
                                html   : htmlResponse,
                                title  : 'Contactos que ya existen con la misma información',
                                heigth : 100
                            });
                        }
                    },
                    beforeSend : function(arr, $form, options) {
                        $("#content-area").showLoading();
                    },
                    error:function (xhr, textStatus, thrownError){  
                        //Ocultar el Loading
                        $('#content-area').hideLoading();

                        if(xhr['responseText']){
                            $.showAlert({
                                type    : 'error',
                                showTo  : '#content-area',
                                content : '<b>Error.</b> ' + xhr['responseText']
                            });
                        }else{
                            $.showAlert({
                                type    : 'error',
                                showTo  : '#content-area',
                                content : '<b>Error.</b> ' + thrownError
                            });
                        }
                    },
                    type: "POST"
                });
            }else {
                $('#frmAdministrar').submit();
            }
        }else{
            $.showAlert({
                type    : 'error',
                content : '<b>Por favor ingresar la información requerida (*).</b> '
            });
        }
    },
    
    //Validar formulario para guardar los cambios
    validarFormulario : function(){
        var $validate = $('#frmAdministrar').validate();
        if($validate.form()){
            $('#frmAdministrar').submit();
        }else{
            $.showAlert({
                type    : 'error',
                content : '<b>Por favor ingresar la información requerida (*).</b> '
            });
        }
    },
    
    //Evento al agregar matriculas al estudiante
    uiBtnMatricula : function(){
        $("#uiBtnMatricula").on("click", function(e){
            $.cdAjaxModal({
                url           : Routing.generate('cenfo_matricula_form_registrar', {
                    pidEntity      : $('#uiTxtIdContactoCRM').val(),
                    ptipoContacto  : _formAdministrarBase.vars.tipoContacto
                }),
                title         : 'Información de la matricula',
                showLoadingTo : '.button-basic-blue',
                heigth        : 100,
                success       : function($modal){
                    //Calcular el nuevo tamaño para el Chosen.
                    resize_chosen();
                }
            });
            e.preventDefault();
        });
    },
    
    //Mostrar Canton solo cuando se selecciona pais como Costa Rica
    uiSelPaisResidencia : function(){
        $("#uiSelPaisResidencia").on("change", function(e){
            if($(this).find(":selected").text() == "Costa Rica"){
                $("#cgSelCanton").show();
                $("#uiSelCanton").removeAttr("disabled");
            }else{
                $("#cgSelCanton").hide();
                $("#uiSelCanton").attr("disabled", true);
            }
            $("#uiSelCanton").trigger('liszt:updated');
            resize_chosen();
        });
        $("#uiSelPaisResidencia").trigger('change');
    },
    
    //Crear la tabla para manejar los correos del contacto
    initTableCorreos : function(){
        $.initSimpleTable({
            idTable     : '#table-correos',
            btnAgregar  : '.btn-agregar-correo',
            btnBorrar   : '.btn-borrar-correo',
            rowTemplate : rowTablaCorreos,
            prefixIdRow : 'fila-tabla-correos',
            infoBorrado : '#info-correos-eliminados',
            onRowAdded  : function($newRow, key, idRowBD){
                //Verificar si hay que actualizar o eliminar segun la variable idRowBD
                var nameTxtCorreo = (idRowBD != 0) ? "data[correos][actualizar]["+idRowBD+"]" : "data[correos][registrar]["+key+"]";
                $newRow.find(".uiTxtCorreo").attr("name", nameTxtCorreo).attr("id", nameTxtCorreo);
            }
        }); 
    },
    
    //Crear la tabla para manejar los numeros del contacto
    initTableNumeros : function(){
        $.initSimpleTable({
            idTable     : '#table-numeros',
            btnAgregar  : '.btn-agregar-numero',
            btnBorrar   : '.btn-borrar-numero',
            rowTemplate : rowTablaNumeros,
            prefixIdRow : 'fila-tabla-numeros',
            addIdRowTo  : '.uiSelTipoNumero',
            infoBorrado : '#info-numeros-eliminados',
            onRowAdded  : function($newRow, key, idRowBD){
                //Agregar el Mask para el Numero y la extension.
                $newRow.find(".uiTxtNumero").mask("999999?9999");
                $newRow.find(".uiTxtExt").mask("99?99");

                //Verificar si hay que actualizar o eliminar segun la variable idRowBD
                var nameSelTipoNumero = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][tipoNumero]" : "data[numeros][registrar]["+key+"][tipoNumero]";
                var nameTxtNumer = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][numero]" : "data[numeros][registrar]["+key+"][numero]";
                var nameTxtExt = (idRowBD != 0) ? "data[numeros][actualizar]["+idRowBD+"][ext]" : "data[numeros][registrar]["+key+"][ext]";

                $newRow.find(".uiSelTipoNumero").attr("name", nameSelTipoNumero).attr("id", nameSelTipoNumero);
                $newRow.find(".uiTxtNumero").attr("name", nameTxtNumer).attr("id", nameTxtNumer);
                $newRow.find(".uiTxtExt").attr("name", nameTxtExt).attr("id", nameTxtExt);
            },
            onSuccess   : function($table){
                //Evento al cambiar de tipo de numero.
                $table.on("change", ".uiSelTipoNumero", function(e){
                    var $e      = $(this);
                    var $row    = $('#'+$e.attr('row-id'));
                    var selVal  = $e.val();
                    var selText = $e.find('option[value="'+selVal+'"]').text();
                    if(selText=='Teléfono'){
                        $row.find('.uiTxtExt').removeAttr('disabled');
                    }else{
                        $row.find('.uiTxtExt').attr('disabled', true);
                    }
                });
                $(".uiSelTipoNumero").change();
            }
        }); 
    },
    
    //Crear la tabla para manejar los intereses del contacto
    initTableIntereses : function(){
        $.initSimpleTable({
            idTable     : '#table-interes',
            btnAgregar  : '.btn-agregar-interes',
            btnBorrar   : '.btn-borrar-interes',
            rowTemplate : rowTablaInteres,
            prefixIdRow : 'fila-tabla-interes',
            infoBorrado : '#info-intereses-eliminados',
            onRowAdded  : function($newRow, key, idRowBD){
                var nameDtpFecha     = (idRowBD != 0) ? "data[intereses][actualizar]["+idRowBD+"][fecha]" : "data[intereses][registrar]["+key+"][fecha]",
                    nameSelInteres   = (idRowBD != 0) ? "data[intereses][actualizar]["+idRowBD+"][idInteres]" : "data[intereses][registrar]["+key+"][idInteres]",
                    nameSelModalidad = (idRowBD != 0) ? "data[intereses][actualizar]["+idRowBD+"][idModalidad]" : "data[intereses][registrar]["+key+"][idModalidad]";

                $newRow.find(".uiSelModalidad").attr("name", nameSelModalidad).attr("id", nameSelModalidad);
                $newRow.find(".uiDtpFecha").attr("name", nameDtpFecha).attr("id", nameDtpFecha);
                $newRow.find(".uiSelInteres").attr("name", nameSelInteres).attr("id", nameSelInteres);
                $newRow.find(".uiDtpFecha").datepicker({
                    language : 'es', 
                    format   : 'yyyy-mm-dd'
                });
                $newRow.find(".chosen-select").chosen({
                    allow_single_deselect: true
                }); 

                //Calcular el nuevo tamaño para el Chosen.
                resize_chosen();
            }
        }); 
    },
    
    //Ajax Form para guardar los cambios de programa academico.
    frmAdministrarAjaxForm : function() {
        $('#frmAdministrar').ajaxForm({
            success : function(response) {
                var datos;

                if (response.indexOf("ok") >= 0) {
                    datos = JSON.parse(response);

                    window.location.href = datos.linkToRedirect;
                } else {
                    $.showAlert({
                        type    : 'error',
                        content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                    });

                    //Ocultar el Loading
                    $("#content-area").hideLoading();
                }
            },
            beforeSubmit : function(arr, $form, options) {
                $("#content-area").showLoading();

            },
            error : function(xhr, textStatus, thrownError) {
                //Ocultar el Loading
                $("#content-area").hideLoading();

                if(xhr['responseText']){
                    $.showAlert({
                        type    : 'error',
                        content : '<b>Error.</b> ' + xhr['responseText']
                    });
                }else{
                    $.showAlert({
                        type    : 'error',
                        content : '<b>Error.</b> ' + thrownError
                    });
                }
            }  
        });
    }
};