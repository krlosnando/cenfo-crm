var _apiPosgrado = {
    //Copiar contacto de Posgrado a BD Programa Academico
    copyToProgAcademico : {
        fn      : function(options){
            //Pasar el contacto de Posgrado a la base de datos de Programa Academico.
            $.ajax({
                url     : Routing.generate('cenfo_posgrado_copy_to_prog_academico'),
                data    : {
                    pidPosgrado : options['idPosgrado']
                },
                beforeSend : function(){
                    $('#content-area').showLoading();
                },
                success : function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Cuando todo se haya copiado correctamente.
                        if (_apiPosgrado.copyToProgAcademico.success) {
                            _apiPosgrado.copyToProgAcademico.success();
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    
                    $('#content-area').hideLoading();
                },
                error:function (xhr, textStatus, thrownError){  
                    $('#content-area').hideLoading();
                    if(xhr['responseText']){
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + xhr['responseText'] 
                        });
                    }else{
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + thrownError 
                        });
                    }
                },
                type: "POST"
            });
        },
        event   : function(e, options){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);

            if(selRow){
                //Pasar el contacto de Posgrado a la base de datos de Posgrado.
                _apiPosgrado.copyToProgAcademico.fn({
                    idPosgrado        : selRow.IDPOS,
                    idContactoCRM : selRow.id
                });
            }

            e.preventDefault();
        },
        success : null
    },
    //Registrar nuevo contacto
    loadFormRegistrar : {
        fn : function(options){
            var url = Routing.generate('cenfo_posgrado_form_registrar');
            window.location.href = url;
            $('#content-area').showLoading();
        },
        event : function(e){
            _apiPosgrado.loadFormRegistrar.fn();
            e.preventDefault();
        }
    },
    //Modificar contacto
    loadFormModificar : {
        fn : function(options){
            //Agregamos el id del contacto de programa acadmico que se selecciono.
            var url = Routing.generate('cenfo_posgrado_form_modificar');
            window.open($.addToQueryString(url, 'pidPosgrado', options['idPosgrado']), '_blank');
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiPosgrado.loadFormModificar.fn({
                    idPosgrado : selRow.IDPOS
                });
            }
        }
    },
    //Elimina un contacto
    eliminar : {
        fn : function(options){
            var url = Routing.generate('cenfo_posgrado_eliminar');
            url = $.addToQueryString( url, 'pidPosgrado', options['idPosgrado']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : '#content-area', 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridPOS']){
                            var TGrid = Grids['GridPOS'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Deleted = 1;
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#content-area').hideLoading();
                },
                beforeSend: function(){
                    $('#content-area').showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#content-area').hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "POST"
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
                
            if (selRow) {
                _apiPosgrado.eliminar.fn({
                    idPosgrado : selRow.IDPOS
                });
            }
            e.preventDefault();
        }
    }
};

//POS
//------------------------------------------------------------------------------
//Funcionalidad para Copiar un Contacto de Posgrado a PA
$("#uiBtnCopyPosgradoToPA").on("click", _apiPosgrado.copyToProgAcademico.event);

//Ver informacion del contacto
$("#uiVerInformacionPosgrado").on("click", _apiCRM.verPerfil.event);

//Modificar contacto de POS
$("#uiBtnModificarPosgrado").on("click", _apiPosgrado.loadFormModificar.event);

//Registrar nuevo Contacto de POS
$("#uiBtnRegistrarPosgrado").on("click", _apiPosgrado.loadFormRegistrar.event);

//Eliminar Contacto de POS
$("#uiBtnEliminarPosgrado").on("click", _apiPosgrado.eliminar.event);

//Seguimientos
//------------------------------------------------------------------------------
if (typeof _apiSeg != "undefined") { 
    _apiSeg.settings.tipoContacto = 'POS';

    //Ver seguimientos
    $("#uiBtnVerSeguimientosPosgrado").on("click", _apiSeg.modalVerSeguimientos.event);

    //Agregar seguimientos
    $("#uiBtnAgregarSeguimientoPosgrado").on("click", _apiSeg.modalFormRegistrar.event);
}