var _apiPA = {
    //Copiar contacto de ACTI a BD Programa Academico
    copyToActi : {
        fn      : function(options){
            //Pasar el contacto de Prog Academico a la base de datos de Acti.
            $.ajax({
                url     : Routing.generate('cenfo_prog_academico_copy_to_acti'),
                data    : {
                    pidPA : options['idPA']
                },
                beforeSend : function(){
                    $('#content-area').showLoading();
                },
                success : function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Cuando todo se haya copiado correctamente.
                        if (_apiPA.copyToActi.success) {
                            _apiPA.copyToActi.success();
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    
                    $('#content-area').hideLoading();
                },
                error:function (xhr, textStatus, thrownError){  
                    $('#content-area').hideLoading();
                    if(xhr['responseText']){
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + xhr['responseText'] 
                        });
                    }else{
                        $.showAlert({ 
                            type    : 'error',  
                            content : '<b>Error.</b> ' + thrownError 
                        });
                    }
                },
                type: "POST"
            });
        },
        event   : function(e, options){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);

            if(selRow){
                //Pasar el contacto de Acti a la base de datos de Posgrado.
                _apiPA.copyToActi.fn({
                    idPA          : selRow.IDPA,
                    idContactoCRM : selRow.id
                });
            }
            e.preventDefault();
        },
        success : null
    },
    //Agregar matricula        
    loadFormAddMatricula : {
        fn : function(options){
            var url = Routing.generate('cenfo_matricula_form_registrar');
            url = $.addToQueryString( url, 'pidEntity', options['idEntity']);
            url = $.addToQueryString( url, 'ptipoContacto', options['tipoContacto']);
            
            $.cdAjaxModal({
                url           : url,
                title         : 'Información de la matricula',
                showLoadingTo : '#content-area',
                heigth        : 100,
                success       : function($modal){
                    //Calcular el nuevo tamaño para el Chosen.
                    resize_chosen();
                }
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiPA.loadFormAddMatricula.fn({
                    idEntity     : selRow.id,
                    tipoContacto : 'PA'
                });
            }
            e.preventDefault();
        }
    },
    //Registrar nuevo contacto
    loadFormRegistrar : {
        fn : function(options){
            var url = Routing.generate('cenfo_prog_academico_form_registrar');
            window.location.href = url;
            $('#content-area').showLoading();
        },
        event : function(e){
            _apiPA.loadFormRegistrar.fn();
            e.preventDefault();
        }
    },
    //Modificar contacto
    loadFormModificar : {
        fn : function(options){
            //Agregamos el id del contacto de programa acadmico que se selecciono.
            var url = Routing.generate('cenfo_prog_academico_form_modificar');
            window.open($.addToQueryString(url, 'pidPA', options['idPA']), '_blank');
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiPA.loadFormModificar.fn({
                    idPA : selRow.IDPA
                });
            }
        }
    },
    //Elimina un contacto
    eliminar : {
        fn : function(options){
            var url = Routing.generate('cenfo_prog_academico_eliminar');
            url = $.addToQueryString( url, 'pidPA', options['idPA']);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : '#content-area', 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridPA']){
                            var TGrid = Grids['GridPA'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Deleted = 1;
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $('#content-area').hideLoading();
                },
                beforeSend: function(){
                    $('#content-area').showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $('#content-area').hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : '#content-area', 
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "POST"
            });
        },
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
                
            if (selRow) {
                _apiPA.eliminar.fn({
                    idPA : selRow.IDPA
                });
            }
            e.preventDefault();
        }
    }
};

//Programa Academico
//------------------------------------------------------------------------------
//Copiar contacto de Programa Academico a la base de datos de ACTI
$("#uiBtnCopyPAToActi").on("click", _apiPA.copyToActi.event);

//Ver informacion del contacto
$("#uiVerInformacionPA").on("click", _apiCRM.verPerfil.event);

//Modificar contacto de Programa Academico
$("#uiBtnModificarPA").on("click", _apiPA.loadFormModificar.event);

//Registrar nuevo Contacto de Programa Academico
$("#uiBtnRegistrarPA").on("click", _apiPA.loadFormRegistrar.event);

//Eliminar Contacto de Programa Academico
$("#uiBtnEliminarPA").on("click", _apiPA.eliminar.event);

//Agregar matricula al contacto de Programa Academico
$("#uiBtnAdminMatriculaPA").on("click", _apiPA.loadFormAddMatricula.event);


//Seguimientos
//------------------------------------------------------------------------------
if (typeof _apiSeg != "undefined") { 
    _apiSeg.settings.tipoContacto = 'PA';

    //Ver seguimientos
    $("#uiBtnVerSeguimientosPA").on("click", _apiSeg.modalVerSeguimientos.event);

    //Agregar seguimientos
    $("#uiBtnAgregarSeguimientoPA").on("click", _apiSeg.modalFormRegistrar.event);
}
    
