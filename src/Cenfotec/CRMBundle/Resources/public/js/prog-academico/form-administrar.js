//Configuracion
_formAdministrarBase.vars.tipoContacto = "PA";
        
//Evento al agregar matriculas al estudiante
_formAdministrarBase.uiBtnMatricula();

//Mostrar Canton solo cuando se selecciona pais como Costa Rica
_formAdministrarBase.uiSelPaisResidencia();

//Crear la tabla para manejar los correos del contacto
_formAdministrarBase.initTableCorreos();

//Crear la tabla para manejar los numeros del contacto
_formAdministrarBase.initTableNumeros();

//Crear la tabla para manejar los intereses del contacto
_formAdministrarBase.initTableIntereses();

//Ajax Form para guardar los cambios de programa academico.
_formAdministrarBase.frmAdministrarAjaxForm();