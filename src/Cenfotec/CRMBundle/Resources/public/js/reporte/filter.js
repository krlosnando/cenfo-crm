$(document).ready(function() {
    $('#uiBtnBuscarPA').click(function(e){
        var $this        = $(this);
        var dataUrl      = $this.attr('grid-data-url');
        var G            = Grids[0];
        var pfechaInicio = "", 
            pfechaFin    = "";
        
        //Busqueda por medio del Filtro
        dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Filtro');

        if($('#uiDtpFechaInicioPA').val() !== ""){
            pfechaInicio = $('#uiDtpFechaInicioPA').val();
            dataUrl      = $.addToQueryString(dataUrl, 'pfechaInicioContacto', pfechaInicio);
        }
        
        if($('#uiDtpFechaFinPA').val() !== ""){
            pfechaFin = $('#uiDtpFechaFinPA').val();
            dataUrl   = $.addToQueryString(dataUrl, 'pfechaFinContacto', pfechaFin);
        }

        //Actulizamos las URL para los datos.
        G.Source.Data.Url = dataUrl;
        G.Reload();
                
        //Prevenir que se refresque la pagina al dar click en <a></a>
        e.preventDefault();
    });
    
    $('#uiBtnBuscarACTIPos').click(function(e){
        var $this        = $(this);
        var dataUrl      = $this.attr('grid-data-url');
        var G            = Grids[0];
        var pfechaInicio = "", 
            pfechaFin    = "",
            pinteres     = "";
        
        //Busqueda por medio del Filtro
        dataUrl = $.addToQueryString( dataUrl,'ptipoBusqueda', 'Filtro');

        if($('#uiDtpFechaInicioACTIPos').val() !== ""){
            pfechaInicio = $('#uiDtpFechaInicioACTIPos').val();
            dataUrl      = $.addToQueryString(dataUrl, 'pfechaInicioInteres', pfechaInicio);
        }
        
        if($('#uiDtpFechaFinPA').val() !== ""){
            pfechaFin = $('#uiDtpFechaFinACTIPos').val();
            dataUrl   = $.addToQueryString(dataUrl, 'pfechaFinInteres', pfechaFin);
        }
        
        if($('#uiSelInteres').val() !== null){
            pinteres = $('#uiSelInteresACTIPos').val();
            dataUrl  = $.addToQueryString(dataUrl, 'pinteres',pinteres);
        }

        //Actulizamos las URL para los datos.
        G.Source.Data.Url = dataUrl;
        G.Reload();
                
        //Prevenir que se refresque la pagina al dar click en <a></a>
        e.preventDefault();
    });
    
    
    $('#uiBtnLimpiarPA').click(function(){
        $('#frmFiltersPA')[0].reset();
    });

    $('#uiBtnLimpiarACTIPos').click(function(){
        $('#frmFiltersACTIPos')[0].reset();
    });
});

