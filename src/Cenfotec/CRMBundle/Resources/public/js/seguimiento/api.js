var _apiSeg = {
    //Configuracion Global para el api de seguimientos
    settings : {
        tipoContacto    : '',
        idEntity        : 0,
        refreshLoadings : function(element){
            var $e = $(element);
            //Actualizar las variables dondes se muestran los loadings y alerts
            if ($e.data('alert-success-to')) {
                _apiSeg.modalFormAdministrar.settings.elementAlertSuccess = $e.data('alert-success-to');
            }
            if ($e.data('data-alert-error-to')) {
                _apiSeg.modalFormAdministrar.settings.elementAlertError = $e.data('data-alert-error-to');
            }
            if ($e.data('loading-saving-to')) {
                _apiSeg.modalFormAdministrar.settings.elementLoadingSaving = $e.data('loading-saving-to');
            }
            if ($e.data('loading-opening-to')) {
                _apiSeg.modalFormAdministrar.settings.elementLoadingOpening = $e.data('loading-opening-to');
            }
            if ($e.data('alert-error-to')) {
                _apiSeg.modalFormAdministrar.settings.elementAlertError = $e.data('alert-error-to');
            }
            if ($e.data('alert-sel-row')) {
                _apiSeg.modalFormAdministrar.settings.elementAlertSelRow = $e.data('alert-sel-row');
            }
        }
    },
    //Ver los seguimientos de algun contacto de PA, ACTI, POS, colegio o empresa.
    modalVerSeguimientos : {
        //Funcio que muestra el popup
        show :function(options){
            var url = Routing.generate('cenfo_seguimiento_listar');
            url = $.addToQueryString( url, 'pidEntity', options['idEntity']);
            url = $.addToQueryString( url, 'ptipoContacto', _apiSeg.settings.tipoContacto);

            $.cdAjaxModal({
                url           : url,
                title         : 'Seguimientos del contacto',
                showLoadingTo : '#content-area',
                heigth        : 100
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid);
            if(selRow){
                _apiSeg.settings.idEntity = selRow.id;
                _apiSeg.modalVerSeguimientos.show({
                    idEntity : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Configuracion para el formulario para administrar seguimientos       
    modalFormAdministrar : {
        //Configuracion Global para el api de seguimientos
        settings : {
            elementAlertSelRow    : '#content-area',        //btn-attr: data-alert-sel-row
            elementAlertSuccess   : '#content-area',        //btn-attr: data-alert-success-to
            elementAlertError     : '#modal-seguimiento',   //btn-attr: data-alert-error-to
            elementLoadingSaving  : '#modal-seguimiento',   //btn-attr: data-loading-saving-to
            elementLoadingOpening : '.crm-box-seg-toolbar'  //btn-attr: data-loading-opening-to
        },
        //Function que se ejecuta cuando los datos se registran correctamente en el formulario de registrar.
        success : function(response) {
            var datos;

            if (response.indexOf("ok") >= 0) {
                datos = JSON.parse(response);
                $.showAlert({
                    type    : 'success',
                    showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertSuccess, 
                    content : '<b>' + datos.msg + '</b> '
                });

                //Verificar si existe el Grid de seguimientos para refrescarlo
                if(Grids['GridSeg']){
                    Grids['GridSeg'].Reload();
                }
                
                //Refrescar la cantidad de seguimientos en el Grid. 
                var IDGrid = "Grid" + _apiSeg.settings.tipoContacto;
                if(Grids[IDGrid]){
                    var TGrid = Grids[IDGrid];
                    var TRow  = TGrid.GetRowById(datos.idEntity);
                    TRow.Seg  = datos.countSeg;
                    TGrid.RefreshCell(TRow, 'Seg');
                }
                
                //Ocultar el Loading antes de cerrar el Popup, por que sino queda el loading en pantalla.
                $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
            
                //Cerrar el popup
                $('#btnCancelar').click();
            } else {
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError, 
                    content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                });
            }
            //Ocultar el Loading
            $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
        },
        //Function que se ejecuta antes de guardar la informacion del seguimiento
        beforeSubmit : function(arr, $form, options) {
            $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).showLoading();
        },
        //Function que se ejecuta cuando ocurre un error al tratar de guardar los datos del seguimiento.
        error : function(xhr, textStatus, thrownError) {
            //Ocultar el Loading
            $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
            
            if(xhr['responseText']){
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError,
                    content : '<b>Error.</b> ' + xhr['responseText']
                });
            }else{
                $.showAlert({
                    type    : 'error',
                    showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError,
                    content : '<b>Error.</b> ' + thrownError
                });
            }
        }        
    },
    //Muestra el formulario para registrar un nuevo seguimiento        
    modalFormRegistrar : {
        //Muestra el formulario para registrar un seguimiento
        show : function(options){
            var url = Routing.generate('cenfo_seguimiento_form_registrar');
            url = $.addToQueryString( url, 'pidEntity', options['idEntity']);
            url = $.addToQueryString( url, 'ptipoContacto', _apiSeg.settings.tipoContacto);
            
            $.cdAjaxModal({
                url           : url,
                title         : 'Agregar seguimiento al contacto',
                showLoadingTo : _apiSeg.modalFormAdministrar.settings.elementLoadingOpening,
                heigth        : 100,
                success       : function($modal){
                    //Chosen
                    $("select").chosen({allow_single_deselect: true});
                    //Calcular el nuevo tamaño para el Chosen.
                    resize_chosen();
                    //Timepicker
                    $(".timepick").timepicker({defaultTime: "current", minuteStep: 1, disableFocus: !0, template: "dropdown"});
                    //Datepick
                    $(".datepick").datepicker({language: 'es', format: 'yyyy-mm-dd'});
                }
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiSeg.settings.refreshLoadings(this);
            
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var idEntity;
            var selRow;
            
            //Se utiliza mas que todo en el popup de listar seguimientos donde
            //ya se sabe a que idEntity registrar el nuevo seguimiento
            if($('#idEntity').val()){
                idEntity = $('#idEntity').val();
            }else{
                selRow = getSelectedRow(grid);
                if (selRow) {
                    idEntity = selRow.id;
                }
            }
            
            if (idEntity) {
                _apiSeg.modalFormRegistrar.show({
                    idEntity : idEntity
                });
            }
            e.preventDefault();
        }
    },
    //Muestra el formulario para modificar un seguimiento
    modalFormModificar : {
        //Muestra el formulario
        show : function(options){
            var url = Routing.generate('cenfo_seguimiento_form_modificar');
            url = $.addToQueryString( url, 'pidSeg', options['idSeg']);
            url = $.addToQueryString( url, 'pidEntity', _apiSeg.settings.idEntity);
            url = $.addToQueryString( url, 'ptipoContacto', _apiSeg.settings.tipoContacto);
            
            $.cdAjaxModal({
                url           : url,
                title         : 'Modificar seguimiento',
                showLoadingTo : _apiSeg.modalFormAdministrar.settings.elementLoadingOpening,
                heigth        : 100,
                success       : function($modal){
                    //Chosen
                    $("select").chosen({allow_single_deselect: true});
                    //Calcular el nuevo tamaño para el Chosen.
                    resize_chosen();
                    //Timepicker
                    $(".timepick").timepicker({defaultTime: "current", minuteStep: 1, disableFocus: !0, template: "dropdown"});
                    //Datepick
                    $(".datepick").datepicker({language: 'es', format: 'yyyy-mm-dd'});
                }
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiSeg.settings.refreshLoadings(this);
            
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid, _apiSeg.modalFormAdministrar.settings.elementAlertSelRow);
                
            if (selRow) {
                _apiSeg.modalFormModificar.show({
                    idSeg : selRow.id
                });
            }
            e.preventDefault();
        }
    },
    //Elimina un seguimiento
    eliminar : {
        //Elimina un seguimiento
        fn : function(options){
            var url = Routing.generate('cenfo_seguimiento_eliminar');
            url = $.addToQueryString( url, 'pidSeg', options['idSeg']);
            url = $.addToQueryString( url, 'pidEntity', _apiSeg.settings.idEntity);
            url = $.addToQueryString( url, 'ptipoContacto', _apiSeg.settings.tipoContacto);
                        
            $.ajax({
                url: url,
                success: function(response){
                    var datos;

                    if (response.indexOf("ok") >= 0) {
                        datos = JSON.parse(response);
                        $.showAlert({
                            type    : 'success',
                            showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertSuccess, 
                            content : '<b>' + datos.msg + '</b> '
                        });
                        
                        //Verificar si existe el Grid de seguimientos para refrescarlo
                        if(Grids['GridSeg']){
                            Grids['GridSeg'].Reload();
                        }
                       
                        //Refrescar la cantidad de seguimientos en el Grid. 
                        if(Grids['GridPA']){
                            var TGrid = Grids['GridPA'];
                            var TRow  = TGrid.GetRowById(datos.idEntity);
                            TRow.Seg  = datos.countSeg;
                            TGrid.RefreshCell(TRow, 'Seg');
                        }
                    } else {
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError, 
                            content : '<b>Ocurrió un error, por favor trata de nuevo.</b> ' + response
                        });
                    }
                    //Ocultar el Loading
                    $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();
                },
                beforeSend: function(){
                    $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).showLoading();
                },
                error : function(xhr, textStatus, thrownError) {
                    //Ocultar el Loading
                    $(_apiSeg.modalFormAdministrar.settings.elementLoadingSaving).hideLoading();

                    if(xhr['responseText']){
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError,
                            content : '<b>Error.</b> ' + xhr['responseText']
                        });
                    }else{
                        $.showAlert({
                            type    : 'error',
                            showTo  : _apiSeg.modalFormAdministrar.settings.elementAlertError,
                            content : '<b>Error.</b> ' + thrownError
                        });
                    }
                },
                type: "POST"
            });
        },
        //Evento que se ejecuta al hacer click al boton que se le asigne la funcionalidad
        event : function(e){
            _apiSeg.settings.refreshLoadings(this);
            
            //Obtener las filas que se seleccionaron
            var $e     = $(this);
            var grid   = Grids[$e.data('grid-id')];
            var selRow = getSelectedRow(grid, _apiSeg.modalFormAdministrar.settings.elementAlertSelRow);
                
            if (selRow) {                
                _apiSeg.eliminar.fn({
                    idSeg : selRow.id
                });
            }
            e.preventDefault();
        }
    }
};