<?php

namespace Cenfotec\CRMBundle\TwigExtensions;

/**
 * Description of MyTwigExtension
 *
 * @author Carlos.Dominguez
 */
class MyTwigExtension extends \Twig_Extension {

    public function getFilters()
    {
        return array(
            //Custom Filters (class methods)
            'getArrayFromString'      => new \Twig_Filter_Method($this, 'getArrayFromString'),
        );
    }

    public function getArrayFromString($str) {
        $datos = (!empty($str)) ? explode('|', $str) : array();
        return $datos;
    }

    public function getName()
    {
        return 'my_twig_extension';
    }
}
