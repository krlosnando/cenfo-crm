<?php

namespace Components\EJSTreeGridBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreeFormatter;

class EJSTreeGridController extends Controller
{
    /**
     * @Route("/", name="tg_index")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('ComponentsEJSTreeGridBundle:Default:index.html.twig', array('name' => 'EJSTreeBundle'));
    }
    
     /**
     * @Route("/grid-data", name="cd_data_test_grid", defaults={"_format" = "xml"})
     * @Template()
     */
    public function gridDataAction() {     
    	$request  = $this->getRequest();
    	$pnumData = $request->query->get("pnumData");
    	if($pnumData == 1){
    		return $this->render('ComponentsEJSTreeGridBundle::Default/dataTestGrid01.xml.twig');
    	}
    	if($pnumData == 2){
    		return $this->render('ComponentsEJSTreeGridBundle::Default/dataTestGrid02.xml.twig');
    	}
    	if($pnumData == 3){
    		return $this->render('ComponentsEJSTreeGridBundle::Default/dataTestGrid03.xml.twig');
    	}
        return array();
    }
    
    /**
     * @Route("/Demos", name="tg_demos")
     * @Template("ComponentsEJSTreeGridBundle:Demos:demos.html.twig")
     */
    public function demosAction() {
        $response = $this->render("ComponentsEJSTreeGridBundle:Demos:demos.html.twig");
        $url = $response->getContent();
        return $this->redirect($url);
    }
    
    //EJEMPLO DE TREE GRID CON TABLAS ANIDADAS
    //--------------------------------------------------------------------------------------------------------------------
    /**
     * @Route("/example01", name="tg_example_01")
     * @Template("ComponentsEJSTreeGridBundle:Demos/TreeGridWithTable:index.html.twig")
     */
    public function example01Action() {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerDebtsGrid');
        
        $gridOptionsGenerator
            ->setGridId('Example01Grid')
            ->setOptions(array(
                'Layout_Url' => $router->generate('tg_example_01_xml_master_layout_json'),
                'Data_Url'   => $router->generate('tg_example_01_json_master_data')
            ));
        
        return array('gridOptionsGenerator' => $gridOptionsGenerator);
    }
    
    /**
     * @Route("/example01-xml-master-layout", name="tg_example_01_xml_master_layout", defaults={"_format" = "xml"})
     * @Template()
     */
    public function example01MasterLayoutAction() {     
    	return $this->render('ComponentsEJSTreeGridBundle:Demos/TreeGridWithTable:MasterLayout.xml.twig');
    }
    
    /**
     * @Route("/example01-xml-master-layout-json", name="tg_example_01_xml_master_layout_json", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function example01MasterLayoutJSONAction() {     
    	$router          = $this->get('router');
        $layoutGenerator = new GridLayoutGenerator();
        $layoutGenerator->setConfiguration(array(
            'id'          => "MasterNested",
            'SuppressCfg' => 1,
            'MainCol'     => "NAME",
            'Sync'        => "All",
            'SyncId'      => "DetailNested",
            'MaxVScroll'  => 600
        ));
        
        $layoutGenerator->addVariableColumn(array(
            'Name'       => "NAME",
            'Type'       => "Text",
            'GroupWidth' => 1,
            'Width'      => 150,
        ))->addVariableColumn(array(
            'Name'       => "SURNAME",
            'Type'       => "Text",
            'Width'      => 150,
        ))->addVariableColumn(array(
            'Name'       => "AGE",
            'Type'       => "Int",
        ))->addVariableColumn(array(
            'Name'       => "MARRIED",
            'Type'       => "Bool",
        ));
        
        $layoutGenerator->setPanel(array(
            'Copy' => 1
        ));
        
        // The default row for all master rows
        $layoutGenerator->addDefaultRow(array(
            'Name'         => "MAIN",
            'CDef'         => "DETAIL",
            'Expanded'     => 0
        ));
        
        // The default row for all detail grid parents
        $layoutGenerator->addDefaultRow(array(
            'Name'         => "DETAIL",
            'DetailCol'    => "NAME",
            'DetailLayout' => $router->generate('tg_example_01_json_master_detail'),
            'Spanned'      => 1,
            'NAMESpan'     => 4,
            'Expanded'     => 0,
            'CanExpand'    => 0,
            'CanFilter'    => 0,
            'CDef'         => "DETAILROW",
            'AcceptDef'    => "DETAILROW"
        ));
        
        // The default row for rows in detail grid
        $layoutGenerator->addDefaultRow(array(
            'Name' => "DETAILROW"
        ));
        
        $layoutGenerator->setRootRow(array(
            'CDef'      => "MAIN",
            'AcceptDef' => "MAIN"
        ));
        
        $layoutGenerator->setHeaderRow(array(
            'NAME' => "First name"
        ));
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/example01-xml-master-data", name="tg_example_01_xml_master_data", defaults={"_format" = "xml"})
     * @Template()
     */
    public function example01MasterDataAction() {     
    	return $this->render('ComponentsEJSTreeGridBundle:Demos/TreeGridWithTable:MasterData.xml.twig');
    }
    
    /**
     * @Route("/example01-json-master-data", name="tg_example_01_json_master_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function example01MasterDataJSONAction() {  
        $dataFormatter = new GridDataTreeFormatter();
        
        for ($index = 0; $index < 5; $index++) {
            $dataFormatter->addRow(array(
                'NAME'         => 'James',
                'SURNAME'      => 'Jackson',
                'AGE'          => 24,
                'MARRIED'      => 0,
            ));
            $dataFormatter->addSubRow(array(),1);

            $dataFormatter->addSubRow(array( 'ADDRESS' => '3703 Tenth Ave, New York, NY 10034' ),2);
            $dataFormatter->addSubRow(array( 'ADDRESS' => '201 Varick Street 4th Floor New York, NY 10014' ),2);
            $dataFormatter->addSubRow(array( 'ADDRESS' => '1419 Westwood Blvd, Los Angeles, CA 90024' ),2);
            $dataFormatter->addSubRow(array( 'ADDRESS' => '11525 Del Amo Blvd., Cerritos, CA 90703' ),2);
        }
        
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/example01-xml-master-detail", name="tg_example_01_xml_master_detail", defaults={"_format" = "xml"})
     * @Template()
     */
    public function example01MasterDetailAction() {     
    	return $this->render('ComponentsEJSTreeGridBundle:Demos/TreeGridWithTable:MasterDetail.xml.twig');
    }
    
    /**
     * @Route("/example01-xml-master-detail-json", name="tg_example_01_json_master_detail", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function example01MasterDetailJSONAction() {     
    	$layoutGenerator = new GridLayoutGenerator();
        $layoutGenerator->setConfiguration(array(
            'id'       => 'DetailNested',
            'Dragging' => 0,              
            'Sync'     => 'All',     
            'SyncId'   => 'DetailNested',
            'CfgId'    => 'DetailNested'
        ));
        
        $layoutGenerator->addVariableColumn(array(
            'Name'      => "ADDRESS",
            'Type'      => "Lines",
            'Width'     => 220, 
        ))->addVariableColumn(array(
            'Name'      => "NOTE",
            'Type'      => "Lines",
            'Width'     => 100, 
        ));
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
}
