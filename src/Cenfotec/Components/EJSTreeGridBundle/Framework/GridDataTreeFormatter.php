<?php

namespace Components\EJSTreeGridBundle\Framework;

/**
 * Data Formatter for JSON format
 *
 * @author Luis Diego Flores Villalobos
 */
class GridDataTreeFormatter {
    /**
     * Data formatted
     * @var array Array containing data's formatted representation
     */
    private $data;

    /**
     * Configuration for paging
     * @var array
     */
    private $config;

    function __construct(array $data = array(array("Pos" => 0, "Items" => array()))) {
        $this->data = $data;
    }

    /**
     * Sets data's formatted representation
     * @param array $data
     * @return \Gnd\EJSTreeGridBundle\Framework\GridDataFormatter Self for method chaining
     */
    public function setData(array $data, $page = 0) {
        $this->data[$page] = $data;

        return $this;
    }

    /**
     * Adds one row's information
     * @param array $row
     * @return \Gnd\EJSTreeGridBundle\Framework\GridDataFormatter Self for method chaining
     */
    public function addRow(array $row, $page = 0) {
        $this->data[$page]['Items'][] = $row;

        return $this;
    }
    
    /**
     * Adds one row's information
     * @param array $row
     * @return \Gnd\EJSTreeGridBundle\Framework\GridDataFormatter Self for method chaining
     */
    public function setPosPage($pos, $indexPage = 0) {
        $this->data[$indexPage]['Pos'] = $pos;

        return $this;
    }
    
    /**
     * Adds one Attribute row's information
     * @return \Gnd\EJSTreeGridBundle\Framework\GridDataFormatter 
     */
    public function addAttRow($nameAtt, $value, $indexRow = 0, $indexPage = 0){
        $this->data[$indexPage]['Items'][$indexRow][$nameAtt] = $value;

        return $this;
    }

    /**
     * Adds one subrow's information
     * @param type $subRow The information of the subrow to add
     * @param type $level The level of the [sub]row parent to which add the subrow.
     *      I.e. This parameter is set according to the level of the parent, not the level of the subrow to create.
     *          Parent Row Level 1
     *              Parent Row Level 2
     *                  Parent Row Level 3
     *                      ...
     * @return \Gnd\EJSTreeGridBundle\Framework\GridDataFormatter Self for method chaining
     * @throws \InvalidArgumentException If either the level, i.e. The level is less than 1
     */
    public function addSubRow(array $subRow, $level = 1, $page = 0) {
        if ($level < 1) {
            throw new \InvalidArgumentException("Invalid level ($level). Level must be greater than or equal to 1.");
        }

        $parentRow = &$this->data[$page]['Items'];
        for ($i = 1; $i <= $level; ++$i) {
            $rowCount = isset($parentRow) && is_array($parentRow)
                ? count($parentRow)
                : 0;
            $rowIndex = $rowCount === 0 ? 0 : $rowCount - 1;
            $parentRow = &$parentRow[$rowIndex]['Items'];
        }
        $parentRow[] = $subRow;
        unset($parentRow);

        return $this;
    }

    /**
     * Sets dynamic configuration in data
     * @param array $configuration
     */
    public function setConfiguration(array $configuration) {
        $this->config = $configuration;
    }

    /**
     * Sets dynamic configuration in data
     * @param String $parameter Parameter to set
     * @param String $value Value to set
     */
    public function addConfigurationParameter($parameter, $value) {
        $this->config[$parameter] = $value;
    }

    /**
     * Adds a page to the data collection
     * @param array $data
     */
    public function addPage($data = array()) {
        $this->data[] = $data;
    }

    /**
     * Adds pages to the data collection
     * @param array $data
     */
    public function addPages($pageQuantity) {
        $this->data = array_pad($this->data, count($this->data) + $pageQuantity, array());
    }

    /**
     * Returns the array representation of the formatted data of the table
     * @return type
     */
    public function getFormattedData() {
        $data = array();

        if (!empty($this->config)) {
            $data['Cfg'] = $this->config;
        }

        $data['Body'] = $this->data;

        return $data;
    }
}
