/********************************************************************************************************************************************
 * Autor: Carlos Domínguez Lara
 * Fecha: 02 de febrere del 2012
 * Funcion que se encarga de eliminar el Mensaje de trial version de grid
 * Params (parametros): 
 *      G: Grid en el que se genero el evento.   
 * Return (retorna): --
 ********************************************************************************************************************************************/
function removeTrialMessage(objGrid, idGrid, callback){
    objGrid.OnRenderFinish = function(pG){ 
        var G = document.getElementById(idGrid);

        //Despues de que renderiso toda la tabla agregamos el evento que detectara en que
        //momento se agrega el mensaje a la tabla y lo removera automaticamente.
        if(G && G['parentNode']){
            $(G.parentNode).bind('DOMNodeInserted', function(event) {
                var Grid = document.getElementById(idGrid);

                if(Grid != null){
                    removeElementGrid();

                    //Removemos el evento una vez que elimine el mensajes del trial version, para que
                    //no se siga disparando.
                    $(Grid.parentNode).unbind('DOMNodeInserted');
                    return false; //Exit for loop
                }
            });

            if (typeof callback !== 'undefined') {
                callback();
            }
        }
        removeElementGrid();
    };
}

function removeElementGrid(){
    //Eliminamos los mensajes en el Grid
    $("div[onmousemove='this.parentNode.parentNode.removeChild(this.parentNode);']").remove();

    //Eliminamos los textos del Toolbar
    $("span:contains('Trial unregistered')").remove();
}

function getSelectedRow(G, ElementAlert){
    //Obtener las filas que se seleccionaron
    var rows   = G.GetSelRows();
    var selRow = null;

    //Validar si seleccionaron filas
    if($(rows).size() != 0){
        if($(rows).size() == 1){
            selRow = rows[0];
        }else{
            $.showAlert({
                type    : 'error',
                showTo  : ElementAlert,
                content : 'Solo se puede seleccionar 1 registro.'
            });
        }
    }else{
        $.showAlert({
            type    : 'error',
            showTo  : ElementAlert,
            content : 'Por favor seleccione al menos 1 fila.'
        });
    }
    return selRow;
}