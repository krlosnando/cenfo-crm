<?php

namespace Components\HighchartsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return new \Symfony\Component\HttpFoundation\Response('Highcharts');
    }
}
