<?php

namespace Cenfotec\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\SeguridadBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/permiso")
*/
class PermisoController extends Controller
{
    /**
     * @Route("/listar", name="cenfo_permiso_listar")
     * @Template()
     */
    public function listarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridPermiso');
        
        $gridOptionsGenerator
            ->setGridId('GridPermiso')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_permiso_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_permiso_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_permiso_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutPermisoAction()
    {
        $r                 = $this->getRequest();
        $layoutGenerator   = new GridLayoutGenerator();
        $layoutGenerator->setConfigurationOption('Deleting', 1);
        $layoutGenerator->setConfigurationOption('Adding', 1);
        
        $textColumnDefault = array(
            'CanEdit'   => 1,
            'CanFilter' => 1,
            'CanSort'   => 1,
            'Type'      => "Text",
            'Align'     => 'Left',
        );
        
        $columns = array(
            array(
                'CanEdit'   => 1,
                'Width'     => 0,
                'Name'      => "id",
                'Type'      => "Text",
            ),array(
                'Width'     => 0,
                'Name'      => "Name",
                'Type'      => "Text", 
            ),array_merge(array(
                'Name'     => "Nombre",
                'RelWidth' => 1
            ), $textColumnDefault),array_merge(array(
                'Name'     => "Descripcion",
                'RelWidth' => 3
            ), $textColumnDefault)
        );
        
        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        $layoutGenerator->setVariableColumns($columns);
        
        $layoutGenerator->setPanel(array(
            'Delete' => 1
        ))->setToolbar(array(
            'Cells'                => "Add,Reload,BtnGuardar,Cnt",
            'CanFocus'             => '0',

            'CntRelWidth'          => '1',
            'CntType'              => 'Html',
            'CntFormula'           => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'             => 'Right',
            'CntPrintHPage'        => '2',
            
            'BtnGuardar'           => "1",
            'BtnGuardarButtonText' => "Guardar",
            'BtnGuardarType'       => "Button",
            'BtnGuardarAlign'      => 'Left',
            'BtnGuardarOnClick'    => "guardarGridPermisos(Grid)",
        ));
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_permiso_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataPermisoAction() 
    { 
        $r             = $this->getRequest();
        $dataFormatter = new GridDataTreePagingFormatter();
        $permisos      = $this->getDoctrine()->getRepository('CenfotecSeguridadBundle:Permiso')->findBy(array(), array('nombre'=>'asc'));
        
        foreach ($permisos as $objPermiso) {
            $row = array(
                'id'             => $objPermiso->getId(),
                'Name'           => $objPermiso->getNombre(),
                'Nombre'         => $objPermiso->getNombre(),
                'Descripcion'    => $objPermiso->getDescripcion(),
            );
            $dataFormatter->addRow($row);
        }
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/grid-guardar", name="cenfo_permiso_grid_guardar")
     */
    public function gridGuardarAction()
    {
        $r           = $this->getRequest();
        $em          = $this->getDoctrine()->getEntityManager();
        $repoPermiso = $em->getRepository('CenfotecSeguridadBundle:Permiso');
        $pcambios    = $r->request->get('pcambios');
        $pnuevos     = $r->request->get('pnuevos');
        $pborrados   = $r->request->get('pborrados');
        
        //Verificar si hay cambios
        if(count($pcambios) > 0){
            foreach ($pcambios as $cambio) {
                $objPermiso = $repoPermiso->find($cambio['id']);
                $objPermiso->setNombre($cambio['nombre']);
                $objPermiso->setDescripcion($cambio['descripcion']);
                $em->merge($objPermiso);
            }
        }
        
        //Verificar si hay datos para nuevos registros
        if(count($pnuevos) > 0){
            foreach ($pnuevos as $nuevo) {
                $objNewPermiso = new \Cenfotec\SeguridadBundle\Entity\Permiso();
                $objNewPermiso->setNombre($nuevo['nombre']);
                $objNewPermiso->setDescripcion($nuevo['descripcion']);
                $em->persist($objNewPermiso);
            }
        }
        
        //Verificar si hay que borrar los datos
        if(count($pborrados) > 0){
            foreach ($pborrados as $idPermiso) {
                $objPermiso = $repoPermiso->find($idPermiso);
                
                //Remover el Permiso a los roles que lo tienen
                foreach ($objPermiso->getRoles() as $objRol) {
                    $objRol->removePermiso($objPermiso);
                    $em->merge($objRol);
                }
                
                //Eliminar el Permiso
                $em->remove($objPermiso);
            }
        }
        
        $em->flush();
        return new \Symfony\Component\HttpFoundation\Response('done');
    }    
}
