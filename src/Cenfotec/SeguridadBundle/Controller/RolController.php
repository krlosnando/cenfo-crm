<?php

namespace Cenfotec\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cenfotec\SeguridadBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/rol")
*/
class RolController extends Controller
{
    /**
     * @Route("/listar", name="cenfo_rol_listar")
     * @Template()
     */
    public function listarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridRol');
        
        $gridOptionsGenerator
            ->setGridId('GridRol')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_rol_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_rol_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_rol_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutRolAction()
    {
        $r               = $this->getRequest();
        $layoutGenerator = new GridLayoutGenerator();
        $layoutGenerator->setConfigurationOption('Deleting', 1);
        $layoutGenerator->setConfigurationOption('Adding', 1);
        
        $textColumnDefault = array(
            'CanEdit'   => 1,
            'CanFilter' => 1,
            'CanSort'   => 1,
            'Type'      => "Text",
            'Align'     => 'Left',
        );
        
        $columns = array(
            array(
                'CanEdit'   => 1,
                'Width'     => 0,
                'Name'      => "id",
                'Type'      => "Text",
            ),array(
                'Width'     => 0,
                'Name'      => "Name",
                'Type'      => "Text", 
            ),array_merge(array(
                'Name'     => "Nombre",
                'RelWidth' => 1
            ), $textColumnDefault),
            array(
                'Name'      => "Descripcion",
                'Type'      => "Lines",
                'Height'    => 20,
                'RelWidth'  => 5,
                'CanEdit'   => 1
            ),array(
                'Name'      => "Permiso",
                'RelWidth'  => 3,
                'CanEdit'   => 1,
                'CanFilter' => 1,
                'CanSort'   => 1,
                'Range'     => "1",
                'Type'      => "Enum",
                'Align'     => 'Left',
            )
        );
        
        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        $layoutGenerator->setVariableColumns($columns);
        
        $layoutGenerator->setPanel(array(
            'Delete' => 1
        ))->setToolbar(array(
            'Cells'                => "Add,Reload,BtnGuardar,Cnt",
            'CanFocus'             => '0',
            'CntRelWidth'          => '1',
            'CntType'              => 'Html',
            'CntFormula'           => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'             => 'Right',
            'CntPrintHPage'        => '2',
            'BtnGuardar'           => "1",
            'BtnGuardarButtonText' => "Guardar",
            'BtnGuardarType'       => "Button",
            'BtnGuardarAlign'      => 'Left',
            'BtnGuardarOnClick'    => "guardarGridRoles(Grid)",
        ));
        
        //Obtener todos los permisos disponibles.
        $permisos = $this->getDoctrine()->getRepository('CenfotecSeguridadBundle:Permiso')->findBy(array(), array("nombre"=>"asc"));
        $enumPermisos     = "";
        $enumKeysPermisos = "";
        
        foreach ($permisos as $objPermiso) {
            $enumPermisos     .= "|" . $objPermiso->getNombre();
            $enumKeysPermisos .= "|" . $objPermiso->getId();
        }
        
        $layoutGenerator->addDefaultRow(array(
            'Name'            => 'R',
            'PermisoEnum'     => $enumPermisos,
            'PermisoEnumKeys' => $enumKeysPermisos
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_rol_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataRolAction() 
    { 
        $r             = $this->getRequest();
        $dataFormatter = new GridDataTreePagingFormatter();
        $roles      = $this->getDoctrine()->getRepository('CenfotecSeguridadBundle:Rol')->findBy(array(), array('nombre'=>'asc'));
        
        foreach ($roles as $objRol) {
            //Obtener los permisos del rol.
            $permisos   = $objRol->getPermisos();
            $idPermisos = array();

            foreach ($permisos as $objPermiso) {
                $idPermisos[] = $objPermiso->getId();
            }

            $row = array(
                'id'          => $objRol->getId(),
                'Name'        => $objRol->getNombre(),
                'Nombre'      => $objRol->getNombre(),
                'Descripcion' => $objRol->getDescripcion(),
                'Permiso'     => implode(';', $idPermisos)
            );
            $dataFormatter->addRow($row);
        }
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/grid-guardar", name="cenfo_rol_grid_guardar")
     */
    public function gridGuardarAction()
    {
        $r           = $this->getRequest();
        $em          = $this->getDoctrine()->getEntityManager();
        $repoRol     = $em->getRepository('CenfotecSeguridadBundle:Rol');
        $repoPermiso = $em->getRepository('CenfotecSeguridadBundle:Permiso');
        $pcambios    = $r->request->get('pcambios');
        $pnuevos     = $r->request->get('pnuevos');
        $pborrados   = $r->request->get('pborrados');
        
        //Verificar si hay cambios
        if(count($pcambios) > 0){
            foreach ($pcambios as $cambio) {
                $objRol = $repoRol->find($cambio['id']);
                $objRol->setNombre($cambio['nombre']);
                $objRol->setDescripcion($cambio['descripcion']);
                
                //Quitar los permisos, para agregar los que el usuario ingreso.
                $objRol->setPermisos(new \Doctrine\Common\Collections\ArrayCollection());
                
                //Validar si le asignaron permisos al rol
                if(count($cambio['permisos'])>0){
                    //Agregar los permisos
                    foreach ($cambio['permisos'] as $idPermiso) {
                        if (!empty($idPermiso)) {
                            $objPermiso = $repoPermiso->find($idPermiso);
                            $objRol->addPermiso($objPermiso);
                        }
                    }
                }
                $em->merge($objRol);
            }
        }
        
        //Verificar si hay datos para nuevos registros
        if(count($pnuevos) > 0){
            foreach ($pnuevos as $nuevo) {
                $objNewRol = new \Cenfotec\SeguridadBundle\Entity\Rol();
                $objNewRol->setNombre($nuevo['nombre']);
                $objNewRol->setDescripcion($nuevo['descripcion']);
                
                //Validar si le asignaron permisos al rol
                if(count($nuevo['permisos'])>0){
                    //Agregar los permisos
                    foreach ($nuevo['permisos'] as $idPermiso) {
                        if (!empty($idPermiso)) {
                            $objPermiso = $repoPermiso->find($idPermiso);
                            $objNewRol->addPermiso($objPermiso);
                        }
                    }
                }
                $em->persist($objNewRol);
            }
        }
        
        //Verificar si hay que borrar los datos
        if(count($pborrados) > 0){
            foreach ($pborrados as $idRol) {
                $objRol = $repoRol->find($idRol);
                $em->remove($objRol);
            }
        }
        
        $em->flush();
        return new \Symfony\Component\HttpFoundation\Response('done');
    }    
}
