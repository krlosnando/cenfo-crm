<?php

namespace Cenfotec\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Templating\Asset\PathPackage;
use Components\EJSTreeGridBundle\Framework\GridOptionsGenerator,
    Components\EJSTreeGridBundle\Framework\GridLayoutGenerator,
    Components\EJSTreeGridBundle\Framework\GridDataTreePagingFormatter;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
* @Route("/usuario")
*/
class UsuarioController extends Controller
{
    /**
     * @Route("/", name="cenfo_usuario_administrar")
     * @Template()
     */
    public function administrarAction()
    {
        $router               = $this->get('router');
        $gridOptionsGenerator = new GridOptionsGenerator('ContainerGridUsuario');
        
        $gridOptionsGenerator
            ->setGridId('GridUsuario')
            ->setOptions(array(
                'Layout_Url' => $router->generate('cenfo_usuario_grid_layout'),
                'Data_Url'   => $router->generate('cenfo_usuario_grid_data'),
                'Upload_Url' => $router->generate('cd_ejstreegrid_upload_default'),
                'Export_Url' => $router->generate('cd_ejstreegrid_export'),
            ));
        
        return array( 
            'gridOptionsGenerator' => $gridOptionsGenerator
        );
    }
    
    /**
     * @Route("/grid-layout", name="cenfo_usuario_grid_layout", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridLayout.json.twig")
     */
    public function gridLayoutUsuarioAction()
    {
        $r                 = $this->getRequest();
        $layoutGenerator   = new GridLayoutGenerator();
        
        $textColumnDefault = array(
            'CanEdit'   => 0,
            'RelWidth'  => 1,
            'CanFilter' => 1,
            'CanSort'   => 1,
            'Type'      => "Text",
            'Align'     => 'Left',
        );
        
        $columns = array(
            array(
                'CanEdit'   => 1,
                'Width'     => 0,
                'Name'      => "id",
                'Type'      => "Text",
            ),array(
                'Width'     => 0,
                'Name'      => "Name",
                'Type'      => "Text", 
            ),array_merge(array(
                'Name' => "NombreCompleto"
            ), $textColumnDefault),array_merge(array(
                'Name' => "Usuario"
            ), $textColumnDefault),array_merge(array(
                'Name' => "Rol"
            ), $textColumnDefault)
        );
        
        $layoutGenerator->addTopRowFilter(array(
            'id' => "Filter"
        ));
        $layoutGenerator->setVariableColumns($columns);
        
        $layoutGenerator->setPanel(array(
            'Delete' => 0
        ))->setToolbar(array(
            'Cells'         => "Reload,Export,Cnt,Sel",
            'CanFocus'      => '0',

            'CntRelWidth'   => '1',
            'CntType'       => 'Html',
            'CntFormula'    => '"Filas:<b>"+count(7)+"</b> Mostrando:<b>"+count(6)+"</b>"',
            'CntAlign'      => 'Right',
            'CntPrintHPage' => '2',

            'SelType'       => 'Html',
            'SelFormula'    => 'var cnt=count(15);return cnt?"Selección:<b>"+cnt+"</b>":""',
            'SelWidth'      => '-1',
            'SelWrap'       => '0',
            'SelPrintHPage' => '2',
        ));
        
        return array('gridLayoutGenerator' => $layoutGenerator);
    }
    
    /**
     * @Route("/grid-data", name="cenfo_usuario_grid_data", defaults={"_format" = "json"})
     * @Template("ComponentsEJSTreeGridBundle::gridData.json.twig")
     */
    public function gridDataUsuarioAction() 
    { 
        $r             = $this->getRequest();
        $dataFormatter = new GridDataTreePagingFormatter();
        $usuarios      = $this->getDoctrine()->getRepository('CenfotecSeguridadBundle:Usuario')->findBy(array(), array('nombre'=>'asc'));
        
        foreach ($usuarios as $objUsuario) {
            $roles = array();
            
            foreach ($objUsuario->getRealRoles() as $objRol) {
                $roles[] = $objRol->getNombre();
            }
            
            $row = array(
                'id'             => $objUsuario->getId(),
                'Name'           => $objUsuario->getNombreCompleto(),
                'NombreCompleto' => $objUsuario->getNombreCompleto(),
                'Usuario'        => $objUsuario->getNombreUsuario(),
                'Rol'            => implode(', ', $roles)
            );
            $dataFormatter->addRow($row);
        }
        return array('gridDataFormatter' => $dataFormatter);
    }
    
    /**
     * @Route("/form-registrar", name="cenfo_usuario_form_registrar")
     * @Template("CenfotecSeguridadBundle:Usuario:formRegistrar.html.twig")
     */
    public function formRegistrarAction()
    {
        $roles = $this->getDoctrine()->getRepository('CenfotecSeguridadBundle:Rol')->findBy(array(), array('nombre'=>'asc'));
        return array(
            'ptwRoles' => $roles
        );
    }
    
    /**
     * @Route("/form-modificar", name="cenfo_usuario_form_modificar")
     * @Template("CenfotecSeguridadBundle:Usuario:formModificar.html.twig")
     */
    public function formModificarAction()
    {
        $doctrine   = $this->getDoctrine();
        $pidUsuario = $this->getRequest()->query->get('pidUsuario');
        $objUsuario = $doctrine->getRepository('CenfotecSeguridadBundle:Usuario')->find($pidUsuario);
        $roles      = $doctrine->getRepository('CenfotecSeguridadBundle:Rol')->findBy(array(), array('nombre'=>'asc'));
        
        return array(
            'ptwObjUsuario' => $objUsuario,
            'ptwRoles'      => $roles
        );
    }
    
    /**
     * @Route("/registrar", name="cenfo_usuario_registrar")
     */
    public function registrarAction()
    {
        $r               = $this->getRequest();
        $em              = $this->getDoctrine()->getEntityManager();
        $repoRol         = $em->getRepository('CenfotecSeguridadBundle:Rol');
        $uiDataUsuario   = $r->request->get('uiDataNuevoUsuario');
        $factory         = $this->get('security.encoder_factory');
        
        //Crear el nuevo usuario.
        $objNuevoUsuario = new \Cenfotec\SeguridadBundle\Entity\Usuario();
        $encoder         = $factory->getEncoder($objNuevoUsuario);
        $objNuevoUsuario->setSalt(md5(time()));
        $password       = $encoder->encodePassword($uiDataUsuario['Password'], $objNuevoUsuario->getSalt());
        $objNuevoUsuario->setCorreo($uiDataUsuario['Correo']);
        $objNuevoUsuario->setPassword($password);
        $objNuevoUsuario->setNombre($uiDataUsuario['Nombre']);
        $objNuevoUsuario->setApellido1($uiDataUsuario['Apellido1']);
        $objNuevoUsuario->setApellido2($uiDataUsuario['Apellido2']);
        $objNuevoUsuario->setNombreUsuario($uiDataUsuario['NombreUsuario']);
        
        //Agregar los roles que se le asignaron al usuario
        foreach ($uiDataUsuario['Roles'] as $idRol) {
            $objRol = $repoRol->find($idRol);
            $objNuevoUsuario->addRole($objRol);
        }
        
        //Registrarlo a base de datos.
        $em->persist($objNuevoUsuario);
        $em->flush();
        
        //mostrar el mensaje de que se registro correctamente el usuario
        $this->get('session')->setFlash(
            'notice',
            'El usuario ' . $objNuevoUsuario->getNombreUsuario() . ' se ha creado correctamente.'
        );
        
        return $this->redirect($this->generateUrl('cenfo_usuario_administrar'));
    }

    /**
     * @Route("/modificar", name="cenfo_usuario_modificar")
     */
    public function modificarAction()
    {
        $r             = $this->getRequest();
        $em            = $this->getDoctrine()->getEntityManager();
        $repoRol       = $em->getRepository('CenfotecSeguridadBundle:Rol');
        $pidUsuario    = $r->query->get('pidUsuario');
        $uiDataUsuario = $r->request->get('uiDataUsuario');
        $factory       = $this->get('security.encoder_factory');
        
        //Crear el nuevo usuario.
        $objUsuario = $em->getRepository('CenfotecSeguridadBundle:Usuario')->find($pidUsuario);
        $encoder    = $factory->getEncoder($objUsuario);
        
        $objUsuario->setCorreo($uiDataUsuario['Correo']);
        $objUsuario->setNombre($uiDataUsuario['Nombre']);
        $objUsuario->setApellido1($uiDataUsuario['Apellido1']);
        $objUsuario->setApellido2($uiDataUsuario['Apellido2']);
        $objUsuario->setNombreUsuario($uiDataUsuario['NombreUsuario']);
        
        if($uiDataUsuario['Password'] != $objUsuario->getPassword()){
            $objUsuario->setSalt(md5(time()));
            $password = $encoder->encodePassword($uiDataUsuario['Password'], $objUsuario->getSalt());
            $objUsuario->setPassword($password);
        }
        
        //Quitar los roles, para agregar los que el usuario ingreso.
        $objUsuario->setRoles(new \Doctrine\Common\Collections\ArrayCollection());
                
        //Agregar los roles que se le asignaron al usuario
        foreach ($uiDataUsuario['Roles'] as $idRol) {
            $objRol = $repoRol->find($idRol);
            $objUsuario->addRole($objRol);
        }
        
        //Registrarlo a base de datos.
        $em->persist($objUsuario);
        $em->flush();
       
        return new \Symfony\Component\HttpFoundation\Response('done');
    }

    /**
     * @Route("/borrar", name="cenfo_usuario_borrar")
     * @Template("CenfotecSeguridadBundle:Usuario:borrar.html.twig")
     */
    public function borrarAction()
    {
        return array();
    }
}
