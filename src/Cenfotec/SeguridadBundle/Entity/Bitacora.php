<?php

namespace Cenfotec\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\SeguridadBundle\Entity\Bitacora
 *
 * @ORM\Table(name="t_bitacora")
 * @ORM\Entity(repositoryClass="\Cenfotec\SeguridadBundle\Repository\BitacoraRepository")
 */
class Bitacora
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="fecha_evento", type="datetime")
     */
    private $fechaEvento;
    
    /**
     * @ORM\Column(name="descripcion", type="string", length=5000)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="TipoEvento")
     * @ORM\JoinColumn(name="tipo_evento_id", referencedColumnName="id")
     */
    private $tipoEvento;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fechaEvento = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaEvento
     *
     * @param \DateTime $fechaEvento
     * @return Bitacora
     */
    public function setFechaEvento($fechaEvento)
    {
        $this->fechaEvento = $fechaEvento;
    
        return $this;
    }

    /**
     * Get fechaEvento
     *
     * @return \DateTime 
     */
    public function getFechaEvento()
    {
        return $this->fechaEvento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Bitacora
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipoEvento
     *
     * @param \Cenfotec\SeguridadBundle\Entity\TipoEvento $tipoEvento
     * @return Bitacora
     */
    public function setTipoEvento(\Cenfotec\SeguridadBundle\Entity\TipoEvento $tipoEvento = null)
    {
        $this->tipoEvento = $tipoEvento;
    
        return $this;
    }

    /**
     * Get tipoEvento
     *
     * @return \Cenfotec\SeguridadBundle\Entity\TipoEvento 
     */
    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    /**
     * Set usuario
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuario
     * @return Bitacora
     */
    public function setUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Cenfotec\SeguridadBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}