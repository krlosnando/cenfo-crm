<?php

namespace Cenfotec\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\SeguridadBundle\Entity\Permiso
 *
 * @ORM\Table(name="t_permiso")
 * @ORM\Entity(repositoryClass="\Cenfotec\SeguridadBundle\Repository\PermisoRepository")
 */
class Permiso
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;
    
    /**
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="Rol", mappedBy="permisos")
     */
    private $roles;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Permiso
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Permiso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Add usuarios
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarios
     * @return Permiso
     */
    public function addUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios[] = $usuarios;
    
        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Add roles
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Rol $roles
     * @return Permiso
     */
    public function addRole(\Cenfotec\SeguridadBundle\Entity\Rol $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Rol $roles
     */
    public function removeRole(\Cenfotec\SeguridadBundle\Entity\Rol $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }
}