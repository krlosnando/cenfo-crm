<?php

namespace Cenfotec\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\SeguridadBundle\Entity\Rol
 *
 * @ORM\Table(name="t_rol")
 * @ORM\Entity(repositoryClass="\Cenfotec\SeguridadBundle\Repository\RolRepository")
 */
class Rol
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;
    
    /**
     * @ORM\Column(name="descripcion", type="string", length=255, unique=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="Usuario", mappedBy="roles")
     */
    private $usuarios;
    
    /**
     * @ORM\ManyToMany(targetEntity="Permiso", inversedBy="roles", cascade={"remove"})
     * @ORM\JoinTable(name="t_rol_permiso",
     *      joinColumns={@ORM\JoinColumn(name="rol_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="permiso_id", referencedColumnName="id")}
     *      )
     */
    private $permisos;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->permisos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Rol
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Rol
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Add usuarios
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarios
     * @return Rol
     */
    public function addUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios[] = $usuarios;
    
        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\Cenfotec\SeguridadBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Add permisos
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Permiso $permisos
     * @return Rol
     */
    public function addPermiso(\Cenfotec\SeguridadBundle\Entity\Permiso $permisos)
    {
        $this->permisos[] = $permisos;
    
        return $this;
    }

    /**
     * Remove permisos
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Permiso $permisos
     */
    public function removePermiso(\Cenfotec\SeguridadBundle\Entity\Permiso $permisos)
    {
        $this->permisos->removeElement($permisos);
    }

    /**
     * Get permisos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPermisos()
    {
        return $this->permisos;
    }
    
    /**
     * Set Permisos
     *
     * @param \Doctrine\Common\Collections\Collection $permisos
     */
    public function setPermisos(\Doctrine\Common\Collections\ArrayCollection $permisos)
    {
        $this->permisos = $permisos;
        return $this;
    }
}