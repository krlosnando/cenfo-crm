<?php

namespace Cenfotec\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cenfotec\SeguridadBundle\Entity\TipoEvento
 *
 * @ORM\Table(name="t_tipo_evento")
 * @ORM\Entity(repositoryClass="\Cenfotec\SeguridadBundle\Repository\TipoEventoRepository")
 */
class TipoEvento
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoEvento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}