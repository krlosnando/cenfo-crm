<?php

namespace Cenfotec\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Cenfotec\SeguridadBundle\Entity\Usuario
 *
 * @ORM\Table(name="t_usuario")
 * @ORM\Entity(repositoryClass="\Cenfotec\SeguridadBundle\Repository\UsuarioRepository")
 */
class Usuario implements UserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    
    /**
     * @ORM\Column(name="apellido1", type="string", length=50)
     */
    private $apellido1;
    
    /**
     * @ORM\Column(name="apellido2", type="string", length=50)
     */
    private $apellido2;
    
    /**
     * @ORM\Column(name="nombre_usuario", type="string", length=25, unique=true)
     */
    private $nombreUsuario;
    
     /**
     * @ORM\Column(name="salt", type="string", length=32)
     */
    private $salt;
    
    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;
    
    /**
     * @ORM\Column(name="correo", type="string", length=60, unique=true)
     */
    private $correo;
    
    /**
     * @ORM\ManyToMany(targetEntity="Rol", inversedBy="usuarios")
     * @ORM\JoinTable(name="t_usuario_rol",
     *      joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="rol_id", referencedColumnName="id")}
     *      )
     */
    private $roles;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombre . ' ' . $this->apellido1 . ' ' . $this->apellido2;
    }
    
    /**
     * Set nombreUsuario
     *
     * @param string $nombreUsuario
     * @return Usuario
     */
    public function setNombreUsuario($nombreUsuario)
    {
        $this->nombreUsuario = $nombreUsuario;
    
        return $this;
    }

    /**
     * Get nombreUsuario
     *
     * @return string 
     */
    public function getNombreUsuario()
    {
        return $this->nombreUsuario;
    }

    /**
     * Get Username
     *
     * @return string 
     */
    public function getUsername() 
    {
        return $this->nombreUsuario;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Usuario
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get Username
     *
     * @return string 
     */
    public function getSalt() 
    {
        return $this->salt;
    }
    
    /**
     * Set password
     *
     * @param string $password
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Usuario
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    
        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }
    
    /**
     * Array con los permisos del usuario
     *
     * @return array 
     */
    public function getPermisos() 
    {
        $roles           = $this->roles;
        $permisosUsuario = array();
        
        //Recorrer los roles que tiene asignado el usuario.
        foreach ($roles as $objRol) {
            $permisosRol  = $objRol->getPermisos();
            
            //Recorrer los permisos que tiene asignado cada rol
            foreach ($permisosRol as $objPermiso) {
                $permisosUsuario[] = $objPermiso->getNombre();
            }
        }
        
        //Varios Roles pueden tener un mismo permiso asi que hay que remover los duplicados.
        return array_unique($permisosUsuario);
    }
    
    /**
     * Get Roles (Es como obtener los permisos del usuario) 
     * NOTA: Si se cambia a getPermisos pasan muchos problemas
     *
     * @return array 
     */
    public function getRoles() 
    {
        $roles        = $this->roles;
        $rolesUsuario = array();
        
        //Obtener los nombres de los roles del usuario
        foreach ($roles as $objRol) {
            $rolesUsuario[] = $objRol->getNombre();
        }
        
        //Varios Roles pueden tener un mismo permiso asi que hay que remover los duplicados.
        return array_unique($rolesUsuario);
    }
    
    /**
     * Obtiene los roles del usuario
     *
     * @return array 
     */
    public function getRealRoles() 
    {
        return $this->roles;
    }
    
    /**
     * Set Roles
     *
     * @param \Doctrine\Common\Collections\Collection $roles
     */
    public function setRoles(\Doctrine\Common\Collections\ArrayCollection $roles)
    {
        $this->roles = $roles;
        return $this;
    }
    
    /**
     * Add roles
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Rol $roles
     * @return Usuario
     */
    public function addRole(\Cenfotec\SeguridadBundle\Entity\Rol $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Cenfotec\SeguridadBundle\Entity\Rol $roles
     */
    public function removeRole(\Cenfotec\SeguridadBundle\Entity\Rol $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     * @return Usuario
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;
    
        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string 
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     * @return Usuario
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;
    
        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string 
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }
    
    public function eraseCredentials() 
    {
        
    }
}