function guardarGridPermisos(G){
    //XMLString almacena el xml de cambios que retorna el Grid en el siguiente formato:
    //<Grid>
    //      <IO/>
    //      <Changes>
    //          <I id="ExpenseDetail_594" Changed="1" Cantidad="700"/>
    //          <I id="Detalle_177" Moved="2" Parent="Category_7" Next="" Prev="Detalle_80"/>
    //          <I id="Category_12" Changed="1" NodeCol="Compras Nuevas"/>
    //          <I id="ExpenseDetail_712" Deleted="1"/>
    //      </Changes>
    //</Grid>
    var rowXML;
    var objRow;
    var XMLString   = G.GetChanges();
    
    //Si hay cambios actualizamos.
    if(XMLString != "<Grid><IO/><Changes></Changes></Grid>"){
        var xmlDoc      = $.parseXML( XMLString );
        var $XMLChanges = $( xmlDoc );
        var results     = $XMLChanges.find( "I" );
        var rowXMLID    = "";
        var cambios     = [];
        var nuevos      = [];
        var borrados    = [];

        $.each(results, function() {
            rowXML     = this;
            //rowXML.attributes =
            //    [0] name: id,     value: id de la fila que se modifico
            //    [1] name: Changed value: boolean que indica si cambio.
            rowXMLID   = $(rowXML).attr('id');
            objRow     = G.GetRowById(rowXMLID);

            //Validar si es un Cambio, nuevo registro o si se borro.
            if($(rowXML).attr('Changed')){
                cambios.push({
                    id          : objRow.id,
                    nombre      : objRow.Nombre,
                    descripcion : objRow.Descripcion
                });
            }
            if($(rowXML).attr('Added')){
                if(objRow.Nombre || objRow.Descripcion){
                    nuevos.push({
                        nombre      : objRow.Nombre,
                        descripcion : objRow.Descripcion
                    });
                }
            }
            if($(rowXML).attr('Deleted')){
                if($.isNumeric(objRow.id)){
                    borrados.push(objRow.id);
                }
            }
        });

        //Guardamos o actualizamos los datos por ajax, por medio de un Action.
        $.ajax({
            url: _global.urls.guardarGridPermisos,
            data: {
                pcambios  : cambios,
                pnuevos   : nuevos,
                pborrados : borrados
            },
            success: function(result){
                $('#' + _global.gridContainerId).unblock();
                if(result == 'done'){
                    $.showAlert({
                        type    : 'success',
                        content : 'Los datos se guardaron correctamente.',
                        showTo  : '#content-area'
                    });
                    G.Reload();
                }else{
                    $.showAlert({
                        type    : 'error',
                        content : 'Un error ocurrio, por favor intentar de nuevo. ' + result,
                        showTo  : '#content-area'
                    });
                }
            },
            beforeSend: function(){
                $('#' + _global.gridContainerId).block({
                    message: 'Guardando los cambios...',
                    css: {
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .8,
                        color: '#fff',
                        border: '2px solid #81BEF7'
                    }
                });
            },
            error:function (xhr, ajaxOptions, throwXMLnError){
                $('#' + _global.gridContainerId).unblock();
                var msgError = "";
                if(xhr['responseText']){
                    msgError = xhr['responseText'];
                }else{
                    msgError = throwXMLnError;
                }
                $.showAlert({
                    type    : 'error',
                    content : 'Un error ocurrio, por favor intentar de nuevo. ' + msgError,
                    showTo  : '#content-area'
                });
            },
            type: "POST"
        });
    }else{
        $.showAlert({
            type    : 'info',
            content : 'No hay cambios que guardar.',
            showTo  : '#content-area'
        });
    }
}