$(document).ready(function() {
    $('#uiBtnModificar').click(function(e){
        var G  = Grids[0],
            $e = $(this);

        if(G.GetSelRows().length > 0){
            var row = G.GetSelRows()[0];
            $e.attr('href', $e.attr('href') + '?pidUsuario=' + row.id);
        }else{
            $.showAlert({
                type    : 'error',
                content : 'Seleccione el usuario que desea modificar.',
                showTo  : '#content-area'
            });
            e.preventDefault();
        }
    });
    
    $('#frmModificarUsuario').ajaxForm({
        success : function(dataResponse) {
            $('#frmModificarUsuario').unblock();
            if(dataResponse == 'done'){
                $.showAlert({
                    type    : 'success',
                    content : 'Los cambios se han guardado correctamente.',
                    showTo  : '#content-area'
                });
            }else{
                $.showAlert({
                    type    : 'error',
                    content : 'Ha ocurrido un error. ' + dataResponse,
                    showTo  : '#content-area'
                });
            }
        },
        beforeSubmit: function(arr, $form, options) {
            $('#frmModificarUsuario').block({
                message: 'Guardando los cambios...',
                css: {
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .8,
                    color: '#fff',
                    border: '2px solid #81BEF7'
                }
            });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('#frmModificarUsuario').unblock();
            if(XMLHttpRequest['responseText']){
                $.showAlert({
                    type    : 'error',
                    content : 'Ha ocurrido un error. ' + XMLHttpRequest['responseText'],
                    showTo  : '#content-area'
                });
            }else{
                $.showAlert({
                    type    : 'error',
                    content : 'Ha ocurrido un error. ' + errorThrown,
                    showTo  : '#content-area'
                });
            }
        }
    });
});