$(document).ready(function() {
    /***************************************************************************************************
    * Funcion que se encarga de mostrar los datos .
    * Params (parametros):
    *      G: Grind en el que se ejecuto el evento.
    *      row: Fila en el grid.
    *      autoupdate: Indica si los cambios se guardan automaticamente.
    * Return (retorna): --
    ***************************************************************************************************/
    $('.btn-toolbar').find('.button').click(function(e){
        var $this        = $(this);
        var tipoContacto = $this.attr('tipo-contacto');
        var dataUrl      = "";
        var layoutUrl    = "";
        var G            = Grids[0];

        switch(tipoContacto){
            case 'ProgAcademico' :
                dataUrl   = _global.urls.dataUrlProgAcademico;
                layoutUrl = _global.urls.layoutUrlProgAcademico;
                break;

            case 'ACTIPosgrado' :
                dataUrl   = _global.urls.dataUrlACTIPosgrado;
                layoutUrl = _global.urls.layoutUrlACTIPosgrado;
                break;

            case 'Empresa' :
                dataUrl   = _global.urls.dataUrlEmpresa;
                layoutUrl = _global.urls.layoutUrlEmpresa;
                break;

            case 'Colegio' :
                dataUrl   = _global.urls.dataUrlColegio;
                layoutUrl = _global.urls.layoutUrlColegio;
                break;
        }

        //Actulizamos las URL para los datos.
        G.Source.Data.Url   = dataUrl;
        G.Source.Layout.Url = layoutUrl;
        G.Reload();
        
        //Cerrrar el mensaje si existe.
        $('.close').click();
        
        //Remover la clase disabled del boton que se habia presionado anteriormente
        $('.btn-toolbar').find('.button').removeClass('disabled');
        
        //Agregar la clase disabled al boton que se dio click
        $this.addClass('disabled');
        
        //Poner el texto al titulo de la pagina
        $('#page-header-title').text('Telemercadeo a ' + $this.text());
        
        //Prevenir que se refresque la pagina al dar click en <a></a>
        e.preventDefault();
    });
});