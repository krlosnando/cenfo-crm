/********************************************************************************************************************************************
 * Autor: Carlos Domínguez Lara
 * Fecha: 02 de febrere del 2012
 * Funcion que se encarga de eliminar el Mensaje de trial version de grid
 * Params (parametros): 
 *      G: Grid en el que se genero el evento.   
 * Return (retorna): --
 ********************************************************************************************************************************************/
function removeTrialMessage(objGrid, idGrid, callback){
    objGrid.OnRenderFinish = function(pG){ 
        var G = document.getElementById(idGrid);

        //Despues de que renderiso toda la tabla agregamos el evento que detectara en que
        //momento se agrega el mensaje a la tabla y lo removera automaticamente.
        $(G.parentNode).bind('DOMNodeInserted', function(event) {
            var Grid = document.getElementById(idGrid);

            if(Grid != null){
                removeElementGrid();
                
                //Removemos el evento una vez que elimine el mensajes del trial version, para que
                //no se siga disparando.
                $(Grid.parentNode).unbind('DOMNodeInserted');
                return false; //Exit for loop
            }
        });
        
        if (typeof callback !== 'undefined') {
            callback();
        }
        removeElementGrid();
    }
}

function removeElementGrid(){
    //Eliminamos los mensajes en el Grid
    $("div[onmousemove='this.parentNode.parentNode.removeChild(this.parentNode);']").remove();

    //Eliminamos los textos del Toolbar
    $("span:contains('Trial unregistered')").remove();
}