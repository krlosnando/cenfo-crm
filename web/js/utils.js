//Funcion que validad si el elmento ya tiene algun evento.
$.fn.isBound = function(type, fn) {
    var data = this.data('events')[type];

    if (data === undefined || data.length === 0) {
        return false;
    }

    return (-1 !== $.inArray(fn, data));
};

//Funcion para debug en Javascript.
$.dump = function(obj) {
    console.log('Type: ', typeof obj);
    if (typeof obj == 'object') {
        var keys = Object.keys(obj);
        console.log('Keys: ', keys);
        for (var i in keys) {
            console.log(keys[i], '=', $(obj).attr(keys[i]));
        }
    } else {
        console.log(obj);
    }
};

//Funcion para agregar parametros a una URL
$.addToQueryString = function(url, key, value) {
    var query = url.indexOf('?');
    var anchor = url.indexOf('#');
    if (query == url.length - 1) {
        // Strip any ? on the end of the URL
        url = url.substring(0, query);
        query = -1;
    }
    return (anchor > 0 ? url.substring(0, anchor) : url)
            + (query > 0 ? "&" + key + "=" + value : "?" + key + "=" + value)
            + (anchor > 0 ? url.substring(anchor) : "");
};

//Funcion que descarga un archivo de excel
$.downloadExcel = function(purl, fnGetParams){
    var downloadUrl = purl;
    var params      = null;
    
    if(fnGetParams){
        params = fnGetParams();
    }
    
    $.fileDownload(downloadUrl, {
        successCallback: function (url) {
            $.showAlert({
                type    : 'success',
                showTo  : '#content-area',
                content : 'El archivo se generó correctamente, por favor esperar un momento para que termine la descarga. (La velocidad depende de tu conexión a internet).'
            });
        },
        failCallback: function (html, url) {
            $.showAlert({
                type    : 'error',
                showTo  : '#content-area',
                content : 'Hubo un error al generar el archivo. ' + html
            });
        },
        data : params,
        dialogOptions: {
            zIndex : 9999,
            modal  : true
        }
    });
};

//Hacer que todos los elementos con href="#" no refresquen la pagina.
$(document).ready(function() {
    //Todos los elementos <a> que no tienen href prevenir que refresque la pagina
    $('[href="#"]').click(function(e) {
        e.preventDefault();
    });

    //Mostrar el loading de la pagina.
    $("#overlay").delay(950).fadeOut(300);
    
    //Hacer que todos los select tengan el plugin Chosen
    $("select").chosen({allow_single_deselect: true});

    //Calcular el nuevo tamaño para el Chosen segun el tamaño de la interfaz.
    resize_chosen();

    //Para JQuery Validate.
    $("select").change(function() {
        $(this).trigger('keyup');
        $(this).trigger('focusin');
    });
});

