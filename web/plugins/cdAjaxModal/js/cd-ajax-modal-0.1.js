/**********************************************************************************
* Funcion que se encargara de cargar un Modal con la respuesta de algun Ajax.
* Version          : 1.0
* Author           : Carlos Domínguez Lara
* Fecha creado     : Martes 14 de Marzo del 2013 03:30 p.m
* Fecha Modificado : Domingo 27 de Octubre del 2013 06:17 p.m
* Parametros       :
*      Options => Objetos con la configuracion.
*         - url      (String)      : Url de la pagina web que se va a llamar
*         - params   (Object)      : Parametros para la Url
*         - title    (String)      : Titulo del Modal
*         - showLoadingTo (String) : Id del elemento donde se desea mostrar un Loading ej '#myelemente'
*         - sucess   (funcion) : Funcion que se ejecutara cuando termine de cargar los datos.
*         - remove   (funcion) : Funcion que se ejecutara cuando se cierra o remueve el Modal.
* Retorna          : --
**********************************************************************************/
$.cdAjaxModal = function (options){
    var strModal   = "",
        idModal    = 'currentModal';

    //Default Options.
    var defaultOptions = { };
    
    //Hacer un merge con las opciones que nos enviaron y las default.
    var defaultKeys = Object.keys(defaultOptions);
    $(defaultKeys).each(function(index, value){
        if(typeof defaultOptions[value] == 'object'){
            var dafaultKeysValue = Object.keys(defaultOptions[value]);

            $(dafaultKeysValue).each(function(iV, propertyValue){
                if(options[value]){
                    if(!options[value][propertyValue]){
                        //Si no tiene algun valor lo asignamos y salimos.
                        options[value][propertyValue] = defaultOptions[value][propertyValue];
                    }
                }else{
                    //Si no tiene algun valor lo asignamos y salimos.
                    options[value] = defaultOptions[value];

                    return false;
                }
            });
        }else{
            if(!options[value]){
                //Si no tiene algun valor lo asignamos y salimos.
                options[value] = defaultOptions[value];
            }
        }
    });
    
    //Mostrar el Loading
    if(options['showLoadingTo']){
        $(options['showLoadingTo']).showLoading();
    }
    
    //Remover modal viejo
    if($('.modal').length > 0){
        idModal += $('.modal').length;
    }
    
    //HTML params
    if (options['html']) {
        //Ocultar el Loading
        if(options['showLoadingTo']){
            $(options['showLoadingTo']).hideLoading();
        }

        //Crear el html que va a mostrar el modal.
        strModal = '<div id="'+idModal+'" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="'+idModal+'Label" aria-hidden="true">' +
                        ':modalBody\n\
                   </div>';

        //Establecer el Body del Modal.
        strModal = strModal.replace(':modalBody', options['html']);

        var newModal = $(strModal).appendTo('body');
        //Remover el width y height por que el scroll salia de lado.
        newModal.find('.modal-body').css({
            width  : ''
        });

        //Mostrar el Modal al usuario.
        newModal.modal();

        //Ejecutar el evento Success.
        if(options['success']){
            options['success'](newModal);
        }
    }else{
        //Cargamos los datos que tendra el modal.
        $.ajax({
            url     : options['url'],
            data    : options['params'],
            success : function(htmlResponse){
                //Ocultar el Loading
                if(options['showLoadingTo']){
                    $(options['showLoadingTo']).hideLoading();
                }

                //Crear el html que va a mostrar el modal.
                strModal = '<div id="'+idModal+'" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="'+idModal+'Label" aria-hidden="true">' +
                                ':modalBody\n\
                           </div>';

                //Establecer el Body del Modal.
                strModal = strModal.replace(':modalBody', htmlResponse);

                var newModal = $(strModal).appendTo('body');
                //Remover el width y height por que el scroll salia de lado.
                newModal.find('.modal-body').css({
                    width  : ''
                });

                //Mostrar el Modal al usuario.
                newModal.modal();

                //Ejecutar el evento Success.
                if(options['success']){
                    options['success'](newModal);
                }
            },
            error:function (xhr, textStatus, thrownError){  
                //Ocultar el Loading
                if(options['showLoadingTo']){
                    $(options['showLoadingTo']).hideLoading();
                }

                if(xhr['responseText']){
                    $.showAlert({
                        type    : 'error',
                        showTo  : '#content-area',
                        content : '<b>Error.</b> ' + xhr['responseText']
                    });
                }else{
                    $.showAlert({
                        type    : 'error',
                        showTo  : '#content-area',
                        content : '<b>Error.</b> ' + thrownError
                    });
                }
            },
            type: "POST"
        });
    }
};