/************************************************************************************************************************************
* Funcion que se encarga de crear una simple tabla que se puede eliminar, agregar y editar columnas.
* Author           : Carlos Domínguez Lara
* Fecha creado     : Martes 21 de Mayo del 2013 03:13 p.m
* Fecha Modificado : Martes 21 de Mayo del 2013 03:13 p.m
* Parametros       :
*      Options => Objetos con la configuracion.
*         - idAccordion (String)      : Selector del elemento al que se creara el accordion.
*         - btnAgregar  (String)      : Selector del elemento que agregara una nueva fila.
*         - btnBorrar   (String)      : Selector del elemento que borrara una nueva fila.
*         - rowTemplate (String)      : HTML de la fila nueva que se va agregar.
*         - prefixIdRow (String)      : Nombre del prefijo que va a llevar los ids de las filas.
*         - addIdRowTo  (String)      : Selector de los elementos a los que se desea agregar el id de la fila que se crea.
*         - onRowAdded  (function)    : Funcion que se ejecutará despues de agregar una fila a la tabla.
*         - onSuccess   (function)    : Funcion que se ejecutará cuando se termine la funcion Init.
*      $table.data() :
*         - row-default               : Indica si hay que poner siempre una fila default si no hay ninguna.
*         - cant-numeros              : Cantidad de filas que tiene la tabla
*      $row.data() :
*         - db-id                     : Idenficador de la fila en la base de datos
************************************************************************************************************************************/
$.initSimpleAccordion = function(options){
    var $table = $(options.idAccordion);
    
    //Calcular cuantas filas tiene la tabla, ignorar la primera fila que es el encabezado
    $table.data('cant-numeros', $table.find('.accordion-group').length);
    
    //Agregar el identificador a cada fila
    $table.find('.accordion-group').addClass(options.prefixIdRow);
    
    //Evento al agregar una nueva fila de numero
    $(options.btnAgregar).on("click", function(e){
        var cantRows = $table.data('cant-numeros'); cantRows ++;
        var idNewRow = options.prefixIdRow + '-new-' + cantRows;
        var $newRow  = $(options.rowTemplate);
        
        //Agregar el id a la fila
        $newRow.attr('id',idNewRow);
                
        //Agregar el id de la fila al boton borrar.
        $newRow.find(options.btnBorrar).attr('row-id',idNewRow);

        //Agregar la clase para contar cuantas filas tiene la tabla
        $newRow.addClass(options.prefixIdRow);

        //Validar si hay que agregar el id de la fila que se genero algun otro elemento.
        if(options['addIdRowTo']){
            $newRow.find(options.addIdRowTo).attr('row-id',idNewRow);
        }
        
        //Agregar la fila a la tabla.
        $table.append($newRow);
        
        //Establecer el nuevo id para el contador
        $table.data('cant-numeros', cantRows);

        //Evento que se ejecuta al agregar una nueva fila
        if(options['onRowAdded']){
            options['onRowAdded']($newRow, cantRows, 0);
        }   
        
        if($table.data('row-default')){
            //Validar si ya no queda un telefono para poder mostrar el boton de borrar
            if($table.find('.'+options.prefixIdRow).length > 1){
                $table.find(options.btnBorrar).show();
            }
        }else{
            $table.find(options.btnBorrar).show();
        }
        e.preventDefault();
    });
    
    //Evento al borrar una fila de numero
    $table.on("click", options.btnBorrar, function(e){
        var $e       = $(this);
        var rowId    = $e.attr('row-id');
        
        //Agregar la informacion de la fila eliminada solo si esta registrado en bd.
        if($('#'+rowId).data('db-id')){
            $(options.infoBorrado).val($(options.infoBorrado).val() + $('#'+rowId).data('db-id') + ",");
        }
        
        //Eliminar la fila
        $('#'+rowId).remove();

        if($table.data('row-default')){
            //Validar si quedo una fila y esconder el boton de borrar
            if($table.find('.'+options.prefixIdRow).length == 1){
                $table.find(options.btnBorrar).hide();
            }
        }
        e.preventDefault();
    });
    
    //Ejecutar el codigo a cada una de las filas que ya estaban mas que todo en modificar.
    if(options['onRowAdded']){
        var rows = $table.find('.'+options.prefixIdRow);
        var $row;
        var idNewRow;
    
        $.each(rows, function(){
            $row = $(this);
            if ($row.data('db-id')) {
                idNewRow = options.prefixIdRow + '-a-' + $row.data('db-id');
                
                //Añadir el id a la fila para borrarla con el btn
                $row.attr("id", idNewRow);
                $row.find(options.btnBorrar).attr("row-id", idNewRow);
            }
            
            options['onRowAdded']($(this), 0, $(this).data('db-id'));
        });
    }
    
    //Validar si hay que agregar una fila default solo si no existe ninguna fila
    if($table.data('row-default')){
        if($table.find('.accordion-group').size() == 0){
            //Agregar la primera fila default
            $(options.btnAgregar).click();
        }
    }
    
    //Evento que se ejecuta al agregar una nueva fila
    if(options['onSuccess']){
        options['onSuccess']($table);
    }
};